import logging
import typing

from d3m import index
from d3m.metadata.pipeline import Pipeline, Resolver
from d3m.primitive_interfaces import base

from modules.utils.constants import LISTS

__all__ = ('Pipeline', 'BlackListResolver')

logger = logging.getLogger(__name__)


class BlackListResolver(Resolver):
    """
    A resolver to resolve primitives and pipelines.

    It resolves primitives from available primitives on the system,
    and resolves pipelines from files in pipeline search paths.

    Attributes
    ----------
    strict_resolving : bool
        If resolved primitive does not fully match specified primitive reference, raise an exception?
    pipeline_search_paths : Sequence[str]
        A list of paths to directories with pipelines to resolve from.
        Their files should be named ``<pipeline id>.json`` or ``<pipeline id>.yml``.

    Parameters
    ----------
    strict_resolving : bool
        If resolved primitive does not fully match specified primitive reference, raise an exception?
    pipeline_search_paths : Sequence[str]
        A list of paths to directories with pipelines to resolve from.
        Their files should be named ``<pipeline id>.json`` or ``<pipeline id>.yml``.
    respect_environment_variable : bool
        Use also (colon separated) pipeline search paths from ``PIPELINES_PATH`` environment variable?
    """

    def __init__(self, black_list=LISTS.BLACK_LIST, *, strict_resolving: bool = False, strict_digest: bool = False,
                 pipeline_search_paths: typing.Sequence[str] = None,
                 respect_environment_variable: bool = True, load_all_primitives: bool = True,
                 primitives_blacklist: typing.Collection[str] = None) -> None:
        super().__init__(strict_resolving=strict_resolving, strict_digest=strict_digest,
                         pipeline_search_paths=pipeline_search_paths,
                         respect_environment_variable=respect_environment_variable,
                         load_all_primitives=load_all_primitives, primitives_blocklist=primitives_blacklist)
        self.black_list = black_list
        if len(black_list) == 0:
            self.black_list = None

    def _get_primitive(self, primitive_description: typing.Dict) -> typing.Optional[typing.Type[base.PrimitiveBase]]:
        if not self._primitives_loaded:
            self._primitives_loaded = True

            index.load_all(blocklist=self.black_list)

        return index.get_primitive_by_id(primitive_description['id'])
