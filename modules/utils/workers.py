import binascii
import logging.handlers
import math
import random
import time

from d3m import container
from d3m.metadata import problem as problem_module

logger = logging.getLogger(__name__)


def hex_to_binary(hex_identifier):
    return binascii.unhexlify(hex_identifier)


def binary_to_hex(identifier):
    hex_identifier = binascii.hexlify(identifier)
    return hex_identifier.decode()


def summarize_performance_metrics(performance_metrics):
    """
    A function that averages all the folds if they exist.

    Parameters
    ----------
    performance_metrics: dict
        A dictionary containing the fold, metrics targets and values from evaluation.
    """
    sumarized_performance_metrics = {}

    for metric in performance_metrics.metric.unique():
        mean = performance_metrics[performance_metrics.metric == metric]['value'].mean()
        std = performance_metrics[performance_metrics.metric == metric]['value'].std()
        if math.isnan(std):
            std = 0
        sumarized_performance_metrics[metric] = {
            'mean': mean,
            'std': std,
        }
    return sumarized_performance_metrics


def compute_score(sumarized_performance_metrics):
    """
    A function that computes the internal score based on the average normalized metrics.

    Parameters
    ----------
    sumarized_performance_metrics: dict
     A dictionary containing the summarized version.
    """
    score = 0

    for metric, info in sumarized_performance_metrics.items():
        score += problem_module.PerformanceMetric[metric].normalize(info['mean'])

    score = score / float(len(sumarized_performance_metrics))
    return score


def compute_rank(sumarized_performance_metrics):
    """
    A function that computes the rank based on the average normalized metrics.

    Parameters
    ----------
    sumarized_performance_metrics: dict
     A dictionary containing the summarized version.
    """
    ranks = {}
    mean = 0
    for metric, info in sumarized_performance_metrics.items():
        try:
            ranks[metric] = problem_module.PerformanceMetric[metric].normalize(abs(info['mean'] - info['std']))
        except:
            ranks[metric] = 0
        mean += ranks[metric]

    mean = mean / len(sumarized_performance_metrics)
    # rank = 1 - ranks[min(ranks.keys(), key=(lambda k: ranks[k]))] + random.randint(10, 30)**-6
    rank = 1 - mean

    # We add some randomness on the rank to avoid duplications
    noise = 0
    sign = -1 if random.randint(0, 1) == 0 else 1
    range_0 = -9
    range_1 = -5
    if rank < 1e-5:
        range_0 = -12
        range_1 = -9

    for i in range(range_0, range_1):
        noise += random.randint(0, 9) * 10 ** i
    rank = rank + noise * sign
    if rank < 0:
        rank *= -1
    return rank


def random_rank():
    ranks = 0
    average_number = 5
    for i in range(average_number):
        ranks += random.uniform(0, 1)
    ranks = ranks/average_number
    return ranks


def get_tabular_resource_id(dataset):
    """
    A function that retrieves the main resource id

    Parameters
    ----------
    dataset: Dataset
        A dataset.

    Returns
    -------
    resource_id: str
        An id of the main resource.
    """

    resource_id = None
    for dataset_resource_id in dataset.keys():
        if dataset.metadata.has_semantic_type((dataset_resource_id,),
                                              'https://metadata.datadrivendiscovery.org/types/DatasetEntryPoint'):
            resource_id = dataset_resource_id
            break

    if resource_id is None:
        tabular_resource_ids = [dataset_resource_id for dataset_resource_id, dataset_resource in dataset.items() if
                                isinstance(dataset_resource, container.DataFrame)]
        if len(tabular_resource_ids) == 1:
            resource_id = tabular_resource_ids[0]

    if resource_id is None:
        resource_id = 'learningData'

    return resource_id


class TimeTracker:
    """
    Manage the time left (usually used on search)

    Parameters
    ----------
    time_bound: Union[int, float]
        Time left, if  lower than 1 we set to 2 hours.
    useful_time: float
        Time modifier to time_left.

    Attributes
    ----------
    _time_left: float
        Time left.
    self._check_point: float
        Last time registered that is going to be removed from _time_left.
    """

    def __init__(self, time_bound, useful_time=0.85):
        # Normalize time:
        if time_bound < 1:
            # lets set 2 hours.
            self._time_left = 2 * 60 * 60
        else:
            # Setting 90% of time for search.
            self._time_left = time_bound * useful_time

        self._check_point = time.time()

    def update(self):
        """
        Update time left.
        """
        current_time = time.time()
        self._time_left -= current_time - self._check_point
        self._check_point = current_time

    def enough_time(self, constraint):
        """
        Check if there enough time based on a constrain.

        Parameters
        ----------
        constraint: float
            Check if time left is greater than the constraint.

        Returns
        -------
        enough_time: bool
        """
        if self._time_left - constraint > 1:
            return True
        else:
            return False

    def time_left(self):
        """
        Check if there is enough time left.

        Returns
        -------
        time_left: bool
        """
        return self.enough_time(0)
