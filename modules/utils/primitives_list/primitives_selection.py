import os
from d3m import index, utils
import json
import time
import psutil
from tqdm import tqdm


def benchmarking():
    """
    A function to create the blacklist and do some benchmarking, we need to re-run this function
    whenever we update the container that we are working on.
    """
    benchmark = dict()
    blacklist = list()
    complete_benchmark = dict()

    if os.path.exists('modules/utils/primitives_list/blacklist.json'):
        with open('modules/utils/primitives_list/blacklist.json') as file:
            blacklist = json.load(file)

    for primitive_path in tqdm(index.search()):
        if primitive_path not in blacklist:
            try:
                start = time.time()
                with utils.silence():
                    index.get_primitive(primitive_path)
                loading_time = time.time() - start
                benchmark[primitive_path] = loading_time
                complete_benchmark[primitive_path] = {
                    'time': loading_time,
                    'cpu_usage': psutil.cpu_percent(),
                    'memory': psutil.virtual_memory()._asdict()
                }
                if loading_time > 5:
                    blacklist.append(primitive_path)
            except Exception:
                blacklist.append(primitive_path)
                benchmark[primitive_path] = 'Cannot be loaded'
                complete_benchmark[primitive_path] = 'Cannot be loaded'

    with open('modules/utils/primitives_list/blacklist.json', 'w') as file:
        json.dump(blacklist, file, indent=2)
    with open('modules/utils/primitives_list/benchmark.json', 'w') as file:
        json.dump(benchmark, file, indent=2)
    with open('modules/utils/primitives_list/complete_benchmark.json', 'w') as file:
        json.dump(complete_benchmark, file, indent=2)


def available_primitives():
    primitives_info = list()

    with open('modules/utils/primitives_list/blacklist.json', 'r') as file:
        blacklist = json.load(file)

    # with utils.silence():
    for primitive_path in tqdm(index.search()):
        if primitive_path in blacklist:
            continue
        primitive = index.get_primitive(primitive_path)
        id = primitive.metadata.query()['id']
        version = primitive.metadata.query()['version']
        python_path = primitive.metadata.query()['python_path']
        name = primitive.metadata.query()['name']
        digest = primitive.metadata.query().get('digest', None)
        primitive_info = {
            'id': id,
            'version': version,
            'python_path': python_path,
            'name': name,
            'digest': digest
        }
        primitives_info.append(primitive_info)
    with open('modules/utils/primitives_list/available_primitives.json', 'w') as file:
        json.dump(primitives_info, file, indent=2)


def main():
    print("Generating BlackList of primitives.")
    benchmarking()
    print("Generating List of Available Primitives")
    available_primitives()


if __name__ == '__main__':
    main()