import random

def select_random_elements(all, seen, size):
    cp_seen = seen.copy()
    selection = []
    for i in range(0, size):
        random.shuffle(all)
        for element in all:
            if element not in cp_seen:
                cp_seen.add(element)
                selection.append(element)
                break
    return selection