import os


def check_directory(dir_name):
    dir_name = os.path.abspath(dir_name)
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)
