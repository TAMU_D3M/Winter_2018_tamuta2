import multiprocessing
import resource
from functools import partial
from multiprocessing.dummy import Pool as ThreadPool


# In GB
def memory_limit(mem_limit):
    mem_limit = mem_limit * 1000000
    soft, hard = resource.getrlimit(resource.RLIMIT_AS)
    resource.setrlimit(resource.RLIMIT_AS, (int(mem_limit * 0.8), mem_limit))


# In kb
def get_memory():
    with open('/proc/meminfo', 'r') as mem:
        free_memory = 0
        for i in mem:
            sline = i.split()
            if str(sline[0]) in ('MemFree:', 'Buffers:', 'Cached:'):
                free_memory += int(sline[1])
    return free_memory


# TODO create passing messages
def execution_timeout(func, *args, **kwargs):
    timeout = kwargs.get('timeout', None)

    p = ThreadPool(1)
    res = p.apply_async(func, args=args)
    try:
        out = res.get(timeout)
        return out
    except multiprocessing.TimeoutError:
        p.terminate()
        return None


def resource_restriction(func, *args, **kwargs):
    timeout = kwargs.get('timeout', None)
    mem_limit = kwargs.get('mem_limit', None)

    # setting, memory cap
    memory_limit(mem_limit)

    fun_restriction = partial(execution_timeout, func, timeout=timeout)
    return fun_restriction
