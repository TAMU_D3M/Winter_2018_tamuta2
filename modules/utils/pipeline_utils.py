import argparse
import os
import shutil
import typing

import d3m.index
import networkx as nx
from d3m import utils as d3m_utils
from d3m.metadata.base import Context
from d3m.metadata.pipeline import Pipeline, PlaceholderStep, PrimitiveStep, SubpipelineStep, get_pipeline
from d3m.metadata import base as metadata_base
from d3m.metadata import problem as problem_module
from d3m.metadata.pipeline import Resolver

from modules.utils.checks import check_directory
from modules.utils.constants import Path, LISTS, SearchPath

import logging

logger = logging.getLogger(__name__)


def int_to_step(n_step: int) -> str:
    """
    Convert the step number to standard str step format

    Parameters
    ----------
    n_step: int

    Returns
    -------
    str
        str format in "steps.<n_step>.produce"
    """
    return 'steps.' + str(n_step) + '.produce'


def step_to_int(step: str) -> int:
    """
    Extract the step number

    Parameters
    ----------
    step: str
        The str of step, whose format is "steps.<n_step>.produce"

    Returns
    -------
    int
    """
    return int(step.split('.')[1])


def load_pipeline(pipeline_uri):
    """
    Load pipeline from a pipeline URI

    Parameters
    ----------
    pipeline_uri: Union[str, dict[str]]
        The URI pointing to a json file of pipeline or dict of string that is a pipeline

    Returns
    -------
    pipeline: Pipeline
        An object of Pipeline

    """
    if isinstance(pipeline_uri, dict):
        try:
            pipeline = Pipeline.from_json_structure(pipeline_uri)
        except:
            pipeline = None
    else:
        with d3m_utils.silence():
            pipeline = get_pipeline(pipeline_path=pipeline_uri, load_all_primitives=False)
    return pipeline


def get_stepholder_pipeline(orig_pipeline: Pipeline, resolver) -> Pipeline:
    """
    Copy original pipeline and delete the PlaceholderStep

    Parameters
    ----------
    orig_pipeline: Pipeline
        The original pipeline is copied
    resolver

    Returns
    -------
    new_pipeline: Pipeline
        The new pipeline based on the original pipeline
    """
    new_pipeline = Pipeline(context=Context.TESTING)
    for input in orig_pipeline.inputs:
        new_pipeline.add_input(**input)

    for step in orig_pipeline.steps:
        if isinstance(step, PlaceholderStep):
            break
        step.index = None
        step.resolver = resolver
        new_pipeline.add_step(step)

    return new_pipeline


def save_pipeline_to_json_file(pipeline: Pipeline) -> str:
    """
    Save a Pipeline into a json file under the pipeline URI, which is from `get_pipeline_uri`

    Paramters
    ---------
    pipeline: Pipeline
        An object of Pipeline.

    Returns
    -------
    uri: str
        The URI of the pipeline
    """
    uri = get_pipeline_uri(pipeline)

    with open(uri, 'w') as file:
        pipeline.to_json(file, indent=2, sort_keys=True, ensure_ascii=False)

    return uri


def get_pipeline_uri(pipeline: Pipeline) -> str:
    """
    Compose the pipeline URI according to pipeline

    Parameters
    ----------
    pipeline: Pipeline
        An object of Pipeline

    Returns
    -------
    uri: str
        The URI of pipeline. Current format is "<PATH>/<ID>.json"
    """
    id = pipeline.id
    # context = pipeline.context
    # path = Path.TEMP_PIPELINE_STORAGE
    path = Path.TEMP_PIPELINE_STORAGE
    check_directory(path)
    uri = os.path.join(path, id + '.json')
    return uri


def get_pipeline_num_steps(pipeline_uri: str) -> int:
    """
    Get the number of pipeline steps

    Parameters
    ----------
    pipeline_uri: str
        The path of pipeline file

    Returns
    -------
    int
        The number of pipeline steps
    """
    pipeline = load_pipeline(pipeline_uri)
    return len(pipeline.steps)


def check_black_list(primitive_name: str) -> bool:
    """
    Check if the primitive is in the black list, which is from `LIST.BlACK_LIST`

    Parameters
    ----------
    primitive_name: str
        The name of the primitive

    Returns
    -------
    bool

    """
    for banned_element in LISTS.BLACK_LIST:
        if banned_element in primitive_name:
            return True
    return False


def get_primitive_candidates(task_type: str, data_types: typing.Iterable, semi: bool) -> typing.List:
    """
    Get a list of primitive candidates related to the task type except those primitives in `BLACK_LIST`

    Parameters
    ----------
    task_type: str
        The task type
    data_types: typing.Iterable
        The data types
    semi: bool
        Is it semi-supervised problem

    Returns
    -------
    list
        A list of primitives
    """
    specific_task = infer_primitive_family(task_type, data_types, semi)
    logger.info('SPECIFIC TASK: {}'.format(specific_task))
    primitives_path = d3m.index.search()
    primitives = list()
    for primitive_path in primitives_path:
        if check_black_list(primitive_path):
            continue
        try:
            primitive = d3m.index.get_primitive(primitive_path)
            primitive_family = primitive.metadata.query()['primitive_family'].name
            # A special condition where semi-supervised learning must use specific model
            if semi:
                if primitive_family == specific_task:
                    primitives.append((primitive, specific_task))
            else:
                if primitive_family == task_type:
                    primitives.append((primitive, task_type))
                elif primitive_family == specific_task:
                    primitives.append((primitive, specific_task))
        # TODO what exception?
        except Exception as e:
            continue
    return primitives


def infer_primitive_family(task_type: str, data_types: typing.Iterable, is_semi: bool = False) -> typing.Optional[str]:
    """
    Infer target primitive family by task and data_types

    Parameters
    ----------
    task_type: str
        The task type
    data_types: typing.Iterable
        The data types
    is_semi: bool
        Is semi supervised probelm

    Returns
    -------
    str
        The primitive family
    """

    #TODO temp solution
    if problem_module.TaskKeyword.CLASSIFICATION == task_type and \
            problem_module.TaskKeyword.TIME_SERIES in data_types and \
            problem_module.TaskKeyword.GROUPED in data_types:
        return metadata_base.PrimitiveFamily.CLASSIFICATION
    if problem_module.TaskKeyword.CLASSIFICATION == task_type and \
            problem_module.TaskKeyword.TIME_SERIES in data_types:
        return metadata_base.PrimitiveFamily.TIME_SERIES_CLASSIFICATION.name
    if problem_module.TaskKeyword.FORECASTING and problem_module.TaskKeyword.TIME_SERIES in data_types:
        return metadata_base.PrimitiveFamily.TIME_SERIES_FORECASTING.name
    if problem_module.TaskKeyword.CLASSIFICATION == task_type and is_semi:
        return metadata_base.PrimitiveFamily.SEMISUPERVISED_CLASSIFICATION.name
    if problem_module.TaskKeyword.IMAGE in data_types:
        return metadata_base.PrimitiveFamily.DIGITAL_IMAGE_PROCESSING.name
    if problem_module.TaskKeyword.VIDEO in data_types:
        return metadata_base.PrimitiveFamily.DIGITAL_SIGNAL_PROCESSING.name

    return task_type


def save_pipeline(pipeline_path, mode, *, search_id=None, rank=None):
    """
    A function that make a copy of an already scored pipeline to scored directory according with specifications.

    Parameters
    ----------
    pipeline_path: str
        A pipeline path from where we make a copy
    mode: str
        Determine where to store the pipeline.
    """
    base_path = SearchPath(search_id=search_id)
    if mode == 'SCORE':
        save_path = os.path.join(base_path.pipelines_scored, os.path.basename(pipeline_path))
        shutil.copy(pipeline_path, save_path)
    elif mode == 'RANK':

        save_path = os.path.join(base_path.pipelines_ranked, os.path.basename(pipeline_path))
        shutil.copyfile(pipeline_path, save_path)

        rank_path = os.path.basename(pipeline_path).split('.')[0] + ".rank"
        rank_path = os.path.join(base_path.pipelines_ranked, rank_path)

        with open(rank_path, 'w') as file:
            file.write('{rank}'.format(rank=rank))


def get_primitives(primitives_dict):
    """
    A function that loads and returns a dictionary of primitives

    Parameters
    ----------
    primitives_dict: dict[str, str]
        A dictionary that contains the alias and the primitives to load.

    Returns
    -------
    loaded_primitives_dict: dict[str, str]
        A dictionary containing the aliases and the loaded primitives.
    """
    loaded_primitives_dict = {}
    for primitive_name in primitives_dict.keys():
        loaded_primitives_dict[primitive_name] = d3m.index.get_primitive(primitives_dict[primitive_name])
    return loaded_primitives_dict


def get_primitives_by_task_and_name(task, name):
    """
    A function that returns a dictionary that contains primitives that
    has as family type task and contains in their path name.

    Parameters
    ----------
    task: str
        A task that is going to be use to filter Family type.
    name: str
        A string that the primitive path must contain.

    Returns
    -------
    _primitives: dict[str, d3m.metadata.Primitive]
    """
    primitives_path = d3m.index.search()
    _primitives = {}
    for primitive_path in primitives_path:
        if check_black_list(primitive_path):
            continue
        try:
            if name in primitive_path:
                primitive = d3m.index.get_primitive(primitive_path)
                if primitive.metadata.query()['primitive_family'].name == task.name:
                    _primitives[primitive_path] = primitive
        except Exception as e:
            continue
    return _primitives


def plot_pipeline(pipeline_path, output_path):
    with d3m_utils.silence():
        resolver = Resolver(primitives_blacklist=LISTS.BLACK_LIST, load_all_primitives=False)

        with open(pipeline_path, 'r', encoding='utf8') as pipeline_file:
            if pipeline_path.endswith('.yml'):
                pipeline = Pipeline.from_yaml(pipeline_file, resolver=resolver)
            elif pipeline_path.endswith('.json'):
                pipeline = Pipeline.from_json(pipeline_file, resolver=resolver)
            else:
                raise ValueError("Unknown file extension.")

    path = os.path.join(output_path, pipeline.id + '.png')
    graph = get_pipeline_graph(pipeline)
    A = nx.nx_agraph.to_agraph(graph)
    A.layout('dot', args='-Nfontsize=20 -Nwidth="1" -Nheight=".7" -Nmargin=0 -Gfontsize=12')
    A.draw(path)


def __get_header(index, step):
    if isinstance(step, PrimitiveStep):
        header = 'steps.' + str(index) + '  -  ' + step.primitive.metadata.query()['python_path']
    elif isinstance(step, PlaceholderStep):
        header = 'steps.' + str(index) + '  -  ' + 'PlaceHolderStep'
    elif isinstance(step, SubpipelineStep):
        header = 'steps.' + str(index) + '  -  ' + 'SubPipeline'
    return header


def get_pipeline_graph(pipeline):
    graph = nx.DiGraph()

    for i in range(0, len(pipeline.steps)):
        if isinstance(pipeline.steps[i], PrimitiveStep) or isinstance(pipeline.steps[i], PlaceholderStep):
            target = __get_header(i, pipeline.steps[i])
            graph.add_node(target)
            for argument in pipeline.steps[i].arguments.keys():
                data = pipeline.steps[i].arguments[argument]['data']
                if 'input' in data:
                    source = 'inputs'
                else:
                    index = int(data.split('.')[1])
                    source = __get_header(index, pipeline.steps[index])
                label = argument + '-' + data
                graph.add_edge(source, target, label=label)

            for hp in pipeline.steps[i].hyperparams.keys():
                if pipeline.steps[i].hyperparams[hp]['type'] == metadata_base.ArgumentType.PRIMITIVE:
                    index = pipeline.steps[i].hyperparams[hp]['data']
                    source = __get_header(index, pipeline.steps[index])
                    label = 'Step {} hyperparam - {}'.format(i, hp)
                    graph.add_edge(source, target, label=label)
        else:
            # TODO add support here for subpipelines
            continue

    for i in range(0, len(pipeline.outputs)):
        index = int(pipeline.outputs[i]['data'].split('.')[1])
        source = __get_header(index, pipeline.steps[index])
        label = 'outputs.{}'.format(i)
        graph.add_edge(source, 'output', label=label)

    return graph


def configure_parser(parser, *, skip_arguments=()):
    parser.add_argument(
        '-p', '--pipeline-path', type=str, default="modules/utils/pipelines/default/xgboost-gbtree-classifier.json",
        help="pipeline path to plot."
    )
    parser.add_argument(
        '-o', '--output-path', type=str, default=os.getcwd(),
        help="output path to plot."
    )


if __name__ == '__main__':
    # Creating parser
    parser = argparse.ArgumentParser(description="Test from command line")
    configure_parser(parser)
    arguments = parser.parse_args()

    # Getting test root path
    pipeline_path = arguments.pipeline_path
    output_path = arguments.output_path

    plot_pipeline(pipeline_path, output_path)
