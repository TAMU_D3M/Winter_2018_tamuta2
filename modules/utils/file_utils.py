import json
import os
import pickle
import shutil

from modules.utils import checks
from modules.utils import constants


def load_json(path):
    with open(path) as file:
        json_file = json.load(file)
    return json_file


def clean_directory(path):
    """
    Remove all the directories and files below the directory.

    Parameters
    ----------
    path: std
        A path indicating files and directories to be delete below.
    """
    for _file in os.listdir(path):
        if _file == '.gitkeep':
            continue
        file_path = os.path.join(path, _file)
        if os.path.isfile(file_path):
            os.unlink(file_path)
        elif os.path.isdir(file_path):
            shutil.rmtree(file_path)


def save_problem(problem, search_id, save_path=constants.Path.TEMP_STORAGE_ROOT):
    """
    Save a parsed problem to a json file.

    Parameters
    ----------
    problem: dict
        A parsed problem description.
    search_id: str
        A id that it was used for the search.
    save_path: str
        Path to be use for storing the problem.


    Returns
    -------
    str
        The complete path where the problem was stored.
    """
    # Case where we pass full pipeline or somehting else is missing.
    if problem is None:
        return None

    # Make sure that the search directory is created
    search_path = os.path.join(save_path, search_id)
    checks.check_directory(search_path)

    # Make sure the problem directory exists.
    problem_path = os.path.join(search_path, 'problem')
    checks.check_directory(problem_path)

    # Create final uri for the problem.
    problem_doc_uri = os.path.join(problem_path, 'problem.json')

    # Saving the problem
    with open(problem_doc_uri, 'w') as file:
        json.dump(problem.to_json_structure(), file, indent=2)

    return problem_doc_uri


def save_pipeline_template(pipeline_template, search_id, save_path=constants.Path.TEMP_STORAGE_ROOT):
    """
    Save a parsed problem to a json file.

    Parameters
    ----------
    problem: dict
        A parsed problem description.
    search_id: str
        A id that it was used for the search.
    save_path: str
        Path to be use for storing the problem.


    Returns
    -------
    pipeline_doc_uri: Union[None, str]
        The complete path where the pipeline_template was stored.
    """

    # For the case that the template does not exists.
    if pipeline_template is None:
        return None

    # Make sure that the search directory is created
    search_path = os.path.join(save_path, search_id)
    checks.check_directory(search_path)

    # Make sure the pipeline_template directory exists.
    pipeline_template_path = os.path.join(search_path, 'pipeline_template')
    checks.check_directory(pipeline_template_path)

    # Create final uri for the pipeline_template.
    pipeline_doc_uri = os.path.join(pipeline_template_path, 'pipeline_template.json')

    # Saving the pipeline!
    with open(pipeline_doc_uri, 'w') as file:
        pipeline_template.to_json(file, indent=2, sort_keys=True, ensure_ascii=False)

    return pipeline_doc_uri


def save_pipeline_to_json_file(pipeline, save_path):
    """
    Save a Pipeline into a json file under the pipeline URI, which is from `get_pipeline_uri`

    Paramters
    ---------
    pipeline: Pipeline
        An object of Pipeline.
    save_path:
        A path where the pipeline is going tobe stored.

    Returns
    -------
    pipeline_uri: str
        The URI of the stored pipeline
    """
    pipeline_uri = os.path.join(save_path, pipeline.id + '.json')

    with open(pipeline_uri, 'w') as file:
        pipeline.to_json(file, nest_subpipelines=True, indent=2, sort_keys=True, ensure_ascii=False)

    return pipeline_uri


def save_solution(solution, save_path=constants.Path.TEMP_STORAGE_ROOT):
    """
    Save a solution to a json file.

    Parameters
    ----------
    solution: dict
        A solution. Example: {
            "solution_id": "solution_id",
            "pipeline_id": "pipeline_id",
            "problem_path": "problem_uri",
            "pipeline_path": "pipeline_uri"
        }.
    save_path: str
        Path to be use for storing the solution.


    Returns
    -------
    str
        The complete path where the solution was stored.
    """
    # Make sure the solutions directory exists.
    solutions_path = os.path.join(save_path, 'solutions')
    checks.check_directory(solutions_path)

    # Create final uri for the solution.
    solution_uri = os.path.join(solutions_path, '{}.json'.format(solution['solution_id']))

    # Saving the solution
    with open(solution_uri, 'w') as file:
        json.dump(solution, file, indent=2)

    return solution_uri


def save_fitted_solution(fitted_solution_id, fitted_solution, save_path=constants.Path.TEMP_STORAGE_ROOT):
    """
    Save a fitted solution to a json file.

    Parameters
    ----------
    fitted_solution_id: str
        A id that it was used for the fitted solution.
    fitted_solution: dict
        A fitted solution. Example: {
            "runtime": runtime_obj,
            "pipeline_path": "pipeline_uri",
        }.
    save_path: str
        Path to be use for storing the fitted solution.


    Returns
    -------
    str
        The complete path where the fitted solution was stored.
    """
    # Make sure the fitted_solutions directory exists.
    fitted_solutions_path = os.path.join(save_path, 'fitted_solutions')
    checks.check_directory(fitted_solutions_path)

    # Create final uri for the fitted solution.
    fitted_solution_uri = os.path.join(fitted_solutions_path, '{}.json'.format(fitted_solution_id))
    runtime_pickle_uri = os.path.join(fitted_solutions_path, '{}_runtime.pickle'.format(fitted_solution_id))

    # Pickle runtime
    with open(runtime_pickle_uri, 'wb') as f:
        pickle.dump(fitted_solution['runtime'], f)

    saved_fitted_solution = {
        'fitted_solution_id': fitted_solution_id,
        'runtime_path': runtime_pickle_uri,
        'pipeline_path': fitted_solution['pipeline_path'],
    }
    # Saving the fitted solution
    with open(fitted_solution_uri, 'w') as file:
        json.dump(saved_fitted_solution, file, indent=2)

    return fitted_solution_uri
