import copy
import logging
import os

from d3m.metadata import problem as problem_module
from d3m.metadata.problem import TaskKeyword, PerformanceMetric

from modules.utils import file_utils
from modules.utils.constants import Path

logger = logging.getLogger(__name__)

# This metrics should be update every once in a while based on
# https://datadrivendiscovery.org/wiki/display/work/Matrix+of+metrics

TASK_TYPE = {
    TaskKeyword.CLASSIFICATION, TaskKeyword.REGRESSION,
    TaskKeyword.CLUSTERING, TaskKeyword.LINK_PREDICTION,
    TaskKeyword.VERTEX_NOMINATION, TaskKeyword.COMMUNITY_DETECTION,
    TaskKeyword.GRAPH_MATCHING, TaskKeyword.COLLABORATIVE_FILTERING,
    TaskKeyword.OBJECT_DETECTION, TaskKeyword.VERTEX_CLASSIFICATION,
    TaskKeyword.FORECASTING
}

TASK_SUBTYPES = {
    TaskKeyword.MULTIVARIATE,
    TaskKeyword.BINARY,
    TaskKeyword.NONOVERLAPPING,
    TaskKeyword.OVERLAPPING,
    TaskKeyword.UNIVARIATE,
    TaskKeyword.MULTICLASS,
    TaskKeyword.MULTILABEL,
}

DATA_TYPES = {
    TaskKeyword.TIME_SERIES,
    TaskKeyword.AUDIO,
    TaskKeyword.TABULAR,
    TaskKeyword.TEXT,
    TaskKeyword.VIDEO,
    TaskKeyword.GRAPH,
    TaskKeyword.IMAGE,
    TaskKeyword.GEOSPATIAL,
    TaskKeyword.RELATIONAL,
    TaskKeyword.GROUPED,
    TaskKeyword.LUPI
}

CLASSIFICATION_METRICS = [
    {'metric': PerformanceMetric.ACCURACY, 'params': {}},
    {'metric': PerformanceMetric.PRECISION, 'params': {}},
    {'metric': PerformanceMetric.RECALL, 'params': {}},
    {'metric': PerformanceMetric.F1, 'params': {}},
    {'metric': PerformanceMetric.F1_MICRO, 'params': {}},
    {'metric': PerformanceMetric.F1_MACRO, 'params': {}},
    {'metric': PerformanceMetric.ROC_AUC, 'params': {}},
    # {'metric': PerformanceMetric.JACCARD_SIMILARITY_SCORE, 'params': {}},
]

BINARY_CLASSIFICATION_METRICS = [
    {'metric': PerformanceMetric.ACCURACY, 'params': {}},
    # {'metric': PerformanceMetric.F1, 'params': {}},
    # {'metric': PerformanceMetric.JACCARD_SIMILARITY_SCORE, 'params': {}},
]

MULTICLASS_CLASSIFICATION_METRICS = [
    {'metric': PerformanceMetric.ACCURACY, 'params': {}},
    {'metric': PerformanceMetric.F1_MICRO, 'params': {}},
    {'metric': PerformanceMetric.F1_MACRO, 'params': {}},
    # {'metric': PerformanceMetric.JACCARD_SIMILARITY_SCORE, 'params': {}},
]

MULTILABEL_CLASSIFICATION_METRICS = [
    {'metric': PerformanceMetric.ACCURACY, 'params': {}},
]

REGRESSION_METRICS = [
    {'metric': PerformanceMetric.MEAN_ABSOLUTE_ERROR, 'params': {}},
    {'metric': PerformanceMetric.MEAN_SQUARED_ERROR, 'params': {}},
    {'metric': PerformanceMetric.ROOT_MEAN_SQUARED_ERROR, 'params': {}},
    {'metric': PerformanceMetric.R_SQUARED, 'params': {}},
]

CLUSTERING_METRICS = [
    {'metric': PerformanceMetric.NORMALIZED_MUTUAL_INFORMATION, 'params': {}},
]

LINK_PREDICTION_METRICS = [
    {'metric': PerformanceMetric.ACCURACY, 'params': {}},
    # {'metric': PerformanceMetric.JACCARD_SIMILARITY_SCORE, 'params': {}},
]

VERTEX_NOMINATION_METRICS = [
    {'metric': PerformanceMetric.ACCURACY, 'params': {}},
]

COMMUNITY_DETECTION_METRICS = [
    {'metric': PerformanceMetric.NORMALIZED_MUTUAL_INFORMATION, 'params': {}},
]

GRAPH_CLUSTERING_METRICS = []

GRAPH_MATCHING_METRICS = [
    {'metric': PerformanceMetric.ACCURACY, 'params': {}}
]

TIME_SERIES_FORECASTING_METRICS = REGRESSION_METRICS

COLLABORATIVE_FILTERING_METRICS = REGRESSION_METRICS

OBJECT_DETECTION_METRICS = [
    {'metric': PerformanceMetric.OBJECT_DETECTION_AVERAGE_PRECISION, 'params': {}},
]

MULTICLASS_VERTEX_METRICS = MULTICLASS_CLASSIFICATION_METRICS

SEMI_SUPERVISED_MULTICLASS_CLASSIFICATION_METRICS = MULTICLASS_CLASSIFICATION_METRICS

SEMI_SUPERVISED_REGRESSION_METRICS = REGRESSION_METRICS


def get_metrics_from_task(task_des, perf_metrics=None):
    """
    Provides a dictionary of metrics ready to use for perfromance_metrics

    Parameters
    ----------
    task_des: dict
        A dictionary describe the task
    perf_metrics: dict
        A dictionary specifying the needed performance metric parameters

    Returns
    -------
    performance_metrics: dict
        A dict containing performance metrics.
    """
    # For the case thet the user only want to run a full pipeline
    task_type = task_des['task_type']
    task_subtype = task_des['task_subtype']
    data_types = task_des['data_types']
    if not task_des:
        return None
    if TaskKeyword.CLASSIFICATION == task_type or \
            TaskKeyword.VERTEX_CLASSIFICATION == task_type:
        if task_des['semi']:
            # TODO: Temporary solution to binary semi supervised classification
            metrics = SEMI_SUPERVISED_MULTICLASS_CLASSIFICATION_METRICS
        elif TaskKeyword.BINARY == task_subtype:
            metrics = BINARY_CLASSIFICATION_METRICS
        elif TaskKeyword.MULTICLASS == task_subtype:
            metrics = MULTICLASS_CLASSIFICATION_METRICS
        elif TaskKeyword.MULTILABEL == task_subtype:
            metrics = MULTILABEL_CLASSIFICATION_METRICS
        else:
            metrics = CLASSIFICATION_METRICS
    elif TaskKeyword.REGRESSION == task_type:
        metrics = REGRESSION_METRICS
    elif TaskKeyword.CLUSTERING == task_type:
        metrics = CLUSTERING_METRICS
    elif TaskKeyword.LINK_PREDICTION == task_type:
        metrics = LINK_PREDICTION_METRICS
    elif TaskKeyword.VERTEX_NOMINATION == task_type:
        metrics = VERTEX_NOMINATION_METRICS
    elif TaskKeyword.COMMUNITY_DETECTION == task_type:
        metrics = COMMUNITY_DETECTION_METRICS
    elif TaskKeyword.GRAPH_MATCHING == task_type:
        metrics = GRAPH_MATCHING_METRICS
    elif TaskKeyword.TIME_SERIES in data_types and TaskKeyword.FORECASTING:
        metrics = TIME_SERIES_FORECASTING_METRICS
    elif TaskKeyword.COLLABORATIVE_FILTERING == task_type:
        metrics = COLLABORATIVE_FILTERING_METRICS
    elif TaskKeyword.OBJECT_DETECTION == task_type:
        metrics = OBJECT_DETECTION_METRICS
    else:
        raise ValueError('Task keywords not supported, keywords: {}'.format(task_des))

    for i, metric in enumerate(metrics):
        for perf_metric in perf_metrics:
            if perf_metric['metric'] == metric['metric'] and 'params' in perf_metric:
                copy_metric = copy.deepcopy(metric)
                copy_metric['params']['pos_label'] = perf_metric['params']['pos_label']
                metrics[i] = copy_metric
    logger.info('get_metrics_from_task:metrics: {}'.format(metrics))
    return metrics


def get_eval_configuration(task_type, data_types, semi):
    """
    Determines which method of evaluation to use, cross_fold, holdout, etc.

    Parameters
    ----------
    task_type: str
        task type
    data_types: list
        data types
    semi: bool
        is it semi-supervised problem

    Returns
    -------
    eval_configuration: dict
        A dict that contains the evaluation method to use.
    """
    k_fold_tabular = {
        'method': 'K_FOLD',
        'number_of_folds': '3',
        'stratified': 'false',
        'shuffle': 'true',
        'randomSeed': '42',
    }

    holdout = {
        'method': 'HOLDOUT',
        'train_score_ratio': '0.2',
        'shuffle': 'true',
        'stratified': 'true',
        'randomSeed': '42',
    }

    no_stratified_holdout = {
        'method': 'HOLDOUT',
        'train_score_ratio': '0.2',
        'shuffle': 'true',
        'stratified': 'false',
        'randomSeed': '42',
    }

    no_split = {
        'method': 'TRAINING_DATA',
        'number_of_folds': '1',
        'stratified': 'true',
        'shuffle': 'true',
        'randomSeed': '42',
    }
    # for the case of no problem return None.
    if not task_type:
        return

    if semi:
        # Splitting semi may get empty ground truth, which can cause error in sklearn metric.
        return no_split

    if TaskKeyword.CLASSIFICATION == task_type:
        # These data types tend to take up a lot of time to run, so no k_fold.
        if TaskKeyword.AUDIO in data_types or TaskKeyword.VIDEO in data_types \
                or TaskKeyword.IMAGE in data_types:
            return holdout
        else:
            return k_fold_tabular
    elif TaskKeyword.REGRESSION in data_types:
        return no_stratified_holdout
    else:
        return no_split


# def save_pipeline_run(pipelines_runs, id=None):
#     """
#     A function to store pipeline_runs on the correct directory
#
#     Parameters
#     ----------
#     pipelines_runs: list[pipelines_runs]
#         A list of pipelines_run to store.
#     id: Union[str, None]
#         And id that would be the direcotry name if necessary, we don't need it so far.
#     """
#
#     for pipeline_run in pipelines_runs:
#         save_run_path = os.path.join(Path.PIPELINE_RUNS, pipeline_run.to_json_structure()['id'] + '.yml')
#         with open(save_run_path, 'w') as file:
#             pipeline_run.to_yaml(file, indent=2)
#     return


def get_task_mapping(task: str) -> str:
    """
    Map the task in problem_doc to the task types that are currently supported

    Parameters
    ----------
    task: str
        The task type in problem_doc

    Returns
    -------
    str
        One of task types that are supported

    """
    mapping = {
        'LINK_PREDICTION': 'CLASSIFICATION',
        TaskKeyword.VERTEX_CLASSIFICATION: 'CLASSIFICATION',
        'COMMUNITY_DETECTION': 'CLASSIFICATION',
        'GRAPH_MATCHING': 'CLASSIFICATION',
        TaskKeyword.FORECASTING: 'REGRESSION',
        'OBJECT_DETECTION': 'CLASSIFICATION',
        'VERTEX_CLASSIFICATION': 'CLASSIFICATION',
    }
    if task in mapping:
        return mapping[task]
    else:
        return task


def get_score_information(problem):
    """
    A function that get the target information from problem doc that would be expose to the ta3-2 api later.

    Parameters
    ----------
    problem: Union[dict, None]
        A parsed problem description.

    Returns
    -------
    info: Union[dict, None]
        A dict with targets and dataset, or None if no problem
    """
    if problem is None:
        return
    else:
        info = list()
        if 'inputs' in problem:
            for input in problem['inputs']:
                if 'targets' in input and 'dataset_id' in input:
                    info.append({'targets': input['targets'], 'dataset_id': input['dataset_id']})
            return info
        else:
            return


def get_task_des(keywords) -> dict:
    """
    A function that parse the keywords from the problem and map them to
    TaskType, SubTasktype and data type eg. tabular, images, audio, etc

    Parameters
    ----------
    keywords: List[d3m.problem.TaskKeyword]
        List of keywords that comes from d3m problem description

    Returns
    -------
    dict
        {
            task_type: str
            task_subtype: str
            data_types: list
            semi: bool
        }
    """

    task_type = None
    task_subtype = None
    data_types = []
    semi = False
    for keyword in keywords:
        if keyword in TASK_TYPE:
            task_type = keyword.name
        elif keyword in TASK_SUBTYPES:
            task_subtype = keyword.name
        elif keyword in DATA_TYPES:
            data_types.append(keyword.name)
        elif keyword.name == TaskKeyword.SEMISUPERVISED:
            semi = True

    # if data_types is none we assume is tabular:
    if data_types is None:
        data_types = TaskKeyword.TABULAR

    return {'task_type': task_type, 'task_subtype': task_subtype, 'data_types': data_types, 'semi': semi}


def load_problem(problem_doc_uri):
    """
    Load an encoded problem doc.

    Parameters
    ----------
    problem_doc_uri: str
        A path to the problem doc
    """
    return problem_module.Problem.from_json_structure(file_utils.load_json(problem_doc_uri))
