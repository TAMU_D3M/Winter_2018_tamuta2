import uuid
from modules.pipeline_generator.models.pipeline_generator_base import PipelineGeneratorBase
from modules.utils import file_utils, schemas_utils, pipeline_utils
import json
from d3m.metadata.pipeline import Pipeline


class DefaultPipelineGenerator(PipelineGeneratorBase):
    """
    Default Pipeline generator that provides basic pre defined pipelines.
    """

    pipelines_dir = 'modules/utils/pipelines/default_pipelines.json'

    def __init__(self, problem_doc_uri, inputs_metadata, save_path, *, main_resource=None, metafeatures=None,
                 template_metadata=None,
                 expected_num_iter=10, pipeline_template_uri=None, primitives_blacklist=None):
        super().__init__(
            problem_doc_uri=problem_doc_uri,
            inputs_metadata=inputs_metadata,
            save_path=save_path,
            main_resource=main_resource,
            metafeatures=metafeatures,
            template_metadata=template_metadata,
            expected_num_iter=expected_num_iter,
            pipeline_template_uri=pipeline_template_uri,
            primitives_blacklist=primitives_blacklist
        )
        self._seen_pipelines = []
        self.description = {
            'contact_email': 'dmartinez05@tamu.com',
            'contact_name': 'Diego Martinez, Tsunglin Yang',
            'description': 'DefaultPipelineGenerator'
        }

        task_des = schemas_utils.get_task_des(self.problem['problem']['task_keywords'])
        self.available_pipelines = self._return_pipelines(
            task_des['task_type'], task_des['task_subtype'], task_des['data_types'])

    def _return_pipelines(self, task_type, task_subtype, data_type):
        """
        A function that return predefined pipelines given a task type.

        Returns
        -------
            A predefined pipelines if there are pipelines left, also if there is template
            returns the new pipeline with the template.

        """
        # TODO incorporate task_subtype and data_type for future problems
        with open(DefaultPipelineGenerator.pipelines_dir) as file:
            possible_pipelines = json.load(file)

        if task_type not in possible_pipelines:
            self.pipeline_left = False
            return None

        possible_pipelines = possible_pipelines[task_type]

        if not possible_pipelines:
            self.pipeline_left = False
            return None

        return possible_pipelines

    def _get_pipeline(self):
        pipeline = None
        while self.available_pipelines:
            pipeline_dict = self.available_pipelines.pop()
            try:
                pipeline = pipeline_utils.load_pipeline(pipeline_dict)
                break
            except Exception as e:
                print(e)

        if not self.available_pipelines and pipeline is None:
            self.pipeline_left = False
            return None

        # update id
        pipeline.id = str(uuid.uuid4())

        # update time
        pipeline.created = Pipeline().created

        if self.pipeline_template is not None:
            pipeline = self._replace_placeholder_step(pipeline)

        # Update information.
        pipeline = self._set_pipline_description_info(
            pipeline=pipeline, pipeline_name=pipeline.id, **self.description)

        return pipeline

    def get_pipelines(self, num_pipelines=1, time_left=-1):
        """
        A function that is called in order to get certain number of pipelines generated by this generator.

        Parameters
        ----------
        num_pipelines: int
            Desirable number of pipelines that the generator need to return, this parameter is an upper bound,
            the generator should return at most num_pipelines.
        time_left: float
            Updates the time left on the generator.

        Returns
        -------
        generated_pipelines: dict
            A dictionary described as: { 'pipeline_id': {'path': pipeline_path, 'outputs': True} that contains at most
            num_pipelines abd whether or not they want to know the output of the pipelines.

        """
        self.time_left = time_left
        # if there is no pipeline left return empty dict
        if not self.pipeline_left:
            return {}

        pipelines = {}
        for i in range(0, num_pipelines):
            pipeline = self._get_pipeline()

            if not self.available_pipelines:
                break

            pipeline_uri = file_utils.save_pipeline_to_json_file(pipeline, self.save_path)
            # pipelines[pipeline.id] = {'path': pipeline_uri, 'expose_outputs': ['steps.4.produce', 'steps.5.produce']}
            pipelines[pipeline.id] = {'path': pipeline_uri}

        return pipelines

    def update_pipelines(self, pipelines, time_left):
        """
        Updates the internal state of pipelines.

        Parameters
        ----------
        pipelines: dict
            A dictionary containing all information related with pipelines.
        time_left: float
            Updates the time left on the generator.
        """
        self.time_left = time_left
        self._update_pipelines(pipelines)
        return
