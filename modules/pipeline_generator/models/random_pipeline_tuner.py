import logging

from modules.pipeline_generator.models.base_pipeline_tuner import BasePipelineTuner
from modules.tuner.randomsearch import RandomSearchOracle

logger = logging.getLogger(__name__)


class RandomPipelineTuner(BasePipelineTuner):
    """
        Pipeline Random tuner.
    """
    def __init__(self, problem_doc_uri, inputs_metadata, save_path, *, main_resource=None, metafeatures=None,
                 template_metadata=None, expected_num_iter=10, pipeline_template_uri=None, primitives_blacklist=None,
                ):
        super(RandomPipelineTuner, self).__init__(
            problem_doc_uri=problem_doc_uri,
            inputs_metadata=inputs_metadata,
            save_path=save_path,
            main_resource=main_resource,
            metafeatures=metafeatures,
            template_metadata=template_metadata,
            expected_num_iter=expected_num_iter,
            pipeline_template_uri=pipeline_template_uri,
            primitives_blacklist=primitives_blacklist
        )
        # logger.info('Hello World, RandomPipelineTuner')
        self.description = {
            'contact_email': 'yiwei_chen@tamu.com',
            'contact_name': 'Yi-Wei Chen',
            'description': 'RandomPipelineTuner'
        }

    def init_oracle(self):
        self.project_name = 'random_pipeline_tuner'
        self.oracle = RandomSearchOracle(
            objective=self.objective,
            max_trials=10000,  # pre-defined number,
            seed=None,  # seed
            hyperparameters=self.hyperparameters,
        )
        self.oracle._set_project_dir(
            self.directory, self.project_name, overwrite=True)
