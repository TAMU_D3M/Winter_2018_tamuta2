import hashlib
import json
import itertools
from copy import deepcopy
import uuid

from d3m import index
from d3m.metadata import pipeline as pipeline_module
from d3m.metadata.base import ArgumentType
from d3m.metadata.pipeline import PrimitiveStep

from modules.pipeline_generator.models.pipeline_generator_base import PipelineGeneratorBase
from modules.utils import custom_resolver
from modules.utils import file_utils
import logging

logger = logging.getLogger(__name__)
primitives = {
    'DatasetToDataFrame': 'd3m.primitives.data_transformation.dataset_to_dataframe.Common',
    'ColumnParser': 'd3m.primitives.data_transformation.column_parser.Common',
    'HorizontalConcat': 'd3m.primitives.data_transformation.horizontal_concat.DataFrameCommon',
    'ExtractColumnsBySemanticTypes':
        'd3m.primitives.data_transformation.extract_columns_by_semantic_types.Common',
    'ConstructPredictions': 'd3m.primitives.data_transformation.construct_predictions.Common',
    'RenameDuplicateColumns': 'd3m.primitives.data_transformation.rename_duplicate_name.DataFrameCommon',
    'LGBMClassifier': 'd3m.primitives.classification.light_gbm.Common',
    # 'voting': 'd3m.primitives.classification.ensemble_voting.DSBOX',
    'OneHot': 'd3m.primitives.data_transformation.one_hot_encoder.DistilOneHotEncoder',
    'replaceSematic': 'd3m.primitives.data_transformation.replace_semantic_types.Common',
    'XGBoostClassifier': 'd3m.primitives.classification.xgboost_gbtree.Common',
    'XGBoostRegressor': 'd3m.primitives.regression.xgboost_gbtree.Common',
    'catImputer': 'd3m.primitives.data_transformation.imputer.DistilCategoricalImputer',
    # 'feature_selection': 'd3m.primitives.feature_selection.joint_mutual_information.AutoRPI',
    'scaler': 'd3m.primitives.data_preprocessing.robust_scaler.SKlearn',
}
for primitive_name in primitives.keys():
    primitives[primitive_name] = index.get_primitive(primitives[primitive_name])


class Ensemble(PipelineGeneratorBase):
    """
    Random Pipeline generator that provides basic pre defined pipelines.
    """

    def __init__(self, problem_doc_uri, inputs_metadata, save_path, *, main_resource=None, metafeatures=None,
                 template_metadata=None,
                 expected_num_iter=10, pipeline_template_uri=None, primitives_blacklist=None):
        super().__init__(
            problem_doc_uri=problem_doc_uri,
            inputs_metadata=inputs_metadata,
            save_path=save_path,
            main_resource=main_resource,
            metafeatures=metafeatures,
            template_metadata=template_metadata,
            expected_num_iter=expected_num_iter,
            pipeline_template_uri=pipeline_template_uri,
            primitives_blacklist=primitives_blacklist
        )

        self._seen_pipelines = []
        self.resolver = custom_resolver.BlackListResolver()
        self.description = {
            'contact_email': 'dmartinez05@tamu.com',
            'contact_name': 'Diego Martinez',
            'description': 'EnsemblePipelineGenerator'
        }

        targets = []
        for _input in self.problem.get('inputs', []):
            for target in _input.get('targets', []):
                targets.append((target['column_name'], target['resource_id']))

        # if multiple outputs we can't generate ensemble (in this generator).
        if not targets or len(targets) > 1:
            self.pipeline_left = False
            return

        metadata = self.inputs_metadata[-1]
        idx = metadata.get_column_index_from_column_name(targets[-1][0], at=(targets[-1][1], ))
        if 'https://metadata.datadrivendiscovery.org/types/CategoricalData' in \
                metadata.query((targets[-1][1], '_ALL_ELEMNTS_', idx))['semantic_types']:
            self.target_type = 'categorical'
        else:
            self.target_type = 'numerical'

        # pipeline_id : hash_list
        self.known_pipelines = {}
        # hash: step
        self.known_steps = {}
        self.candidates = {}
        self.known_combinations = []

    def _get_preprocessing_pipeline_id(self):
        best_rank = 1
        best_pipeline_id = None
        for pipeline_id, info in self.candidates.items():
            if info['rank'] < best_rank:
                best_rank = info['rank']
                best_pipeline_id = pipeline_id
        return best_pipeline_id

    def _get_pipeline(self):

        compatible_candidates = {}
        for pipeline_id in self._select_candidates(list(self.candidates.keys())):
            compatible_candidates[pipeline_id] = self.candidates[pipeline_id]

        if len(compatible_candidates) <= 1:
            return

        ranks = [info['rank'] for info in self.candidates.values()]
        mean_rank = sum(ranks)/len(ranks)
        best_rank = min(ranks)

        threshold = mean_rank - ((mean_rank - best_rank) / 5) * 4

        candidates = []
        for info in compatible_candidates.values():
            # rank lower is better
            if info['rank'] <= threshold:
                candidates.append(info['pipeline'])

        # candidates = [info['pipeline'] for info in self.candidates.values()]
        # check if we have more that one pipeline
        if len(candidates) <= 1:
            return None

        # we get best prerocessing step
        pre_prepcessing_pipeline = self.candidates[self._get_preprocessing_pipeline_id()]['pipeline']
        primitive_path_preprocessing = None
        for i in range(len(pre_prepcessing_pipeline.steps) - 1, -1, -1):
            primitive_path = pre_prepcessing_pipeline.steps[i].primitive.metadata.query()['python_path']
            if 'classification' in primitive_path or 'regression' in primitive_path:
                primitive_path_preprocessing = pre_prepcessing_pipeline.steps[
                    int(pre_prepcessing_pipeline.steps[i].arguments['inputs']['data'].split('.')[1])].primitive.metadata.query()['python_path']
                if 'concat' not in primitive_path_preprocessing:
                    break

        new_pipeline = self.merge_multiple_pipelines(candidates)
        new_pipeline = concat_pipeline_outputs(new_pipeline, self.target_type, primitive_path_preprocessing, self.resolver)

        # for the case that we have categorical data we do voting (for now)

        return new_pipeline
        # for now we grab every pipeline just for testing

    def get_pipelines(self, num_pipelines=1, time_left=-1):
        """
        A function that is called in order to get certain number of pipelines generated by this generator.

        Parameters
        ----------
        num_pipelines: int
            Desirable number of pipelines that the generator need to return, this parameter is an upper bound,
            the generator should return at most num_pipelines.
        time_left: float
            Updates the time left on the generator.

        Returns
        -------
        generated_pipelines: dict
            A dictionary described as: { 'pipeline_id': {'path': pipeline_path, 'outputs': True} that contains at most
            num_pipelines abd whether or not they want to know the output of the pipelines.

        """
        self.time_left = time_left
        if self.time_limit == 0:
            self.time_limit = time_left

        pipeline = self._get_pipeline()
        if pipeline is None:
            return {}
        else:
            pipeline_uri = file_utils.save_pipeline_to_json_file(pipeline, self.save_path)
            self.pipeline_generated.append(pipeline.id)
            return {pipeline.id: {'path': pipeline_uri}}

    def update_pipelines(self, pipelines, time_left):
        """
        Updates the internal state of pipelines.

        Parameters
        ----------
        pipelines: dict
            A dictionary containing all information related with pipelines.
        time_left: float
            Updates the time left on the generator.
        """
        self.time_left = time_left
        if self.time_limit == 0:
            self.time_limit = time_left
        self._update_pipelines(pipelines)

        # We ingest every completed pipeline.
        for pipeline_id, info in pipelines.items():
            if info['status'] == 'COMPLETED':
                pipeline = pipeline_module.get_pipeline(info['path'])
                self._diggest_pipeline(pipeline)
                self.candidates[pipeline.id] = {
                    'pipeline': pipeline,
                    'scores': info['scores'],
                    'internal_score': info['internal_score'],
                    'rank': info['rank']
                }
        return

    def _diggest_pipeline(self, pipeline):
        hash_list = []
        for step in pipeline.steps:
            # We transform the step to dict and then to string
            step_representation = json.dumps(step.to_json_structure(), sort_keys=True).encode('utf-8')
            # Then we hash the step, for now we use sha 256
            step_representation = hashlib.sha256(step_representation).hexdigest()
            hash_list.append(step_representation)
            # hash: step
            self.known_steps[step_representation] = step
        self.known_pipelines[pipeline.id] = hash_list

    def merge_pipelines(self, pipeline_1, pipeline_2):
        # we need to return the indexes of the outputs in the new pipeline as well as the new pipeline
        pipeline_1_hash = self.known_pipelines[pipeline_1.id]
        pipeline_2_hash = self.known_pipelines[pipeline_2.id]

        shared_steps = longest_path(pipeline_1_hash, pipeline_2_hash)

        # we always start with copying the first piptline
        pipeline = deepcopy(pipeline_1)
        pipeline.id = str(uuid.uuid4())

        # if the case is that they only share one step and is the last one or they dont share at all
        if (len(shared_steps) == 1 and (shared_steps[-1] == pipeline_1_hash[-1] or
                                        shared_steps[-1] == pipeline_2_hash[-1])) or len(shared_steps) == 0:

            offset = len(pipeline.steps)
            # just add the steps of the second pipeline to the first
            for step in pipeline_2.steps:
                pipeline.add_step(add_offset_to_step(offset, step))
            # and then update the outputs
            for output in pipeline_2.outputs:
                pipeline.add_output(add_offset_to_argument(offset, output['data']))

        # then we go for the case when they share other stuff
        else:
            step_indexes = [pipeline_1_hash.index(step) for step in shared_steps]
            range_list = [pipeline_2_hash.index(step) for step in shared_steps]

            offset = len(pipeline.steps) - max(range_list) - 1

            for i in range(0, len(pipeline_2.steps)):
                if i not in range_list:
                    pipeline.add_step(add_offset_to_step_not_in_list(offset, range_list, pipeline_2.steps[i]))
            for output in pipeline_2.outputs:
                if int(output['data'].split('.')[1]) not in range_list:
                    pipeline.add_output(add_offset_to_argument(offset, output['data']))
                else:
                    pipeline.add_output(output['data'])
        return pipeline

    def merge_multiple_pipelines(self, pipelines):
        # we need to return the indexes of the outptus in the new pipeline as well as the new pipeline
        pipelines_left = pipelines
        ids = [pipeline.id for pipeline in pipelines_left]

        if self.combination_exist(ids):
            return

        while True:
            if len(pipelines_left) == 1:
                break
            id_1, id_2 = self.pipelines_to_merge(ids)
            index = ids.index(id_1)
            pipeline_1 = pipelines_left[index]
            del pipelines_left[index]
            del ids[index]
            index = ids.index(id_2)
            pipeline_2 = pipelines_left[index]
            del pipelines_left[index]
            del ids[index]
            # merge pipelines and the diggest them
            merged_pipeline = self.merge_pipelines(pipeline_1, pipeline_2)
            self._diggest_pipeline(merged_pipeline)
            pipelines_left.append(merged_pipeline)
            ids.append(merged_pipeline.id)

        return pipelines_left[-1]

    def pipelines_to_merge(self, pipelines_ids):
        combinations = list(itertools.combinations(pipelines_ids, 2))
        longest_components = []
        for combination in combinations:
            component = longest_path(self.known_pipelines[combination[0]],
                                     self.known_pipelines[combination[1]])
            longest_components.append(len(component))

        index = longest_components.index(max(longest_components))
        return combinations[index]

    def _select_candidates(self, pipelines_ids):
        combinations = list(itertools.combinations(pipelines_ids, 2))
        longest_components = []
        for combination in combinations:
            component = longest_path(self.known_pipelines[combination[0]],
                                     self.known_pipelines[combination[1]])
            longest_components.append(len(component))

        valid_combinations = set()
        for i in range(0, len(longest_components)):
            if longest_components[i] != 0:
                valid_combinations.add(combinations[i][0])
                valid_combinations.add(combinations[i][1])
        return list(valid_combinations)

    def combination_exist(self, pipelines_id):
        exists = False
        ids = set(pipelines_id)
        for combination in self.known_combinations:
            if ids == combination:
                exists = True
                break
        if not exists:
            self.known_combinations.append(ids)
        return exists


def longest_path(hashed_pipeline_1, hashed_pipeline_2):
    answer = []
    len1, len2 = len(hashed_pipeline_1), len(hashed_pipeline_2)
    for i in range(len1):
        match = []
        for j in range(len2):
            if i + j < len1 and hashed_pipeline_1[i + j] == hashed_pipeline_2[j]:
                match.append(hashed_pipeline_2[j])
            else:
                if len(match) > len(answer):
                    answer = match
                match = []
    return answer


def copy_step(pipeline_step):
    """
    A function that copy a pipeline step.

    Parameters
    ----------
    primitive_step: d3m.metadata.pipeline.PrimitiveStep
        PrimitiveStep to copy.

    Returns
    -------
    copy_step:d3m.metadata.pipeline.PrimitiveStep
        A copy of the input step.
    """
    step = deepcopy(pipeline_step)
    step.index = None
    return step


def add_offset_to_step(offset, pipeline_step):
    step = copy_step(pipeline_step)
    arguments = {}
    for argument, info in step.arguments.items():
        data = info['data']
        _type = info['type']
        # if the argument is not an input we update it
        data = add_offset_to_argument(offset, data)
        arguments[argument] = {
            'data': data,
            'type': _type
        }

    step.arguments = arguments
    return step


def add_offset_to_argument(offset, argument):
    if 'input' not in argument:
        data_parts = argument.split('.')
        # We get the step and add the offset
        data_step = str(int(data_parts[1]) + offset)
        new_argument = ''
        for i in range(0, len(data_parts)):
            if i == 0:
                new_argument += data_parts[i]
            elif i == 1:
                new_argument += '.' + data_step
            else:
                new_argument += '.' + data_parts[i]
        return new_argument
    else:
        return argument


def add_offset_to_step_not_in_list(offset, range_list, pipeline_step):
    step = copy_step(pipeline_step)
    arguments = {}
    for argument, info in step.arguments.items():
        data = info['data']
        _type = info['type']
        if int(data.split('.')[1]) not in range_list:
            # if the argument is not an input we update it
            data = add_offset_to_argument(offset, data)
        arguments[argument] = {
            'data': data,
            'type': _type
        }

    step.arguments = arguments
    return step


def concat_pipeline_outputs(pipeline, target_type, primitive_path_preprocessing, resolver):
    if len(pipeline.outputs) <= 1:
        return pipeline
    else:
        # concat all the outputs
        outputs = [output['data'] for output in pipeline.outputs]
        new_outputs = []
        while len(outputs) > 1:
            new_outputs = []

            n_outputs = len(outputs)
            if n_outputs % 2 != 0:
                n_outputs -= 1
            for i in range(0, n_outputs, 2):
                current_step = len(pipeline.steps)
                concat_step = PrimitiveStep(primitive_description=primitives['HorizontalConcat'].metadata.query(),
                                            resolver=resolver)
                concat_step.add_argument(name='left', argument_type=ArgumentType.CONTAINER, data_reference=outputs[i])
                concat_step.add_argument(name='right', argument_type=ArgumentType.CONTAINER, data_reference=outputs[i+1])
                concat_step.add_hyperparameter(name='use_index', argument_type=ArgumentType.VALUE, data=True)
                concat_step.add_output('produce')
                pipeline.add_step(concat_step)

                new_outputs.append('steps.{}.produce'.format(current_step))

            if len(outputs) % 2 != 0:
                new_outputs.append(outputs[-1])

            outputs = new_outputs

        # fix the names of the columns since is likely they are repeated
        fix_names = PrimitiveStep(primitive_description=primitives['RenameDuplicateColumns'].metadata.query(),
                                  resolver=resolver)
        fix_names.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=outputs[-1])
        fix_names.add_output('produce')
        pipeline.add_step(fix_names)
        current_step = 'steps.{}.produce'.format(len(pipeline.steps) - 1)

        drop_index = PrimitiveStep(
            primitive_description=primitives['ExtractColumnsBySemanticTypes'].metadata.query(),
            resolver=resolver)
        drop_index.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                data_reference=current_step)
        drop_index.add_hyperparameter(name='negate', argument_type=ArgumentType.VALUE, data=True)
        drop_index.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                                      data=['https://metadata.datadrivendiscovery.org/types/PrimaryKey'])
        drop_index.add_output('produce')
        pipeline.add_step(drop_index)
        current_step = 'steps.{}.produce'.format(len(pipeline.steps) - 1)

        # add semantic types
        add_semantic = PrimitiveStep(primitive_description=primitives['replaceSematic'].metadata.query(),
                                     resolver=resolver)
        add_semantic.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=current_step)
        add_semantic.add_hyperparameter(name='to_semantic_types', argument_type=ArgumentType.VALUE,
                                        data=['https://metadata.datadrivendiscovery.org/types/Attribute'])
        add_semantic.add_hyperparameter(name='from_semantic_types', argument_type=ArgumentType.VALUE,
                                        data=['https://metadata.datadrivendiscovery.org/types/TrueTarget',
                                              'https://metadata.datadrivendiscovery.org/types/Target',
                                              'https://metadata.datadrivendiscovery.org/types/SuggestedTarget',
                                              'https://metadata.datadrivendiscovery.org/types/PredictedTarget'])
        add_semantic.add_output('produce')
        pipeline.add_step(add_semantic)
        current_step = 'steps.{}.produce'.format(len(pipeline.steps) - 1)

        # add one hot if there is categorical values.
        if target_type == 'categorical':
            # cat_mputer = PrimitiveStep(primitive_description=primitives['catImputer'].metadata.query(),
            #                         resolver=resolver)
            # cat_mputer.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=current_step)
            # cat_mputer.add_output('produce')
            # pipeline.add_step(cat_mputer)
            # current_step = 'steps.{}.produce'.format(len(pipeline.steps) - 1)

            one_hot = PrimitiveStep(primitive_description=primitives['OneHot'].metadata.query(),
                                      resolver=resolver)
            one_hot.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=current_step)
            one_hot.add_output('produce')
            pipeline.add_step(one_hot)
            current_step = 'steps.{}.produce'.format(len(pipeline.steps) - 1)

        targets_step = None
        for i in range(0, len(pipeline.steps)):
            primitive_path = pipeline.steps[i].primitive.metadata.query()['python_path']
            if 'classification' in primitive_path or 'regression' in primitive_path:
                targets_step = pipeline.steps[i].arguments['outputs']['data']
                break

        attributes_step = current_step
        for i in range(0, len(pipeline.steps)):
            if pipeline.steps[i].primitive.metadata.query()['python_path'] == primitive_path_preprocessing:
                attributes_step = 'steps.{}.produce'.format(i)

        # feature_selection = PrimitiveStep(primitive_description=primitives['feature_selection'].metadata.query(),
        #                                   resolver=resolver)
        # feature_selection.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes_step)
        # feature_selection.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets_step)
        # feature_selection.add_output('produce')
        # pipeline.add_step(feature_selection)
        # attributes_step = 'steps.{}.produce'.format(len(pipeline.steps) - 1)

        concat_step = PrimitiveStep(primitive_description=primitives['HorizontalConcat'].metadata.query(),
                                    resolver=resolver)
        concat_step.add_argument(name='left', argument_type=ArgumentType.CONTAINER, data_reference=attributes_step)
        concat_step.add_argument(name='right', argument_type=ArgumentType.CONTAINER, data_reference=current_step)
        concat_step.add_output('produce')
        pipeline.add_step(concat_step)
        attributes_step = 'steps.{}.produce'.format(len(pipeline.steps) - 1)

        scaler = PrimitiveStep(primitive_description=primitives['scaler'].metadata.query(), resolver=resolver)
        scaler.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes_step)
        scaler.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
        scaler.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
        scaler.add_output('produce')
        pipeline.add_step(scaler)
        attributes_step = 'steps.{}.produce'.format(len(pipeline.steps) - 1)

        # XGBOOST
        estimator = 'XGBoostRegressor'
        if target_type == 'categorical':
            estimator = 'LGBMClassifier'

        learner = PrimitiveStep(primitive_description=primitives[estimator].metadata.query(), resolver=resolver)
        learner.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes_step)
        learner.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets_step)
        learner.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
        learner.add_output('produce')
        pipeline.add_step(learner)
        predictions_step = 'steps.{}.produce'.format(len(pipeline.steps) - 1)

        dataframe_step = None
        for i in range(0, len(pipeline.steps)):
            primitive_path = pipeline.steps[i].primitive.metadata.query()['python_path']
            if 'dataset_to_dataframe' in primitive_path:
                dataframe_step = 'steps.{}.produce'.format(i)
                break

        # Add step prediction into pipeline
        construct = PrimitiveStep(primitive_description=primitives['ConstructPredictions'].metadata.query(),
                                  resolver=resolver)
        construct.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                               data_reference=predictions_step)
        construct.add_argument(name='reference', argument_type=ArgumentType.CONTAINER,
                               data_reference=dataframe_step)
        construct.add_output('produce')
        pipeline.add_step(construct)
        current_step = 'steps.{}.produce'.format(len(pipeline.steps) - 1)

        # then we clear the outputs and add the last step as output
        pipeline.outputs = []
        pipeline.add_output(current_step)

        return pipeline
