
import logging
from d3m.metadata.base import ArgumentType
from d3m import utils as d3m_utils
from d3m.metadata.pipeline import Pipeline, PrimitiveStep

from modules.pipeline_generator.models.pipeline_generator_base import PipelineGeneratorBase
from modules.utils import file_utils, custom_resolver, schemas_utils
from modules.utils.pipeline_utils import get_primitives_by_task_and_name, get_primitives

logger = logging.getLogger(__name__)

primitives = {
    'ConstructPredictions': 'd3m.primitives.data_transformation.construct_predictions.Common',
    'DatasetToDataFrame': 'd3m.primitives.data_transformation.dataset_to_dataframe.Common',
    'ColumnParser': 'd3m.primitives.data_transformation.column_parser.Common',
    'ExtractColumnsBySemanticTypes': 'd3m.primitives.data_transformation.extract_columns_by_semantic_types.Common',
    'Denormalize': 'd3m.primitives.data_transformation.denormalize.Common',
    'Imputer': 'd3m.primitives.data_cleaning.imputer.SKlearn',
    'SDFS': 'd3m.primitives.feature_construction.deep_feature_synthesis.SingleTableFeaturization',
    'MDFS': 'd3m.primitives.feature_construction.deep_feature_synthesis.MultiTableFeaturization',
    'DMap': 'd3m.primitives.operator.dataset_map.DataFrameCommon',
    'ConstructPredictions': 'd3m.primitives.data_transformation.construct_predictions.Common',
    'XGBoostClassifier': 'd3m.primitives.classification.xgboost_gbtree.Common',
    'XGBoostRegressor': 'd3m.primitives.regression.xgboost_gbtree.Common',
    'LGBM': 'd3m.primitives.classification.light_gbm.Common',
    'scaler': 'd3m.primitives.data_preprocessing.robust_scaler.SKlearn',
    'fselect': 'd3m.primitives.feature_selection.select_fwe.SKlearn',
    'SemiClassification': 'd3m.primitives.semisupervised_classification.iterative_labeling.AutonBox',
}


class QuickPipelineGenerator(PipelineGeneratorBase):
    """
    Random Pipeline generator that provides basic pre defined pipelines.
    """

    def __init__(self, problem_doc_uri, inputs_metadata, save_path, *, main_resource=None, metafeatures=None,
                 template_metadata=None,
                 expected_num_iter=10, pipeline_template_uri=None, primitives_blacklist=None):
        super().__init__(
            problem_doc_uri=problem_doc_uri,
            inputs_metadata=inputs_metadata,
            save_path=save_path,
            main_resource=main_resource,
            metafeatures=metafeatures,
            template_metadata=template_metadata,
            expected_num_iter=expected_num_iter,
            pipeline_template_uri=pipeline_template_uri,
            primitives_blacklist=primitives_blacklist
        )

        self.primitives = get_primitives(primitives)

        estimators = {
            'CLASSIFICATION': ['XGBoostClassifier', 'LGBM'],
            'REGRESSION': ['XGBoostRegressor'],
        }

        self.pipeline_generated = []
        self.description = {
            'contact_email': 'dmartinez05@tamu.com',
            'contact_name': 'Diego Martinez',
            'description': 'QuickPipelineGenerator'
        }

        self.task_des = schemas_utils.get_task_des(self.problem['problem']['task_keywords'])
        if self.task_des['task_type'] == 'SEMISUPERVISED_CLASSIFICATION':
            # load semi_supervised primitives.
            with d3m_utils.silence():
                semi_supervised_prims = get_primitives_by_task_and_name('CLASSIFICATION', 'SKlearn')
                self.primitives = dict(**self.primitives, **semi_supervised_prims)
                estimators['SEMISUPERVISED_CLASSIFICATION'] = list(semi_supervised_prims.keys())

        self.primitive_candidates = estimators[self.task_des['task_type']]

        self.primitive_used = set()
        self.resolver = custom_resolver.BlackListResolver()

    def _get_pipeline(self, estimator='XGBoostClassifier'):
        """
        A function that retrieves one pipeline based on the task_type.

        Returns
        -------
        pipeline: d3m.metadata.pipeline.Pipeline
            A pipeline
        """
        try:
            current_step = 0

            # Creating pipeline
            pipeline_description = Pipeline()
            pipeline_description.add_input(name='inputs')

            dataset_parser = PrimitiveStep(primitive=self.primitives['DMap'], resolver=self.resolver)
            dataset_parser.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
            dataset_parser.add_hyperparameter(name='resources', argument_type=ArgumentType.VALUE, data='all')
            dataset_parser.add_hyperparameter(name='primitive', argument_type=ArgumentType.VALUE, data=self.primitives['ColumnParser'])
            dataset_parser.add_hyperparameter(name='fit_primitive', argument_type=ArgumentType.VALUE, data='no')
            dataset_parser.add_output('produce')

            pipeline_description.add_step(dataset_parser)

            denormalize_step = PrimitiveStep(primitive_description=self.primitives['Denormalize'].metadata.query(),
                                             resolver=self.resolver)
            denormalize_step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
            denormalize_step.add_hyperparameter(name='starting_resource', argument_type=ArgumentType.VALUE,
                                                data=self.main_resource)
            denormalize_step.add_output('produce')
            pipeline_description.add_step(denormalize_step)
            current_step += 1

            step_dtd = PrimitiveStep(primitive_description=self.primitives['DatasetToDataFrame'].metadata.query(),
                                     resolver=self.resolver)
            step_dtd.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                  data_reference='steps.{i}.produce'.format(i=current_step))

            step_dtd.add_output('produce')
            pipeline_description.add_step(step_dtd)
            current_step += 1
            dataframe_step = 'steps.{i}.produce'.format(i=current_step)

            # check if we have privilege data.
            columns_with_privileges = self.inputs_metadata[-1].list_columns_with_semantic_types(
                ['https://metadata.datadrivendiscovery.org/types/SuggestedPrivilegedData'], at=('learningData',))
            if columns_with_privileges:
                # get attributes
                remove_priviliged_data = PrimitiveStep(
                    primitive_description=self.primitives['ExtractColumnsBySemanticTypes'].metadata.query(),
                    resolver=self.resolver)
                remove_priviliged_data.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                                    data_reference=dataframe_step)
                remove_priviliged_data.add_hyperparameter(name='exclude_columns', argument_type=ArgumentType.VALUE,
                                                          data=columns_with_privileges)
                remove_priviliged_data.add_output('produce')
                pipeline_description.add_step(remove_priviliged_data)
                current_step += 1

            # get attributes
            extract_attributes = PrimitiveStep(primitive_description=self.primitives['ExtractColumnsBySemanticTypes'].metadata.query(),
                                               resolver=self.resolver)
            extract_attributes.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                            data_reference='steps.{}.produce'.format(current_step))
                                            # data_reference=dataframe_step)
            extract_attributes.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                                                  data=['https://metadata.datadrivendiscovery.org/types/Attribute'])
            extract_attributes.add_output('produce')
            pipeline_description.add_step(extract_attributes)
            current_step += 1
            attributes_step = current_step

            # get targets
            extract_targets = PrimitiveStep(primitive_description=self.primitives['ExtractColumnsBySemanticTypes'].metadata.query(),
                                            resolver=self.resolver)
            extract_targets.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                         data_reference=dataframe_step)
            extract_targets.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                                               data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
            extract_targets.add_output('produce')
            pipeline_description.add_step(extract_targets)
            current_step += 1
            targets_step = 'steps.{i}.produce'.format(i=current_step)

            # feature aug
            dfs = PrimitiveStep(primitive_description=self.primitives['SDFS'].metadata.query(), resolver=self.resolver)
            dfs.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                             data_reference='steps.{i}.produce'.format(i=attributes_step))
            dfs.add_output('produce')
            pipeline_description.add_step(dfs)
            current_step += 1
            attributes_step = 'steps.{i}.produce'.format(i=current_step)

            imputer = PrimitiveStep(primitive_description=self.primitives['Imputer'].metadata.query(), resolver=self.resolver)
            imputer.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes_step)
            imputer.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
            imputer.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
            imputer.add_output('produce')
            pipeline_description.add_step(imputer)
            current_step += 1
            attributes_step = 'steps.{i}.produce'.format(i=current_step)

            scaler = PrimitiveStep(primitive_description=self.primitives['scaler'].metadata.query(), resolver=self.resolver)
            scaler.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes_step)
            scaler.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
            scaler.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
            scaler.add_output('produce')
            pipeline_description.add_step(scaler)
            current_step += 1
            attributes_step = 'steps.{i}.produce'.format(i=current_step)

            if self.task_des['task_type'] == 'SEMISUPERVISED_CLASSIFICATION':
                auton = PrimitiveStep(primitive_description=self.primitives['SemiClassification'].metadata.query(),
                                    resolver=self.resolver)
                auton.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes_step)
                auton.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets_step)
                auton.add_hyperparameter(name='blackbox', argument_type=ArgumentType.VALUE, data=self.primitives[estimator])
                auton.add_output('produce')
                pipeline_description.add_step(auton)
            else:
                # XGBOOST
                xgb = PrimitiveStep(primitive_description=self.primitives[estimator].metadata.query(), resolver=self.resolver)
                xgb.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes_step)
                xgb.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets_step)
                xgb.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
                xgb.add_output('produce')
                pipeline_description.add_step(xgb)
            current_step += 1
            predictions_step = 'steps.{i}.produce'.format(i=current_step)

            # Add step prediction into pipeline
            construct = PrimitiveStep(primitive_description=self.primitives['ConstructPredictions'].metadata.query(), resolver=self.resolver)
            construct.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=predictions_step)
            construct.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference=dataframe_step)
            construct.add_output('produce')
            pipeline_description.add_step(construct)
            current_step += 1

            # Get the last step for the output
            output = 'steps.{i}.produce'.format(i=current_step)

            # Adding output step to the pipeline
            pipeline_description.add_output(name='Predictions from the input dataset', data_reference=output)

            # adding previous steps if exists
            if self.pipeline_template is not None and pipeline_description is not None:
                pipeline_description = self._replace_placeholder_step(pipeline_description)

                # Update information.
                pipeline_description = self._set_pipline_description_info(pipeline=pipeline_description,
                                                                          pipeline_name=pipeline_description.id,
                                                                          **self.description)
            # self.pipeline_left = False
        except Exception as e:
            logger.info('{}'.format(e))
            pipeline_description = None

        return pipeline_description

    def get_pipelines(self, num_pipelines=1, time_left=-1):
        """
        A function that is called in order to get certain number of pipelines generated by this generator.

        Parameters
        ----------
        num_pipelines: int
            Desirable number of pipelines that the generator need to return, this parameter is an upper bound,
            the generator should return at most num_pipelines.
        time_left: float
            Updates the time left on the generator.

        Returns
        -------
        generated_pipelines: dict
            A dictionary described as: { 'pipeline_id': {'path': pipeline_path, 'outputs': True} that contains at most
            num_pipelines abd whether or not they want to know the output of the pipelines.

        """
        self.update_time_and_constrains(time_left, threshold=0.4)

        # if there is no pipeline left return empty dict
        if not self.pipeline_left:
            return {}

        pipelines = {}
        for i in range(0, num_pipelines):
            estimator = None
            for _estimator in self.primitive_candidates:
                if _estimator not in self.primitive_used:
                    self.primitive_used.add(_estimator)
                    estimator = _estimator
                    break

            if estimator is None:
                self.pipeline_left = False

            if not self.pipeline_left:
                break

            pipeline = self._get_pipeline(estimator=estimator)

            if pipeline is None:
                self.pipeline_left = False
                break

            pipeline_uri = file_utils.save_pipeline_to_json_file(pipeline, self.save_path)
            # pipelines[pipeline.id] = {'path': pipeline_uri, 'expose_outputs': ['steps.4.produce', 'steps.5.produce']}
            pipelines[pipeline.id] = {'path': pipeline_uri,}
            self.pipeline_generated.append(pipeline.id)

        return pipelines

    def update_pipelines(self, pipelines, time_left):
        """
        Updates the internal state of pipelines.

        Parameters
        ----------
        pipelines: dict
            A dictionary containing all information related with pipelines.
        time_left: float
            Updates the time left on the generator.
        """
        self.update_time_and_constrains(time_left, threshold=0.5)
        self._update_pipelines(pipelines)
        return
