import abc
import logging
import typing

from d3m import index
from d3m.metadata import base as metadata_base
from d3m.metadata.base import ArgumentType, Context
from d3m.metadata.pipeline import Pipeline, PrimitiveStep, Resolver
from d3m.metadata.problem import TaskKeyword

from modules.utils import pipeline_utils, custom_resolver

# Loading primitives
primitives = {
    'DatasetToDataFrame': 'd3m.primitives.data_transformation.dataset_to_dataframe.Common',
    'ImageReader': 'd3m.primitives.data_preprocessing.image_reader.Common',
    'ImageFeatureExtraction': 'd3m.primitives.feature_extraction.image_transfer.DistilImageTransfer',
    'ColumnParser': 'd3m.primitives.data_transformation.column_parser.Common',
    'ExtractColumnsBySemanticTypes': 'd3m.primitives.data_transformation.extract_columns_by_semantic_types.Common',
    'AudioReader': 'd3m.primitives.data_preprocessing.audio_reader.DistilAudioDatasetLoader',
    'AudioFeatureExtraction': 'd3m.primitives.feature_extraction.audio_transfer.DistilAudioTransfer',
    'NdArrayToDataFrame': 'd3m.primitives.data_transformation.ndarray_to_dataframe.Common',
    'Denormalize': 'd3m.primitives.data_transformation.denormalize.Common',
    'Imputer': 'd3m.primitives.data_cleaning.imputer.SKlearn',
    'TimeSeriesFormatter': 'd3m.primitives.data_preprocessing.data_cleaning.DistilTimeSeriesFormatter',
    'TimeSeriesFeaturization': 'd3m.primitives.feature_extraction.random_projection_timeseries_featurization.DSBOX',
    'FeatureSelection': 'd3m.primitives.feature_selection.variance_threshold.SKlearn',
    'TimeSeriesToList': 'd3m.primitives.data_preprocessing.time_series_to_list.DSBOX',
    'DataFrameToTensor': 'd3m.primitives.data_preprocessing.dataframe_to_tensor.DSBOX',
    'ResNet50Featurizer': 'd3m.primitives.feature_extraction.resnet50_image_feature.DSBOX',
    'SimpleProfiler': 'd3m.primitives.schema_discovery.profiler.Common',
    'GroupingCompose': 'd3m.primitives.data_transformation.grouping_field_compose.Common',
    'UnseenLabelEncoder': 'd3m.primitives.data_preprocessing.label_encoder.Common',
    'TextEncoder': 'd3m.primitives.data_transformation.encoder.DistilTextEncoder',
    'SingleGraphLoader': 'd3m.primitives.data_transformation.load_single_graph.DistilSingleGraphLoader',
    'TextReader': 'd3m.primitives.data_preprocessing.text_reader.Common',
    'Sent2Vec': 'd3m.primitives.feature_extraction.nk_sent2vec.Sent2Vec'

}
logger = logging.getLogger(__name__)

for primitive_name in primitives.keys():
    primitives[primitive_name] = index.get_primitive(primitives[primitive_name])


def get_preprocessor(task, treatment, data_types, semi, inputs_metadata, problem, main_resource, template_metadata=None,
                     **kwargs):
    preprocessor_candidates = []
    for p in preprocessors:
        # TODO refactor this part
        if (not p.task or (p.task == task and (p.treatment == treatment or p.treatment is None))) and \
                (not p.expected_data_types or (
                        p.expected_data_types and any([edt in p.expected_data_types for edt in data_types]))) and \
                (not p.unsupported_data_types or not any([edt in p.unsupported_data_types for edt in data_types])):
            preprocessor_candidates.append(p(inputs_metadata, main_resource, data_types, problem))
    if not preprocessor_candidates:
        preprocessor_candidates.append(TabularPreprocessor(inputs_metadata, main_resource, data_types))
    return preprocessor_candidates


class Preprocessor(abc.ABC):
    task: str
    treatment: str
    expected_data_types: set
    unsupported_data_types: set
    semi: bool

    def __init__(self, metadata, main_resource, data_types, problem=None, start_resource='inputs.0'):
        self.metadata = metadata
        self.main_resource = main_resource
        self.data_types = data_types
        self.start_resource = start_resource
        self.problem = problem
        self.pipeline, self.d2d_step, self.attr_step, self.targets_step = \
            self._generate_pipeline(metadata, main_resource, start_resource)

    def __init_subclass__(cls, task: str, treatment: str, expected_data_types: set, **kargs):
        cls.task = task
        cls.treatment = treatment
        cls.expected_data_types = expected_data_types
        cls.unsupported_data_types = kargs['unsupported_data_types'] if 'unsupported_data_types' in kargs else None
        cls.semi = kargs['semi'] if 'semi' in kargs else False

    @property
    def pipeline_description(self) -> Pipeline:
        return self.pipeline

    @property
    def dataset_to_dataframe_step(self) -> typing.Optional[str]:
        return self.d2d_step

    @property
    def attributes(self) -> typing.Optional[str]:
        return self.attr_step

    @property
    def targets(self) -> typing.Optional[str]:
        return self.targets_step

    @property
    def resolver(self) -> Resolver:
        return custom_resolver.BlackListResolver()

    @abc.abstractmethod
    def _generate_pipeline(self, metadata, main_resource, start_resource) -> \
            typing.Tuple[Pipeline, str, str, typing.Optional[str]]:
        raise NotImplementedError()

    @property
    def gpu_budget(self) -> float:
        return 0


class TimeSeriesTabularPreprocessor(Preprocessor, task=metadata_base.PrimitiveFamily.TIME_SERIES_CLASSIFICATION.name,
                                    treatment=metadata_base.PrimitiveFamily.CLASSIFICATION.name,
                                    expected_data_types=None,
                                    unsupported_data_types={TaskKeyword.TABULAR, TaskKeyword.RELATIONAL}):
    def _generate_pipeline(self, metadata, main_resource, start_resource) -> \
            typing.Tuple[Pipeline, str, str, str]:
        # denormalize -> dataset_to_df -> timeseries_to_list -> timeseries_featurization -> extract_col_by_semantic * 2
        pipeline_description, denorm_step, dataset_to_dataframe_step = common_boilerplate(
            metadata=metadata, main_resource_id=main_resource, start_resource=start_resource, resolver=self.resolver)
        timeseries_tolist_step = PrimitiveStep(primitive=primitives['TimeSeriesToList'],
                                               resolver=self.resolver)
        timeseries_tolist_step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                            data_reference=dataset_to_dataframe_step)
        timeseries_tolist_step.add_output('produce')
        pipeline_description.add_step(timeseries_tolist_step)
        timeseries_featurization_step = PrimitiveStep(primitive=primitives['TimeSeriesFeaturization'],
                                                      resolver=self.resolver)
        timeseries_featurization_step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                                   data_reference=pipeline_utils.int_to_step(
                                                       timeseries_tolist_step.index))
        timeseries_featurization_step.add_output('produce')
        pipeline_description.add_step(timeseries_featurization_step)
        add_extract_col_by_semantic_types_step(self.resolver,
                                               pipeline_utils.int_to_step(timeseries_featurization_step.index),
                                               pipeline_description,
                                               ['https://metadata.datadrivendiscovery.org/types/Attribute'])
        add_extract_col_by_semantic_types_step(self.resolver, dataset_to_dataframe_step,
                                               pipeline_description,
                                               ['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
        last_step = len(pipeline_description.steps) - 1
        attributes = pipeline_utils.int_to_step(last_step - 1)
        targets = pipeline_utils.int_to_step(last_step)

        return pipeline_description, dataset_to_dataframe_step, attributes, targets


class TimeSeriesPreprocessor(Preprocessor, task=metadata_base.PrimitiveFamily.TIME_SERIES_CLASSIFICATION.name,
                             treatment=metadata_base.PrimitiveFamily.TIME_SERIES_CLASSIFICATION.name,
                             expected_data_types=None,
                             unsupported_data_types={TaskKeyword.TABULAR, TaskKeyword.RELATIONAL}):
    def _generate_pipeline(self, metadata, main_resource, start_resource) -> \
            typing.Tuple[Pipeline, str, str, str]:
        pipeline_description = Pipeline(context=Context.TESTING)
        pipeline_description.add_input(name='inputs')
        ts_formatter = PrimitiveStep(primitive=primitives['TimeSeriesFormatter'], resolver=self.resolver)
        ts_formatter.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=start_resource)
        ts_formatter.add_output('produce')
        pipeline_description.add_step(ts_formatter)
        dtd_step = add_dataset_to_dataframe_step(self.resolver, pipeline_utils.int_to_step(ts_formatter.index),
                                                 pipeline_description)
        dtd_without_ts_format = add_dataset_to_dataframe_step(self.resolver, start_resource, pipeline_description)
        extract_target_step = \
            add_extract_col_by_semantic_types_step(self.resolver,
                                                   pipeline_utils.int_to_step(dtd_without_ts_format.index),
                                                   pipeline_description,
                                                   ['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
        target_column_parser_step = add_column_parser_step(self.resolver,
                                                           pipeline_utils.int_to_step(extract_target_step.index),
                                                           pipeline_description,
                                                           to_parse=[
                                                               "http://schema.org/Boolean",
                                                               "http://schema.org/Integer",
                                                               "http://schema.org/Float",
                                                               "https://metadata.datadrivendiscovery.org/types/FloatVector"
                                                           ])
        return pipeline_description, pipeline_utils.int_to_step(dtd_without_ts_format.index), \
               pipeline_utils.int_to_step(dtd_step.index), \
               pipeline_utils.int_to_step(target_column_parser_step.index)


class TimeSeriesForecastingTabularPreprocessor(Preprocessor,
                                               task=metadata_base.PrimitiveFamily.TIME_SERIES_FORECASTING.name,
                                               treatment=metadata_base.PrimitiveFamily.TIME_SERIES_FORECASTING.name,
                                               expected_data_types={TaskKeyword.GROUPED.name}):
    # TODO: Pipeline will fail for integer target because simple_profiler profiles it as Categorical data,
    #  not Float or Integer.

    def _generate_pipeline(self, metadata, main_resource, start_resource) -> \
            typing.Tuple[Pipeline, str, str, str]:
        pipeline_description, denorm_step, d2d_step = \
            common_boilerplate(resolver=self.resolver, start_resource=start_resource, metadata=metadata,
                               main_resource_id=main_resource)
        # Do not parse categorical data or GroupingCompose will fail.
        column_parser = add_column_parser_step(self.resolver, d2d_step,
                                               pipeline_description, [
                                                   "http://schema.org/DateTime",
                                                   "http://schema.org/Boolean",
                                                   "http://schema.org/Integer",
                                                   "http://schema.org/Float",
                                                   "https://metadata.datadrivendiscovery.org/types/FloatVector"
                                               ])
        column_parser_step = column_parser.get_output_data_references().pop()
        attribute_step = add_extract_col_by_semantic_types_step(self.resolver,column_parser_step, pipeline_description, [
            'https://metadata.datadrivendiscovery.org/types/Attribute'
        ])
        grouping = PrimitiveStep(primitive=primitives['GroupingCompose'], resolver=self.resolver)
        grouping.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                              data_reference=attribute_step.get_output_data_references().pop())
        grouping.add_output('produce')
        pipeline_description.add_step(grouping)
        attributes = grouping.get_output_data_references().pop()
        target_step = add_extract_col_by_semantic_types_step(self.resolver, column_parser_step, pipeline_description, [
            'https://metadata.datadrivendiscovery.org/types/TrueTarget'
        ])
        target = target_step.get_output_data_references().pop()
        return pipeline_description, d2d_step, attributes, target


class AudioPreprocessor(Preprocessor, task=metadata_base.PrimitiveFamily.DIGITAL_SIGNAL_PROCESSING.name,
                        treatment=None,
                        expected_data_types=None):

    def _generate_pipeline(self, metadata, main_resource, start_resource):
        pipeline_description = Pipeline(context=Context.TESTING)
        pipeline_description.add_input(name='inputs')
        audio_reader = PrimitiveStep(primitive=primitives['AudioReader'], resolver=self.resolver)
        audio_reader.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=start_resource)
        audio_reader.add_output('produce')
        audio_reader.add_output('produce_collection')
        pipeline_description.add_step(audio_reader)
        column_parser = PrimitiveStep(primitive=primitives['ColumnParser'], resolver=self.resolver)
        column_parser.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                   data_reference=pipeline_utils.int_to_step(audio_reader.index))
        column_parser.add_hyperparameter('parse_semantic_types', argument_type=ArgumentType.VALUE,
                                         data=["http://schema.org/Boolean",
                                               "http://schema.org/Integer",
                                               "http://schema.org/Float",
                                               "https://metadata.datadrivendiscovery.org/types/FloatVector"])

        column_parser.add_output('produce')
        pipeline_description.add_step(column_parser)
        audio_feature = PrimitiveStep(primitive=primitives['AudioFeatureExtraction'], resolver=self.resolver)
        audio_feature.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                   data_reference='steps.{}.produce_collection'.format(audio_reader.index))
        audio_feature.add_output('produce')
        pipeline_description.add_step(audio_feature)
        add_extract_col_by_semantic_types_step(self.resolver, pipeline_utils.int_to_step(column_parser.index),
                                               pipeline_description,
                                               ['https://metadata.datadrivendiscovery.org/types/TrueTarget'])

        attributes = pipeline_utils.int_to_step(audio_feature.index)
        targets = pipeline_utils.int_to_step(len(pipeline_description.steps) - 1)

        return pipeline_description, audio_reader.get_output_data_references().pop(), attributes, targets


class ImageDataFramePreprocessor(Preprocessor, task=metadata_base.PrimitiveFamily.DIGITAL_IMAGE_PROCESSING.name,
                                 treatment=None,
                                 expected_data_types={TaskKeyword.IMAGE.name}):
    def _generate_pipeline(self, metadata, main_resource, start_resource):
        pipeline_description, denorm_step, dataset_to_dataframe_step = \
            common_boilerplate(self.resolver, start_resource=start_resource, metadata=metadata,
                               main_resource_id=main_resource)
        image_reader = PrimitiveStep(primitive=primitives['ImageReader'], resolver=self.resolver)
        image_reader.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                  data_reference=dataset_to_dataframe_step)
        image_reader.add_hyperparameter('return_result', ArgumentType.VALUE, 'replace')
        image_reader.add_output('produce')
        pipeline_description.add_step(image_reader)
        column_parser = PrimitiveStep(primitive=primitives['ColumnParser'], resolver=self.resolver)
        column_parser.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                   data_reference=pipeline_utils.int_to_step(image_reader.index))
        column_parser.add_hyperparameter(name='parse_semantic_types', argument_type=ArgumentType.VALUE,
                                         data=['http://schema.org/Boolean', 'http://schema.org/Integer',
                                               'http://schema.org/Float',
                                               'https://metadata.datadrivendiscovery.org/types/FloatVector'])
        column_parser.add_output('produce')
        pipeline_description.add_step(column_parser)
        image_feature_extraction = PrimitiveStep(primitive=primitives['ImageFeatureExtraction'], resolver=self.resolver)
        image_feature_extraction.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                              data_reference=pipeline_utils.int_to_step(column_parser.index))
        image_feature_extraction.add_output('produce')
        pipeline_description.add_step(image_feature_extraction)
        add_extract_col_by_semantic_types_step(self.resolver, dataset_to_dataframe_step, pipeline_description,
                                               ['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
        last_step = len(pipeline_description.steps) - 1
        attributes = pipeline_utils.int_to_step(image_feature_extraction.index)
        targets = pipeline_utils.int_to_step(last_step)
        return pipeline_description, dataset_to_dataframe_step, attributes, targets


class ImageTensorPreprocessor(Preprocessor, task=metadata_base.PrimitiveFamily.DIGITAL_IMAGE_PROCESSING.name,
                              treatment=None,
                              expected_data_types={TaskKeyword.IMAGE.name}):
    def _generate_pipeline(self, metadata, main_resource, start_resource):
        pipeline_description, denorm_step, dataset_to_dataframe_step = \
            common_boilerplate(start_resource, metadata=metadata, main_resource_id=main_resource)
        dataframe_to_tensor = PrimitiveStep(primitive=primitives['DataFrameToTensor'], resolver=self.resolver)
        dataframe_to_tensor.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                         data_reference=pipeline_utils.int_to_step(len(pipeline_description.steps) - 1))
        dataframe_to_tensor.add_output('produce')
        pipeline_description.add_step(dataframe_to_tensor)
        resnet50_featurizer = PrimitiveStep(primitive=primitives['ResNet50Featurizer'], resolver=self.resolver)
        resnet50_featurizer.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                         data_reference=pipeline_utils.int_to_step(dataframe_to_tensor.index))
        resnet50_featurizer.add_output('produce')
        pipeline_description.add_step(resnet50_featurizer)
        add_extract_col_by_semantic_types_step(self.resolver, dataset_to_dataframe_step, pipeline_description,
                                               ['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
        last_step = len(pipeline_description.steps) - 1
        attributes = pipeline_utils.int_to_step(resnet50_featurizer.index)
        targets = pipeline_utils.int_to_step(last_step)
        return pipeline_description, dataset_to_dataframe_step, attributes, targets


class TabularPreprocessor(Preprocessor, task=None, treatment=None, expected_data_types={TaskKeyword.TABULAR.name}):
    def _generate_pipeline(self, metadata, main_resource, start_resource):
        return tabular_common(metadata, main_resource, self.resolver, start_resource)


class CollaborativeFilteringPreprocessor(Preprocessor, task=metadata_base.PrimitiveFamily.COLLABORATIVE_FILTERING.name,
                                         treatment=None,
                                         expected_data_types=None):
    def _generate_pipeline(self, metadata, main_resource, start_resource):
        return tabular_common(metadata=metadata, main_resource=main_resource, start_resource=start_resource,
                              resolver=self.resolver, target_at_column_parser=True)


class TextPreprocessor(Preprocessor, task=None, treatment=None,
                       expected_data_types={TaskKeyword.TEXT}):
    def _generate_pipeline(self, metadata, main_resource, start_resource):
        pipeline_description, denorm_step, dataset_to_dataframe_step = common_boilerplate(
            self.resolver, start_resource=start_resource, metadata=metadata, main_resource_id=main_resource)

        # Simple preprocessor
        pipeline_description, attributes, targets = base(
            pipeline_description=pipeline_description, start_resource=dataset_to_dataframe_step, resolver=self.resolver)

        text_reader_step = PrimitiveStep(primitive=primitives['TextReader'])
        text_reader_step.add_argument('inputs', ArgumentType.CONTAINER, attributes)
        text_reader_step.add_output('produce')
        text_reader_step.add_hyperparameter('return_result', ArgumentType.VALUE, 'replace')
        pipeline_description.add_step(text_reader_step)

        pipeline_description, attributes = imputer(pipeline_description,
                                                   text_reader_step.get_output_data_references().pop(), self.resolver)
        pipeline_description, attributes = simple_text_handler(pipeline_description, attributes, targets, self.resolver)
        return pipeline_description, dataset_to_dataframe_step, attributes, targets


class TextSent2VecPreprocessor(Preprocessor, task=None, treatment=None, expected_data_types={TaskKeyword.TEXT.name}):
    def _generate_pipeline(self, metadata, main_resource, start_resource):
        pipeline_description, denorm_step, dataset_to_dataframe_step = common_boilerplate(
            self.resolver, start_resource=start_resource, metadata=metadata, main_resource_id=main_resource)

        # Simple preprocessor
        pipeline_description, attributes, targets = base(
            pipeline_description=pipeline_description, start_resource=dataset_to_dataframe_step, resolver=self.resolver)

        sent2vec = PrimitiveStep(primitive=primitives['Sent2Vec'])
        sent2vec.add_argument('inputs', ArgumentType.CONTAINER, attributes)
        sent2vec.add_output('produce')
        pipeline_description.add_step(sent2vec)

        pipeline_description, attributes = imputer(pipeline_description,
                                                   sent2vec.get_output_data_references().pop(), self.resolver)
        return pipeline_description, dataset_to_dataframe_step, attributes, targets


class LupiPreprocessor(Preprocessor, task=None, treatment=None,
                       expected_data_types={TaskKeyword.LUPI.name}):
    def _generate_pipeline(self, metadata, main_resource, start_resource) -> \
            typing.Tuple[Pipeline, str, str, typing.Optional[str]]:
        pipeline_description, denorm_step, dataset_to_dataframe_step = common_boilerplate(
            self.resolver, start_resource=start_resource, metadata=metadata, main_resource_id=main_resource)

        privileged_column_indices = [info['column_index'] for info in self.problem['inputs'][0]['privileged_data']]
        pipeline_description, attributes, targets = base(
            pipeline_description=pipeline_description, start_resource=dataset_to_dataframe_step, resolver=self.resolver,
            exclude_attr_columns=privileged_column_indices)
        pipeline_description, attributes = imputer(pipeline_description, attributes, self.resolver)

        return pipeline_description, dataset_to_dataframe_step, attributes, targets


# class VertexClassification(Preprocessor, task=metadata_base.PrimitiveFamily.VERTEX_CLASSIFICATION.name,
#                            treatment=metadata_base.PrimitiveFamily.VERTEX_CLASSIFICATION.name):
#     def _generate_pipeline(self, metadata, main_resource, start_resource):
#         graph_loader = PrimitiveStep(primitive=primitives['SingleGraphLoader'], resolver=self.resolver)
#         graph_loader.add_argument('inputs', ArgumentType.CONTAINER, self.start_resource)
#         graph_loader.add_output('produce')
#         graph_loader.add_output('produce_target')


def tabular_common(metadata, main_resource, resolver, start_resource='inputs.0',
                   target_at_column_parser=False):
    pipeline_description, denorm_step, dataset_to_dataframe_step = common_boilerplate(
        resolver, start_resource=start_resource, metadata=metadata, main_resource_id=main_resource)

    # Simple preprocessor
    pipeline_description, attributes, targets = base(
        pipeline_description=pipeline_description, start_resource=dataset_to_dataframe_step,
        target_at_column_parser=target_at_column_parser, resolver=resolver)
    metadata
    # Adding Imputer
    pipeline_description, attributes = imputer(pipeline_description=pipeline_description, attributes=attributes,
                                               resolver=resolver)
    pipeline_description, attributes = simple_text_handler(pipeline_description, attributes, targets, resolver)
    return pipeline_description, dataset_to_dataframe_step, attributes, targets


def common_boilerplate(resolver, start_resource='inputs.0', metadata=None, main_resource_id=None,
                       include_dataset_to_dataframe=True, include_simple_profiler=True):
    """
    This boilerplate provides the basic init pipline that contains denormalize and dataset_to_dataframe.

    Arguments
    ---------
    metadata: metadata_base.DataMetadata
        A dict containing the metadata input
    main_resource_id: str
        A name of the main resource of the inputs
    resolver: Pipeline.Resolver
        A resolver that contains the black listed primitives.
    include_dataset_to_dataframe: bool
        Whether to include dataset_to_dataframe step.
    start_resource: str
        start resource sting
    include_simple_profiler:  bool
        whether or not to include simple profiler
    """
    dtd_step = None
    denorm_step = None

    # Creating pipeline
    pipeline_description = Pipeline(context=Context.TESTING)
    pipeline_description.add_input(name='inputs')

    # if metadata provided we try to update the semantic types if we don't know them.
    # if metadata is not None:
    # pipeline_description, start_resource, current_step, resolver = update_semantic_types(
    #     pipeline_description=pipeline_description,
    #     start_resource=start_resource, current_step=current_step, resolver=resolver,
    #     metadata=metadata, resource_id=main_resource_id)

    # if there is more that one resource we denormalize
    if len(metadata.get_elements(())) > 1:
        denorm_step = add_denormalize_step(resolver, start_resource, main_resource_id, pipeline_description)
        denorm_step = denorm_step.get_output_data_references().pop()
        start_resource = denorm_step

    # Finally we transfer to a dataframe.
    if include_dataset_to_dataframe:
        dtd_step = add_dataset_to_dataframe_step(resolver, start_resource, pipeline_description)
        dtd_step = dtd_step.get_output_data_references().pop()
        start_resource = dtd_step

    if include_simple_profiler:
        simple_profiler_step = PrimitiveStep(primitive=primitives['SimpleProfiler'], resolver=resolver)
        simple_profiler_step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                          data_reference=start_resource)
        simple_profiler_step.add_output('produce')
        simple_profiler_step.add_hyperparameter('categorical_max_ratio_distinct_values', ArgumentType.VALUE, 1)
        simple_profiler_step.add_hyperparameter('categorical_max_absolute_distinct_values', ArgumentType.VALUE, None)
        pipeline_description.add_step(simple_profiler_step)
        dtd_step = simple_profiler_step.get_output_data_references().pop()

    return pipeline_description, denorm_step, dtd_step


# Denormalize -> DatasetToDataFrame -> ColumnParser -> ExtractAttributes*
#                                                   -> ExtractTargets*
def base(pipeline_description, start_resource, resolver, target_at_column_parser=False, exclude_attr_columns=None):
    dataset_dataframe_step_pos = start_resource

    # Step 2: ColumnParser
    column_parser_step = PrimitiveStep(primitive=primitives['ColumnParser'], resolver=resolver)
    column_parser_step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                    data_reference=dataset_dataframe_step_pos)
    column_parser_step.add_output('produce')
    pipeline_description.add_step(column_parser_step)

    colum_parser_resource = pipeline_utils.int_to_step(column_parser_step.index)
    # Step 3: ExtractAttributes
    add_extract_col_by_semantic_types_step(resolver, colum_parser_resource, pipeline_description,
                                           ['https://metadata.datadrivendiscovery.org/types/Attribute'],
                                           exclude_attr_columns)
    target_source_step = colum_parser_resource if target_at_column_parser else dataset_dataframe_step_pos
    # Step 4: ExtractTargets
    add_extract_col_by_semantic_types_step(resolver, target_source_step, pipeline_description,
                                           ['https://metadata.datadrivendiscovery.org/types/TrueTarget'])

    last_step = len(pipeline_description.steps) - 1
    attributes = pipeline_utils.int_to_step(last_step - 1)
    targets = pipeline_utils.int_to_step(last_step)

    return pipeline_description, attributes, targets


# Attributes -> Imputer*
def imputer(pipeline_description: Pipeline, attributes: str, resolver):
    primitive = primitives['Imputer']
    # Current step
    current_step = len(pipeline_description.steps)

    # SklearnImputer
    step_0 = PrimitiveStep(primitive=primitive, resolver=resolver)
    step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes)

    if 'return_result' in primitive.metadata.query()['primitive_code']['class_type_arguments'][
        'Hyperparams'].configuration:
        step_0.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
    if 'use_semantic_types' in primitive.metadata.query()['primitive_code']['class_type_arguments'][
        'Hyperparams'].configuration:
        step_0.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE, data=True)
    step_0.add_hyperparameter(name='error_on_no_input', argument_type=ArgumentType.VALUE, data=False)
    step_0.add_output('produce')
    pipeline_description.add_step(step_0)

    attributes = pipeline_utils.int_to_step(current_step)

    return pipeline_description, attributes


def add_extract_col_by_semantic_types_step(resolver, data_reference, pipeline_description,
                                           target_semantic_types, exclude_columns=None):
    step = PrimitiveStep(primitive=primitives['ExtractColumnsBySemanticTypes'],
                         resolver=resolver)
    step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=data_reference)
    step.add_output('produce')
    if exclude_columns:
        step.add_hyperparameter(name='exclude_columns', argument_type=ArgumentType.VALUE, data=exclude_columns)
    step.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
                            data=target_semantic_types)
    pipeline_description.add_step(step)
    return step


def add_denormalize_step(resolver, start_resource, data, pipeline_description):
    denormalize_step = PrimitiveStep(primitive=primitives['Denormalize'],
                                     resolver=resolver)
    denormalize_step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                  data_reference=start_resource)
    denormalize_step.add_hyperparameter(name='starting_resource', argument_type=ArgumentType.VALUE,
                                        data=data)
    denormalize_step.add_output('produce')
    pipeline_description.add_step(denormalize_step)
    return denormalize_step


def add_dataset_to_dataframe_step(resolver, start_resource, pipeline_description):
    step_dtd = PrimitiveStep(primitive=primitives['DatasetToDataFrame'],
                             resolver=resolver)
    step_dtd.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=start_resource)
    step_dtd.add_output('produce')
    pipeline_description.add_step(step_dtd)
    return step_dtd


def add_column_parser_step(resolver, data_reference, pipeline_description, to_parse=None):
    column_parser = PrimitiveStep(primitive=primitives['ColumnParser'], resolver=resolver)
    column_parser.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                               data_reference=data_reference)
    if to_parse:
        column_parser.add_hyperparameter('parse_semantic_types', argument_type=ArgumentType.VALUE,
                                         data=to_parse)

    column_parser.add_output('produce')
    pipeline_description.add_step(column_parser)
    return column_parser


def simple_text_handler(pipeline_description, attributes, targets, resolver):
    text_encoder = PrimitiveStep(primitive=primitives['TextEncoder'], resolver=resolver)
    text_encoder.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                              data_reference=attributes)
    text_encoder.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets)
    text_encoder.add_hyperparameter(name='encoder_type', argument_type=ArgumentType.VALUE, data='tfidf')
    text_encoder.add_output('produce')
    pipeline_description.add_step(text_encoder)
    return pipeline_description, text_encoder.get_output_data_references().pop()


preprocessors = [
    TimeSeriesTabularPreprocessor, TimeSeriesPreprocessor, AudioPreprocessor, ImageDataFramePreprocessor,
    CollaborativeFilteringPreprocessor, ImageTensorPreprocessor, TextSent2VecPreprocessor, TextPreprocessor,
    TimeSeriesForecastingTabularPreprocessor, LupiPreprocessor
]
