import argparse
import json
import logging
import os
import pathlib
import time
# Other imports
import warnings
from concurrent import futures

# GRPC imports
# default grpc port 45042
import google.protobuf.timestamp_pb2 as p_timestamp
import grpc
from d3m import utils as d3m_utils, index as d3m_index
from d3m.metadata import problem as problem_module
from ta3ta2_api import core_pb2, core_pb2_grpc, primitive_pb2, value_pb2, utils

from modules.manager.execution_manager import ExecutionManger
from modules.utils import constants, file_utils
from modules.utils.constants import Path, EnvVars, LISTS
from modules.utils.custom_resolver import BlackListResolver
from modules.utils.pipeline_utils import load_pipeline

# Parallel stuff

# TODO remove DEBUG

__version__ = '2019.4.4_pre'
_ONE_DAY_IN_SECONDS = 60 * 60 * 24

logger = logging.getLogger(__name__)
MAX_NUM_PIPLINES_SESSION = 30
AGENT = 'TAMU-UW.6.3_pre'
ALLOWED_VALUE_TYPES = ['RAW', 'DATASET_URI', 'CSV_URI']
SUPPORTED_EXTENSIONS = []


class Core(core_pb2_grpc.CoreServicer):
    """
    A class that works as a server that provides support for the pipeline searches, and provides the interfaces
    defined on the TA3-2 API.

    Attributes
    ----------
    version: str
        A str that represents the version of the Ta3-2 api that is supporting.
    user_agents: dict()
        A simple dictionary that keep the relation of the different users.
    manager: ExecutionManger
        Schedules the searches, and all resources related with the search.
    """

    def __init__(self, process_type):
        logger.info('########## Initializing Service ##########')
        self.version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]
        self.user_agents = {}
        self.manager = ExecutionManger(n_processes=EnvVars.D3MCPU, process_type=process_type)

        self.manager.start_workers()

    def SearchSolutions(self, request, context):
        user_agent = request.user_agent

        # LOGGING
        logger.info('method=SearchSolution, agent=%s', user_agent)

        # Adding information of the user_agent
        if user_agent not in self.user_agents:
            self.user_agents[user_agent] = {}

        # Checking version of protocol.
        if request.version != self.version:
            logger.info(' method=SearchSolution, info=Different api version%s', self.version)

        # Priority of request (Not use for now)
        priority = request.priority

        # Types allowed by client
        # allowed_value_types = self._get_value_types(request.allowed_value_types)
        allowed_value_types = list(request.allowed_value_types)

        # TODO take a look into ranking solution when a pipeline limit is set
        # Max num of ranked solutions
        rank_solutions_limit = request.rank_solutions_limit
        # print("#########$$$$$$$$*******", rank_solutions_limit)

        # TODO update this when more types supported
        if not allowed_value_types:
            allowed_value_types = ALLOWED_VALUE_TYPES

        problem = utils.decode_problem_description(request.problem)

        # Parsing and storing Pipeline Template (store this to a file instead of passing it)
        with d3m_utils.silence():
            template = utils.decode_pipeline_description(pipeline_description=request.template,
                                                         resolver=BlackListResolver())

        time_bound_search = request.time_bound_search
        time_bound_search = time_bound_search * 60

        inputs = [[utils.decode_value(x)] for x in request.inputs ]

        search_id = self.manager.add_search(time_bound_search=time_bound_search, priority=priority,
                                            allowed_value_types=allowed_value_types,
                                            problem=problem, pipeline_template=template, inputs=inputs)
        response = core_pb2.SearchSolutionsResponse(search_id=search_id)
        return response

    # TODO add the case when there is no scores.
    def GetSearchSolutionsResults(self, request, context):
        search_id = request.search_id
        # LOGGING
        logger.info('method=GetSearchSolutionsResults, search_id=%s', search_id)

        # We will wait up to 10 segs to create a search.
        max_wait = 10
        while search_id not in self.manager.searches_status:
            time.sleep(1)
            max_wait -= 1
            if max_wait == 0:
                progress_state = 'ERRORED'
                progress_status = 'No search: ' + search_id
                progress = core_pb2.Progress(state=progress_state, status=progress_status)
                response = core_pb2.GetSearchSolutionsResultsResponse(progress=progress)
                logger.info(
                    'method=GetSearchSolutionsResults, search_id=%s, status=ERRORED, info=No search found',
                    search_id)
                yield response
                return

        solutions_id = set()

        progress_start = p_timestamp.Timestamp()
        progress_end = p_timestamp.Timestamp()

        # progress _variables
        progress_state = None
        progress_status = None
        progress_start.GetCurrentTime()

        # response variables
        done_ticks = 0
        all_ticks = 0
        current_solution_id = None
        previous_solution_id = None
        internal_score = 0
        scores = None

        if self.manager.search_scores_info[search_id] is not None:
            targets = self.manager.search_scores_info[search_id][-1]['targets']
            dataset_id = self.manager.search_scores_info[search_id][-1]['dataset_id']
        else:
            targets = None
            dataset_id = None

        scoring_configuration = self.manager.searches_details[search_id]['eval_configuration']
        scoring_configuration = self._encode_scoring_configuration(scoring_configuration)

        if self.manager.searches_status[search_id]['status'] == 'PENDING':
            if progress_state != 'PENDING':
                progress_state = 'PENDING'
                progress = core_pb2.Progress(state=progress_state)
                response = core_pb2.GetSearchSolutionsResultsResponse(
                    progress=progress,
                    done_ticks=done_ticks,
                    all_ticks=all_ticks,
                )
                yield response
            else:
                while self.manager.searches_status[search_id]['status'] == 'PENDING':
                    time.sleep(0.2)

        progress_state = self.manager.searches_status[search_id]['status']
        progress = core_pb2.Progress(state=progress_state)
        response = core_pb2.GetSearchSolutionsResultsResponse(
            progress=progress,
            done_ticks=done_ticks,
            all_ticks=all_ticks,
        )
        yield response

        while True:
            # check if there are any new pipelines
            if len(solutions_id) != len(self.manager.searches[search_id]):
                for solution in self.manager.searches[search_id]:
                    if solution not in solutions_id:
                        solutions_id.add(solution)
                        current_solution_id = solution
                        # updating ticks
                        done_ticks += 1
                        break
            else:
                previous_solution_id = current_solution_id

            if self.manager.searches_status[search_id]['status'] == 'COMPLETED':
                if progress_state != 'COMPLETED':
                    progress_state = 'COMPLETED'
                    progress_status = 'Search Completed'
                    logger.info('method=GetSearchSolutionsResults, search_id=%s, status=COMPLETED',
                                 search_id)
                    progress_end.GetCurrentTime()

            elif self.manager.searches_status[search_id]['status'] == 'ERRORED':
                if progress_state != 'ERRORED':
                    progress_state = 'ERRORED'
                    progress_status = self.manager.searches_status[search_id]['info']
                    logger.info('method=GetSearchSolutionsResults, search_id=%s, status=ERRORED, info=%s',
                                 search_id, progress_status)
                    progress_end.GetCurrentTime()

            elif self.manager.searches_status[search_id]['status'] == 'RUNNING':
                if progress_state != 'RUNNING':
                    progress_state = 'RUNNING'

            if current_solution_id is None:
                if progress_state == 'ERRORED':
                    # setting the progress
                    progress = core_pb2.Progress(
                        state=progress_state,
                        status=progress_status,
                        start=progress_start,
                        end=progress_end)
                    response = core_pb2.GetSearchSolutionsResultsResponse(
                        progress=progress,
                        done_ticks=done_ticks,
                        all_ticks=all_ticks,
                    )
                    yield response
                    break
                else:
                    time.sleep(0.2)
                    continue
            else:
                if previous_solution_id == current_solution_id:
                    if progress_state == 'RUNNING':
                        continue

                pipeline_id = self.manager.solutions[current_solution_id]['pipeline']
                if 'internal_score' in self.manager.pipelines[pipeline_id]:
                    internal_score = self.manager.pipelines[pipeline_id]['internal_score']
                else:
                    internal_score = None

                if 'scores' in self.manager.pipelines[pipeline_id]:
                    scores = self.encode_scores(self.manager.pipelines[pipeline_id]['scores']['all'])
                    scores = [core_pb2.SolutionSearchScore(scoring_configuration=scoring_configuration, scores=scores)]
                else:
                    scores = None

                # setting the progress
                if progress_state == 'COMPLETED' or progress_state == 'ERRORED':
                    progress = core_pb2.Progress(
                        state=progress_state,
                        status=progress_status,
                        start=progress_start,
                        end=progress_end)
                else:
                    progress = core_pb2.Progress(
                        state=progress_state,
                        status=progress_status,
                        start=progress_start)

                response = core_pb2.GetSearchSolutionsResultsResponse(
                    progress=progress,
                    done_ticks=done_ticks,
                    all_ticks=all_ticks,
                    solution_id=current_solution_id,
                    internal_score=internal_score,
                    scores=scores)
                yield response

                if progress_state == 'COMPLETED' or progress_state == 'ERRORED':
                    break

    # # TODO implement for parallel version
    # INFO This is a Dummy method
    def EndSearchSolutions(self, request, context):
        search_id = request.search_id
        logger.info('method=EndSearchSolutions search_id=%s', search_id)

        respone = core_pb2.EndSearchSolutionsResponse()
        return respone

    def StopSearchSolutions(self, request, context):
        search_id = request.search_id
        self.manager.stop_search(search_id)
        logger.info('method=StopSearchSolutions search_id=%s', search_id)

        respone = core_pb2.StopSearchSolutionsResponse()
        return respone

    def DescribeSolution(self, request, context):
        solution_id = request.solution_id
        logger.info('method=DescribeSolution, solution_id=%s', solution_id)

        if solution_id not in self.manager.solutions:
            logger.info('method=DescribeSolution, solution_id=%s, error=Solution_id not found', solution_id)
            response = core_pb2.DescribeSolutionResponse()
            return response

        pipeline_id = self.manager.solutions[solution_id]['pipeline']
        solution_uri = self.manager.pipelines[pipeline_id]['path']

        with d3m_utils.silence():
            pipeline = utils.encode_pipeline_description(load_pipeline(solution_uri), ALLOWED_VALUE_TYPES,
                                                         constants.Path.TEMP_STORAGE_ROOT)
        response = core_pb2.DescribeSolutionResponse(pipeline=pipeline)

        return response

    def ScoreSolution(self, request, context):
        solution_id = request.solution_id

        # LOGGING
        logger.info('method=SocreSolution, solution_id=%s', solution_id)

        # check if we have the solution
        if solution_id not in self.manager.solutions:
            logger.info('method=Scoreolution, solution_id=%s, status=ERRORED, info=No solution found',
                         solution_id)
            response = core_pb2.ScoreSolutionResponse()
            return response

        inputs = [ [utils.decode_value(x)] for x in request.inputs ]

        performance_metrics = [utils.decode_performance_metric(metric) for metric in request.performance_metrics]

        users = self._get_users(request)

        configuration = self._decode_scoring_configuration(request.configuration)

        request_id = self.manager.add_score_job(
            solution_id=solution_id,
            inputs=inputs,
            users=users,
            performance_metrics=performance_metrics,
            configuration=configuration,
            save_path=Path.TEMP_STORAGE_ROOT)
        response = core_pb2.ScoreSolutionResponse(request_id=request_id)
        return response

    def GetScoreSolutionResults(self, request, context):
        request_id = request.request_id

        # LOGGING
        logger.info('method=GetScoreSolutionResults, request_id=%s', request_id)

        # We will wait up to 10 segs to create a search.
        max_wait = 10
        while request_id not in self.manager.request_status:
            time.sleep(1)
            max_wait -= 1
            if max_wait == 0:
                progress_state = 'ERRORED'
                progress_status = 'No request: ' + request_id
                progress = core_pb2.Progress(state=progress_state, status=progress_status)
                logger.info('method=GetScoreSolutionResults, request_id=%s, status=ERRORED, info=%s',
                             request_id, progress_status)
                response = core_pb2.GetScoreSolutionResultsResponse(progress=progress)
                yield response
                return

        progress_start = p_timestamp.Timestamp()
        progress_end = p_timestamp.Timestamp()
        progress_start.GetCurrentTime()

        if self.manager.request_status[request_id]['status'] == 'PENDING':
            progress = core_pb2.Progress(state='PENDING', status='Waiting for score job to be scheduled')
            response = core_pb2.GetScoreSolutionResultsResponse(progress=progress)
            yield response
            while self.manager.request_status[request_id]['status'] == 'PENDING':
                time.sleep(0.2)

        if self.manager.request_status[request_id]['status'] == 'RUNNING':
            progress = core_pb2.Progress(state='RUNNING', status='Running score job', start=progress_start)
            response = core_pb2.GetScoreSolutionResultsResponse(progress=progress)
            yield response
            while self.manager.request_status[request_id]['status'] == 'RUNNING':
                time.sleep(0.2)

        if self.manager.request_status[request_id]['status'] == 'ERRORED':
            progress_end.GetCurrentTime()
            progress_status = self.manager.request_status[request_id]['status']
            progress = core_pb2.Progress(
                state='ERRORED',
                status=progress_status,
                start=progress_start,
                end=progress_end
            )
            logger.info('method=GetScoreSolutionResults, request_id=%s, status=ERRORED, info=%s',
                         request_id, progress_status)
            response = core_pb2.GetScoreSolutionResultsResponse(progress=progress)
            yield response
        elif self.manager.request_status[request_id]['status'] == 'COMPLETED':
            progress_end.GetCurrentTime()
            progress = core_pb2.Progress(
                state='COMPLETED',
                status='Fit job COMPLETED',
                start=progress_start,
                end=progress_end
            )
            # Getting targets and dataset_id
            search_scores_info = self.manager._get_search_scores_info(
                self.manager.request_status[request_id]['problem'])
            targets = search_scores_info[-1]['targets']
            dataset_id = search_scores_info[-1]['dataset_id']

            # Scores stuff
            scores = self.encode_scores(self.manager.score_solutions[request_id])

            logger.info('method=GetScoreSolutionResults, request_id=%s, status=COMPLETED', request_id)
            response = core_pb2.GetScoreSolutionResultsResponse(progress=progress, scores=scores)
            yield response

    def FitSolution(self, request, context):
        solution_id = request.solution_id

        # LOGGING
        logger.info('method=FitSolution solution_id=%s', solution_id)

        if solution_id not in self.manager.solutions:
            response = core_pb2.FitSolutionResponse()
            logger.info('method=FitSolution solution_id=%s, status=ERRORED, info=No solution found',
                         solution_id)
            return response

        inputs = [[utils.decode_value(x)] for x in request.inputs ]

        expose_outputs = [expose_output for expose_output in request.expose_outputs]
        # expose_value_types = self._get_value_types(request.expose_value_types)
        expose_value_types = list(request.expose_value_types)

        # TODO update this when more types supported
        if not expose_value_types:
            expose_value_types = ALLOWED_VALUE_TYPES

        users = self._get_users(request)

        request_id = self.manager.add_fit_job(
            solution_id=solution_id,
            inputs=inputs,
            expose_outputs=expose_outputs,
            expose_value_types=expose_value_types,
            users=users,
            save_path=Path.TEMP_STORAGE_ROOT)

        response = core_pb2.FitSolutionResponse(request_id=request_id)
        return response

    def GetFitSolutionResults(self, request, context):
        request_id = request.request_id

        # LOGGING
        logger.info('method=GetFitSolutionResults request_id=%s', request_id)

        # We will wait up to 30 segs to create a search.
        max_wait = 10
        while request_id not in self.manager.request_status:
            time.sleep(1)
            max_wait -= 1
            if max_wait == 0:
                progress_state = 'ERRORED'
                progress_status = 'No request: ' + request_id
                progress = core_pb2.Progress(state=progress_state, status=progress_status)
                response = core_pb2.GetFitSolutionResultsResponse(progress=progress)
                logger.info('method=GetFitSolutionResults request_id=%s, status=ERRORED, info=%s',
                             request_id, progress_status)
                yield response
                return

        progress_start = p_timestamp.Timestamp()
        progress_end = p_timestamp.Timestamp()
        progress_start.GetCurrentTime()

        if self.manager.request_status[request_id]['status'] == 'PENDING':
            progress = core_pb2.Progress(state='PENDING', status='Waiting for fit job to be scheduled')
            response = core_pb2.GetFitSolutionResultsResponse(progress=progress)
            yield response
            while self.manager.request_status[request_id]['status'] == 'PENDING':
                time.sleep(0.2)

        if self.manager.request_status[request_id]['status'] == 'RUNNING':
            progress = core_pb2.Progress(state='RUNNING', status='Running fit job', start=progress_start)
            response = core_pb2.GetFitSolutionResultsResponse(progress=progress)
            yield response
            while self.manager.request_status[request_id]['status'] == 'RUNNING':
                time.sleep(0.2)

        if self.manager.request_status[request_id]['status'] == 'ERRORED':
            progress_end.GetCurrentTime()
            progress = core_pb2.Progress(
                state='ERRORED',
                status=self.manager.request_status[request_id]['status'],
                start=progress_start,
                end=progress_end
            )
            response = core_pb2.GetFitSolutionResultsResponse(progress=progress)
            logger.info('method=GetFitSolutionResults request_id=%s, status=ERRORED, info=%s',
                         request_id, progress)
            yield response
        elif self.manager.request_status[request_id]['status'] == 'COMPLETED':
            progress_end.GetCurrentTime()
            progress = core_pb2.Progress(
                state='COMPLETED',
                status='Fit job COMPLETED',
                start=progress_start,
                end=progress_end
            )

            # TODO add here:
            step_progress = []

            exposed_outputs = dict()
            if request_id in self.manager.request_values:
                for name, value in self.manager.request_values[request_id].items():
                    exposed_outputs[name] = utils.encode_value(value, ALLOWED_VALUE_TYPES, Path.TEMP_STORAGE_ROOT)
            fitted_solution_id = self.manager.request_status[request_id]['fitted_solution_id']
            response = core_pb2.GetFitSolutionResultsResponse(progress=progress, steps=step_progress,
                                                              exposed_outputs=exposed_outputs,
                                                              fitted_solution_id=fitted_solution_id)
            logger.info('method=GetFitSolutionResults request_id=%s, status=COMPLETED', request_id)
            yield response

    def ProduceSolution(self, request, context):
        fitted_solution_id = request.fitted_solution_id

        # LOGGING
        logger.info('method=ProduceSolution, fitted_solution_id=%s', fitted_solution_id)

        if fitted_solution_id not in self.manager.fitted_solutions:
            logger.info(
                'method=ProduceSolution, fitted_solution_id=%s, status=ERRORED info=No fitted_solution_id found',
                fitted_solution_id)
            response = core_pb2.ProduceSolutionResponse()
            return response

        inputs = [ [utils.decode_value(x)] for x in request.inputs ]

        expose_outputs = [expose_output for expose_output in request.expose_outputs]
        # expose_value_types = self._get_value_types(request.expose_value_types)
        expose_value_types = list(request.expose_value_types)

        # TODO update this when more types supported
        if not expose_value_types:
            expose_value_types = ALLOWED_VALUE_TYPES

        users = self._get_users(request)

        request_id = self.manager.add_produce_job(
            fitted_solution_id=fitted_solution_id,
            inputs=inputs,
            expose_outputs=expose_outputs,
            expose_value_types=expose_value_types,
            users=users,
            save_path=Path.TEMP_STORAGE_ROOT
        )
        response = core_pb2.ProduceSolutionResponse(request_id=request_id)
        return response

    def GetProduceSolutionResults(self, request, context):
        request_id = request.request_id

        # LOGGING
        logger.info('method=GetProduceSolutionResults, request_id=%s', request_id)

        # We will wait up to 30 segs to create a search.
        max_wait = 10
        while request_id not in self.manager.request_status:
            time.sleep(1)
            max_wait -= 1
            if max_wait == 0:
                progress_state = 'ERRORED'
                progress_status = 'No request: ' + request_id
                progress = core_pb2.Progress(state=progress_state, status=progress_status)
                response = core_pb2.GetProduceSolutionResultsResponse(progress=progress)
                logger.info('method=GetProduceSolutionResults, request_id=%s, status=ERRORED, info=%s',
                             request_id, progress_status)
                yield response
                return

        progress_start = p_timestamp.Timestamp()
        progress_end = p_timestamp.Timestamp()
        progress_start.GetCurrentTime()

        if self.manager.request_status[request_id]['status'] == 'PENDING':
            progress = core_pb2.Progress(state='PENDING', status='Waiting for fit job to be scheduled')
            response = core_pb2.GetProduceSolutionResultsResponse(progress=progress)
            yield response
            while self.manager.request_status[request_id]['status'] == 'PENDING':
                time.sleep(0.2)

        if self.manager.request_status[request_id]['status'] == 'RUNNING':
            progress = core_pb2.Progress(state='RUNNING', status='Running fit job', start=progress_start)
            response = core_pb2.GetProduceSolutionResultsResponse(progress=progress)
            yield response
            while self.manager.request_status[request_id]['status'] == 'RUNNING':
                time.sleep(0.2)

        if self.manager.request_status[request_id]['status'] == 'ERRORED':
            progress_end.GetCurrentTime()
            progress = core_pb2.Progress(
                state='ERRORED',
                status=self.manager.request_status[request_id]['status'],
                start=progress_start,
                end=progress_end
            )
            response = core_pb2.GetProduceSolutionResultsResponse(progress=progress)
            logger.info('method=GetProduceSolutionResults, request_id=%s, status=ERRORED, info=%s',
                         request_id, self.manager.request_status[request_id]['status'])
            yield response
        elif self.manager.request_status[request_id]['status'] == 'COMPLETED':
            progress_end.GetCurrentTime()
            progress = core_pb2.Progress(
                state='COMPLETED',
                status='Fit job COMPLETED',
                start=progress_start,
                end=progress_end
            )
            # TODO add here:
            step_progress = []

            # encoding expose outputs
            exposed_outputs = dict()
            for name, value in self.manager.request_values[request_id].items():
                exposed_outputs[name] = utils.encode_value(value, ALLOWED_VALUE_TYPES, Path.TEMP_STORAGE_ROOT)

            response = core_pb2.GetProduceSolutionResultsResponse(progress=progress, steps=step_progress,
                                                                  exposed_outputs=exposed_outputs)
            logger.info('method=GetProduceSolutionResults, request_id=%s, status=COMPLETED', request_id)
            yield response

    def SolutionExport(self, request, context):
        solution_id = request.solution_id
        rank = request.rank

        if solution_id not in self.manager.solutions:
            logger.info('method=SolutionExport, solution_id=%s, status=ERRORED info=No solution_id found',
                         solution_id)
            response = core_pb2.SolutionExportResponse()
        else:
            # LOGGING
            logger.info('method=SolutionExport solution_id=%s', solution_id)

            self.manager.rank_solution(solution_id, rank)
            response = core_pb2.SolutionExportResponse()
        return response

    def SaveSolution(self, request, context):
        solution_id = request.solution_id
        logger.info('method=SaveSolution solution_id=%s', solution_id)

        if solution_id not in self.manager.solutions:
            logger.info('method=SaveSolution, solution_id=%s, error=Solution_id not found', solution_id)
            response = core_pb2.SaveSolutionResponse()
        else:
            solution_uri = self.manager.save_solution(solution_id)
            response = core_pb2.SaveSolutionResponse(solution_uri=solution_uri)
        return response

    def LoadSolution(self, request, context):
        solution_uri = request.solution_uri
        logger.info('method=LoadSolution solution_uri=%s', solution_uri)

        if not os.path.exists(solution_uri):
            logger.info('method=LoadSolution, solution_uri=%s, error=solution_uri not found', solution_uri)
            response = core_pb2.LoadSolutionResponse()
        else:
            solution_id = self.manager.load_solution(solution_uri)
            response = core_pb2.LoadSolutionResponse(solution_id=solution_id)
        return response

    def SaveFittedSolution(self, request, context):
        fitted_solution_id = request.fitted_solution_id
        logger.info('method=SaveFittedSolution, fitted_solution_id=%s', fitted_solution_id)

        if fitted_solution_id not in self.manager.fitted_solutions:
            logger.info('method=SaveFittedSolution, fitted_solution_id=%s, status=ERRORED, '
                        'info=No fitted_solution_id found', fitted_solution_id)
            response = core_pb2.SaveFittedSolutionResponse()
        else:
            fitted_solution_uri = self.manager.save_fitted_solution(fitted_solution_id)
            response = core_pb2.SaveFittedSolutionResponse(fitted_solution_uri=fitted_solution_uri)
        return response

    def LoadFittedSolution(self, request, context):
        fitted_solution_uri = request.fitted_solution_uri
        logger.info('method=LoadFittedSolution solution_uri=%s', fitted_solution_uri)

        if not os.path.exists(fitted_solution_uri):
            logger.info('method=LoadFittedSolution, solution_uri=%s, error=solution_uri not found', fitted_solution_uri)
            response = core_pb2.LoadFittedSolutionResponse()
        else:
            fitted_solution_id = self.manager.load_fitted_solution(fitted_solution_uri)
            response = core_pb2.LoadFittedSolutionResponse(fitted_solution_id=fitted_solution_id)
        return response

    def ScorePredictions(self, request, context):
        logger.info('method=ScorePredictions')
        predictions = utils.decode_value(request.predictions)
        score_input = utils.decode_value(request.score_input)
        problem = utils.decode_problem_description(request.problem)
        metrics = [utils.decode_performance_metric(_metric) for _metric in request.metric]

        scores, score_result = self.manager.score_predictions(predictions, score_input, problem, metrics)
        if score_result.has_error():
            logger.info('method=ScorePredictions, error={}', score_result.error)
            response = core_pb2.ScorePredictionsResponse()
        else:
            scores = self.encode_scores(scores)
            response = core_pb2.ScorePredictionsResponse(scores=scores)
        return response

    def DataAvailable(self, request, context):
        user_agent = request.user_agent
        version = request.version
        time_bound = request.time_bound
        priority = request.priority
        data = [utils.decode_value(d) for d in request.data]
        logger.info('method=DataAvailable, agent={}, version={}, time_bound={}, priority={}'.format(
            user_agent, version, time_bound, priority))
        response = core_pb2.DataAvailableResponse()
        return response

    def SplitData(self, request, context):
        input_value = utils.decode_value(request.input)
        scoring_configuration = self._decode_scoring_configuration(request.scoring_configuration)
        # allowed_value_types = self._get_value_types(request.allowed_value_types)
        allowed_value_types = list(request.allowed_value_types)

        problem_description = utils.decode_problem_description(request.problem)

        outputs, data_result = self.manager.split_data(
            input_value, scoring_configuration, allowed_value_types, problem_description)
        if data_result.has_error():
            logger.info('method=SplitData, error={}', data_result.error)
            response = core_pb2.SplitDataResponse()
            yield response
            return
        else:
            for i, (train_output, test_output, score_output) in enumerate(zip(*outputs)):
                uri_list = []
                for output, tag in (
                    (train_output, 'train'),
                    (test_output, 'test'),
                    (score_output, 'score'),
                ):
                    path = os.path.join(
                        constants.Path.TEMP_STORAGE_ROOT, '{}_output_{}'.format(tag, i), 'datasetDoc.json')
                    uri = self.__get_uri(path)
                    output.save(uri)
                    uri_list.append(uri)
                # response
                response = core_pb2.SplitDataResponse(
                    train_output=value_pb2.Value(dataset_uri=uri_list[0]),
                    test_output=value_pb2.Value(dataset_uri=uri_list[1]),
                    score_output=value_pb2.Value(dataset_uri=uri_list[2]),
                )
                yield response

    # TODO ask for this method to be deprecated
    def UpdateProblem(self, request, context):
        response = core_pb2.UpdateProblemResponse()
        return response

    def ListPrimitives(self, request, context):
        logger.info('method=ListPrimitives')
        primitives_list = []
        # primitives_info = get_primitives_info()
        primitives_info = file_utils.load_json('modules/utils/primitives_list/available_primitives.json')
        for primitive_info in primitives_info:
            primitives_list.append(primitive_pb2.Primitive(**primitive_info))
        response = core_pb2.ListPrimitivesResponse(primitives=primitives_list)
        return response

    def Hello(self, request, context):
        # LOGGING
        logger.info('method=Hello')
        user_agent = AGENT
        version = core_pb2.DESCRIPTOR.GetOptions().Extensions[core_pb2.protocol_version]
        allowed_value_types = ALLOWED_VALUE_TYPES
        supported_extensions = SUPPORTED_EXTENSIONS

        response = core_pb2.HelloResponse(
            user_agent=user_agent,
            version=version,
            allowed_value_types=allowed_value_types,
            supported_extensions=supported_extensions
        )
        return response

    def _get_value_types(self, value_types):
        return translate_to_string(list(value_types), value_pb2.ValueType)

    def encode_scores(self, all_scores):
        """
        Encode a dict of scores to a GRPC message

        Parameters
        ----------
        all_scores: dict
            A dictionary containing all the scores.
        tagets: list[str]
            A list with the problem targets.
        dataset_id: str
            A dataset id.
        scoring_configuration: dict
            A dictionary with the scoring configuration.

        Returns
        -------
        score_message: GRPC
        A GRPC message
        """
        scores = list()
        for score in all_scores.to_dict('index').values():
            score['random_seed'] = score['randomSeed']
            try:
                score['metric'] = {'metric': score['metric']}
            except:
                score['metric'] = {'metric': problem_module.PerformanceMetric[score['metric']]}

            scores.append(utils.encode_score(score, ALLOWED_VALUE_TYPES, Path.TEMP_STORAGE_ROOT))
        return scores

    def _encode_scoring_configuration(self, scoring_configuration):
        """
        Decode a scoring configuration from grpc

        Parameters
        ----------
        configuration: dict
            A dictionary with the scoring configuration.

        Returns
        -------
        scoring_configuration: core_pb2.ScoringConfiguration
            A grpc ScoringConfiguration message.
        """
        if scoring_configuration is None:
            return core_pb2.ScoringConfiguration()
        else:
            method = scoring_configuration['method']
            folds = scoring_configuration.get('number_of_folds', None)
            if folds is not None:
                folds = int(folds)
            train_test_ratio = scoring_configuration.get('train_score_ratio', None)
            if train_test_ratio is not None:
                train_test_ratio = float(train_test_ratio)
            shuffle = scoring_configuration.get('shuffle', None)
            if shuffle is not None:
                shuffle = json.loads(shuffle.lower())
            random_seed = scoring_configuration.get('randomSeed', None)
            if random_seed is not None:
                random_seed = int(random_seed)
            stratified = scoring_configuration.get('stratified', None)
            if stratified is not None:
                stratified = json.loads(stratified.lower())
            return core_pb2.ScoringConfiguration(method=method, folds=folds, train_test_ratio=train_test_ratio,
                                                 shuffle=shuffle, random_seed=random_seed, stratified=stratified)

    def _decode_scoring_configuration(self, scoring_configuration):
        """
        Decode a scoring configuration from grpc

        Parameters
        ----------
        scoring_configuration: core_pb2.ScoringConfiguration
            A grpc ScoringConfiguration message.

        Returns
        -------
        configuration: dict
            A dictionary with the scoring configuration.
        """
        method = scoring_configuration.method
        configuration = {
            'method': method,
            'train_score_ratio': str(scoring_configuration.train_test_ratio),
            'stratified': str(scoring_configuration.stratified).lower(),
            'shuffle': str(scoring_configuration.shuffle).lower(),
            'randomSeed': str(scoring_configuration.random_seed),
        }
        if method == 'K_FOLD':
            configuration['number_of_folds'] = str(scoring_configuration.folds)
        return configuration

    def _get_users(self, request):
        users = []
        for user in request.users:
            _user = {
                'id': user.id,
                'chosen': user.chosen,
                'reason': user.reason
            }
            users.append(_user)
        return users

    def __get_uri(self, path):
        return pathlib.Path(os.path.abspath(path)).as_uri()

def translate_to_string(values, function):
    if isinstance(values, list):
        results = []
        for value in values:
            results.append(function.Name(value))
        return results
    else:
        return [function.Name(values)]


class Server:
    def __init__(self, arguments):
        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
        self.core = Core(process_type=arguments.proc_spawn)

        core_pb2_grpc.add_CoreServicer_to_server(self.core, self.server)
        self.server.add_insecure_port('[::]:45042')

    def start(self):
        self.server.start()

    def stop(self):
        self.server.stop(0)


def configure_parser(parser, *, skip_arguments=()):
    parser.add_argument(
        '-p', '--proc-spawn', type=str, default='spawn',
        help='To use \'spawn\' or \'fork\' to spawn process'
    )
    parser.add_argument(
        '-o', '--output-path', type=str, default=os.path.join(os.getcwd(), "output/"),
        help="path where the outputs would be stored"
    )
    parser.add_argument(
        '-v', '--verbose', type=bool, default=True,
        help="Display detailed log"
    )


def main():
    # Creating parser
    parser = argparse.ArgumentParser(description="Starts server from command line")
    configure_parser(parser)
    arguments = parser.parse_args()

    # Setup logger
    verbose_format = '%(asctime)s %(levelname)-8s %(processName)-15s [%(filename)s:%(lineno)d] %(message)s'
    concise_format = '%(asctime)s %(levelname)-8s %(message)s'
    log_format = verbose_format if arguments.verbose else concise_format
    logging.basicConfig(format=log_format,
                        handlers=[logging.StreamHandler(),
                                  logging.FileHandler('{}/d3m.log'.format(Path.TEMP_STORAGE_ROOT), 'w', 'utf-8')],
                        datefmt='%m/%d %H:%M:%S')
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    warnings.filterwarnings('ignore')

    server = Server(arguments)

    try:
        load_time = time.time()
        server.start()
        with d3m_utils.silence():
            d3m_index.load_all(blocklist=LISTS.BLACK_LIST)
        time.sleep(5)
        print('load_time', time.time() - load_time)
        logger.info('---------- Waiting for Requests ----------')
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        logger.info('############ STOPPING SERVICE ############')
        server.stop()


if __name__ == '__main__':
    main()
