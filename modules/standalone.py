import multiprocessing
import os
import time
import json
import argparse
import logging
import warnings
from d3m.metadata import problem as problem_module
from modules.manager.execution_manager import ExecutionManger
from modules.utils.constants import EnvVars, Path
from modules.utils import schemas_utils
from pprint import pprint
from ta3ta2_api import utils

logger = logging.getLogger(__name__)
ALLOWED_VALUE_TYPES = ['RAW', 'DATASET_URI', 'CSV_URI']


def main(problem_path, datasets_dir, time_bound, process_type):
    manager = ExecutionManger(n_processes=EnvVars.D3MCPU, process_type=process_type)
    manager.start_workers()
    time.sleep(60)
    logger.info('Wait 10 seconds ')
    problem = problem_module.get_problem(problem_path)

    if len(problem['inputs']) != 1:
        raise RuntimeError('Expected number of datasets is 1 got {n_datasets}'.format(n_datasets=len(problem['inputs'])))
    else:
        dataset_id = problem['inputs'][-1]['dataset_id']

    for dirpath, dirnames, filenames in os.walk(datasets_dir, followlinks=True):
        if 'datasetDoc.json' in filenames:
            # Do not traverse further (to not parse "datasetDoc.json" or "problemDoc.json" if they
            # exists in raw data filename).
            dirnames[:] = []
            dataset_path = os.path.join(os.path.abspath(dirpath), 'datasetDoc.json')

            try:
                with open(dataset_path, 'r', encoding='utf8') as dataset_file:
                    dataset_doc = json.load(dataset_file)

                if dataset_id == dataset_doc['about']['datasetID']:
                    break

            except (ValueError, KeyError):
                logger.exception(
                    "Unable to read dataset '%(dataset)s'.", {
                        'dataset': dataset_path,
                    },
                )
    else:
        raise ValueError('Dataset {dataset} not found'.format(dataset=dataset_id))

    dataset_uri = 'file://{}'.format(dataset_path)
    inputs = [[
        {
            'type': 'dataset_uri',
            'value': dataset_uri,
        }
    ]]
    problem = utils.encode_problem_description(problem)
    pprint(problem)
    problem = utils.decode_problem_description(problem)
    print('@'*100)
    pprint(problem)
    print('@' * 100)
    search_id = manager.add_search(time_bound_search=time_bound, priority=1,
                                   allowed_value_types=ALLOWED_VALUE_TYPES,
                                   problem=problem, pipeline_template=None, inputs=inputs)

    time_left = time_bound
    while manager.searches_status[search_id]['status'] != 'COMPLETED' and \
            manager.searches_status[search_id]['status'] != 'ERRORED' and time_left > 1:
        time_left -= 5
        time.sleep(5)

    solution_found = {}
    if manager.searches_status[search_id]['status'] == 'ERRORED':
        logger.info('SEARCH FAILED {}'.format(manager.searches_status[search_id]['info']))
    else:
        if len(manager.searches[search_id]):
            logger.info('SEARCH COMPLETED {} pipelines'.format(len(manager.searches[search_id])))
            for solution_id in manager.searches[search_id]:
                pipeline_id = manager.solutions[solution_id]['pipeline']
                rank = manager.pipelines[pipeline_id]['rank']
                manager.rank_solution(solution_id, rank)
                logger.info('Pipeline {} rank {}'.format(pipeline_id, rank))
                pprint(manager.pipelines[pipeline_id]['scores']['all'])
                solution_found[pipeline_id] = manager.pipelines[pipeline_id]['scores']['all']
        else:
            logger.info('SEARCH FAILED NO PIPELINES')
    logger.info('SEARCH DONE')
    task_des = schemas_utils.get_task_des(problem['problem']['task_keywords'])
    logger.info('#' * 40 + ' Task: {}'.format(task_des) + '#' * 40)

    manager.stop_workers()
    return solution_found


def configure_parser(parser, *, skip_arguments=()):
    parser.add_argument(
        '-q', '--proc-type', type=str, default='spawn',
        choices=['spawn', 'fork'], help='Type of process'
    )
    parser.add_argument(
        '-p', '--problem-path', type=str,
        help="Problem Path."
    )
    parser.add_argument(
        '-d', '--datasets-dir', type=str,
        help="Dataset directory"
    )
    parser.add_argument(
        '-t', '--time_bound', type=int, default=600,
        help="Time bound for search in seconds"
    )
    parser.add_argument(
        '-v', '--verbose', type=bool, default=True,
        help="Display detailed log"
    )


if __name__ == '__main__':
    # Creating parser
    parser = argparse.ArgumentParser(description="Run system on stand alone")
    configure_parser(parser)
    arguments = parser.parse_args()

    # Setup logger
    verbose_format = '%(asctime)s %(levelname)-8s %(processName)-15s [%(filename)s:%(lineno)d] %(message)s'
    concise_format = '%(asctime)s %(levelname)-8s %(message)s'
    log_format = verbose_format if arguments.verbose else concise_format
    logging.basicConfig(format=log_format,
                        handlers=[logging.StreamHandler(),
                                  logging.FileHandler('{}/d3m.log'.format(Path.TEMP_STORAGE_ROOT), 'w', 'utf-8')],
                        datefmt='%m/%d %H:%M:%S')
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    warnings.filterwarnings('ignore')

    problem_path = arguments.problem_path
    datasets_dir = arguments.datasets_dir
    time_bound = arguments.time_bound
    process_type = arguments.proc_type

    main(problem_path, datasets_dir, time_bound, process_type)