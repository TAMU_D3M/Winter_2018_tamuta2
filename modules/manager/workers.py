import copy
import logging
import logging.handlers
import os
import time
import uuid
import pickle

import pandas as pd
import pyarrow.plasma as plasma
from d3m import container
from d3m import container as container_module
from d3m import index as d3m_index
from d3m import runtime as runtime_module
from d3m import utils as d3m_utils
from d3m.metadata import pipeline as pipeline_module
from d3m.metadata.base import Context
from d3m.metadata.problem import TaskKeyword

# TODO enable this generator when the dependency datamart_nyu is updated.
# from modules.pipeline_generatr.models.datamart_generator import DataAugmentationGenerator

from modules.utils import schemas_utils
from modules.utils.checks import check_directory
from modules.utils.constants import Path, LISTS, EnvVars, SearchPath
from modules.utils.pipeline_utils import save_pipeline, load_pipeline
from modules.utils.schemas_utils import load_problem, get_task_des
from modules.utils import workers as workers_utils
from modules.utils.workers import TimeTracker

logger = logging.getLogger(__name__)

UTILS_PATH = 'modules/utils/pipelines/'
UTILS_PIPELINES = {
    'SCORING': UTILS_PATH + 'scoring.yml',
    'K_FOLD': UTILS_PATH + 'k-fold-tabular-split.yml',
    'HOLDOUT': UTILS_PATH + 'train-test-tabular-split.yml',
    'HOLDOUT_FIXED': UTILS_PATH + 'fixed-split-tabular-split.yml',
    'TRAINING_DATA': UTILS_PATH + 'no-split-tabular-split.yml',
}

KEYWORDS_TO_SKIP = [
    'OBJECT_DETECTION',
]


def get_data(inputs_list, plasma_client=None):
    """
    Load data according with its type.

    Parameters
    ----------
    inputs_list: list[list[dict[str, str]]
        A list of list of dict with data available i.e.
        [[{'type': 'dataset_uri', 'value': 'file:///D3M/datasets/seed_datasets_current/185_baseball/185_baseball_dataset/datasetDoc.json'}]]
    plasma_client: Union[plasma_client, None]
        A plasma client that it would potentially be used for cache..

    Returns
    -------
        A list of d3m containers

    """

    inputs = []

    for input in inputs_list:
        input_types_available = [data_available['type'] for data_available in input]
        _input = None

        if plasma_client is not None and 'plasma_id' in input_types_available:
            data_index = input_types_available.index('plasma_id')
            object_id = plasma.ObjectID(workers_utils.hex_to_binary(input[data_index]['value']))
            _input = plasma_client.get(object_id)
        elif 'dataset_uri' in input_types_available:
            data_index = input_types_available.index('dataset_uri')
            _input = container_module.dataset.get_dataset(input[data_index]['value'])
        elif 'csv_uri' in input_types_available:
            data_index = input_types_available.index('csv_uri')
            df = pd.read_csv(input[data_index]['value'])
            _input = container.DataFrame(df, generate_metadata=True)
        else:
            logger.info("Input type {} not supported, ignoring it.".format(input[0]['type']))

        inputs.append(_input)
    return inputs


def cache_data_prep(problem_path, inputs, configuration, plasma_client=None):
    if plasma_client is None:
        return None, None

    problem_description = load_problem(problem_path)
    data_pipeline_path = UTILS_PIPELINES[configuration['method']]
    data_pipeline = load_pipeline(data_pipeline_path)

    outputs, data_result = runtime_module.prepare_data(
        data_pipeline=data_pipeline, problem_description=problem_description,
        inputs=inputs, data_params=configuration, context=Context.TESTING,
        volumes_dir=EnvVars.D3MSTATICDIR,
    )

    if data_result.has_error():
        return None, None

    try:
        object_id_outputs = plasma_client.put(outputs)
        object_id_outputs = workers_utils.binary_to_hex(object_id_outputs.binary())

        result = pickle.dumps(data_result)
        object_id_result = plasma_client.put(result)
        object_id_result = workers_utils.binary_to_hex(object_id_result.binary())

        return object_id_outputs, object_id_result
    except Exception as e:
        logger.info("Not able to cache data preparation {}".format(e))
        return None, None


def cache_inputs(inputs, inputs_list, plasma_client=None):
    new_data_available = []
    if plasma_client is None:
        return inputs_list
    else:
        for i in range(0, len(inputs_list)):
            input_types_available = [data_available['type'] for data_available in inputs_list[i]]
            if 'plasma_id' in input_types_available:
                new_data_available.append(inputs_list[i])
            else:
                try:
                    # we try to cache the input.
                    object_id = plasma_client.put(inputs[i])
                    object_id = workers_utils.binary_to_hex(object_id.binary())
                    # update the types
                    new_data_available.append(
                        [{'type': 'plasma_id', 'value': object_id}] + inputs_list[i]
                    )
                except Exception as e:
                    logger.info("Not able to cache inputs {}".format(e))
                    new_data_available.append(inputs_list[i])

        return new_data_available


# TODO Add support for more types of expose values.
def store_outputs_by_value_type(request_id, exposed_outputs, expose_value_types, save_path=Path.OTHER_OUTPUTS):
    """
    Defining a worker that should be able to deal with all

    Parameters
    ----------
    request_id; str
        An request id to store the values.
    exposed_outputs: dict
        A dict that containes the exposed values with its names.
    expose_value_types: list[str]
        A list with allowed value types that should be use to store the outputs.
    save_path: str
        A path to store the files.
    """
    if not len(expose_value_types):
        expose_value_types.append('CSV_URI')
    outputs = {}
    if 'CSV_URI' in expose_value_types:
        for key, output in exposed_outputs.items():
            output_directory = os.path.join(save_path, request_id)
            check_directory(output_directory)
            if isinstance(output, container.DataFrame):
                output_file = os.path.join(output_directory, key + '.csv')
                output.to_csv(output_file)
                outputs[key] = {'type': 'csv_uri', 'value': output_file}
    return outputs


def evaluate(pipeline, data_pipeline, scoring_pipeline, problem_description, inputs, data_params,
             metrics, *, context, scoring_params=None, hyperparams=None, random_seed=0, data_random_seed=0,
             scoring_random_seed=0, volumes_dir=None, scratch_dir=None, runtime_environment=None,
             data_preparation_id=None, plasma_client=None):
    """
    Values in ``data_params`` should be serialized as JSON, as obtained by JSON-serializing
    the output of hyper-parameter's ``value_to_json_structure`` method call.
    """
    logger.info('#' * 100)
    logger.info('plasma {} data_preparation {}'.format(plasma_client, data_preparation_id))
    logger.info('#' * 100)
    if plasma_client is None and data_preparation_id is None:
        outputs, data_result = runtime_module.prepare_data(
            data_pipeline=data_pipeline, problem_description=problem_description,
            inputs=inputs, data_params=data_params, context=context, random_seed=data_random_seed,
            volumes_dir=volumes_dir, scratch_dir=scratch_dir, runtime_environment=runtime_environment,
        )
        if data_result.has_error():
            return [], runtime_module.MultiResult([data_result])
    else:
        try:
            outputs_id = plasma.ObjectID(workers_utils.hex_to_binary(data_preparation_id[0]))
            data_results_id = plasma.ObjectID(workers_utils.hex_to_binary(data_preparation_id[1]))
            outputs = plasma_client.get(outputs_id)
            data_result = pickle.loads(plasma_client.get(data_results_id))
        except:
            outputs, data_result = runtime_module.prepare_data(
                data_pipeline=data_pipeline, problem_description=problem_description,
                inputs=inputs, data_params=data_params, context=context, random_seed=data_random_seed,
                volumes_dir=volumes_dir, scratch_dir=scratch_dir, runtime_environment=runtime_environment,
            )

            if data_result.has_error():
                return [], runtime_module.MultiResult([data_result])

    fold_group_uuid = uuid.uuid4()

    all_scores = []
    all_results = runtime_module.MultiResult()
    for fold_index, (train_inputs, test_inputs, score_inputs) in enumerate(zip(*outputs)):
        fitted_pipeline, predictions, fit_result = runtime_module.fit(
            pipeline, [train_inputs], problem_description=problem_description, context=context, hyperparams=hyperparams,
            random_seed=random_seed, volumes_dir=volumes_dir, scratch_dir=scratch_dir,
            runtime_environment=runtime_environment,
        )

        # Modifies "fit_result.pipeline_run" in-place.
        runtime_module.combine_pipeline_runs(
            fit_result.pipeline_run, data_pipeline_run=data_result.pipeline_run,
            fold_group_uuid=fold_group_uuid, fold_index=fold_index,
        )

        all_results.append(fit_result)
        if fit_result.has_error():
            assert all_results.has_error()
            return all_scores, all_results

        predictions, produce_result = runtime_module.produce(fitted_pipeline, [test_inputs])

        # Modifies "produce_result.pipeline_run" in-place.
        runtime_module.combine_pipeline_runs(
            produce_result.pipeline_run, data_pipeline_run=data_result.pipeline_run,
            fold_group_uuid=fold_group_uuid, fold_index=fold_index
        )

        all_results.append(produce_result)
        if produce_result.has_error():
            assert all_results.has_error()
            return all_scores, all_results

        scores, score_result = runtime_module.score(
            predictions, [score_inputs], scoring_pipeline=scoring_pipeline, problem_description=problem_description,
            metrics=metrics, predictions_random_seed=random_seed, scoring_params=scoring_params, context=context,
            random_seed=scoring_random_seed, volumes_dir=volumes_dir, scratch_dir=scratch_dir,
            runtime_environment=runtime_environment,
        )

        # Modifies "produce_result.pipeline_run" in-place.
        runtime_module.combine_pipeline_runs(
            produce_result.pipeline_run, scoring_pipeline_run=score_result.pipeline_run,
        )
        # Sets the error, if there are any.
        produce_result.error = score_result.error

        # We modified "produce_result.pipeline_run" in-place and "produce_result"
        # is already among "all_results", so we do not add it again.
        if score_result.has_error():
            assert all_results.has_error()
            return all_scores, all_results

        # Modifies "produce_result.pipeline_run" in-place.
        runtime_module.combine_pipeline_runs(
            produce_result.pipeline_run, metrics=metrics, scores=scores,
        )

        all_scores.append(scores)

    return all_scores, all_results


def _fit_worker(request_id, job_type, pipelines, solutions, solution_id, inputs, expose_outputs, expose_value_types,
                users, fitted_solutions, request_status, request_values, save_path, internal_cache, plasma_client):
    """
    A function to fit a solution based on a request.

    Parameters
    ----------
    request_id: str
        Request id
    job_type: str
        Job type.
    pipelines: dict
        A shared dict that contains all pipelines {id: {'path': path, 'status': status, 'scores': {scores}}}
    solutions: dict
        A shared dict that contains all solutions {soludion_id: {'pipeline': pipeline_id, 'problem':problem}}.
        problem: A str that indicates the problem in case exist, othewise None.
    solution_id: str
        Solution id to be fitted.
    inputs: list[str]:
        A list of strings that indicates the inputs.
    expose_outputs: list[str]
        A list of strings that indicates which outputs to expose.
    expose_value_types: list[enum]
        A list that conains in order the ways to store the outputs exposed.
    users: dict
        A dict containing user information.
    fitted_solutions: dict
         A shared dict to store the fitted solutions {fitted_solution_id: Runtime}.
    request_status: dict
        A shared dict to store the status of a given request {request_id: {'status':status, 'id':info}}.
    request_values: dict
        A shared dict to store exposed outputs {request_id: {exposed_values}}.
    save_path:
        A path where the exposed outputs will be saved
    plasma_client: Union[plasma_client, None]
        A plasma client that it would potentially be used for cache.
    """

    try:
        # Update status Running
        request_status[request_id] = {'status': 'RUNNING'}
        logger.info('FIT-SOLUTION solution_id=%s request_id=%s status=RUNNING',
                    solution_id, request_id)

        datasets = get_data(inputs, plasma_client)
        pipeline = load_pipeline(pipelines[solutions[solution_id]['pipeline']]['path'])

        problem = solutions[solution_id]['problem']
        if problem is not None:
            problem = load_problem(problem)

        with d3m_utils.silence():
            runtime = runtime_module.Runtime(pipeline=pipeline, problem_description=problem,
                                             context=Context.EVALUATION, volumes_dir=EnvVars.D3MSTATICDIR)
        result = runtime.fit(inputs=datasets, return_values=expose_outputs)

        # Update status if not error Complete, error otherwise
        # if completed store runtime fitted instance
        if result is not None:
            # Generating fitted solution id and saving runtime instance
            fitted_solution_id = str(uuid.uuid4())
            fitted_solutions[fitted_solution_id] = {
                'runtime': runtime,
                'pipeline_path': pipelines[solutions[solution_id]['pipeline']]['path']
            }

            # Storing exposed_values.
            # TODO add method to store outputs according with its types.
            if expose_outputs is not None:
                outputs = store_outputs_by_value_type(request_id, result.values, expose_value_types, save_path)
                request_values[request_id] = outputs

            # TODO un-comment this when this pipeline_run is fixed on core package
            # save_pipeline_run([result.pipeline_run])
            # we have to set on the runtime construction is_standard_pipeline=True, but implies
            # that there are stardat outputs, that it might not be always true.

            # Updating Status.
            request_status[request_id] = {'status': 'COMPLETED', 'fitted_solution_id': fitted_solution_id}
            logger.info('FIT-SOLUTION solution_id=%s request_id=%s status=COMPLETED',
                        solution_id, request_id)
        else:
            request_status[request_id] = {'status': 'ERRORED'}
            logger.info('FIT-SOLUTION solution_id=%s request_id=%s status=ERRORED info=%s',
                        solution_id, request_id, result.error, exc_info=result.error)

    except Exception as error:
        logger.info('FIT-SOLUTION solution_id=%s request_id=%s status=ERRORED info=%s',
                    solution_id, request_id, error, exc_info=error)


def _produce_worker(request_id, job_type, fitted_solution_id, inputs, expose_outputs, expose_value_types, users,
                    fitted_solutions, request_status, request_values, save_path, internal_cache, plasma_client):
    """
    A function to produce a fitted solution based on a request.

    Parameters
    ----------
    request_id: str
        Request id
    job_type: str
        Job type.
    fitted_solution_id: str
        Id of fitted solution that would be use for produce.
    inputs: list[str]:
        A list of strings that indicates the inputs.
    expose_outputs: list[str]
        A list of strings that indicates which outputs to expose.
    expose_value_types: list[enum]
        A list that conains in order the ways to store the outputs exposed.
    users: dict
        A dict containing user information.
    problem: Union[str, None]
        A str that indicates the problem in case exist, othewise None.
    fitted_solutions: dict
         A shared dict to store the fitted solutions {fitted_solution_id: Runtime}.
    request_status: dict
        A shared dict to store the status of a given request {request_id: {'status':status, 'id':info}}.
    request_values: dict
        A shared dict to store exposed outputs {request_id: {exposed_values}}.
    save_path:
        A path where the exposed outputs will be saved
    plasma_client: Union[plasma_client, None]
        A plasma client that it would potentially be used for cache.
    """

    try:
        # Update status Running
        request_status[request_id] = {'status': 'RUNNING'}
        logger.info('PRODUCE- fitted_solution_id=%s request_id=%s status=RUNNING',
                    fitted_solution_id, request_id)

        datasets = get_data(inputs, plasma_client)

        # check if expose outptus is nothing
        if isinstance(expose_outputs, list) and not len(expose_outputs):
            expose_outputs = None

        result = fitted_solutions[fitted_solution_id]['runtime'].produce(inputs=datasets, return_values=expose_outputs)

        # Update status if not error Complete, eror otherwise
        # if completed store runtime fitted instance
        if result is not None:
            # Storing exposed outputs
            # TODO add method to store outputs according with its types.
            outputs = store_outputs_by_value_type(request_id, result.values, expose_value_types, save_path)
            request_values[request_id] = outputs

            # TODO un-comment this when this pipeline_run is fixed on core package
            # save_pipeline_run([result.pipeline_run])

            # Updating status
            request_status[request_id] = {'status': 'COMPLETED'}
            logger.info(
                'PRODUCE-SOLUTION fitted_solution_id=%s request_id=%s status=COMPLETED',
                fitted_solution_id, request_id)
        else:
            request_status[request_id] = {'status': 'ERRORED'}
            logger.info(
                'PRODUCE-SOLUTION fitted_solution_id=%s request_id=%s status=ERRORED, info=%s',
                fitted_solution_id, request_id, result.error, exc_info=result.error)

    except Exception as error:
        request_status[request_id] = {'status': 'ERRORED'}
        logger.info(
            'PRODUCE-SOLUTION fitted_solution_id=%s request_id=%s status=ERRORED, info=%s',
            fitted_solution_id, request_id, error, exc_info=error)


def _score_worker(request_id, job_type, pipelines, solutions, solution_id, inputs, performance_metrics,
                  users, configuration, request_status, score_solutions, internal_cache, plasma_client):
    """
    A function to score a solution based on a request.

    Parameters
    ----------
    request_id: str
        Request id
    job_type: str
        Job type.
    pipelines: dict
        A shared dict that contains all pipelines {id: {'path': path, 'status': status, 'scores': {scores}}}
    solutions: dict
        A shared dict that contains all solutions {soludion_id: {'pipeline': pipeline_id, 'problem':problem}}.
        problem: A str that indicates the problem in case exist, othewise None.
    solution_id: str
        Solution id to be fitted.
    inputs: list[str]:
        A list of strings that indicates the inputs.
    performance_metrics: dict.
        A dictionary of metics such as {'metric':'ACCURACY'}
    users: dict
        A dict containing user information.
    configuration: Union[dict, None]
        A dict containing the evaluation params.(data_params).
    problem: Union[str, None]
        A str that indicates the problem in case exist, othewise None.
    request_status: dict
        A shared dict to store the status of a given request {request_id: {'status':status, 'id':info}}.
    score_solutions: dict
        A shared dict to store scores of solutions {solution_id: dict(scores)}.
    plasma_client: Union[plasma_client, None]
        A plasma client that it would potentially be used for cache.
    """

    try:
        # Update status Running
        request_status[request_id] = {'status': 'RUNNING'}
        logger.info('SCORE-SOLUTION solution_id=%s request_id=%s status=RUNNING',
                    solution_id, request_id)

        pipeline = load_pipeline(pipelines[solutions[solution_id]['pipeline']]['path'])

        # Pipeline for splits
        if configuration is None:
            raise AttributeError('Expecting configuration for data preprocessing')

        # check if we support that splits
        elif configuration['method'] not in UTILS_PIPELINES and configuration['method'] != 'RANKING':
            raise AttributeError(configuration['method'] + ' not supported for scoring')

        scoring_pipeline = load_pipeline(UTILS_PIPELINES['SCORING'])

        # Problem description
        problem = solutions[solution_id]['problem']
        if problem is not None:
            problem = load_problem(problem)
        else:
            raise AttributeError('Expecting problem description')

        ranking = False
        if configuration['method'] == 'RANKING':
            task_des = get_task_des(problem['problem']['task_keywords'])
            performance_metrics = schemas_utils.get_metrics_from_task(task_des,
                                                                      problem['problem']['performance_metrics'])
            configuration = schemas_utils.get_eval_configuration(task_des['task_type'], task_des['data_types'],
                                                                 task_des['semi'])
            ranking = True

        data_pipeline_path = UTILS_PIPELINES[configuration['method']]
        data_pipeline = load_pipeline(data_pipeline_path)

        datasets = get_data(inputs, plasma_client)

        with d3m_utils.silence():
            scores_list, results_list = runtime_module.evaluate(pipeline=pipeline, data_pipeline=data_pipeline,
                                                                scoring_pipeline=scoring_pipeline,
                                                                problem_description=problem, inputs=datasets,
                                                                data_params=configuration,
                                                                metrics=performance_metrics,
                                                                context=Context.TESTING,
                                                                volumes_dir=EnvVars.D3MSTATICDIR)

        # runtime_module._output_pipeline_runs({'output_run': True}, results_list.pipeline_runs)
        results_list.check_success()
        results = runtime_module.combine_folds(scores_list)

        if ranking:
            rank = {
                'metric': 'RANK',
                'value': workers_utils.compute_rank(
                    workers_utils.summarize_performance_metrics(results)
                ),
                'randomSeed': 0,
                'fold': 0,
            }
            results = results.append(rank, ignore_index=True)
        # save_pipeline_run(results_list.pipeline_runs)

        score_solutions[request_id] = results
        request_status[request_id] = {'status': 'COMPLETED', 'problem': solutions[solution_id]['problem']}
        logger.info('SCORE-SOLUTION solution_id=%s request_id=%s status=COMPLETED',
                    solution_id, request_id)

    except Exception as error:
        request_status[request_id] = {'status': 'ERRORED', 'info': error}
        logger.info('SCORE-SOLUTION solution_id=%s request_id=%s status=ERRORED info=%s',
                    solution_id, request_id, error, exc_info=error)


# TODO add a way to run incomplete pipelines
def _search_score_worker(searches, problems, search_id, job_type, pipelines, pipeline_id, solutions, inputs,
                         performance_metrics, configuration, internal_cache, plasma_client, data_preparation_id):
    """
    A function to score a pipelines based on a search.
    On the ideal case we eill be computing all the scores according with a task.

    Parameters
    ----------
    searches: dict
        A shared dict for storing the pipelines related with the search {search_id: list}
    search_id: str
        Search id
    job_type: str
        Job type.
    pipelines: dict
        A shared dict that contains all pipelines
        {pipeline_id: {'path': path, 'status': status, 'scores': {'all': scores, 'condensed': scores}}, 'solution_id': solution_id, 'internal_score': internal_score}
    pipeline_id: str
        Pipeline id to be evaluated.
    solutions: dict
        A shared dict that contains all solutions {soludion_id: {'pipeline': pipeline_id, 'problem':problem}}.
        problem: A str that indicates the problem in case exist, othewise None.
    inputs: list[str]:
        A list of strings that indicates the inputs.
    performance_metrics: dict.
        A dictionary of metics such as {'metric':'ACCURACY'}
    configuration: Union[dict, None]
        A dict containing the evaluation params.(data_params).
    problem: dict
        A dict for storing the problems associated with the search {search_id: problem_path}
    plasma_client: Union[plasma_client, None]
        A plasma client that it would potentially be used for cache.
    """

    # Update status Running
    logger.info('SEARCH pipeline=%s status=RUNNING', pipeline_id)
    try:
        # Pipeline to evaluate
        pipeline = load_pipeline(pipelines[pipeline_id]['path'])

        # Problem description
        problem = problems[search_id]

        if 'expose_outputs' not in pipelines[pipeline_id]:
            # for the case that we know what to do with the pipeline.
            if problem is not None:
                # load the problem
                problem = load_problem(problem)

                # check if we support that splits
                if configuration['method'] not in UTILS_PIPELINES:
                    raise AttributeError(configuration['method'] + 'not supported for scoring')

                # Getting data preprocessing pipeline.
                data_pipeline_path = UTILS_PIPELINES[configuration['method']]
                data_pipeline = load_pipeline(data_pipeline_path)

                # Pipeline for scoring
                scoring_pipeline = load_pipeline(UTILS_PIPELINES['SCORING'])

                datasets = get_data(inputs, plasma_client)

                with d3m_utils.silence():
                    scores_list, results_list = evaluate(pipeline=pipeline, data_pipeline=data_pipeline,
                                                         scoring_pipeline=scoring_pipeline,
                                                         problem_description=problem, inputs=datasets,
                                                         data_params=configuration,
                                                         metrics=performance_metrics,
                                                         context=Context.TESTING,
                                                         volumes_dir=EnvVars.D3MSTATICDIR,
                                                         data_preparation_id=data_preparation_id,
                                                         plasma_client=plasma_client)

                runtime_module._output_pipeline_runs({'output_run': True}, results_list.pipeline_runs)
                results_list.check_success()

                results = runtime_module.combine_folds(scores_list)

                # save_pipeline_run(results_list.pipeline_runs)
                save_pipeline(pipeline_path=pipelines[pipeline_id]['path'], search_id=search_id, mode='SCORE')

                # Updating pipeline information.
                solution_id = str(uuid.uuid4())
                summarize_performance = workers_utils.summarize_performance_metrics(results)
                updated_pipeline_information = pipelines[pipeline_id]
                updated_pipeline_information['status'] = 'COMPLETED'
                updated_pipeline_information['solution_id'] = solution_id
                updated_pipeline_information['internal_score'] = workers_utils.compute_score(summarize_performance)
                rank = workers_utils.compute_rank(summarize_performance)
                updated_pipeline_information['rank'] = rank

                ranking = {
                    'metric': 'RANK',
                    'value': rank,
                    'randomSeed': 0,
                    'fold': 0,
                }
                results = results.append(ranking, ignore_index=True)

                updated_pipeline_information['scores'] = {
                    'all': results,
                    'condensed': summarize_performance,
                }

            # case that is only a pipeline that we need to add a solution or produce the results
            else:
                # Updating pipeline information.
                solution_id = str(uuid.uuid4())
                updated_pipeline_information = pipelines[pipeline_id]
                updated_pipeline_information['status'] = 'COMPLETED'
                updated_pipeline_information['solution_id'] = solution_id

            # Updating pipeline Infromation on the pipeline dict
            pipelines[pipeline_id] = updated_pipeline_information

            # Creating solution
            solutions[solution_id] = {'problem': problems[search_id], 'pipeline': pipeline_id}

            # logger that everything works
            logger.info('SEARCH pipeline=%s status=COMPLETED', pipeline_id)

            # We need to copy and update the elements since nested proxies are not allowed with multiprocessing
            # It does not matter the result, we add the pipeline to the search.
            # update_search_information = searches[search_id]
            # update_search_information.append(pipeline_id)
            # searches[search_id] = update_search_information
            searches[search_id].append(solution_id)

        # Case where we want intermediate outputs:
        else:
            if problem is not None:
                # load the problem
                problem = load_problem(problem)
            else:
                raise AttributeError('Expecting Problem Description')

            datasets = get_data(inputs, plasma_client)

            # create an instace of the runtime and get the outputs
            with d3m_utils.silence():
                runtime = runtime_module.Runtime(pipeline=pipeline, problem_description=problem,
                                                 context=Context.EVALUATION, volumes_dir=EnvVars.D3MSTATICDIR)
            result = runtime.fit(inputs=datasets, return_values=pipelines[pipeline_id]['expose_outputs'])

            # Store only the metadata
            metadata_results = dict()
            for key, output in result.values.items():
                metadata_results[key] = output.metadata

            updated_pipeline_information = pipelines[pipeline_id]
            updated_pipeline_information['status'] = 'COMPLETED'
            updated_pipeline_information['expose_outputs'] = metadata_results

            # Updating pipeline Infromation on the pipeline dict
            pipelines[pipeline_id] = updated_pipeline_information

            # logger that everything works
            logger.info('SEARCH pipeline=%s status=COMPLETED', pipeline_id)

    except Exception as error:
        # Updating pipeline information.
        updated_pipeline_information = pipelines[pipeline_id]
        updated_pipeline_information['status'] = 'ERRORED'
        pipelines[pipeline_id] = updated_pipeline_information

        logger.info('SEARCH pipeline=%s status=ERRORED info=%s', pipeline_id, error,
                    exc_info=error)
        logger.info('------------------{}'.format(error))


def _search_no_score_worker(searches, problems, search_id, pipelines, pipeline_id, solutions):
    """
    Similar to score worker, but it does not score and just return the pipeline as valid with a rank

    Parameters
    ----------
    searches: dict
        A shared dict for storing the pipelines related with the search {search_id: list}
    search_id: str
        Search id
    pipelines: dict
        A shared dict that contains all pipelines
        {pipeline_id: {'path': path, 'status': status, 'scores': {'all': scores, 'condensed': scores}}, 'solution_id': solution_id, 'internal_score': internal_score}
    pipeline_id: str
        Pipeline id to be evaluated.
    solutions: dict
        A shared dict that contains all solutions {soludion_id: {'pipeline': pipeline_id, 'problem':problem}}.
        problem: A str that indicates the problem in case exist, othewise None.
    """

    # Update status Running
    logger.info('SEARCH pipeline=%s status=RUNNING NO-SCORE', pipeline_id)

    save_pipeline(pipeline_path=pipelines[pipeline_id]['path'], search_id=search_id, mode='SCORE')

    # Updating pipeline information.
    solution_id = str(uuid.uuid4())
    updated_pipeline_information = pipelines[pipeline_id]
    updated_pipeline_information['status'] = 'COMPLETED'
    updated_pipeline_information['solution_id'] = solution_id

    rank = workers_utils.random_rank()
    updated_pipeline_information['internal_score'] = 1 - rank
    updated_pipeline_information['rank'] = rank

    ranking = {
        'metric': ['RANK'],
        'value': [rank],
        'randomSeed': [0],
        'fold': [0],
    }

    results = pd.DataFrame(ranking)

    updated_pipeline_information['scores'] = {
        'all': results,
        'condensed': pd.DataFrame(columns=['metric', 'value', 'randomSeed', 'fold']),
    }

    # Updating pipeline Infromation on the pipeline dict
    pipelines[pipeline_id] = updated_pipeline_information

    # Creating solution
    solutions[solution_id] = {'problem': problems[search_id], 'pipeline': pipeline_id}

    # logger that everything works
    logger.info('SEARCH pipeline=%s status=COMPLETED', pipeline_id)
    searches[search_id].append(solution_id)


def worker(process_name, jobs, pipelines, solutions, score_solutions, fitted_solutions, request_status,
           request_values, signal_queue, searches, problems, internal_cache, log_queue):
    """
    Defining a worker that should be able to deal with all run tasks.

    Parameters
    ----------
    process_name: str
        A name for the process to be identified.
    jobs: Queue
        A shared Queue containing jobs to be run.
    pipelines: dict
        A shared dict that contains all pipelines {id: {'path': path, 'status': status, 'scores': {scores}}}
    solutions: dict
        A shared dict that contains all solutions {soludion_id: {'pipeline': pipeline_id, 'problem':problem}}.
        problem: A str that indicates the problem in case exist, othewise None.
    score_solutions: multiprocessing.Manager.dict
        A shared dict to store scores of solutions {solution_id: dict(scores)}.
    fitted_solutions: dict
         A shared dict to store the fitted solutions {fitted_solution_id: Runtime}.
    request_status: dict
        A shared dict to store the status of a given request {request_id: {'status':status, 'id':info}}.
    request_values: dict
        A shared dict to store exposed outputs {request_id: {exposed_values}}.
    signal_queue: Queue
        A priority queue to send signals to workers.
    searches: dict
        A shared dict for storing the pipelines related with the search {search_id: multiprocessin.Manager.list()}
    problems: dict
        A shared dict for storing the problems associated with the search {search_id: problem_path}
    log_queue: Queue
        A queue sending log to manager
    """
    qh = logging.handlers.QueueHandler(log_queue)
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    root.addHandler(qh)

    # load all primitives abailable to speed up the process
    with d3m_utils.silence():
        d3m_index.load_all(blocklist=LISTS.BLACK_LIST)

    # starting plasma if possible
    try:
        plasma_client = plasma.connect(EnvVars.PLASMA_SOCKET, num_retries=10)
        logger.info('INIT:PLASMA')
    except:
        plasma_client = None
        logger.info('INIT:PLASMA FAILED')

    try:
        while True:

            if not signal_queue.empty():
                # Getting job
                job = signal_queue.get()
                # A way to send signals
                if job['job_type'] == 'STOP':
                    break

            if not jobs.empty():
                # Getting job
                job = jobs.get()
                # A way to send signals
                if job['job_type'] == 'STOP':
                    break

                if job['job_type'] == 'FIT':
                    job['job_type'] = process_name
                    job['fitted_solutions'] = fitted_solutions
                    job['solutions'] = solutions
                    job['pipelines'] = pipelines
                    job['request_values'] = request_values
                    job['request_status'] = request_status
                    job['plasma_client'] = plasma_client
                    job['internal_cache'] = internal_cache
                    _fit_worker(**job)
                    continue
                elif job['job_type'] == 'PRODUCE':
                    job['job_type'] = process_name
                    job['fitted_solutions'] = fitted_solutions
                    job['request_values'] = request_values
                    job['request_status'] = request_status
                    job['plasma_client'] = plasma_client
                    job['internal_cache'] = internal_cache
                    _produce_worker(**job)
                    continue
                elif job['job_type'] == 'SCORE':
                    job['job_type'] = process_name
                    job['score_solutions'] = score_solutions
                    job['solutions'] = solutions
                    job['pipelines'] = pipelines
                    job['request_status'] = request_status
                    job['plasma_client'] = plasma_client
                    job['internal_cache'] = internal_cache
                    _score_worker(**job)
                    continue
                elif job['job_type'] == 'SEARCH':
                    job['job_type'] = process_name
                    job['searches'] = searches
                    job['problems'] = problems
                    job['pipelines'] = pipelines
                    job['solutions'] = solutions
                    job['plasma_client'] = plasma_client
                    job['internal_cache'] = internal_cache
                    _search_score_worker(**job)
                    continue
                elif job['job_type'] == 'SEARCH-FULL':
                    job['job_type'] = process_name
                    job['searches'] = searches
                    job['problems'] = problems
                    job['pipelines'] = pipelines
                    job['solutions'] = solutions
                    job['plasma_client'] = plasma_client
                    job['internal_cache'] = internal_cache
                    _search_score_worker(**job)
                    continue
                elif job['job_type'] == 'SEARCH-NO-SCORE':
                    job['job_type'] = process_name
                    _search_no_score_worker(searches, problems, job['search_id'], pipelines, job['pipeline_id'], solutions)
                    continue
    except KeyboardInterrupt:
        return


def _create_search_job(search_id, pipeline_id, inputs, performance_metrics, configuration, data_preparation_id):
    """
    Add a search job to the job Queue

    Parameters
    ----------
    search_id: str
        Id of the search where the pipeline is generated.
    pipeline_id: str
        Id of the pipeline to run.
    inputs: list[str]?
        List of inputs now supporting D3MDatasets only.
        A dict with user information.
    performance_metrics: dict.
        A dictionary of metics such as {'metric':'ACCURACY'}
    configuration: Union[dict, None]
        A dict containing the evaluation params.(data_params).
    save_path: str
        A path to store the results.

    """
    search_job = {
        'search_id': search_id,
        'job_type': 'SEARCH',
        'pipeline_id': pipeline_id,
        'inputs': inputs,
        'performance_metrics': performance_metrics,
        'configuration': configuration,
        'data_preparation_id': data_preparation_id
    }

    return search_job


def _stop_signal_search(signal_queue):
    """
    Determine if a stop signal were sent.

    Parameters
    ----------
    signal_queue: Queue
        A priority queue to send signals to workers.

    Returns
    -------
        Returns True if a stop signal were sent, False otherwise.
    """
    if not signal_queue.empty():
        # Getting job
        job = signal_queue.get()
        # A way to send signals
        if job['job_type'] == 'STOP_SEARCH':
            return True
    else:
        return False


def _logging_search(search_id, status, *, template=None, generator=None, pipeline_id=None, error_message=None,
                    trace=None):
    message = 'search_id={} status={}'.format(search_id, status)

    if template is not None:
        message += ' template={}'.format(template)

    if pipeline_id is not None:
        if generator is None:
            message += ' SUBMITTED pipeline_id={}'.format(pipeline_id)
        else:
            message += ' SUBMITTED pipeline_id={} {}'.format(pipeline_id, generator)

    if generator is not None and pipeline_id is None:
        message += ' {}'.format(generator)

    if error_message is not None:
        message += ' info={}'.format(error_message)

    if trace is None:
        logger.info(message)
    else:
        logger.info(message, exc_info=trace)


def _full_template_pipeline(pipelines, pipeline_id, pipeline_template_path, search_id, inputs_list,
                            performance_metrics, eval_configuration, jobs, searches_status):
    # adding it to the global pipelines
    pipelines[pipeline_id] = {'path': pipeline_template_path, 'status': 'PENDING'}
    # Create search job description to be executed
    search_job = _create_search_job(search_id, pipeline_id, inputs_list, performance_metrics,
                                    eval_configuration, data_preparation_id=None)

    # Adding the job to the queue
    jobs.put(search_job)
    _logging_search(search_id, 'RUNNING', pipeline_id=pipeline_id)

    # Wait until the pipeline execution is done.
    while True:
        if _has_pipeline_finished(pipelines, pipeline_id):
            break
        time.sleep(1)

    # Updating Search status
    if pipelines[pipeline_id]['status'] == 'ERRORED':
        _search_errored(searches_status, search_id, 'Complete Pipeline has failed')
    else:
        _search_completed(searches_status, search_id)
    return


def _get_metata_template_pipeline(problem_path, pipeline_template, inputs):
    # We run the template to get the metadata of the outputs.
    problem = load_problem(problem_path)

    # we create a copy of the template so we don't mess up the original
    _pipeline_template = copy.deepcopy(pipeline_template)

    # get the pipeline stepholder index
    placeholder_index = 0
    for i in range(0, len(_pipeline_template.steps)):
        if isinstance(_pipeline_template.steps[i], pipeline_module.PlaceholderStep):
            placeholder_index = i
            break
    _pipeline_template.outputs = list()

    # removing existing outputs
    expose_outputs = list()

    # getting the inputs of stepholder.
    for i in range(0, len(pipeline_template.steps[placeholder_index].inputs)):
        expose_outputs.append(pipeline_template.steps[placeholder_index].inputs[i])
        _pipeline_template.add_output(data_reference=pipeline_template.steps[placeholder_index].inputs[i])

    # check if expose outptus is nothing
    if not len(expose_outputs):
        expose_outputs = None

    # removing the stepholder
    del _pipeline_template.steps[placeholder_index]

    with d3m_utils.silence():
        runtime = runtime_module.Runtime(pipeline=_pipeline_template, problem_description=problem,
                                         context=Context.EVALUATION, volumes_dir=EnvVars.D3MSTATICDIR)
    result = runtime.fit(inputs=inputs, return_values=expose_outputs)

    metadata_results = dict()
    for key, value in result.values.items():
        metadata_results[key] = value.metadata

    return metadata_results[expose_outputs[-1]]


def _get_pipeline_generator_params(problem_doc_uri, inputs_metadata, save_path, main_resource, metafeatures=None,
                                   template_metadata=None, expected_num_iter=10, pipeline_template_uri=None,
                                   primitives_blacklist=None):
    pipeline_generator_params = {
        'problem_doc_uri': problem_doc_uri,
        'inputs_metadata': inputs_metadata,
        'save_path': save_path,
        'main_resource': main_resource,
        'metafeatures': metafeatures,
        'template_metadata': template_metadata,
        'expected_num_iter': expected_num_iter,
        'pipeline_template_uri': pipeline_template_uri,
        'primitives_blacklist': primitives_blacklist,
    }
    return pipeline_generator_params


def _search_completed(searches_status, search_id):
    searches_status[search_id] = {'status': 'COMPLETED', }
    _logging_search(search_id, 'COMPLETED')


def _search_errored(searches_status, search_id, message, trace=None):
    searches_status[search_id] = {'status': 'ERRORED', 'info': message}
    _logging_search(search_id, 'ERRORED', error_message=message, trace=trace)


def _has_pipeline_finished(pipelines, pipeline_id):
    return pipelines[pipeline_id]['status'] == 'COMPLETED' or pipelines[pipeline_id]['status'] == 'ERRORED'


def _get_pipelines_from_generator(search_id, jobs, pipelines, pipeline_generator, n_pipelines, time_left,
                                  inputs_list, performance_metrics, eval_configuration, data_preparation_id,
                                  pipelines_generated, pipelines_generated_per_iteration, pipelines_times,
                                  problem_path):

    generator_name = pipeline_generator.__class__.__name__
    new_pipelines = pipeline_generator.get_pipelines(
            num_pipelines=n_pipelines, time_left=time_left)

    _problem = load_problem(problem_path)
    task_des = schemas_utils.get_task_des(_problem['problem']['task_keywords'])
    skip_pipeline = False

    if task_des['task_type'] in KEYWORDS_TO_SKIP:
        skip_pipeline = True

    # iterate over pipelines to be added on the queue.
    for pipeline_id, pipeline_info in new_pipelines.items():
        # adding status
        info = pipeline_info.copy()
        info['status'] = 'PENDING'

        # adding pipeline to the queue.
        pipelines[pipeline_id] = info

        # Create search job description to be executed
        search_job = _create_search_job(search_id, pipeline_id, inputs_list,
                                        performance_metrics,
                                        eval_configuration, data_preparation_id)
        if skip_pipeline:
            search_job['job_type'] = 'SEARCH-NO-SCORE'

        # Adding the job to the queue
        jobs.put(search_job)

        # saving the pipeline id for the iteration.
        pipelines_generated.append(pipeline_id)
        pipelines_generated_per_iteration[generator_name].append(pipeline_id)
        pipelines_times[pipeline_id] = time.time()

        _logging_search(search_id, 'RUNNING', generator=generator_name, pipeline_id=pipeline_id)


def _search_worker(job_type, jobs, search_jobs, pipelines, signal_queue, stop_search, searches, searches_status,
                   search_id, time_bound, priority, allowed_value_types, problem_path, pipeline_template_path,
                   inputs_list, performance_metrics, eval_configuration, save_path, internal_cache, plasma_client,
                   search_strategy):
    """
    Defining a worker that run the searches.

    Parameters
    ----------
    jobs: Queue
        A shared Queue containing jobs to be run.
    process_name: str
        A name for the process to be identified.
    search_jobs: Queue
        A shared queue that keeps track of the execution of the searches, if there are multiple.
    pipelines: dict
        A shared dict that contains all pipelines {id: {'path': path, 'status': status, 'scores': {scores}}}
    signal_queue: Queue
        A priority queue to send signals to workers.
    searches: dict
        A shared dict for storing the pipelines related with the search {search_id: multiprocessin.Manager.list()}
    searches_status: dict
        A shared dict for storing and update the status of the searches {search_id: status}
    time_bound: int
        Time limit for the search on seconds.
    priority: int
        Priority of the search, higher it has more priority.
    allowed_value_typesL list[str/enum]
        A list that determines which kind of values we accepts and we will write the outputs.
    problem: dict
        A problem on d3m.schema format.
    pipeline_template: d3m.metatada.pipeline.Pipeline()
        A pipeline already on d3m format on Memory.
    inputs: list[raw, dataset_uri, csv_uri].
        A list containing the inputs to use, might be different types, see ta3-2 (value.proto) api for reference
    save_path: str
        A path where all the temporary files are going to be written to.
    plasma_client: Union[plasma_client, None]
        A plasma client that it would potentially be used for cache.
    seach_strategy: list[Union[PipelineGeneratorBase, BasePipelineTuner]]
        A variable to define which strategy or strategies to use during the search.
    save_path: str
        A path where all the temporary files are going to be written to.
    """

    _logging_search(search_id, 'RUNNING')

    # Updating Search status
    searches_status[search_id] = {'status': 'RUNNING', }

    time_tracker = TimeTracker(time_bound, 0.85)

    # Determine the type of the template
    if pipeline_template_path is None:
        template_type = 'NO_TEMPLATE'
    else:
        # Load the pipeline
        pipeline_template = load_pipeline(pipeline_template_path)

        pipeline_id = pipeline_template.id
        if pipeline_template.has_placeholder():
            template_type = 'TEMPLATE'
        else:
            template_type = 'FULL_TEMPLATE'

    _logging_search(search_id, 'RUNNING', template=template_type)

    # Work with cases
    # Case where we only need to run the pipeline that they are asking for.
    if template_type == 'FULL_TEMPLATE':
        _full_template_pipeline(pipelines, pipeline_id, pipeline_template_path, search_id, inputs_list,
                                performance_metrics, eval_configuration, jobs, searches_status)
        return

    # Creating base parameters for pipeline_generators
    # Refactor this path.
    search_path = SearchPath(search_id=search_id)
    save_path = search_path.pipelines_searched
    try:
        inputs = get_data(inputs_list, plasma_client)
    except Exception as e:
        _search_errored(searches_status, search_id, str(e))
        return

    # check if is a standard pipeline, if not, we return an error,
    # only standard pipelines with single input are supported for search.
    if len(inputs) > 1:
        _search_errored(searches_status, search_id, 'Multiple inputs for a search is not supported')
        return

    inputs_list = cache_inputs(inputs, inputs_list, plasma_client)
    data_preparation_id = cache_data_prep(problem_path, inputs, eval_configuration, plasma_client)

    template_metadata = None
    if template_type == 'TEMPLATE':
        template_metadata = _get_metata_template_pipeline(problem_path, pipeline_template, inputs)

    pipeline_generator_params = _get_pipeline_generator_params(
        problem_path, [inputs[-1].metadata], save_path, workers_utils.get_tabular_resource_id(dataset=inputs[-1]),
        template_metadata=template_metadata, pipeline_template_uri=pipeline_template_path)

    time_tracker.update()

    # Adds DataUg Part, we could do something like this for AK
    _pipeline_generators = search_strategy
    _problem = load_problem(problem_path)
    _data_augmentation = False
    if 'data_augmentation' in _problem:
        _data_aug_params = copy.deepcopy(pipeline_generator_params)
        _data_aug_params['inputs_metadata'] = inputs
        _data_augmentation = True

    try:
        pipeline_generators = [generator(**pipeline_generator_params) for generator in
                               _pipeline_generators]
        if _data_augmentation:
            logger.info('Hell yeah Data Augmentation')
            # pipeline_generators.append(DataAugmentationGenerator(**_data_aug_params))

    except Exception as e:
        _search_errored(searches_status, search_id, str(e), e)
        return

    time_tracker.update()

    # variable to keep track
    pipelines_generated = []

    num_generators_left = len(pipeline_generators)
    generators_no_pipelines_left = []

    # A variable to keep track about the longest time spent on running a pipeline iteration
    longest_pipelines_iteration = 0

    pipelines_times = {}
    pipelines_generated_per_iteration = {}

    # max number of pipelines per iteration
    num_pipelines = int(EnvVars.D3MCPU / 2)

    first_batch = num_pipelines
    first_batch_times = []

    while True:
        is_search_stop = False

        if not stop_search.empty():
            # Getting job
            job = stop_search.get()
            # A way to send signals
            if job['search_id'] == search_id:
                is_search_stop = True
            else:
                stop_search.put(job)

        if not time_tracker.time_left():
            is_search_stop = True

        # if the search has not been stopped yet
        if not is_search_stop:
            max_num_pipelines = int(num_pipelines / num_generators_left)
            if max_num_pipelines < 1:
                max_num_pipelines = 1

            for i in range(0, len(pipeline_generators)):
                time_tracker.update()

                if not time_tracker.time_left():
                    break

                generator_name = pipeline_generators[i].__class__.__name__
                n_pipelines_to_generate = max_num_pipelines
                # if the generator cannot generate more pipelines, we skip it.
                if generator_name not in generators_no_pipelines_left:
                    # Ask if we don't know  if this generator does not have pipelines.
                    if pipeline_generators[i].pipeline_left:
                        # check
                        if generator_name in pipelines_generated_per_iteration:
                            n_pipelines_to_generate = \
                                n_pipelines_to_generate - len(pipelines_generated_per_iteration[generator_name])
                        else:
                            pipelines_generated_per_iteration[generator_name] = []

                        if n_pipelines_to_generate > 0:
                            if not time_tracker.enough_time(longest_pipelines_iteration):
                                break

                            try:
                                _get_pipelines_from_generator(search_id, jobs, pipelines, pipeline_generators[i],
                                                              n_pipelines_to_generate, time_tracker._time_left,
                                                              inputs_list, performance_metrics, eval_configuration,
                                                              data_preparation_id, pipelines_generated,
                                                              pipelines_generated_per_iteration, pipelines_times,
                                                              problem_path)

                            except Exception as e:
                                _logging_search(search_id, 'ERRORED Disabling', generator=generator_name,
                                                error_message=str(e), trace=e)
                                pipeline_generators[i].pipeline_left = False

                        # we check again for optimization
                        if not pipeline_generators[i].pipeline_left:
                            if generator_name in pipelines_generated_per_iteration:
                                del pipelines_generated_per_iteration[generator_name]
                            generators_no_pipelines_left.append(generator_name)
                            num_generators_left -= 1
                    else:
                        if generator_name in pipelines_generated_per_iteration:
                            del pipelines_generated_per_iteration[generator_name]
                        generators_no_pipelines_left.append(generator_name)
                        num_generators_left -= 1

        time_tracker.update()

        # if we detect an stop signal change time_left to 0
        if _stop_signal_search(signal_queue):
            time_tracker._time_left = 0

        # case when we don't generate more pipelines
        if len(pipeline_generators) == len(generators_no_pipelines_left) or not time_tracker.enough_time(
                longest_pipelines_iteration):
            message = 'Next search iteration could be over time {} next {}'.format(time_tracker._time_left,
                                                                                   longest_pipelines_iteration)
            _logging_search(search_id, 'RUNNING', error_message=message)
            if len(pipelines_generated) == 0:
                _search_errored(searches_status, search_id, 'TIMEOUT No pipelines founded')
                return
            else:
                # if there is time left we wait for some pipelines to finish
                if time_tracker.time_left():
                    # we wait until all of  running pipelines finish
                    while True:
                        time_tracker.update()
                        if not time_tracker.time_left():
                            break
                        if pipelines_times:
                            pipelines_id_to_remove = []
                            for pipeline_id in pipelines_times.keys():
                                if _has_pipeline_finished(pipelines, pipeline_id):
                                    pipelines_id_to_remove.append(pipeline_id)
                            for pipeline_id in pipelines_id_to_remove:
                                del pipelines_times[pipeline_id]
                            time.sleep(1)
                        else:
                            break
                        if not time_tracker.time_left():
                            break

                is_there_solution = False
                # check if we have at least one solution
                for pipeline_id in pipelines_generated:
                    if 'solution_id' in pipelines[pipeline_id]:
                        is_there_solution = True
                        break
                # if we have at least one solution the search was good.
                if is_there_solution:
                    _search_completed(searches_status, search_id)
                    return

                # if not we timeout :(
                else:
                    _search_errored(searches_status, search_id, 'TIMEOUT No solutions founded')
                    return

        time_tracker.update()

        # Case where we don't have more time left
        if not time_tracker.time_left() or not pipelines_times:
            is_there_solution = False
            # check if we have at least one solution
            for pipeline_id in pipelines_generated:
                if 'solution_id' in pipelines[pipeline_id]:
                    is_there_solution = True
                    break
            # if we have at least one solution the search was good.
            if is_there_solution:
                _search_completed(searches_status, search_id)
                return

            # if not we timeout :(
            else:
                _search_errored(searches_status, search_id, 'TIMEOUT No solutions founded')
                return

        # update knowledge
        pipelines_id_to_remove = []
        for pipeline_id in pipelines_times.keys():
            if _has_pipeline_finished(pipelines, pipeline_id):
                pipelines_id_to_remove.append(pipeline_id)

                if first_batch > 0:
                    pipeline_iteration_time = time.time() - pipelines_times[pipeline_id]
                    longest_pipelines_iteration = pipeline_iteration_time
                    first_batch_times.append(pipeline_iteration_time)
                    first_batch -= 1
                    if first_batch == 0:
                        longest_pipelines_iteration = sum(first_batch_times) / len(first_batch_times)

                # update the generators.
                with d3m_utils.silence():
                    for j in range(0, len(pipeline_generators)):
                        try:
                            pipeline_generators[j].update_pipelines({pipeline_id: pipelines[pipeline_id]},
                                                                    time_left=time_tracker._time_left)
                        except Exception as e:
                            _logging_search(search_id, 'ERRORED Disabling',
                                            generator=pipeline_generators[j].__class__.__name__, error_message=str(e),
                                            trace=e)
                            pipeline_generators[j].pipeline_left = False

                for generator_name in pipelines_generated_per_iteration.keys():
                    if pipeline_id in pipelines_generated_per_iteration[generator_name]:
                        pipelines_generated_per_iteration[generator_name].remove(pipeline_id)
                        break

        for pipeline_id in pipelines_id_to_remove:
            del pipelines_times[pipeline_id]

        # if we detect an stop signal change time_left to 0
        if _stop_signal_search(signal_queue):
            time_tracker._time_left = 0

        time.sleep(1)
        time_tracker.update()


def search_worker(process_name, jobs, search_jobs, pipelines, signal_queue, searches, searches_status, stop_search,
                  internal_cache, log_queue):
    """
    Defining a worker that run the searches.

    Parameters
    ----------
    jobs: Queue
        A shared Queue containing jobs to be run.
    process_name: str
        A name for the process to be identified.
    search_jobs: Queue
        A shared queue that keeps track of the execution of the searches, if there are multiple.
    pipelines: dict
        A shared dict that contains all pipelines {id: {'path': path, 'status': status, 'scores': {scores}}}
    signal_queue: Queue
        A priority queue to send signals to workers.
    searches: dict
        A shared dict for storing the pipelines related with the search {search_id: multiprocessin.Manager.list()}
    problems: dict
        A shared dict for storing the problems associated with the search {search_id: problem_path}
    searches_status: dict
        A shared dict for storing and update the status of the searches {search_id: status}
    stop_search: Queue
        A priority queue to send signals to stop a search.
    log_queue: Queue
        A queue sending log to manager
    """
    # load all primitives abailable to speed up the process
    qh = logging.handlers.QueueHandler(log_queue)
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)
    root.addHandler(qh)

    logger.info('INIT:PLASMA')
    with d3m_utils.silence():
        d3m_index.load_all(blocklist=LISTS.BLACK_LIST)

    # starting plasma if possible
    try:
        plasma_client = plasma.connect(EnvVars.PLASMA_SOCKET, num_retries=10)
        logger.info('INIT:PLASMA')
    except:
        plasma_client = None
        logger.info('INIT:PLASMA FAILED')

    try:
        while True:

            if not signal_queue.empty():
                # Getting job
                job = signal_queue.get()
                # A way to send signals
                if job['job_type'] == 'STOP':
                    break

            if not search_jobs.empty():
                # Getting job
                job = search_jobs.get()

                # A way to send signals
                if job['job_type'] == 'STOP':
                    break

                job['job_type'] = process_name
                job['jobs'] = jobs
                job['search_jobs'] = search_jobs
                job['pipelines'] = pipelines
                job['signal_queue'] = signal_queue
                job['searches'] = searches
                job['searches_status'] = searches_status
                job['stop_search'] = stop_search
                job['plasma_client'] = plasma_client
                job['internal_cache'] = internal_cache

                _search_worker(**job)
    except KeyboardInterrupt:
        return
