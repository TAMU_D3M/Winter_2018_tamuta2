import logging
import math
import pickle
import queue
import threading
import uuid
import multiprocessing
from d3m.metadata.base import Context
from d3m import runtime as runtime_module

from modules.manager.workers import UTILS_PIPELINES, get_data
from modules.manager.workers import worker, search_worker
from modules.utils import schemas_utils
from modules.utils.constants import Path, EnvVars
from modules.utils import file_utils
from modules.utils.pipeline_utils import save_pipeline, load_pipeline

from modules.pipeline_generator.models.random_generator import RandomPipelineGenerator
from modules.pipeline_generator.models.default_generator import DefaultPipelineGenerator
from modules.pipeline_generator.models.random_pipeline_tuner import RandomPipelineTuner

logger = logging.getLogger(__name__)

PIPELINE_GENERATORS = [
    RandomPipelineGenerator,
    DefaultPipelineGenerator,
    # RandomPipelineTuner,
]


class ExecutionManger:
    """
    Manages resources and task scheduling

    Parameters
    ----------
    n_processes: int
        Number of process allowed to use (min 3)

    Attributes
    ----------
    manager: multiprocessing.Manager
        Manages all the distributed resources.
    jobs: multiprocessing.Manager.Queue
        Keep track of the process to be executed.
    pipelines: multiprocessing.Manager.dict
        A shared dict that contains all pipelines {pipeline_id: {'path': path, 'status': status, 'scores': {'all': scores, 'condensed': scores}}}.
    solutions: multiprocessing.Manager.dict
        A shared dict that contains all solutions {soludion_id: {'pipeline': pipeline_id, 'problem':problem}}.
        problem: A str that indicates the problem in case exist, othewise None.
    score_solutions: multiprocessing.Manager.dict
        A shared dict to store scores of solutions {solution_id: dict(scores)}.
    fitted_solutions: multiprocessing.Manager.dict
        A shared dict to store the fitted solutions {fitted_solution_id: Runtime}.
    request_status: multiprocessing.Manager.dict
        A shared dict to store the status of a given request {request_id: {'status':status, 'id':info}}.
    request_values: multiprocessing.Manager.dict
        A shared dict to store exposed outputs {request_id: {exposed_values}}.
    signal_queue: multiprocessing.Manager.Queue
        A priority queue to send signals to workers.
    stop_search_queue: multiprocessing.Manager.Queue
        A priority queue to send signals to stop a search.
    workers: list[multiprocessing.Process]
        A list of all worker available.
    searches: multiprocessing.Manager.dict
        A shared dict for storing the pipelines related with the search {search_id: multiprocessin.Manager.list()}
    problems: multiprocessing.Manager.dict
        A shared dict for storing the problems associated with the search {search_id: problem_path}
    searches_status: multiprocessing.Manager.dict
        A shared dict for storing and update the status of the searches {search_id: status}
    searches_details: multiprocessing.Manager.dict
        A shared dictionary containing details about the search. {search_id: {time_bound, priority, allowed_value_types, problem, pipeline_template, inputs}}
    search_jobs: multiprocessing.Manager.Queue
        Keep track of the execution of the searches, if there are multiple.
    search_scores_info: multiprocessing.Manager.dict
        Keep track of the targets and the dataset if of the search if exists.
    n_workers: int
        Number of multipurpose workers
    n_search_wokers: int
        Number of search workers
    """

    def __init__(self, n_processes=3, process_type='spawn'):
        ctx = multiprocessing.get_context(process_type)
        self.manager = ctx.Manager()
        self.jobs = self.manager.Queue()
        self.pipelines = self.manager.dict()
        self.solutions = self.manager.dict()
        self.score_solutions = self.manager.dict()
        self.fitted_solutions = self.manager.dict()
        self.request_status = self.manager.dict()
        self.request_values = self.manager.dict()
        self.signal_queue = self.manager.Queue()
        self.stop_search_queue = self.manager.Queue()

        # an internal multiprocessing dict to handle cases when plasma fails or is not present at all.
        self.internal_cache = self.manager.dict()

        # SEARCHES.Be careful with this, we will be creating proxies inside this
        # proxy not sure of multiprocessing can handle this correctly.
        self.searches = self.manager.dict()
        self.problems = self.manager.dict()

        self.searches_status = self.manager.dict()
        self.searches_details = self.manager.dict()

        # Keep track of different searchs
        self.search_jobs = self.manager.Queue()

        self.search_scores_info = self.manager.dict()

        self.n_workers = math.floor((n_processes - 1) * 0.8)
        self.n_search_wokers = n_processes - 1 - self.n_workers

        self.log_queue = self.manager.Queue()
        self._is_closed = False
        self._log_thread = threading.Thread(target=self._handle_log, name='log_thread')
        self._log_thread.daemon = True
        self._log_thread.start()

        self.workers = []
        for i in range(0, self.n_workers):
            process_name = 'WORKER-RUN-' + str(i)
            process = ctx.Process(
                name=process_name,
                target=worker,
                args=(
                    process_name, self.jobs, self.pipelines, self.solutions, self.score_solutions,
                    self.fitted_solutions, self.request_status, self.request_values, self.signal_queue,
                    self.searches, self.problems, self.internal_cache, self.log_queue
                )
            )

            self.workers.append(process)

        self.search_workers = []
        for i in range(0, self.n_search_wokers):
            process_name = 'WORKER-SEARCH-' + str(i)
            process = ctx.Process(
                name=process_name,
                target=search_worker,
                args=(
                    process_name, self.jobs, self.search_jobs, self.pipelines, self.signal_queue,
                    self.searches, self.searches_status, self.stop_search_queue, self.internal_cache, self.log_queue
                )
            )
            self.search_workers.append(process)

    def start_workers(self):
        """
        Starts all the workers on Daemon mode.
        """
        logger.info('INIT WORKERS')
        for i in range(0, len(self.workers)):
            self.workers[i].daemon = True
            self.workers[i].start()

        for i in range(0, len(self.search_workers)):
            self.search_workers[i].daemon = True
            self.search_workers[i].start()

    def stop_workers(self):
        """
        Stops all the workers.
        """
        self._is_closed = True

        # Sending a bunch of stop signals
        for i in range(0, len(self.workers)):
            self.signal_queue.put({'job_type': 'STOP'})

        for i in range(0, len(self.search_workers)):
            self.signal_queue.put({'job_type': 'STOP'})

        for i in range(0, len(self.workers)):
            self.workers[i].join(timeout=1.0)
            self.workers[i].terminate()
            logger.info('STOPING WORKER:%s', self.workers[i].name)

        for i in range(0, len(self.search_workers)):
            self.search_workers[i].join(timeout=1.0)
            self.search_workers[i].terminate()
            logger.info('STOPING WORKER:%s', self.workers[i].name)

        logger.info('ALL WORKERS STOP')
        self._log_thread.join(5.0)  # Waits for receive queue to empty.

    def add_fit_job(self, solution_id, inputs, expose_outputs, expose_value_types,
                    users, save_path=Path.OTHER_OUTPUTS):
        """
        Add a fit job to the job Queue

        Parameters
        ----------
        solution_id: str
            Id of solution to be fitted.
        inputs: list[str]?
            List of inputs now supporting D3MDatasets only.
        expose_outputs: list[str]
            List of steps results to expose.
        expose_value_types: list[str]
            List of types to use for exporting the results.
        users: dict
            A dict with user information.
        save_path: str
            A path to store the results.

        Returns
        -------
        request_id: str
            An id that represents this request.
        """

        request_id = str(uuid.uuid4())
        fit_job = {
            'job_type': 'FIT',
            'request_id': request_id,
            'solution_id': solution_id,
            'inputs': inputs,
            'expose_outputs': expose_outputs,
            'expose_value_types': expose_value_types,
            'users': users,
            'save_path': save_path,
        }
        # Updating the status of the request
        self.request_status[request_id] = {'status': 'PENDING'}

        # Adding the job to the queue
        self.jobs.put(fit_job)
        logger.info('FIT-SOLUTION solution_id=%s, request_id%s, status=PENDING',
                    solution_id, request_id)
        return request_id

    def add_produce_job(self, fitted_solution_id, inputs, expose_outputs,
                        expose_value_types, users, save_path=Path.OTHER_OUTPUTS):
        """
        Add a produce job to the job Queue

        Parameters
        ----------
        fitted_solution_id: str
            Id of fitted solution to be use for producing.
        inputs: list[str]?
            List of inputs now supporting D3MDatasets only.
        expose_outputs: list[str]
            List of steps results to expose.
        expose_value_types: list[str]
            List of types to use for exporting the results.
        users: dict
            A dict with user information.
        save_path: str
            A path to store the results.

        Returns
        -------
        request_id: str
            An id that represents this request.
        """

        request_id = str(uuid.uuid4())
        produce_job = {
            'job_type': 'PRODUCE',
            'request_id': request_id,
            'fitted_solution_id': fitted_solution_id,
            'inputs': inputs,
            'expose_outputs': expose_outputs,
            'expose_value_types': expose_value_types,
            'users': users,
            'save_path': save_path,
        }

        # Updating the status of the request
        self.request_status[request_id] = {'status': 'PENDING'}

        # Adding the job to the queue
        self.jobs.put(produce_job)
        logger.info('PRODUCE-SOLUTION fitted_solution_id=%s, request_id%s, status=PENDING',
                    fitted_solution_id, request_id)
        return request_id

    def add_score_job(self, solution_id, inputs, users, performance_metrics,
                      configuration, save_path):
        """
        Add a score job to the job Queue

        Parameters
        ----------
        solution_id: str
            Id of solution to be fitted.
        inputs: list[str]?
            List of inputs now supporting D3MDatasets only.
        users: dict
            A dict with user information.
        performance_metrics: dict.
            A dictionary of metics such as {'metric':'ACCURACY'}
        configuration: Union[dict, None]
            A dict containing the evaluation params.(data_params).
        save_path: str
            A path to store the results.

        Returns
        -------
        request_id: str
            An id that represents this request.
        """

        request_id = str(uuid.uuid4())
        score_job = {
            'job_type': 'SCORE',
            'request_id': request_id,
            'solution_id': solution_id,
            'inputs': inputs,
            'performance_metrics': performance_metrics,
            'users': users,
            'configuration': configuration,
        }

        # Updating the status of the request
        self.request_status[request_id] = {'status': 'PENDING'}

        # Adding the job to the queue
        self.jobs.put(score_job)
        logger.info('SCORE-SOLUTION solution_id=%s, request_id%s, status=PENDING',
                    solution_id, request_id)
        return request_id

    def add_search_job(self, search_id, pipeline_id, inputs, performance_metrics, configuration):
        """
        Add a search job to the job Queue

        Parameters
        ----------
        search_id: str
            Id of the search where the pipeline is generated.
        pipeline_id: str
            Id of the pipeline to run.
        inputs: list[str]?
            List of inputs now supporting D3MDatasets only.
            A dict with user information.
        performance_metrics: dict.
            A dictionary of metics such as {'metric':'ACCURACY'}
        configuration: Union[dict, None]
            A dict containing the evaluation params.(data_params).
        save_path: str
            A path to store the results.

        """
        search_job = {
            'search_id': search_id,
            'job_type': 'SEARCH',
            'pipeline_id': pipeline_id,
            'inputs': inputs,
            'performance_metrics': performance_metrics,
            'configuration': configuration,
        }

        # Updating the status of the pipeline scheduled to run.
        self.pipelines[pipeline_id] = dict(**self.pipelines[pipeline_id], **{'status': 'PENDING'})

        # Adding the job to the queue
        self.jobs.put(search_job)
        logger.info('SEARCH-SOLUTION search_id=%s, pipeline_id=%s, status=PENDING',
                    search_id, pipeline_id)

    def add_search(self, time_bound_search, priority, allowed_value_types, problem, pipeline_template, inputs,
                   save_path=Path.TEMP_STORAGE_ROOT, *, search_strategy=PIPELINE_GENERATORS):
        """
        Add and trigger a new search.

        Parameters
        ----------
        time_bound_search: int
            Time limit for the search on seconds.
        priority: int
            Priority of the search, higher it has more priority.
        allowed_value_typesL list[str/enum]
            A list that determines which kind of values we accepts and we will write the outputs.
        problem: dict
            A problem on d3m.schema format.
        pipeline_template: d3m.metatada.pipeline.Pipeline()
            A pipeline already on d3m format on Memory.
        inputs: list[raw, dataset_uri, csv_uri].
            A list containing the inputs to use, might be different types, see ta3-2 (value.proto) api for reference
        save_path: str
            A path where all the temporary files are going to be written to.
        seach_strategy: Union[str]
            A variable to define which strategy or strategies to use during the search.
        """
        search_id = str(uuid.uuid4())

        # Creating a proxy list for a new search
        self.searches[search_id] = self.manager.list()

        # Storing problem and pipeline.

        # TODO fix saving paths.
        problem_path = file_utils.save_problem(problem, search_id, save_path)
        pipeline_template_path = file_utils.save_pipeline_template(pipeline_template, search_id, save_path)
        inputs_list = inputs

        if problem is not None:
            task_des = schemas_utils.get_task_des(problem['problem']['task_keywords'])
            perf = problem['problem'].get('performance_metrics', [])
        else:
            task_des = None
            perf = []

        performance_metrics = schemas_utils.get_metrics_from_task(task_des, perf)
        eval_configuration = schemas_utils.get_eval_configuration(task_des['task_type'], task_des['data_types'],
                                                                  task_des['semi'])
        logger.info('EVAL CONFIG: {}'.format(eval_configuration))
        search_details = {
            'job_type': 'SEARCH',
            'search_id': search_id,
            # TODO propagate name change/
            'time_bound': time_bound_search,
            'priority': priority,
            'allowed_value_types': allowed_value_types,
            'problem_path': problem_path,
            'pipeline_template_path': pipeline_template_path,
            'inputs_list': inputs_list,
            'performance_metrics': performance_metrics,
            'eval_configuration': eval_configuration,
            'search_strategy': search_strategy,
            'save_path': save_path
        }

        self.search_scores_info[search_id] = schemas_utils.get_score_information(problem)
        self.search_jobs.put(search_details)
        self.searches_details[search_id] = search_details
        self.problems[search_id] = problem_path
        self.searches_status[search_id] = {'status': 'PENDING', }

        logger.info(
            'SEARCH search_id={}, status=PENDING, pipeline_generators={}'.format(search_id, [strategy.__name__ for strategy in search_strategy]))

        return search_id

    def _get_search_scores_info(self, problem_path):
        """
        A function that get the target information from problem doc that would be expose to the ta3-2 api later.

        Parameters
        ----------
        problem: Union[dict, None]
            A parsed problem description.

        Returns
        -------
        info: Union[dict, None]
            A dict with targets and dataset, or None if no problem
        """
        if problem_path is not None:
            problem = file_utils.load_json(problem_path)
            return schemas_utils.get_score_information(problem)
        else:
            return None

    def stop_search(self, search_id):
        """
        A function that send a message to search workers to stop a search by id

        Parameters
        ----------
        search_id: str
            An id of the search  to be stop.

        """
        if search_id in self.searches_status:
            status = self.searches_status[search_id]['status']
            if status != 'ERRORED' and status != 'COMPLETED':
                search_stop_message = {
                    'search_id': search_id,
                }

                self.stop_search_queue.put(search_stop_message)
                logger.info('SEARCH search_id=%s, status=STOPING', search_id)

    def rank_solution(self, solution_id, rank):
        """
        A function to store pipeline with rank.

        Parameters
        ----------
        solution_id: str
            An id of a solution to rank.
        rank: float
            A float number that indicates the rank, lower is better.
        """
        pipeline_id = self.solutions[solution_id]['pipeline']

        search_id = None

        for _search_id, solutions in self.searches.items():
            if solution_id in solutions:
                search_id = _search_id

        save_pipeline(pipeline_path=self.pipelines[pipeline_id]['path'],
                      mode='RANK', search_id=search_id, rank=rank)

    def save_solution(self, solution_id):
        pipeline_id = self.solutions[solution_id]['pipeline']
        problem_path = self.solutions[solution_id]['problem']
        pipeline_path = self.pipelines[pipeline_id]['path']
        solution = {
            'solution_id': solution_id,
            'pipeline_id': pipeline_id,
            'problem_path': problem_path,
            'pipeline_path': pipeline_path
        }
        solution_uri = file_utils.save_solution(solution)
        return solution_uri

    def load_solution(self, solution_uri):
        solution = file_utils.load_json(solution_uri)
        solution_id = solution['solution_id']
        pipeline_id = solution['pipeline_id']
        self.solutions[solution_id] = {
            'pipeline': pipeline_id,
            'problem': solution['problem_path']
        }
        self.pipelines[pipeline_id] = {
            'path': solution['pipeline_path']
        }
        return solution_id

    def save_fitted_solution(self, fitted_solution_id):
        fitted_solution = self.fitted_solutions[fitted_solution_id]
        fitted_solution_uri = file_utils.save_fitted_solution(fitted_solution_id, fitted_solution)
        return fitted_solution_uri

    def load_fitted_solution(self, fitted_solution_uri):
        fitted_solution = file_utils.load_json(fitted_solution_uri)
        fitted_solution_id = fitted_solution['fitted_solution_id']
        with open(fitted_solution['runtime_path'], 'rb') as f:
            runtime = pickle.load(f)
        self.fitted_solutions[fitted_solution_id] = {
            'runtime': runtime,
            'pipeline_path': fitted_solution['pipeline_path'],
        }
        return fitted_solution_id

    def score_predictions(self, predictions, score_input, problem, metrics):
        scoring_pipeline = load_pipeline(UTILS_PIPELINES['SCORING'])

        predictions = get_data([[predictions]])[0]
        score_input = get_data([[score_input]])
        scoring_random_seed = 0
        random_seed = 0
        scores, score_result = runtime_module.score(
            predictions, score_input, scoring_pipeline=scoring_pipeline, problem_description=problem,
            metrics=metrics, predictions_random_seed=random_seed, scoring_params=None, context=Context.TESTING,
            random_seed=scoring_random_seed, volumes_dir=EnvVars.D3MSTATICDIR, scratch_dir=None,
            runtime_environment=None
        )
        if not score_result.has_error():
            scores = runtime_module.combine_folds([scores])
        return scores, score_result

    def split_data(self, input_value, scoring_configuration, allowed_value_types, problem_description):
        input_ = get_data([[input_value]])
        data_pipeline_path = UTILS_PIPELINES[scoring_configuration['method']]
        data_pipeline = load_pipeline(data_pipeline_path)

        data_random_seed = 0
        outputs, data_result = runtime_module.prepare_data(
            data_pipeline=data_pipeline, problem_description=problem_description,
            inputs=input_, data_params=scoring_configuration, context=Context.TESTING, random_seed=data_random_seed,
            volumes_dir=EnvVars.D3MSTATICDIR, scratch_dir=None, runtime_environment=None,
        )
        return outputs, data_result

    def _handle_log(self):
        try:
            while not (self._is_closed and self.log_queue.empty()):
                try:
                    record = self.log_queue.get(timeout=0.1)
                    logger = logging.getLogger(record.name)
                    logger.handle(record)
                except queue.Empty:
                    pass
        except KeyboardInterrupt:
            return
