# Copyright 2019 The Keras Tuner Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"Basic random search tuner."

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from . import oracle as oracle_module
from . import trial as trial_lib

import random


class RandomSearchOracle(oracle_module.Oracle):
    """Random search oracle.

    # Arguments:
        objective: String or `kerastuner.Objective`. If a string,
          the direction of the optimization (min or max) will be
          inferred.
        max_trials: Int. Total number of trials
            (model configurations) to test at most.
            Note that the oracle may interrupt the search
            before `max_trial` models have been tested.
        seed: Int. Random seed.
        hyperparameters: HyperParameters class instance.
            Can be used to override (or register in advance)
            hyperparamters in the search space.
        tune_new_entries: Whether hyperparameter entries
            that are requested by the hypermodel
            but that were not specified in `hyperparameters`
            should be added to the search space, or not.
            If not, then the default value for these parameters
            will be used.
        allow_new_entries: Whether the hypermodel is allowed
            to request hyperparameter entries not listed in
            `hyperparameters`.
    """

    def __init__(self,
                 objective,
                 max_trials,
                 seed=None,
                 hyperparameters=None,
                 allow_new_entries=True,
                 tune_new_entries=True):
        super(RandomSearchOracle, self).__init__(
            objective=objective,
            max_trials=max_trials,
            hyperparameters=hyperparameters,
            tune_new_entries=tune_new_entries,
            allow_new_entries=allow_new_entries)
        self.seed = seed or random.randint(1, 1e4)
        # Incremented at every call to `populate_space`.
        self._seed_state = self.seed
        # Hashes of values tried so far.
        self._tried_so_far = set()
        # Maximum number of identical values that can be generated
        # before we consider the space to be exhausted.
        self._max_collisions = 5

    def _populate_space(self, _):
        """Fill the hyperparameter space with values.

        Args:
          `trial_id`: The id for this Trial.

        Returns:
            A dictionary with keys "values" and "status", where "values" is
            a mapping of parameter names to suggested values, and "status"
            is the TrialStatus that should be returned for this trial (one
            of "RUNNING", "IDLE", or "STOPPED").
        """
        collisions = 0
        while 1:
            # Generate a set of random values.
            values = {}
            penalty_hp = None
            solver_hp = None
            multi_class_hp = None
            for p in self.hyperparameters.space:
                values[p.name] = p.sample(self._seed_state)
                # ToDo: workaround when we tune SKLogisticRegression
                if 'logistic_regression.SKlearn' in p.name:
                    if 'penalty' in p.name:
                        penalty_hp = p
                    if 'solver' in p.name:
                        solver_hp = p
                    if 'multi_class' in p.name:
                        multi_class_hp = p
                self._seed_state += 1

            # ToDo: workaround when we tune SKLogisticRegression
            if 'logistic_regression.SKlearn' in p.name:
                # Only 'saga' solver supports elasticnet penalty.
                if values[penalty_hp.name]['choice'] == 'elasticnet' and values[solver_hp.name] != 'saga':
                    values[solver_hp.name] = 'saga'

                if values[solver_hp.name] == 'liblinear':
                    # Solver liblinear does not support a multinomial backend.
                    if values[multi_class_hp.name] == 'multinomial':
                        values[multi_class_hp.name] = 'ovr'
                    # penalty='none' is not supported for the liblinear solver'
                    while values[penalty_hp.name]['choice'] not in ['l2', 'l1']:
                        values[penalty_hp.name] = penalty_hp.sample(self._seed_state)
                        self._seed_state += 1

                # Solver lbfgs, newton-cg, sag supports only 'l2' or 'none' penalties, got l1 penalty."
                while values[solver_hp.name] in ['lbfgs', 'newton-cg', 'sag'] and \
                        values[penalty_hp.name]['choice'] not in ['l2', 'none']:
                    values[penalty_hp.name] = penalty_hp.sample(self._seed_state)
                    self._seed_state += 1

            # Keep trying until the set of values is unique,
            # or until we exit due to too many collisions.
            values_hash = self._compute_values_hash(values)
            if values_hash in self._tried_so_far:
                collisions += 1
                if collisions > self._max_collisions:
                    return {'status': trial_lib.TrialStatus.STOPPED,
                            'values': None}
                continue
            self._tried_so_far.add(values_hash)
            break
        return {'status': trial_lib.TrialStatus.RUNNING,
                'values': values}

    def get_state(self):
        state = super(RandomSearchOracle, self).get_state()
        state.update({
            'seed': self.seed,
            'seed_state': self._seed_state,
            'tried_so_far': list(self._tried_so_far),
        })
        return state

    def set_state(self, state):
        super(RandomSearchOracle, self).set_state(state)
        self.seed = state['seed']
        self._seed_state = state['seed_state']
        self._tried_so_far = set(state['tried_so_far'])
