# Copyright 2019 The Keras Tuner Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"HyperParameters logic."

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import contextlib
import math

import numpy as np
from d3m.metadata import hyperparams


def _check_sampling_arg(sampling,
                        step,
                        min_value,
                        max_value,
                        hp_type='int'):
    if sampling is None:
        return None
    if hp_type == 'int' and step != 1:
        raise ValueError(
            '`sampling` can only be set on an `Int` when `step=1`.')
    if hp_type != 'int' and step is not None:
        raise ValueError(
            '`sampling` and `step` cannot both be set, found '
            '`sampling`: ' + str(sampling) + ', `step`: ' + str(step))

    _sampling_values = {'linear', 'log', 'reverse_log'}
    sampling = sampling.lower()
    if sampling not in _sampling_values:
        raise ValueError(
            '`sampling` must be one of ' + str(_sampling_values))
    if sampling in {'log', 'reverse_log'} and min_value <= 0:
        raise ValueError(
            '`sampling="' + str(sampling) + '" is not supported for '
            'negative values, found `min_value`: ' + str(min_value))
    return sampling

def GET_CONFIG(param_val):
    config = param_val.to_simple_structure()
    config['p'] = param_val
    if isinstance(param_val, hyperparams.SortedList) or isinstance(param_val, hyperparams.SortedSet):
        config['is_configuration'] = param_val.is_configuration
    return config

def _check_int(val, arg):
    int_val = int(val)
    if int_val != val:
        raise ValueError(
            arg + ' must be an int, found: ' + str(val))
    return int_val


class HyperParameters(object):
    """Container for both a hyperparameter space, and current values.

    # Attributes:
        space: A list of HyperParameter instances.
        values: A dict mapping hyperparameter names to current values.
    """

    def __init__(self):
        # A map from full HP name to HP object.
        self._space = {}
        self.values = {}
        self._scopes = []

    @contextlib.contextmanager
    def name_scope(self, name):
        self._scopes.append(name)
        try:
            yield
        finally:
            self._scopes.pop()

    @contextlib.contextmanager
    def conditional_scope(self, parent_name, parent_values):
        """Opens a scope to create conditional HyperParameters.

        All HyperParameters created under this scope will only be active
        when the parent HyperParameter specified by `parent_name` is
        equal to one of the values passed in `parent_values`.

        When the condition is not met, creating a HyperParameter under
        this scope will register the HyperParameter, but will return
        `None` rather than a concrete value.

        Note that any Python code under this scope will execute
        regardless of whether the condition is met.

        # Arguments:
            parent_name: The name of the HyperParameter to condition on.
            parent_values: Values of the parent HyperParameter for which
              HyperParameters under this scope should be considered valid.
        """
        full_parent_name = self._get_name(parent_name)
        if full_parent_name not in self.values:
            raise ValueError(
                '`HyperParameter` named: ' + full_parent_name + ' '
                'not defined.')

        if not isinstance(parent_values, (list, tuple)):
            parent_values = [parent_values]

        parent_values = [str(v) for v in parent_values]

        self._scopes.append({'parent_name': parent_name,
                             'parent_values': parent_values})
        try:
            yield
        finally:
            self._scopes.pop()

    def _conditions_are_active(self, scopes=None):
        if scopes is None:
            scopes = self._scopes

        partial_scopes = []
        for scope in scopes:
            if self._is_conditional_scope(scope):
                full_name = self._get_name(
                    scope['parent_name'],
                    partial_scopes)
                if str(self.values[full_name]) not in scope['parent_values']:
                    return False
            partial_scopes.append(scope)
        return True

    def _retrieve(self,
                  name,
                  type,
                  config,
                  parent_name=None,
                  parent_values=None,
                  overwrite=False):
        """Gets or creates a `HyperParameter`."""
        if parent_name:
            with self.conditional_scope(parent_name, parent_values):
                return self._retrieve_helper(name, type, config, overwrite)
        return self._retrieve_helper(name, type, config, overwrite)

    def _retrieve_helper(self, name, type, config, overwrite=False):
        self._check_name_is_valid(name)
        full_name = self._get_name(name)

        if full_name in self.values and not overwrite:
            # TODO: type compatibility check,
            # or name collision check.
            retrieved_value = self.values[full_name]
        else:
            retrieved_value = self.register(name, type, config)

        if self._conditions_are_active():
            return retrieved_value
        # Sanity check that a conditional HP that is not currently active
        # is not being inadvertently relied upon in the model building
        # function.
        return None

    def register(self, name, type, config):
        full_name = self._get_name(name)
        # config['name'] = full_name
        p = config['p']
        # p = deserialize(config)
        p.name = full_name
        # config = {'class_name': type, 'config': config}
        self._space[full_name] = p
        value = p.get_default()
        self.values[full_name] = value
        return value

    def get(self, name):
        """Return the current value of this HyperParameter."""

        # Fast path: check for a non-conditional param or for a conditional param
        # that was defined in the current scope.
        full_cond_name = self._get_name(name)
        if full_cond_name in self.values:
            if self._conditions_are_active():
                return self.values[full_cond_name]
            else:
                raise ValueError(
                    'Conditional parameter {} is not currently active'.format(
                        full_cond_name))

        # Check for any active conditional param.
        found_inactive = False
        full_name = self._get_name(name, include_cond=False)
        for name, val in self.values.items():
            hp_parts = self._get_name_parts(name)
            hp_scopes = hp_parts[:-1]
            hp_name = hp_parts[-1]
            hp_full_name = self._get_name(
                hp_name,
                scopes=hp_scopes,
                include_cond=False)
            if full_name == hp_full_name:
                if self._conditions_are_active(hp_scopes):
                    return val
                else:
                    found_inactive = True

        if found_inactive:
            raise ValueError(
                'Conditional parameter {} is not currently active'.format(
                    full_cond_name))
        else:
            raise ValueError(
                'Unknown parameter: {}'.format(full_name))

    def __getitem__(self, name):
        return self.get(name)

    def __contains__(self, name):
        try:
            self.get(name)
            return True
        except ValueError:
            return False

    @property
    def space(self):
        return list([hp for hp in self._space.values()])

    def get_config(self):
        return {
            'space': [{'class_name': p.__class__.__name__,
                       'config': GET_CONFIG(p)}
                      for p in self._space.values()],
            'values': dict((k, v) for (k, v) in self.values.items()),
        }

    @classmethod
    def from_config(cls, config):
        hp = cls()
        for p in config['space']:
            # p = deserialize(p['config'])
            p = p['config']['p']
            hp._space[p.name] = p
        hp.values = dict((k, v) for (k, v) in config['values'].items())
        return hp

    def copy(self):
        return HyperParameters.from_config(self.get_config())

    # def merge(self, hps, overwrite=True):
    #     """Merges hyperparameters into this object.
    #
    #     Arguments:
    #       hps: A `HyperParameters` object or list of `HyperParameter`
    #         objects.
    #       overwrite: bool. Whether existing `HyperParameter`s should
    #         be overridden by those in `hps` with the same name.
    #     """
    #     if isinstance(hps, HyperParameters):
    #         hps = hps.space
    #     for hp in hps:
    #         config = GET_CONFIG(hp)
    #         self._retrieve(
    #             hp.name,
    #             config['type'],
    #             config,
    #             overwrite=overwrite)

    def _get_name(self, name, scopes=None, include_cond=True):
        """Returns a name qualified by `name_scopes`."""
        if scopes is None:
            scopes = self._scopes

        scope_strings = []
        for scope in scopes:
            if self._is_name_scope(scope):
                scope_strings.append(scope)
            elif self._is_conditional_scope(scope) and include_cond:
                parent_name = scope['parent_name']
                parent_values = scope['parent_values']
                scope_string = '{name}={vals}'.format(
                    name=parent_name,
                    vals=','.join([str(val) for val in parent_values]))
                scope_strings.append(scope_string)
        return '/'.join(scope_strings + [name])

    def _get_name_parts(self, full_name):
        """Splits `full_name` into its scopes and leaf name."""
        str_parts = full_name.split('/')
        parts = []

        for part in str_parts:
            if '=' in part:
                parent_name, parent_values = part.split('=')
                parent_values = parent_values.split(',')
                parts.append({'parent_name': parent_name,
                              'parent_values': parent_values})
            else:
                parts.append(part)

        return parts

    def _check_name_is_valid(self, name):
        if '/' in name or '=' in name or ',' in name:
            raise ValueError(
                '`HyperParameter` names cannot contain "/", "=" or "," '
                'characters.')

        for scope in self._scopes[::-1]:
            if self._is_conditional_scope(scope):
                if name == scope['parent_name']:
                    raise ValueError(
                        'A conditional `HyperParameter` cannot have the same '
                        'name as its parent. Found: ' + str(name) + ' and '
                        'parent_name: ' + str(scope['parent_name']))
            else:
                # Names only have to be unique up to the last `name_scope`.
                break

    def _is_name_scope(self, scope):
        return isinstance(scope, str)

    def _is_conditional_scope(self, scope):
        return (isinstance(scope, dict) and
                'parent_name' in scope and 'parent_values' in scope)


def deserialize(config):
    # Bugs when deserializing Union
    cls = config['type']
    structural_type = config['structural_type']
    del config['type']
    del config['structural_type']
    return cls[structural_type](**config)
