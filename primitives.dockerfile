FROM registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.5.18-20200630-050709

COPY requirements.txt /tmp

ENV D3MRUN ta2ta3

RUN apt update
RUN apt-get install -y graphviz libgraphviz-dev pkg-config
RUN pip3 install -r /tmp/requirements.txt
