# How to submit.

# Regular version.

All the scripts are already implemented and the only thing that needs to be done
is update the base image according to the requirements.
The base image should be replaced on `primitives.dockerfile` on the first line:
```bash
FROM [New base primitives image]

COPY requirements.txt /tmp

ENV D3MRUN ta2ta3

RUN apt update
RUN apt-get install -y graphviz libgraphviz-dev pkg-config
RUN pip3 install -r /tmp/requirements.txt
```

Also, we need to update ta3-2 api dependency according to the requirements on 
`requirements.txt`. The hash git has commit needs to be updated

```bash
git+https://gitlab.com/datadrivendiscovery/ta3ta2-api.git@[HASH to be update]
```

After this, we need to update the registy to be submitted to on `build-images.sh`

```bash
if [ "$IMAGE_NAME" = "complete" ]; then
    echo "Bulding "$DOCKER_IMAGE:$IMAGE_NAME""
    docker build -t [Registry to update] -f complete.dockerfile .
    echo "Pushing [Registry to update]"
    docker push [Registry to update]
    echo "Done"
fi
```

And after this, CI is taking care of everything, and making the testing 
image on `devel` branch and push into `dmartinez05/tamuta2:latest`
and `master` branch is generating the complemte image to be pushed at on 
registry `[Registry to update]`.

# Autokeras version.

The same updates apply to the Autokeras, but the `build-image.sh`
needs to be updated on:

```bash
if [ "$IMAGE_NAME" = "autokeras_deployment" ]; then
    echo "Bulding "$DOCKER_IMAGE:$IMAGE_NAME""
    docker build -t [Autokeras registry] -f complete.dockerfile .
    echo "Pushing [Autokeras registry]"
    docker push [Autokeras registry]
    echo "Done"
fi
```

Then, it is necessary to switch to `autokeras` branch and locally
 run
```bash


$IMAGE_NAME="autokeras_deployment" bash build-images.sh


```

