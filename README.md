# D3M
The Data Driven Discovery of Models project repository

## Prerequisites
- [Docker](https://docs.docker.com/v17.12/install/)
- [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Git-LFS](https://github.com/git-lfs/git-lfs/wiki/Installation)


## Environment Setup
First, clone the [repository](https://gitlab.com/TAMU_D3M/Winter_2018_tamuta2)

#### Option 1: Use the docker environment
1. Change directory into the cloned repository
2. Run:
```
sudo docker run --rm -t -i --shm-size=10gb \
--entrypoint=bin/bash -p 45042:45042 -p 8888:8888 \
--volume "$(pwd):/D3M" -e D3MINPUTDIR='/D3M/datasets' \
 -e D3MPROBLEMPATH='/D3M/datasets/seed_datasets_current/185_baseball/185_baseball_problem/problemDoc.json' \
 -e D3MOUTPDUTDIR='/D3M/output_dir/' registry.gitlab.com/tamu_d3m/winter_2018_tamuta2/primitives:latest
```
3. A new Linux shell will pop out. You can see the repository content in `/D3M`, change directory to the repository
 ```cd /D3M```
## Build Your Own Image: 
You can also build your own image. Use `primitives.dockerfile` for access full primitives environment.
```docker build -t {NAME_TO_NAME_YOUR_IMAGE} -f primitives.dockerfile .```. You can even change the version of primitive by changing the base image in primitive.dockerfile.
## Run
- Start the server in the Linux shell that you just spawn using the docker command 
```python3 -m modules.server.refactor_server```
- Run test in the same shell
    - Attach a new shell in the running docker container ```docker exec -i -t {ID_OF_YOUR_CONTAINER} /bin/bash```. To find out your container ID, you can do ```docker ps -la```
    - Then run the server test. ```python3 -m modules.server.server_test -t /D3M/datasets/38/```
- Results are in output_dir/
#### Optional
You can run the system without starting server first then run server test using the standalone.py script. 
```python3 -m modules.standalone -p {PATH_TO_PROBLEM_FILE} -d {PATH_TO_DATASET_DIRECTORY} -t {TIME_LIMIT}```

## Run Pipeline
```
python3 -m d3m runtime fit-produce -p {PIPELINE_FILE.json} \
   -r {PATH_TO_PROBLEM}/problemDoc.json \
   -i {TRAINING_DATASET_PATH}/datasetDoc.json \
   -t {TEST_DATASET_PATH}/datasetDoc.json \
   -o {PATH_TO_STORE_OUTPUT}/results.csv \
   -O {PATH_TO_PIPELINE_RUN}/pipeline_run.yml
```

To get more info you can type:  ```python3 -m d3m runtime```. You can also do ```python3 -m d3m runtime fit-produce --help``` to see all the parameter available for a certain command.

## Extar
For how to test/evaluate pipeline, you can refer to the tutorial [notebook](https://gitlab.com/TAMU_D3M/Winter_2018_tamuta2/blob/devel/pipeline_and_run_pipeline.ipynb) 
