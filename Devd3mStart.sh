#!/bin/bash

alias python="python3"

# check if we are on a deployment container or not.
if [ -d "/user_dev" ]; then
  cd  /user_dev
  echo "Running on deployment"
else
  echo "Running on testing"
fi

# check if Plasma Store server started/
if [[ -z `pgrep plasma-store-se` ]]; then
  # check if we set and env var for cache size
  if [[ -z "$PLASMA_MEMORY" ]]; then
    PLASMA_MEMORY="50000000000" # 50 gb
  else
    PLASMA_MEMORY="$PLASMA_MEMORY"
  fi

  # check output_dir
  if [[ -z "$D3MOUTPUTDIR" ]]; then
    D3MOUTPUTDIR="$(pwd)/output_dir"
    mkdir -p "$D3MOUTPUTDIR"
  else
    D3MOUTPUTDIR="$D3MOUTPUTDIR"
  fi

  # and check for the socket
  if [[ -z "$D3MPLASMASOCKET" ]]; then
    D3MPLASMASOCKET="/tmp/plasma" # default socket
  else
    D3MPLASMASOCKET="$D3MPLASMASOCKET"
  fi

  echo "$(pwd)"

  # check local dir.
#  if [[ -z "$D3MLOCALDIR" ]]; then
#    D3MLOCALDIR="$D3MOUTPUTDIR/temp/plasma"
#    mkdir -p "$D3MLOCALDIR"
#  else
#    D3MLOCALDIR="$D3MLOCALDIR"
#  fi
  D3MLOCALDIR="$D3MOUTPUTDIR/temp/plasma"
  mkdir -p "$D3MLOCALDIR"

  # init plasma inside container if possible
  # e.g. plasma_store -s /tmp/plasma -m 100000000000 -d /D3M/output_dir/temp/plasma
  {
    echo "Init Plasma Store -s $D3MPLASMASOCKET -m $PLASMA_MEMORY -d $D3MLOCALDIR"
    plasma_store -s "$D3MPLASMASOCKET" -m "$PLASMA_MEMORY" -d "$D3MLOCALDIR" &
    sleep 5 # sleep 5secs waiting for plasma store to start.
  } || {
    echo "Cannot start Plasma Store."
  }
else
  echo "Plasma Store Running"
fi
# to stop plasma store do: kill `pgrep plasma-store-se`

# check if time is set, otherwise we use 1 min
if [[ -z "$D3MTIMEOUT" ]]; then
    D3MTIMEOUT="60" # 10 gb
  else
    D3MTIMEOUT="$D3MTIMEOUT"
fi

# execute d3m server.
case $D3MRUN in
 "standalone")
    echo "Executing TAMU TA2 Standalone"
    python3 -m modules.standalone -p $D3MPROBLEMPATH -d $D3MINPUTDIR -t $D3MTIMEOUT -v True
    ;;
  *)
    echo "Executing TAMU TA2"
    python3 -m modules.server.refactor_server -v True
    ;;
esac
