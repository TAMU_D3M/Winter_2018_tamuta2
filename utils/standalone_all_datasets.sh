#!/usr/bin/env bash
IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
output='all_outputs'
mkdir "$output"
for problem_path in $(cat < '_info_datasets/problems.txt'); do
  problem_name=$(basename $(dirname $(dirname $problem_path)))
  output_dir="$output/$problem_name"
  echo "------------------- $problem_name DONE ------------------- "
  mkdir "$output_dir"
  echo "/D3M/$output_dir"

  sudo docker run --rm -t -i --shm-size=10gb --volume "$(pwd):/D3M" -e D3MPROBLEMPATH="$problem_path" -e D3MINPUTDIR='/D3M/datasets' -e D3MOUTPUTDIR="/D3M/$output_dir" -e D3MRUN='standalone' -e D3MTIMEOUT=60 dmartinez05/tamuta2:latest | tee "_info_datasets/$problem_name.log"
  echo "------------------- $problem_name DONE ------------------- "
done