#!/bin/sh -e

problem_json="problemDoc.json"
for problem_path in $(find /D3M/datasets/seed_datasets_current/ -type d -name "*_problem" -print); do
    echo "$problem_path/$problem_json"
done > problems.txt