#!/bin/sh -e

# login to the hub.docker registry
docker login -u "$DOCKER_USER" -p "$DOCKER_PASSWORD" "$DOCKER_REGISTRY"

# login to the gitlab registry
docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"

# login into d3m
docker login -u "$D3M_USER" -p "$D3M_TOKEN" "$D3M_REGISTRY"

for IMAGE_NAME in "$@"; do
    if [ "$IMAGE_NAME" = "complete" ]; then
        echo "Bulding "$DOCKER_IMAGE:$IMAGE_NAME""
        docker build -t registry.datadrivendiscovery.org/ta2-submissions/ta2-tamu/summer2020evaluation_2:latest -f complete.dockerfile .
        echo "Pushing registry.datadrivendiscovery.org/ta2-submissions/ta2-tamu/summer2020evaluation_2:latest"
        docker push registry.datadrivendiscovery.org/ta2-submissions/ta2-tamu/summer2020evaluation_2:latest
        echo "Done"
    fi

    if [ "$IMAGE_NAME" = "primitives" ]; then
        echo "Bulding "$CI_REGISTRY_IMAGE/$IMAGE_NAME":latest"
        docker build -t "$CI_REGISTRY_IMAGE/$IMAGE_NAME:latest" -f primitives.dockerfile .
        echo "Pushing "$CI_REGISTRY_IMAGE/$IMAGE_NAME":latest"
        docker push "$CI_REGISTRY_IMAGE/$IMAGE_NAME:latest"
        echo "Done"
    fi

    if [ "$IMAGE_NAME" = "latest" ]; then
        echo "Bulding "$DOCKER_IMAGE:$IMAGE_NAME""
        docker build -t "$DOCKER_IMAGE:$IMAGE_NAME" -f complete.dockerfile .
        echo "Pushing "$DOCKER_IMAGE:$IMAGE_NAME""
        docker push "$DOCKER_IMAGE:$IMAGE_NAME"
        echo "Done"
    fi

    if [ "$IMAGE_NAME" = "autokeras_deployment" ]; then
        echo "Bulding "$DOCKER_IMAGE:$IMAGE_NAME""
        docker build -t registry.datadrivendiscovery.org/ta2-submissions/ta2-tamu/summer2020dryrun_2:latest -f complete.dockerfile .
        echo "Pushing registry.datadrivendiscovery.org/ta2-submissions/ta2-tamu/summer2020dryrun_2:latest"
        docker push registry.datadrivendiscovery.org/ta2-submissions/ta2-tamu/summer2020dryrun_2:latest
        echo "Done"
    fi

    if [ "$IMAGE_NAME" = "autokeras_test" ]; then
        echo "Bulding "$DOCKER_IMAGE:$IMAGE_NAME""
        docker build -t "$DOCKER_IMAGE:$IMAGE_NAME" -f complete.dockerfile .
        echo "Pushing "$DOCKER_IMAGE:$IMAGE_NAME""
        docker push "$DOCKER_IMAGE:$IMAGE_NAME"
        echo "Done"
    fi
done