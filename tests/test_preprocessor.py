import os
import unittest
from pprint import pprint

# from TimeSeriesD3MWrappers.primitives.classification_knn import Kanine
from d3m import container, index
from d3m.metadata import base as metadata_base
from d3m.metadata.base import ArgumentType
from d3m.metadata.pipeline import PrimitiveStep
from d3m.metadata.problem import TaskKeyword
from d3m.runtime import Runtime
from sklearn_wrap.SKLogisticRegression import SKLogisticRegression

from modules.pipeline_generator.predefined_pipelines import preprocessor
from modules.utils import pipeline_utils, checks, file_utils
from tests.data_util import datasets

Kanine = index.get_primitive('d3m.primitives.time_series_classification.k_neighbors.Kanine')


def run_pipeline(pipeline_description, data, volume_dir='/volumes', primitive_metadata=None):
    runtime = Runtime(pipeline=pipeline_description, context=metadata_base.Context.TESTING, volumes_dir=volume_dir)
    fit_result = runtime.fit([data])
    return fit_result


def add_classifier(pipeline_description, dataset_to_dataframe_step, attributes, targets):
    lr = PrimitiveStep(primitive=SKLogisticRegression)
    lr.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                    data_reference=attributes)
    lr.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER,
                    data_reference=targets)
    lr.add_output('produce')
    pipeline_description.add_step(lr)

    construct_pred = PrimitiveStep(
        primitive=index.get_primitive('d3m.primitives.data_transformation.construct_predictions.Common'))
    construct_pred.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                                data_reference=pipeline_utils.int_to_step(lr.index))
    construct_pred.add_argument(name='reference', argument_type=ArgumentType.CONTAINER,
                                data_reference=dataset_to_dataframe_step)
    construct_pred.add_output('produce')
    pipeline_description.add_step(construct_pred)
    # Final Output
    pipeline_description.add_output(name='output predictions',
                                    data_reference=pipeline_utils.int_to_step(construct_pred.index))


def add_time_series_specific_classifier(pipeline_description, attributes, targets):
    k = PrimitiveStep(primitive=Kanine)
    k.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
                   data_reference=attributes)
    k.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER,
                   data_reference=targets)
    k.add_output('produce')
    pipeline_description.add_step(k)
    pipeline_description.add_output(name='output predictions',
                                    data_reference=pipeline_utils.int_to_step(k.index))
    return k


def _remove_volatile(target_pipe, predef_pipe):
    target_pipe = target_pipe.to_json_structure()
    for step in target_pipe['steps']:
        del step['primitive']['digest']
    subset = {k: v for k, v in target_pipe.items() if k in predef_pipe}
    return subset


class Preprocessor(unittest.TestCase):
    time_series_data: container.Dataset = None
    temp_dir: str = os.path.join(os.path.dirname(__file__), 'temp')

    @classmethod
    def setUpClass(cls) -> None:
        checks.check_directory(cls.temp_dir)
        file_utils.clean_directory(cls.temp_dir)
        cls.maxDiff = None
        cls.time_series_data = datasets.get('timeseries_dataset_2')
        cls.tabular_classification_data = datasets.get('iris_dataset_1')
        cls.image_data = datasets.get('image_dataset_1')
        cls.audio_dataset = datasets.get('audio_dataset_1')

    def test_timeseries_tabular(self):
        pp = preprocessor.get_preprocessor(task=metadata_base.PrimitiveFamily.TIME_SERIES_CLASSIFICATION.name,
                                           treatment=metadata_base.PrimitiveFamily.CLASSIFICATION.name,
                                           data_types=[TaskKeyword.TIME_SERIES], semi=False,
                                           inputs_metadata=self.time_series_data.metadata, problem=None,
                                           main_resource='learningData')[0]
        add_classifier(pp.pipeline_description, pp.dataset_to_dataframe_step, pp.attributes, pp.targets)
        result = run_pipeline(pp.pipeline_description, self.time_series_data)
        result.check_success()
        self.assertEqual(result.error, None)

    def test_timeseries_specific(self):
        pp = preprocessor.get_preprocessor(task=metadata_base.PrimitiveFamily.TIME_SERIES_CLASSIFICATION.name,
                                           treatment=metadata_base.PrimitiveFamily.TIME_SERIES_CLASSIFICATION.name,
                                           data_types=[TaskKeyword.TIME_SERIES], semi=False,
                                           inputs_metadata=self.time_series_data.metadata, problem=None,
                                           main_resource='learningData')[0]

        add_time_series_specific_classifier(pp.pipeline_description, pp.attributes, pp.targets)
        result = run_pipeline(pp.pipeline_description, self.time_series_data)
        result.check_success()
        self.assertEqual(result.error, None)

    def test_tabular(self):
        pp = preprocessor.get_preprocessor(task=metadata_base.PrimitiveFamily.CLASSIFICATION.name,
                                           treatment=metadata_base.PrimitiveFamily.CLASSIFICATION.name,
                                           data_types=[TaskKeyword.TABULAR], semi=False,
                                           inputs_metadata=self.tabular_classification_data.metadata, problem=None,
                                           main_resource='learningData')[0]
        add_classifier(pp.pipeline_description, pp.dataset_to_dataframe_step, pp.attributes, pp.targets)
        result = run_pipeline(pp.pipeline_description, self.tabular_classification_data)
        pprint(pp.pipeline_description.to_json_structure())
        result.check_success()
        self.assertEqual(result.error, None)

    def test_image_tensor(self):
        pp = preprocessor.get_preprocessor(task=metadata_base.PrimitiveFamily.DIGITAL_IMAGE_PROCESSING.name,
                                           treatment=metadata_base.PrimitiveFamily.DIGITAL_IMAGE_PROCESSING.name,
                                           data_types=[TaskKeyword.IMAGE], semi=False,
                                           inputs_metadata=self.image_data.metadata, problem=None,
                                           main_resource='learningData')[0]
        add_classifier(pp.pipeline_description, pp.dataset_to_dataframe_step, pp.attributes, pp.targets)
        pprint(pp.pipeline_description.to_json_structure())
        result = run_pipeline(pp.pipeline_description, self.image_data)
        result.check_success()
        self.assertEqual(result.error, None)

    def test_image_dataframe(self):
        pp = preprocessor.get_preprocessor(task=metadata_base.PrimitiveFamily.DIGITAL_IMAGE_PROCESSING.name,
                                           treatment=metadata_base.PrimitiveFamily.CLASSIFICATION.name,
                                           data_types=[TaskKeyword.IMAGE], semi=False,
                                           inputs_metadata=self.image_data.metadata, problem=None,
                                           main_resource='learningData')[0]
        add_classifier(pp.pipeline_description, pp.dataset_to_dataframe_step, pp.attributes, pp.targets)
        pprint(pp.pipeline_description.to_json_structure())
        result = run_pipeline(pp.pipeline_description, self.image_data)
        result.check_success()
        self.assertEqual(result.error, None)

    def test_text_tabular(self):
        dataset = datasets.get('text_dataset_1')
        pp = preprocessor.get_preprocessor(task=metadata_base.PrimitiveFamily.CLASSIFICATION.name,
                                           treatment=metadata_base.PrimitiveFamily.CLASSIFICATION.name,
                                           data_types={TaskKeyword.TEXT}, semi=False, inputs_metadata=dataset.metadata,
                                           problem=None, main_resource='learningData')[0]
        add_classifier(pp.pipeline_description, pp.dataset_to_dataframe_step, pp.attributes, pp.targets)
        pprint(pp.pipeline_description.to_json_structure())
        result = run_pipeline(pp.pipeline_description, dataset)
        result.check_success()
        self.assertEqual(result.error, None)

    @unittest.expectedFailure
    # TODO Dataset too small for pipeline to work, at least > 16
    def test_text_tabular(self):
        dataset = datasets.get('text_dataset_1')
        pp = preprocessor.get_preprocessor(task=metadata_base.PrimitiveFamily.CLASSIFICATION.name,
                                           treatment=metadata_base.PrimitiveFamily.CLASSIFICATION.name,
                                           data_types={TaskKeyword.TEXT}, semi=False, inputs_metadata=dataset.metadata,
                                           problem=None, main_resource='learningData')[1]
        add_classifier(pp.pipeline_description, pp.dataset_to_dataframe_step, pp.attributes, pp.targets)
        pprint(pp.pipeline_description.to_json_structure())
        result = run_pipeline(pp.pipeline_description, dataset)
        result.check_success()
        self.assertEqual(result.error, None)

    def test_timeseries_forecasting_tabular(self):
        dataset = datasets.get('timeseries_dataset_1')
        pp = preprocessor.get_preprocessor(task=metadata_base.PrimitiveFamily.TIME_SERIES_FORECASTING.name,
                                           treatment=metadata_base.PrimitiveFamily.TIME_SERIES_FORECASTING.name,
                                           data_types=[TaskKeyword.TIME_SERIES.name, TaskKeyword.TABULAR.name],
                                           semi=False, inputs_metadata=dataset.metadata, problem=None,
                                           main_resource='learningData')[0]

        add_classifier(pp.pipeline_description, pp.dataset_to_dataframe_step, pp.attributes, pp.targets)
        result = run_pipeline(pp.pipeline_description, dataset)
        pprint(pp.pipeline_description.to_json_structure())
        result.check_success()
        self.assertEqual(result.error, None)

    @unittest.skip('Not finished yet')
    def test_timeseries_forecasting_grouped(self):
        dataset = datasets.get('timeseries_dataset_3')
        pp = preprocessor.get_preprocessor(task=metadata_base.PrimitiveFamily.TIME_SERIES_FORECASTING.name,
                                           treatment=metadata_base.PrimitiveFamily.TIME_SERIES_FORECASTING.name,
                                           data_types=[TaskKeyword.TIME_SERIES.name, TaskKeyword.GROUPED.name],
                                           semi=False, inputs_metadata=dataset.metadata, problem=None,
                                           main_resource='learningData')[0]

        add_classifier(pp.pipeline_description, pp.dataset_to_dataframe_step, pp.attributes, pp.targets)
        result = run_pipeline(pp.pipeline_description, dataset)
        pprint(pp.pipeline_description.to_json_structure())
        result.check_success()
        self.assertEqual(result.error, None)

    @unittest.expectedFailure
    # TODO No primitive for audio
    def test_audio_dataframe(self):
        pp = preprocessor.get_preprocessor(task=metadata_base.PrimitiveFamily.DIGITAL_SIGNAL_PROCESSING.name,
                                           treatment=metadata_base.PrimitiveFamily.DIGITAL_SIGNAL_PROCESSING.name,
                                           data_types=None, semi=False, inputs_metadata=self.audio_dataset.metadata,
                                           problem=None, main_resource='learningData')[0]
        add_classifier(pp.pipeline_description, pp.dataset_to_dataframe_step, pp.attributes, pp.targets)
        pprint(pp.pipeline_description.to_json_structure())
        result = run_pipeline(pp.pipeline_description, self.audio_dataset)
        result.check_success()

        self.assertEqual(result.error, None)

if __name__ == '__main__':
    unittest.main()
