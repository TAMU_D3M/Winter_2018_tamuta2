import os
import unittest
import json
from unittest import mock

from ta3ta2_api import utils as ta3ta2_utils
from d3m.metadata import base as metadata_base, problem
from d3m.metadata import pipeline as pipeline_module
from d3m.runtime import Runtime
from sklearn_wrap.SKLogisticRegression import SKLogisticRegression
from d3m.metadata import problem as problem_module

from modules.pipeline_generator.models.random_generator import RandomPipelineGenerator
from modules.utils.file_utils import save_problem, clean_directory
from tests.data_util import datasets
from modules.utils.checks import check_directory


class RandomPipelineGeneratorTest(unittest.TestCase):
    iris_dataset = None
    temp_dir = os.path.join(os.path.dirname(__file__), 'temp')
    problem_path = ''

    time_series_problem = {
        "about": {
            "problemID": "LL1_FaceFour_problem",
            "problemName": "LL1 FaceFour problem",
            "problemDescription": "A time series classification problem.",
            "taskKeywords": [
                "classification",
                "multiClass",
                "timeSeries"
            ],
            "problemSchemaVersion": "4.0.0",
            "problemVersion": "4.0"
        },
        "inputs": {
            "data": [
                {
                    "datasetID": "LL1_FaceFour_dataset",
                    "targets": [
                        {
                            "targetIndex": 0,
                            "resID": "learningData",
                            "colIndex": 2,
                            "colName": "label"
                        }
                    ]
                }
            ],
            "dataSplits": {
                "method": "holdOut",
                "testSize": 0.786,
                "stratified": True,
                "numRepeats": 0,
                "splitsFile": "dataSplits.csv"
            },
            "performanceMetrics": [
                {
                    "metric": "f1Macro"
                }
            ]
        },
        "expectedOutputs": {
            "predictionsFile": "predictions.csv"
        }
    }

    @classmethod
    def setUpClass(cls):
        cls.iris_dataset = datasets.get('iris_dataset_1')
        cls.timeseries_dataset_2 = datasets.get('timeseries_dataset_2')
        check_directory(cls.temp_dir)
        clean_directory(cls.temp_dir)
        problem_path = os.path.join(cls.temp_dir, 'problemDoc.json')
        with open(problem_path, 'w+') as f:
            json.dump(cls.time_series_problem, f)
        cls.problem_path = problem_path

    @classmethod
    def tearDown(cls):
        clean_directory(cls.temp_dir)

    @mock.patch('modules.pipeline_generator.models.random_generator.pipeline_utils.get_primitive_candidates')
    def test_tabular_classification(self, mock_get_primitives_candidates):
        mock_get_primitives_candidates.return_value = [
            (SKLogisticRegression, 'CLASSIFICATION')]
        problem = problem_module.get_problem(self.problem_path)
        problem = ta3ta2_utils.decode_problem_description(ta3ta2_utils.encode_problem_description(problem))
        rpg = RandomPipelineGenerator(
            problem_doc_uri=save_problem(problem, 'search_id_here', self.temp_dir),
            inputs_metadata=[self.timeseries_dataset_2.metadata], save_path=self.temp_dir,
            main_resource='learningData')
        pipelines = rpg.get_pipelines(num_pipelines=1)
        pipeline_path = list(pipelines.values())[0]['path']
        self.assertTrue(os.path.isfile(pipeline_path))
        self.assertTrue(list(pipelines.values())[0]['gpu_budget'] == 0)
        pipeline = pipeline_module.get_pipeline(pipeline_path)
        runtime = Runtime(pipeline=pipeline, context=metadata_base.Context.TESTING)
        fit_result = runtime.fit([self.timeseries_dataset_2])
        import pprint
        pprint.pprint(pipeline.to_json_structure())
        fit_result.check_success()
        self.assertIsNone(fit_result.error)
        produce_result = runtime.produce([self.timeseries_dataset_2])
        self.assertIsNone(produce_result.error)


if __name__ == '__main__':
    unittest.main()
