import json
import logging
import os
import sys
import unittest
import warnings
from unittest import TestCase

sys.path.insert(0, os.path.abspath('.'))

from modules import standalone
from modules.utils import checks
from modules.utils.constants import EnvVars, Path

logger = logging.getLogger(__name__)

# read config file to get configuration of every datasets
with open(os.path.join(os.path.dirname(__file__), 'config', 'dataset_test_config.json')) as f:
    config = json.load(f)


def get_param_list():
    datasets_dir = os.environ['TEST_DATASETS']
    default_timebound = os.environ['DEFAULT_TIME_BOUND']
    spawn_method = os.environ['SPAWN_METHOD']
    datasets = os.listdir(datasets_dir)
    # datasets = ['38_sick', '185_baseball']
    params = {}
    for dataset in datasets:
        timebound = config[dataset]['time_to_generate'] \
            if dataset in config and 'time_to_generate' in config[dataset] else default_timebound
        params[dataset] = {
            'problem_path': os.path.abspath(
                os.path.join(datasets_dir, dataset, '{}_problem'.format(dataset), 'problemDoc.json')),
            'datasets_dir': os.path.abspath(os.path.join(datasets_dir, dataset)),
            'time_bound': int(timebound),
            'process_type': spawn_method,
        }
    return params


def save_solution(solution, save_dir, dataset_name):
    checks.check_directory(save_dir)
    solution = {k: v.fillna(0).to_dict('record') for k, v in solution.items()}
    with open(os.path.join(save_dir, dataset_name + '.json'), 'w+') as fp:
        json.dump(solution, fp, indent=2)


class TestAllDatasets(TestCase):
    """
    test cases which runs over all datasets. An integration test instead of unit test.

    Parameters
    ----------
    jobs: Queue
            A shared Queue containing jobs to be run.
    """

    # skip if not run by scheduled pipeline to prevent being tested by regular unit test pipeline
    @unittest.skipUnless('CI_JOB_NAME' in os.environ and os.environ['CI_JOB_NAME'] == 'tests_all',
                         'Skip if not scheduled')
    def test_standalone(self):
        params = get_param_list()
        for dataset, param in params.items():
            logger.info('@' * 50)
            logger.info(dataset)
            logger.info('@' * 50)
            # divide every test into subTest so that failed dataset can be viewed clearly
            with self.subTest('Dataset: {}, Time Bound: {}'.format(dataset, param['time_bound'])):
                solution = standalone.main(**param)
                save_solution(solution, os.path.join(EnvVars.PROJECT_ROOT, 'test_solutions'), dataset)
                self.assertGreaterEqual(len(solution), 1)
            logger.info('')
            logger.info('')
            logger.info('')


if __name__ == '__main__':
    log_format = '%(asctime)s %(levelname)-8s %(processName)-15s [%(filename)s:%(lineno)d] %(message)s'
    logging.basicConfig(format=log_format,
                        handlers=[logging.StreamHandler(),
                                  logging.FileHandler('{}/d3m.log'.format(Path.TEMP_STORAGE_ROOT), 'w', 'utf-8')],
                        datefmt='%m/%d %H:%M:%S')
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)
    warnings.filterwarnings('ignore')
    # use HtmlTestRunner to generate report
    import HtmlTestRunner

    unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner())
