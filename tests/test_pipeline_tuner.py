# import json
# import os
# import pickle
# import shutil
# import unittest
# from unittest.mock import patch
#
# from d3m import index
# from d3m import utils as d3m_utils
# from d3m.metadata import problem as problem_module
# from d3m.metadata.base import ArgumentType, Context
# from d3m.metadata.pipeline import Pipeline, PrimitiveStep
#
# from modules.manager.workers import get_data, _search_score_worker
# from modules.pipeline_generator.models.pipeline_tuner import PipelineTuner
# from modules.pipeline_generator.models.random_pipeline_tuner import RandomPipelineTuner
# from modules.pipeline_generator.models.quick_generator import QuickPipelineGenerator
# from modules.utils import parsers, custom_resolver
# from modules.utils.constants import LISTS, EnvVars
#
# project_root = EnvVars.PROJECT_ROOT
#
# save_path = 'tests/resources/temp/test_pipeline_tuner'
#
# search_id = 'e0e34088-8da2-464b-823e-a32b45466247'
# inputs_list = [[{'type': 'dataset_uri', 'value': 'datasets/185/TRAIN/dataset_TRAIN/datasetDoc.json'}]]
# test_paths = {
#     'TRAIN': {
#         'dataset': 'datasets/185/TRAIN/dataset_TRAIN/datasetDoc.json',
#         'problem': 'file://{}/datasets/185/TRAIN/problem_TRAIN/problemDoc.json'.format(project_root)},
#     'TEST': {
#         'dataset': 'datasets/185/TEST/dataset_TEST/datasetDoc.json',
#         'problem': 'file://{}/datasets/185/TEST/problem_TEST/problemDoc.json'.format(project_root)},
#     'SCORE': {
#         'dataset': 'datasets/185/SCORE/dataset_TEST/datasetDoc.json',
#         'problem': 'file://{}/datasets/185/SCORE/problem_TEST/problemDoc.json'.format(project_root)},
# }
# plasma_client = None
#
# LOAD_PRIMITIVES = True
# LOAD_MATCHING_PRIMITIVES = False
# DEBUG = False
#
# def PRINT(*args, **kwargs):
#     if DEBUG:
#         print(*args, **kwargs)
#
# class TestPipelineTuner(unittest.TestCase):
#
#     @classmethod
#     def setUpClass(cls):
#         if not os.path.exists(save_path):
#             os.makedirs(save_path)
#         problem = problem_module.Problem.load(test_paths['TRAIN']['problem'])
#         cls.problem_path = parsers.save_problem(problem, search_id, save_path)
#         inputs = get_data(inputs_list, plasma_client)
#         cls.inputs_metadata = [input.metadata for input in inputs]
#
#         if LOAD_PRIMITIVES:
#             print('Loading all primitives')
#             with d3m_utils.silence():
#                 index.load_all(blocklist=LISTS.BLACK_LIST)
#             print('Finish')
#
#         if LOAD_MATCHING_PRIMITIVES:
#             keras_wraps_matching_primitive_names = [
#                 'd3m.primitives.layer.max_pooling_3d.KerasWrap',
#                 'd3m.primitives.layer.null.KerasWrap',
#                 'd3m.primitives.layer.average_pooling_2d.KerasWrap',
#                 'd3m.primitives.layer.convolution_3d.KerasWrap',
#                 'd3m.primitives.layer.convolution_1d.KerasWrap',
#                 'd3m.primitives.layer.average_pooling_3d.KerasWrap',
#                 'd3m.primitives.layer.global_average_pooling_3d.KerasWrap',
#                 'd3m.primitives.layer.batch_normalization.KerasWrap',
#                 'd3m.primitives.layer.concat.KerasWrap',
#                 'd3m.primitives.layer.global_average_pooling_2d.KerasWrap',
#                 'd3m.primitives.layer.max_pooling_2d.KerasWrap',
#                 'd3m.primitives.layer.dropout.KerasWrap',
#                 'd3m.primitives.layer.add.KerasWrap',
#                 'd3m.primitives.layer.subtract.KerasWrap',
#                 'd3m.primitives.layer.dense.KerasWrap',
#                 'd3m.primitives.layer.flatten.KerasWrap',
#                 'd3m.primitives.layer.max_pooling_1d.KerasWrap',
#                 'd3m.primitives.layer.average_pooling_1d.KerasWrap',
#                 'd3m.primitives.layer.convolution_2d.KerasWrap',
#                 'd3m.primitives.layer.global_average_pooling_1d.KerasWrap'
#             ]
#             cls.keras_wraps_matching_primitives = [index.get_primitive(n) for n in keras_wraps_matching_primitive_names]
#
#             keras_model_loss_matching_primitive_names = [
#                 'd3m.primitives.loss_function.binary_crossentropy.KerasWrap',
#                 'd3m.primitives.loss_function.logcosh.KerasWrap',
#                 'd3m.primitives.loss_function.sparse_categorical_crossentropy.KerasWrap',
#                 'd3m.primitives.loss_function.mean_absolute_error.KerasWrap',
#                 'd3m.primitives.loss_function.categorical_crossentropy.KerasWrap',
#                 'd3m.primitives.loss_function.mean_squared_logarithmic_error.KerasWrap',
#                 'd3m.primitives.loss_function.categorical_accuracy.KerasWrap',
#                 'd3m.primitives.loss_function.mean_absolute_percentage_error.KerasWrap',
#                 'd3m.primitives.loss_function.mean_squared_error.KerasWrap',
#                 'd3m.primitives.loss_function.kullback_leibler_divergence.KerasWrap',
#                 'd3m.primitives.loss_function.cosine_proximity.KerasWrap',
#                 'd3m.primitives.loss_function.squared_hinge.KerasWrap',
#                 'd3m.primitives.loss_function.categorical_hinge.KerasWrap',
#                 'd3m.primitives.loss_function.poisson.KerasWrap',
#                 'd3m.primitives.loss_function.hinge.KerasWrap',
#             ]
#             cls.keras_model_loss_matching_primitives = [index.get_primitive(n)
#                                                     for n in keras_model_loss_matching_primitive_names]
#
#             search_hybrid_Find_projections_matching_primitive_names = [
#                 "d3m.primitives.classification.sgd.SKlearn",
#                 "d3m.primitives.classification.dummy.SKlearn",
#                 "d3m.primitives.classification.k_neighbors.SKlearn",
#                 "d3m.primitives.classification.svc.SKlearn",
#                 "d3m.primitives.classification.xgboost_dart.DataFrameCommon",
#                 "d3m.primitives.classification.lupi_rf.LupiRFClassifier",
#                 "d3m.primitives.classification.mlp.BBN",
#                 "d3m.primitives.classification.cover_tree.Fastlvm",
#                 "d3m.primitives.classification.linear_svc.SKlearn",
#                 "d3m.primitives.classification.xgboost_gbtree.DataFrameCommon",
#                 "d3m.primitives.classification.random_forest.DataFrameCommon",
#                 "d3m.primitives.classification.tree_augmented_naive_bayes.BayesianInfRPI",
#                 "d3m.primitives.classification.lupi_rfsel.LupiRFSelClassifier",
#                 "d3m.primitives.classification.linear_discriminant_analysis.SKlearn",
#                 "d3m.primitives.classification.bagging.SKlearn",
#                 "d3m.primitives.classification.quadratic_discriminant_analysis.SKlearn",
#                 "d3m.primitives.classification.multinomial_naive_bayes.SKlearn",
#                 "d3m.primitives.classification.bernoulli_naive_bayes.SKlearn",
#                 "d3m.primitives.classification.lstm.DSBOX",
#                 "d3m.primitives.classification.passive_aggressive.SKlearn",
#                 "d3m.primitives.classification.extra_trees.SKlearn",
#                 "d3m.primitives.classification.gaussian_naive_bayes.SKlearn",
#                 "d3m.primitives.classification.decision_tree.SKlearn",
#                 "d3m.primitives.classification.ada_boost.SKlearn",
#                 "d3m.primitives.classification.logistic_regression.SKlearn",
#                 "d3m.primitives.classification.random_classifier.Test",
#                 "d3m.primitives.classification.nearest_centroid.SKlearn",
#                 "d3m.primitives.classification.random_forest.SKlearn",
#                 "d3m.primitives.classification.tensor_machines_binary_classification.TensorMachinesBinaryClassification",
#                 "d3m.primitives.classification.mlp.SKlearn",
#                 "d3m.primitives.classification.search_hybrid.Find_projections",
#                 "d3m.primitives.classification.light_gbm.DataFrameCommon",
#                 "d3m.primitives.classification.search.Find_projections",
#                 "d3m.primitives.classification.gradient_boosting.SKlearn",
#             ]
#             cls.search_hybrid_Find_projections_matching_primitives = [index.get_primitive(n) for n in
#                                                                   search_hybrid_Find_projections_matching_primitive_names]
#
#             search_hybrid_numeric_Find_projections_matching_primitive_names = [
#                 "d3m.primitives.regression.ard.SKlearn",
#                 "d3m.primitives.regression.extra_trees.SKlearn",
#                 "d3m.primitives.regression.gaussian_process.SKlearn",
#                 "d3m.primitives.regression.linear_svr.SKlearn",
#                 "d3m.primitives.regression.cover_tree.Fastlvm",
#                 "d3m.primitives.regression.sgd.SKlearn",
#                 "d3m.primitives.regression.xgboost_gbtree.DataFrameCommon",
#                 "d3m.primitives.regression.search_hybrid_numeric.Find_projections",
#                 "d3m.primitives.regression.rfm_precondition_ed_gaussian_krr.RFMPreconditionedGaussianKRR",
#                 "d3m.primitives.regression.corex_supervised.EchoLinear",
#                 "d3m.primitives.regression.lars.SKlearn",
#                 "d3m.primitives.regression.elastic_net.SKlearn",
#                 "d3m.primitives.regression.tensor_machines_regularized_least_squares.TensorMachinesRegularizedLeastSquares",
#                 "d3m.primitives.regression.ada_boost.SKlearn",
#                 "d3m.primitives.regression.search_numeric.Find_projections",
#                 "d3m.primitives.regression.owl_regression.Umich",
#                 "d3m.primitives.regression.lasso_cv.SKlearn",
#                 "d3m.primitives.regression.rfm_precondition_ed_polynomial_krr.RFMPreconditionedPolynomialKRR",
#                 "d3m.primitives.regression.svr.SKlearn",
#                 "d3m.primitives.regression.decision_tree.SKlearn",
#                 "d3m.primitives.regression.random_forest.SKlearn",
#                 "d3m.primitives.regression.kernel_ridge.SKlearn",
#                 "d3m.primitives.regression.gradient_boosting.SKlearn",
#                 "d3m.primitives.regression.mlp.SKlearn",
#                 "d3m.primitives.regression.monomial.Test",
#                 "d3m.primitives.regression.dummy.SKlearn",
#                 "d3m.primitives.regression.passive_aggressive.SKlearn",
#                 "d3m.primitives.regression.ridge.SKlearn",
#                 "d3m.primitives.regression.k_neighbors.SKlearn",
#                 "d3m.primitives.regression.fast_lad.FastLAD",
#                 "d3m.primitives.regression.lasso.SKlearn",
#             ]
#             cls.search_hybrid_numeric_Find_projections_matching_primitives = [
#                 index.get_primitive(n) for n in search_hybrid_numeric_Find_projections_matching_primitive_names]
#
#             iterative_labeling_AutonBox_matching_primitive_names = [
#                 "d3m.primitives.classification.sgd.SKlearn",
#                 "d3m.primitives.classification.dummy.SKlearn",
#                 "d3m.primitives.classification.k_neighbors.SKlearn",
#                 "d3m.primitives.classification.svc.SKlearn",
#                 "d3m.primitives.classification.xgboost_dart.DataFrameCommon",
#                 "d3m.primitives.classification.lupi_rf.LupiRFClassifier",
#                 "d3m.primitives.classification.mlp.BBN",
#                 "d3m.primitives.classification.cover_tree.Fastlvm",
#                 "d3m.primitives.classification.linear_svc.SKlearn",
#                 "d3m.primitives.classification.xgboost_gbtree.DataFrameCommon",
#                 "d3m.primitives.classification.random_forest.DataFrameCommon",
#                 "d3m.primitives.classification.tree_augmented_naive_bayes.BayesianInfRPI",
#                 "d3m.primitives.classification.lupi_rfsel.LupiRFSelClassifier",
#                 "d3m.primitives.classification.linear_discriminant_analysis.SKlearn",
#                 "d3m.primitives.classification.bagging.SKlearn",
#                 "d3m.primitives.classification.quadratic_discriminant_analysis.SKlearn",
#                 "d3m.primitives.classification.multinomial_naive_bayes.SKlearn",
#                 "d3m.primitives.classification.bernoulli_naive_bayes.SKlearn",
#                 "d3m.primitives.classification.lstm.DSBOX",
#                 "d3m.primitives.classification.passive_aggressive.SKlearn",
#                 "d3m.primitives.classification.extra_trees.SKlearn",
#                 "d3m.primitives.classification.gaussian_naive_bayes.SKlearn",
#                 "d3m.primitives.classification.decision_tree.SKlearn",
#                 "d3m.primitives.classification.ada_boost.SKlearn",
#                 "d3m.primitives.classification.logistic_regression.SKlearn",
#                 "d3m.primitives.classification.random_classifier.Test",
#                 "d3m.primitives.classification.nearest_centroid.SKlearn",
#                 "d3m.primitives.classification.random_forest.SKlearn",
#                 "d3m.primitives.classification.tensor_machines_binary_classification.TensorMachinesBinaryClassification",
#                 "d3m.primitives.classification.mlp.SKlearn",
#                 "d3m.primitives.classification.search_hybrid.Find_projections",
#                 "d3m.primitives.classification.light_gbm.DataFrameCommon",
#                 "d3m.primitives.classification.search.Find_projections",
#                 "d3m.primitives.classification.gradient_boosting.SKlearn",
#             ]
#             cls.iterative_labeling_AutonBox_matching_primitives = [
#                 index.get_primitive(n) for n in iterative_labeling_AutonBox_matching_primitive_names]
#         else:
#             cls.keras_wraps_matching_primitives = None
#             cls.keras_model_loss_matching_primitives = None
#             cls.search_hybrid_Find_projections_matching_primitives = None
#             cls.search_hybrid_numeric_Find_projections_matching_primitives = None
#             cls.iterative_labeling_AutonBox_matching_primitives = None
#
#     @classmethod
#     def tearDownClass(cls):
#         if os.path.exists(save_path):
#             shutil.rmtree(save_path)
#
#     def create_patch(self, name):
#         patcher = patch(name)
#         thing = patcher.start()
#         self.addCleanup(patcher.stop)
#         return thing
#
#     def test__get_primitive_search_space_Primitive_type(self):
#         keras_wraps_matching_primitives = TestPipelineTuner.keras_wraps_matching_primitives
#         keras_model_loss_matching_primitives = TestPipelineTuner.keras_model_loss_matching_primitives
#
#         search_hybrid_Find_projections_matching_primitives = \
#             TestPipelineTuner.search_hybrid_Find_projections_matching_primitives
#
#         search_hybrid_numeric_Find_projections_matching_primitives = \
#             TestPipelineTuner.search_hybrid_numeric_Find_projections_matching_primitives
#
#         iterative_labeling_AutonBox_matching_primitives = \
#             TestPipelineTuner.iterative_labeling_AutonBox_matching_primitives
#
#         fail_num = 0
#         name_config = {}
#         with patch.object(PipelineTuner, "__init__", lambda x, y, z, w: None):
#             tuner = PipelineTuner(1, 2, 3)
#             for name in [
#                 "d3m.primitives.learner.model.KerasWrap",
#                 "d3m.primitives.classification.search_hybrid.Find_projections",
#                 "d3m.primitives.regression.search_hybrid_numeric.Find_projections",
#                 "d3m.primitives.semisupervised_classification.iterative_labeling.AutonBox",
#             ]:
#                 # Assign matching primitives for acceleration
#                 primitive = index.get_primitive(name)
#                 primitive_config = primitive.metadata.query()['primitive_code']['class_type_arguments'][
#                     'Hyperparams'].configuration
#                 if name == "d3m.primitives.learner.model.KerasWrap":
#                     for hyper_param_name in ['loss', 'metric']:
#                         hyper_param = primitive_config[hyper_param_name]
#                         hyper_param.matching_primitives = keras_model_loss_matching_primitives
#
#                     hyper_param_name = 'previous_layer'
#                     hyper_param = primitive_config[hyper_param_name].configuration[hyper_param_name]
#                     hyper_param.matching_primitives = keras_wraps_matching_primitives
#                 else:
#                     mapping = {
#                         "d3m.primitives.classification.search_hybrid.Find_projections":
#                             search_hybrid_Find_projections_matching_primitives,
#                         "d3m.primitives.regression.search_hybrid_numeric.Find_projections":
#                             search_hybrid_numeric_Find_projections_matching_primitives,
#                         "d3m.primitives.semisupervised_classification.iterative_labeling.AutonBox":
#                             iterative_labeling_AutonBox_matching_primitives,
#                     }
#                     matching_primitives = mapping[name]
#                     hyper_param_name = 'blackbox'
#                     hyper_param = primitive_config[hyper_param_name]
#                     hyper_param.matching_primitives = matching_primitives
#
#                 try:
#                     search_space = tuner._get_primitive_search_space(primitive)
#                     smac_config = search_space.sample_configuration()
#                     split_name = name.split('.')
#                     smac_config_name = 'smac_config_{}_{}_{}'.format(split_name[-3], split_name[-2], split_name[-1])
#                     PRINT('{} = {{'.format(smac_config_name))
#                     name_config[name] = smac_config_name
#                     for k in smac_config.keys():
#                         if type(smac_config[k]) == str:
#                             PRINT("\t'{}': '{}',".format(k, smac_config[k]))
#                         else:
#                             PRINT("\t'{}': {},".format(k, smac_config[k]))
#                     PRINT('}\n')
#                 except Exception:
#                     print('{} doest not get its primitive search space'.format(name))
#                     fail_num += 1
#             for k in name_config.keys():
#                 PRINT("\t'{}': {},".format(k, name_config[k]))
#
#         self.assertEqual(fail_num, 0)
#
#     def test__get_primitive_search_space_Choice_type(self):
#         keras_wraps_matching_primitives = TestPipelineTuner.keras_wraps_matching_primitives
#         keras_model_loss_matching_primitives = TestPipelineTuner.keras_model_loss_matching_primitives
#
#         fail_num = 0
#         name_config = {}
#         with patch.object(PipelineTuner, "__init__", lambda x, y, z, w: None):
#             tuner = PipelineTuner(1, 2, 3)
#             for name in [
#                 "d3m.primitives.classification.dummy.SKlearn",  # Choice + Union
#                 "d3m.primitives.data_transformation.fast_ica.SKlearn",
#                 "d3m.primitives.layer.batch_normalization.KerasWrap",
#                 "d3m.primitives.layer.convolution_1d.KerasWrap", # Choice + choice configuration
#                 "d3m.primitives.layer.convolution_2d.KerasWrap",
#                 "d3m.primitives.layer.convolution_3d.KerasWrap",
#                 "d3m.primitives.layer.dense.KerasWrap",
#                 "d3m.primitives.learner.model.KerasWrap",
#                 "d3m.primitives.regression.dummy.SKlearn",
#                 "d3m.primitives.regression.kernel_ridge.SKlearn",
#                 "d3m.primitives.regression.svr.SKlearn",
#             ]:
#                 primitive = index.get_primitive(name)
#                 # Assign matching primitives for acceleration
#                 if 'KerasWrap' in name:
#                     primitive_config = primitive.metadata.query()['primitive_code']['class_type_arguments'][
#                         'Hyperparams'].configuration
#                     for hyper_param_name in ['previous_layer', 'previous_layer_left']:
#                         if hyper_param_name in primitive_config:
#                             hyper_param = primitive_config[hyper_param_name].configuration[hyper_param_name]
#                             hyper_param.matching_primitives = keras_wraps_matching_primitives
#                     if name == "d3m.primitives.learner.model.KerasWrap":
#                         for hyper_param_name in ['loss', 'metric']:
#                             hyper_param = primitive_config[hyper_param_name]
#                             hyper_param.matching_primitives = keras_model_loss_matching_primitives
#                 try:
#                     search_space = tuner._get_primitive_search_space(primitive)
#                     smac_config = search_space.sample_configuration()
#                     split_name = name.split('.')
#                     smac_config_name = 'smac_config_{}_{}_{}'.format(split_name[-3], split_name[-2], split_name[-1])
#                     PRINT('{} = {{'.format(smac_config_name))
#                     name_config[name] = smac_config_name
#                     for k in smac_config.keys():
#                         if type(smac_config[k]) == str:
#                             PRINT("\t'{}': '{}',".format(k, smac_config[k]))
#                         else:
#                             PRINT("\t'{}': {},".format(k, smac_config[k]))
#                     PRINT('}\n')
#                 except Exception:
#                     print('{} doest not get its primitive search space'.format(name))
#                     fail_num += 1
#
#             for k in name_config.keys():
#                 PRINT("\t'{}': {},".format(k, name_config[k]))
#
#         self.assertEqual(fail_num, 0)
#
#     def test__get_primitive_search_space__Sequence_type(self):
#         fail_num = 0
#         name_config = {}
#         with patch.object(PipelineTuner, "__init__", lambda x, y, z, w: None):
#             tuner = PipelineTuner(1, 2, 3)
#             for name in [
#                 "d3m.primitives.data_preprocessing.count_vectorizer.SKlearn",  # SortedList
#                 "d3m.primitives.classification.mlp.SKlearn",  # List
#                 "d3m.primitives.regression.mlp.SKlearn",  # List
#                 "d3m.primitives.data_transformation.one_hot_encoder.SKlearn",
#                 "d3m.primitives.data_preprocessing.tfidf_vectorizer.SKlearn",  # SortedList
#                 "d3m.primitives.data_preprocessing.min_max_scaler.SKlearn",  # SortedSet
#                 "d3m.primitives.data_preprocessing.robust_scaler.SKlearn",  # SortedSet
#             ]:
#                 try:
#                     primitive = index.get_primitive(name)
#                     search_space = tuner._get_primitive_search_space(primitive)
#                     smac_config = search_space.sample_configuration()
#                     split_name = name.split('.')
#                     smac_config_name = 'smac_config_{}_{}_{}'.format(split_name[-3], split_name[-2], split_name[-1])
#                     PRINT('{} = {{'.format(smac_config_name))
#                     name_config[name] = smac_config_name
#                     for k in smac_config.keys():
#                         if type(smac_config[k]) == str:
#                             PRINT("\t'{}': '{}',".format(k, smac_config[k]))
#                         else:
#                             PRINT("\t'{}': {},".format(k, smac_config[k]))
#                     PRINT('}\n')
#                 except Exception as e:
#                     print(e)
#                     print('{} doest not get its primitive search space'.format(name))
#                     fail_num += 1
#             for k in name_config.keys():
#                 PRINT("\t'{}': {},".format(k, name_config[k]))
#
#         self.assertEqual(fail_num, 0)
#
#     def test__get_primitive_search_space_LogUniform_type(self):
#         fail_num = 0
#         with patch.object(PipelineTuner, "__init__", lambda x, y, z, w: None):
#             tuner = PipelineTuner(1, 2, 3)
#             for name in [
#                 "d3m.primitives.classification.mlp.BBN",
#                 "d3m.primitives.classification.tensor_machines_binary_classification.TensorMachinesBinaryClassification",
#                 "d3m.primitives.clustering.ssc_omp.Umich",
#                 "d3m.primitives.data_compression.go_dec.Umich",
#                 "d3m.primitives.data_compression.pcp_ialm.Umich",
#                 "d3m.primitives.data_compression.rpca_lbd.Umich",
#                 "d3m.primitives.regression.rfm_precondition_ed_gaussian_krr.RFMPreconditionedGaussianKRR",
#                 "d3m.primitives.regression.rfm_precondition_ed_polynomial_krr.RFMPreconditionedPolynomialKRR",
#                 "d3m.primitives.regression.tensor_machines_regularized_least_squares.TensorMachinesRegularizedLeastSquares",
#                 "d3m.primitives.time_series_forecasting.rnn_time_series.DSBOX",
#             ]:
#                 # Assign matching primitives for acceleration
#                 try:
#                     primitive = index.get_primitive(name)
#                     search_space = tuner._get_primitive_search_space(primitive)
#                 except Exception as e:
#                     print(e)
#                     print('{} doest not get its primitive search space'.format(name))
#                     fail_num += 1
#         self.assertEqual(fail_num, 0)
#
#     def test__get_primitive_search_space_Union_type(self):
#         keras_wraps_matching_primitives = TestPipelineTuner.keras_wraps_matching_primitives
#         keras_model_loss_matching_primitives = TestPipelineTuner.keras_model_loss_matching_primitives
#
#         fail_num = 0
#         name_config = {}
#         with patch.object(PipelineTuner, "__init__", lambda x, y, z, w: None):
#             tuner = PipelineTuner(1, 2, 3)
#             for name in [
#                 "d3m.primitives.classification.bagging.SKlearn",
#                 "d3m.primitives.classification.decision_tree.SKlearn",
#                 "d3m.primitives.classification.bernoulli_naive_bayes.SKlearn",
#                 "d3m.primitives.classification.extra_trees.SKlearn",
#                 "d3m.primitives.classification.gradient_boosting.SKlearn",
#                 "d3m.primitives.classification.light_gbm.DataFrameCommon",
#                 "d3m.primitives.classification.linear_discriminant_analysis.SKlearn",
#                 "d3m.primitives.classification.linear_svc.SKlearn",
#                 "d3m.primitives.classification.logistic_regression.SKlearn",
#                 "d3m.primitives.classification.lupi_rf.LupiRFClassifier",
#                 "d3m.primitives.classification.lupi_rfsel.LupiRFSelClassifier",
#                 "d3m.primitives.classification.lupi_svm.LupiSvmClassifier",
#                 "d3m.primitives.classification.mlp.SKlearn",
#                 "d3m.primitives.classification.nearest_centroid.SKlearn",
#                 "d3m.primitives.classification.passive_aggressive.SKlearn",
#                 "d3m.primitives.classification.quadratic_discriminant_analysis.SKlearn",
#                 "d3m.primitives.classification.random_forest.DataFrameCommon",
#                 "d3m.primitives.classification.random_forest.SKlearn",
#                 "d3m.primitives.classification.sgd.SKlearn",
#                 "d3m.primitives.classification.svc.SKlearn",
#                 "d3m.primitives.classification.xgboost_dart.DataFrameCommon",
#                 "d3m.primitives.classification.xgboost_gbtree.DataFrameCommon",
#                 "d3m.primitives.clustering.ekss.Umich",
#                 "d3m.primitives.clustering.ssc_admm.Umich",
#                 "d3m.primitives.clustering.ssc_cvx.Umich",
#                 "d3m.primitives.data_cleaning.imputer.SKlearn",
#                 "d3m.primitives.data_cleaning.missing_indicator.SKlearn",
#                 "d3m.primitives.data_compression.go_dec.Umich",
#                 "d3m.primitives.data_compression.pcp_ialm.Umich",
#                 "d3m.primitives.data_preprocessing.dataset_sample.Common",
#                 "d3m.primitives.data_preprocessing.feature_agglomeration.SKlearn",
#                 "d3m.primitives.data_preprocessing.nystroem.SKlearn",
#                 "d3m.primitives.data_preprocessing.random_trees_embedding.SKlearn",
#                 "d3m.primitives.data_transformation.fast_ica.SKlearn",
#                 "d3m.primitives.feature_construction.corex_continuous.DSBOX",
#                 "d3m.primitives.feature_construction.deep_feature_synthesis.Featuretools",
#                 "d3m.primitives.feature_extraction.kernel_pca.SKlearn",
#                 "d3m.primitives.feature_extraction.pca.SKlearn",
#                 "d3m.primitives.feature_selection.generic_univariate_select.SKlearn",
#                 "d3m.primitives.regression.extra_trees.SKlearn",
#                 "d3m.primitives.regression.gaussian_process.SKlearn",
#                 "d3m.primitives.regression.gradient_boosting.SKlearn",
#                 "d3m.primitives.regression.lars.SKlearn",
#                 "d3m.primitives.regression.lasso.SKlearn",
#                 "d3m.primitives.regression.lasso_cv.SKlearn",
#                 "d3m.primitives.regression.mlp.SKlearn",
#                 "d3m.primitives.regression.passive_aggressive.SKlearn",
#                 "d3m.primitives.regression.random_forest.SKlearn",
#                 "d3m.primitives.regression.ridge.SKlearn",
#                 "d3m.primitives.regression.sgd.SKlearn",
#                 "d3m.primitives.regression.xgboost_gbtree.DataFrameCommon",
#                 "d3m.primitives.layer.average_pooling_1d.KerasWrap",
#                 "d3m.primitives.layer.average_pooling_2d.KerasWrap",
#                 "d3m.primitives.layer.average_pooling_3d.KerasWrap",
#                 "d3m.primitives.layer.batch_normalization.KerasWrap",
#                 "d3m.primitives.layer.convolution_1d.KerasWrap",
#                 "d3m.primitives.layer.convolution_2d.KerasWrap",
#                 "d3m.primitives.layer.convolution_3d.KerasWrap",
#                 "d3m.primitives.layer.dense.KerasWrap",
#                 "d3m.primitives.layer.dropout.KerasWrap",
#                 "d3m.primitives.layer.flatten.KerasWrap",
#                 "d3m.primitives.layer.global_average_pooling_1d.KerasWrap",
#                 "d3m.primitives.layer.global_average_pooling_2d.KerasWrap",
#                 "d3m.primitives.layer.global_average_pooling_3d.KerasWrap",
#                 "d3m.primitives.layer.max_pooling_1d.KerasWrap",
#                 "d3m.primitives.layer.max_pooling_2d.KerasWrap",
#                 "d3m.primitives.layer.max_pooling_3d.KerasWrap",
#                 "d3m.primitives.learner.model.KerasWrap",
#                 "d3m.primitives.layer.subtract.KerasWrap",
#                 "d3m.primitives.regression.decision_tree.SKlearn",
#                 "d3m.primitives.data_preprocessing.count_vectorizer.SKlearn",
#                 "d3m.primitives.data_preprocessing.tfidf_vectorizer.SKlearn",
#                 "d3m.primitives.data_transformation.one_hot_encoder.SKlearn",
#             ]:
#                 primitive = index.get_primitive(name)
#                 # Assign matching primitives for acceleration
#                 if 'KerasWrap' in name:
#                     primitive_config = primitive.metadata.query()['primitive_code']['class_type_arguments'][
#                         'Hyperparams'].configuration
#                     for hyper_param_name in ['previous_layer', 'previous_layer_left']:
#                         if hyper_param_name in primitive_config:
#                             hyper_param = primitive_config[hyper_param_name].configuration[hyper_param_name]
#                             hyper_param.matching_primitives = keras_wraps_matching_primitives
#                     if name == "d3m.primitives.learner.model.KerasWrap":
#                         for hyper_param_name in ['loss', 'metric']:
#                             hyper_param = primitive_config[hyper_param_name]
#                             hyper_param.matching_primitives = keras_model_loss_matching_primitives
#                 try:
#                     search_space = tuner._get_primitive_search_space(primitive)
#                     smac_config = search_space.sample_configuration()
#                     split_name = name.split('.')
#                     smac_config_name = 'smac_config_{}_{}_{}'.format(split_name[-3], split_name[-2], split_name[-1])
#                     PRINT('{} = {{'.format(smac_config_name))
#                     name_config[name] = smac_config_name
#                     for k in smac_config.keys():
#                         if type(smac_config[k]) == str:
#                             PRINT("\t'{}': '{}',".format(k, smac_config[k]))
#                         else:
#                             PRINT("\t'{}': {},".format(k, smac_config[k]))
#                     PRINT('}\n')
#                 except Exception as e:
#                     print(e)
#                     print('{} doest not get its primitive search space'.format(name))
#                     fail_num += 1
#             for k in name_config.keys():
#                 PRINT("\t'{}': {},".format(k, name_config[k]))
#         self.assertEqual(fail_num, 0)
#
#     def test__set_primitive_hyperparameters_Primitive_type(self):
#         smac_config_learner_model_KerasWrap = {
#             'batch_size': 87,
#             'loss_PRIMITIVE': 'd3m.primitives.loss_function.categorical_accuracy.KerasWrap',
#             'metric_PRIMITIVE': 'd3m.primitives.loss_function.categorical_accuracy.KerasWrap',
#             'model_type': 'regression',
#             'optimizer_CHOICE': 'Adamax',
#             'previous_layer_UNION': 'none',
#             'CHILD_optimizer_Adamax_beta_1': 0.9,
#             'CHILD_optimizer_Adamax_beta_2': 0.999,
#             'CHILD_optimizer_Adamax_decay': '0.0',
#             'CHILD_optimizer_Adamax_epsilon': '0.0',
#             'CHILD_optimizer_Adamax_lr': 0.002,
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_classification_search_hybrid_Find_projections = {
#             'binsize': 423,
#             'blackbox_PRIMITIVE': 'd3m.primitives.classification.logistic_regression.SKlearn',
#             'purity': 0.1137064022662066,
#             'support': 9502,
#         }
#
#         smac_config_regression_search_hybrid_numeric_Find_projections = {
#             'binsize': 423,
#             'blackbox_PRIMITIVE': 'd3m.primitives.regression.monomial.Test',
#             'support': 2096,
#         }
#
#         smac_config_semisupervised_classification_iterative_labeling_AutonBox = {
#             'blackbox_PRIMITIVE': 'd3m.primitives.classification.passive_aggressive.SKlearn',
#             'frac': 0.8001555865044776,
#             'iters': 63,
#         }
#
#         fail_num = 0
#         with patch.object(PipelineTuner, "__init__", lambda x, y, z, w: None):
#             tuner = PipelineTuner(1, 2, 3)
#             for name, smac_config in {
#                 'd3m.primitives.learner.model.KerasWrap': smac_config_learner_model_KerasWrap,
#                 'd3m.primitives.classification.search_hybrid.Find_projections':
#                     smac_config_classification_search_hybrid_Find_projections,
#                 'd3m.primitives.regression.search_hybrid_numeric.Find_projections':
#                     smac_config_regression_search_hybrid_numeric_Find_projections,
#                 'd3m.primitives.semisupervised_classification.iterative_labeling.AutonBox':
#                     smac_config_semisupervised_classification_iterative_labeling_AutonBox,
#             }.items():
#                 primitive = index.get_primitive(name)
#                 step = PrimitiveStep(primitive=primitive, resolver=custom_resolver.BlackListResolver())
#                 try:
#                     tuner._set_primitive_hyperparameters(step, smac_config)
#                 except Exception as e:
#                     print(e)
#                     print('{} doest not set its primitive search space'.format(name))
#                     fail_num += 1
#         self.assertEqual(fail_num, 0)
#
#     def test__set_primitive_hyperparameters_Choice_type(self):
#         smac_config_classification_dummy_SKlearn = {
#             'strategy_CHOICE': 'uniform',
#         }
#
#         smac_config_data_transformation_fast_ica_SKlearn = {
#             'algorithm': 'parallel',
#             'fun_CHOICE': 'logcosh',
#             'max_iter': 200,
#             'n_components_UNION': 'int',
#             'tol': 0.0001,
#             'w_init_UNION': 'none',
#             'whiten': False,
#             'CHILD_fun_logcosh_alpha': '1',
#             'CHILD_n_components_int': 0,
#             'CHILD_w_init_none': 'None',
#         }
#
#         smac_config_layer_batch_normalization_KerasWrap = {
#             'beta_constraint_CHOICE': 'NonNeg',
#             'beta_initializer_CHOICE': 'glorot_normal',
#             'beta_regularizer_CHOICE': 'l2',
#             'center': False,
#             'epsilon': 0.8766432842210024,
#             'gamma_constraint_CHOICE': 'MinMaxNorm',
#             'gamma_initializer_CHOICE': 'Ones',
#             'gamma_regularizer_CHOICE': 'l1_l2',
#             'momentum': 0.42438639683482504,
#             'moving_mean_initializer_CHOICE': 'Orthogonal',
#             'moving_variance_initializer_CHOICE': 'Constant',
#             'previous_layer_UNION': 'None',
#             'scale': False,
#             'CHILD_beta_initializer_glorot_normal_seed': 'None',
#             'CHILD_beta_regularizer_l2_l': 0.01,
#             'CHILD_gamma_constraint_MinMaxNorm_axis': '0',
#             'CHILD_gamma_constraint_MinMaxNorm_max_value': '1.0',
#             'CHILD_gamma_constraint_MinMaxNorm_min_value': '0.0',
#             'CHILD_gamma_constraint_MinMaxNorm_rate': '1.0',
#             'CHILD_gamma_regularizer_l1_l2_l1': 0.01,
#             'CHILD_gamma_regularizer_l1_l2_l2': 0.01,
#             'CHILD_moving_mean_initializer_Orthogonal_gain': '1.0',
#             'CHILD_moving_mean_initializer_Orthogonal_seed': 'None',
#             'CHILD_moving_variance_initializer_Constant_value': '0.0',
#         }
#
#         smac_config_layer_convolution_1d_KerasWrap = {
#             'activation': 'hard_sigmoid',
#             'activity_regularizer_CHOICE': 'none',
#             'bias_constraint_CHOICE': 'MaxNorm',
#             'bias_initializer_CHOICE': 'he_uniform',
#             'bias_regularizer_CHOICE': 'l2',
#             'data_format': 'channels_last',
#             'dilation_rate': 5,
#             'filters': 797818571979,
#             'kernel_constraint_CHOICE': 'MaxNorm',
#             'kernel_initializer_CHOICE': 'he_uniform',
#             'kernel_regularizer_CHOICE': 'none',
#             'kernel_size': 15,
#             'padding': 'valid',
#             'previous_layer_UNION': 'previous_layer',
#             'strides': 13,
#             'use_bias': True,
#             'CHILD_bias_constraint_MaxNorm_axis': '0',
#             'CHILD_bias_constraint_MaxNorm_max_value': 2,
#             'CHILD_bias_initializer_he_uniform_seed': 'None',
#             'CHILD_bias_regularizer_l2_l': 0.01,
#             'CHILD_kernel_constraint_MaxNorm_axis': '0',
#             'CHILD_kernel_constraint_MaxNorm_max_value': 2,
#             'CHILD_kernel_initializer_he_uniform_seed': 'None',
#             'CHILD_previous_layer_previous_layer_PRIMITIVE': 'd3m.primitives.layer.concat.KerasWrap',
#         }
#
#         smac_config_layer_convolution_2d_KerasWrap = {
#             'activation': 'relu',
#             'activity_regularizer_CHOICE': 'l1',
#             'bias_constraint_CHOICE': 'none',
#             'bias_initializer_CHOICE': 'glorot_normal',
#             'bias_regularizer_CHOICE': 'l2',
#             'data_format': 'channels_last',
#             'dilation_rate': 16,
#             'filters': 770036927986,
#             'kernel_constraint_CHOICE': 'NonNeg',
#             'kernel_initializer_CHOICE': 'Identity',
#             'kernel_regularizer_CHOICE': 'l1',
#             'kernel_size': 15,
#             'padding': 'causal',
#             'previous_layer_UNION': 'previous_layer',
#             'strides': 6,
#             'use_bias': True,
#             'CHILD_activity_regularizer_l1_l': 0.01,
#             'CHILD_bias_initializer_glorot_normal_seed': 'None',
#             'CHILD_bias_regularizer_l2_l': 0.01,
#             'CHILD_kernel_initializer_Identity_gain': '1.0',
#             'CHILD_kernel_regularizer_l1_l': 0.01,
#             'CHILD_previous_layer_previous_layer_PRIMITIVE': 'd3m.primitives.layer.convolution_2d.KerasWrap',
#         }
#
#         smac_config_layer_convolution_3d_KerasWrap = {
#             'activation': 'elu',
#             'activity_regularizer_CHOICE': 'l2',
#             'bias_constraint_CHOICE': 'none',
#             'bias_initializer_CHOICE': 'VarianceScaling',
#             'bias_regularizer_CHOICE': 'l1',
#             'data_format': 'channels_first',
#             'dilation_rate': 16,
#             'filters': 207487200412,
#             'kernel_constraint_CHOICE': 'UnitNorm',
#             'kernel_initializer_CHOICE': 'Identity',
#             'kernel_regularizer_CHOICE': 'l1',
#             'kernel_size': 19,
#             'padding': 'same',
#             'previous_layer_UNION': 'none',
#             'strides': 19,
#             'use_bias': False,
#             'CHILD_activity_regularizer_l2_l': 0.01,
#             'CHILD_bias_initializer_VarianceScaling_distribution': 'normal',
#             'CHILD_bias_initializer_VarianceScaling_mode': 'fan_in',
#             'CHILD_bias_initializer_VarianceScaling_scale': '1.0',
#             'CHILD_bias_initializer_VarianceScaling_seed': 'None',
#             'CHILD_bias_regularizer_l1_l': 0.01,
#             'CHILD_kernel_constraint_UnitNorm_axis': '0',
#             'CHILD_kernel_initializer_Identity_gain': '1.0',
#             'CHILD_kernel_regularizer_l1_l': 0.01,
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_layer_dense_KerasWrap = {
#             'activation': 'softplus',
#             'activity_regularizer_CHOICE': 'l1',
#             'bias_constraint_CHOICE': 'UnitNorm',
#             'bias_initializer_CHOICE': 'RandomNormal',
#             'bias_regularizer_CHOICE': 'l1',
#             'kernel_constraint_CHOICE': 'MinMaxNorm',
#             'kernel_initializer_CHOICE': 'he_uniform',
#             'kernel_regularizer_CHOICE': 'none',
#             'previous_layer_UNION': 'previous_layer',
#             'units': 963920998616,
#             'use_bias': True,
#             'CHILD_activity_regularizer_l1_l': 0.01,
#             'CHILD_bias_constraint_UnitNorm_axis': '0',
#             'CHILD_bias_initializer_RandomNormal_mean': '0.0',
#             'CHILD_bias_initializer_RandomNormal_seed': 'None',
#             'CHILD_bias_initializer_RandomNormal_stddev': 0.05,
#             'CHILD_bias_regularizer_l1_l': 0.01,
#             'CHILD_kernel_constraint_MinMaxNorm_axis': '0',
#             'CHILD_kernel_constraint_MinMaxNorm_max_value': '1.0',
#             'CHILD_kernel_constraint_MinMaxNorm_min_value': '0.0',
#             'CHILD_kernel_constraint_MinMaxNorm_rate': '1.0',
#             'CHILD_kernel_initializer_he_uniform_seed': 'None',
#             'CHILD_previous_layer_previous_layer_PRIMITIVE': 'd3m.primitives.layer.flatten.KerasWrap',
#         }
#
#         smac_config_learner_model_KerasWrap = {
#             'batch_size': 53,
#             'loss_PRIMITIVE': 'd3m.primitives.loss_function.categorical_crossentropy.KerasWrap',
#             'metric_PRIMITIVE': 'd3m.primitives.loss_function.squared_hinge.KerasWrap',
#             'model_type': 'classification',
#             'optimizer_CHOICE': 'RMSprop',
#             'previous_layer_UNION': 'none',
#             'CHILD_optimizer_RMSprop_decay': '0.0',
#             'CHILD_optimizer_RMSprop_epsilon': '0.0',
#             'CHILD_optimizer_RMSprop_lr': 0.001,
#             'CHILD_optimizer_RMSprop_rho': 0.9,
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_regression_dummy_SKlearn = {
#             'strategy_CHOICE': 'mean',
#         }
#
#         smac_config_regression_kernel_ridge_SKlearn = {
#             'alpha': 1,
#             'kernel_CHOICE': 'laplacian',
#             'CHILD_kernel_laplacian_gamma': 0,
#         }
#
#         smac_config_regression_svr_SKlearn = {
#             'C': 1,
#             'cache_size': 200,
#             'epsilon': 0.1,
#             'kernel_CHOICE': 'rbf',
#             'max_iter': -1,
#             'shrinking': False,
#             'tol': 0.001,
#             'CHILD_kernel_rbf_gamma_UNION': 'float',
#             'CHILD_CHILD_kernel_rbf_gamma_float': 0.1,
#         }
#
#         fail_num = 0
#         with patch.object(PipelineTuner, "__init__", lambda x, y, z, w: None):
#             tuner = PipelineTuner(1, 2, 3)
#             for name, smac_config in {
#                 'd3m.primitives.classification.dummy.SKlearn': smac_config_classification_dummy_SKlearn,
#                 'd3m.primitives.data_transformation.fast_ica.SKlearn': smac_config_data_transformation_fast_ica_SKlearn,
#                 'd3m.primitives.layer.batch_normalization.KerasWrap': smac_config_layer_batch_normalization_KerasWrap,
#                 'd3m.primitives.layer.convolution_1d.KerasWrap': smac_config_layer_convolution_1d_KerasWrap,
#                 'd3m.primitives.layer.convolution_2d.KerasWrap': smac_config_layer_convolution_2d_KerasWrap,
#                 'd3m.primitives.layer.convolution_3d.KerasWrap': smac_config_layer_convolution_3d_KerasWrap,
#                 'd3m.primitives.layer.dense.KerasWrap': smac_config_layer_dense_KerasWrap,
#                 'd3m.primitives.learner.model.KerasWrap': smac_config_learner_model_KerasWrap,
#                 'd3m.primitives.regression.dummy.SKlearn': smac_config_regression_dummy_SKlearn,
#                 'd3m.primitives.regression.kernel_ridge.SKlearn': smac_config_regression_kernel_ridge_SKlearn,
#                 'd3m.primitives.regression.svr.SKlearn': smac_config_regression_svr_SKlearn,
#             }.items():
#                 primitive = index.get_primitive(name)
#                 step = PrimitiveStep(primitive=primitive, resolver=custom_resolver.BlackListResolver())
#                 try:
#                     tuner._set_primitive_hyperparameters(step, smac_config)
#                 except Exception as e:
#                     print(e)
#                     print('{} doest not set its primitive search space'.format(name))
#                     fail_num += 1
#         self.assertEqual(fail_num, 0)
#
#     def test__set_primitive_hyperparameters__Sequence_type(self):
#         smac_config_data_preprocessing_count_vectorizer_SKlearn = {
#             'analyzer': 'char',
#             'binary': True,
#             'CHILD_ngram_range_0': 1,
#             'CHILD_ngram_range_1': 1,
#             'CHILD_ngram_range_ASCENDING': 'True',
#             'lowercase': False,
#             'max_df_UNION': 'absolute',
#             'max_features_UNION': 'None',
#             'min_df_UNION': 'proportion',
#             'ngram_range_SORTEDLIST': 2,
#             'stop_words_UNION': 'list',
#             'strip_accents_UNION': 'accents',
#             'token_pattern': '(?u)\b\w\w+\b',
#             'CHILD_max_df_absolute': 1,
#             'CHILD_min_df_proportion': 0.22837921589968047,
#             'CHILD_stop_words_LIST_LIST': 0,
#             'CHILD_strip_accents_accents': 'unicode',
#         }
#
#         smac_config_classification_mlp_SKlearn = {
#             'activation': 'relu',
#             'alpha': 0.0001,
#             'batch_size_UNION': 'auto',
#             'beta_1': 0.40024781186019587,
#             'beta_2': 0.39404292376402594,
#             'CHILD_hidden_layer_sizes_0': 100,
#             'early_stopping': False,
#             'epsilon': 1e-08,
#             'hidden_layer_sizes_LIST': 1,
#             'learning_rate': 'constant',
#             'learning_rate_init': 0.001,
#             'max_iter': 200,
#             'momentum': 0.45819640155782493,
#             'n_iter_no_change': 10,
#             'nesterovs_momentum': True,
#             'power_t': 0.5,
#             'shuffle': False,
#             'validation_fraction': 0.1,
#             'warm_start': True,
#             'CHILD_batch_size_auto': 'auto',
#         }
#
#         smac_config_regression_mlp_SKlearn = {
#             'activation': 'tanh',
#             'alpha': 0.0001,
#             'batch_size_UNION': 'int',
#             'beta_1': 0.7551261290184662,
#             'beta_2': 0.5527292330511101,
#             'CHILD_hidden_layer_sizes_0': 100,
#             'early_stopping': True,
#             'epsilon': 1e-08,
#             'hidden_layer_sizes_LIST': 1,
#             'learning_rate': 'adaptive',
#             'learning_rate_init': 0.001,
#             'max_iter': 200,
#             'momentum': 0.08627788529955716,
#             'n_iter_no_change': 10,
#             'nesterovs_momentum': True,
#             'power_t': 0.5,
#             'shuffle': True,
#             'solver': 'sgd',
#             'warm_start': False,
#             'CHILD_batch_size_int': 16,
#         }
#
#         smac_config_data_transformation_one_hot_encoder_SKlearn = {
#             'categories': 'auto',
#             'handle_unknown': 'error',
#             'n_values_UNION': 'auto',
#             'sparse': False,
#             'CHILD_n_values_auto': 'auto',
#         }
#
#         smac_config_data_preprocessing_tfidf_vectorizer_SKlearn = {
#             'analyzer': 'char_wb',
#             'binary': False,
#             'CHILD_ngram_range_0': 1,
#             'CHILD_ngram_range_1': 1,
#             'CHILD_ngram_range_ASCENDING': 'True',
#             'lowercase': True,
#             'max_df_UNION': 'absolute',
#             'max_features_UNION': 'none',
#             'min_df_UNION': 'proportion',
#             'ngram_range_SORTEDLIST': 2,
#             'norm_UNION': 'str',
#             'smooth_idf': False,
#             'stop_words_UNION': 'None',
#             'strip_accents_UNION': 'accents',
#             'sublinear_tf': True,
#             'token_pattern': '(?u)\b\w\w+\b',
#             'use_idf': True,
#             'CHILD_max_df_absolute': 1,
#             'CHILD_max_features_none': 'None',
#             'CHILD_min_df_proportion': 0.4515462840285176,
#             'CHILD_norm_str': 'l1',
#             'CHILD_strip_accents_accents': 'ascii',
#         }
#
#         smac_config_data_preprocessing_min_max_scaler_SKlearn = {
#             'CHILD_feature_range_0': 0,
#             'CHILD_feature_range_1': 1,
#             'CHILD_feature_range_ASCENDING': 'True',
#             'feature_range_SORTEDSET': 2,
#         }
#
#         smac_config_data_preprocessing_robust_scaler_SKlearn = {
#             'CHILD_quantile_range_0': 90.11727786572773,
#             'CHILD_quantile_range_1': 84.98889689511145,
#             'CHILD_quantile_range_ASCENDING': 'True',
#             'quantile_range_SORTEDSET': 2,
#             'with_centering': True,
#             'with_scaling': False,
#         }
#
#         fail_num = 0
#         with patch.object(PipelineTuner, "__init__", lambda x, y, z, w: None):
#             tuner = PipelineTuner(1, 2, 3)
#             for name, smac_config in {
#                 'd3m.primitives.data_preprocessing.count_vectorizer.SKlearn':
#                     smac_config_data_preprocessing_count_vectorizer_SKlearn,
#                 'd3m.primitives.classification.mlp.SKlearn': smac_config_classification_mlp_SKlearn,
#                 'd3m.primitives.regression.mlp.SKlearn': smac_config_regression_mlp_SKlearn,
#                 'd3m.primitives.data_transformation.one_hot_encoder.SKlearn':
#                     smac_config_data_transformation_one_hot_encoder_SKlearn,
#                 'd3m.primitives.data_preprocessing.tfidf_vectorizer.SKlearn':
#                     smac_config_data_preprocessing_tfidf_vectorizer_SKlearn,
#                 'd3m.primitives.data_preprocessing.min_max_scaler.SKlearn':
#                     smac_config_data_preprocessing_min_max_scaler_SKlearn,
#                 'd3m.primitives.data_preprocessing.robust_scaler.SKlearn':
#                     smac_config_data_preprocessing_robust_scaler_SKlearn,
#             }.items():
#                 primitive = index.get_primitive(name)
#                 step = PrimitiveStep(primitive=primitive, resolver=custom_resolver.BlackListResolver())
#                 try:
#                     tuner._set_primitive_hyperparameters(step, smac_config)
#                 except Exception as e:
#                     print(e)
#                     print('{} doest not set its primitive search space'.format(name))
#                     fail_num += 1
#         self.assertEqual(fail_num, 0)
#
#     def test__set_primitive_hyperparameters_Union_type(self):
#         smac_config_classification_bagging_SKlearn = {
#             'bootstrap': False,
#             'bootstrap_features': False,
#             'max_features_UNION': 1.0,
#             'max_samples_UNION': 'absolute',
#             'n_estimators': 10,
#             'oob_score': False,
#             'warm_start': True,
#             'CHILD_max_samples_absolute': 0,
#         }
#
#         smac_config_classification_decision_tree_SKlearn = {
#             'class_weight_UNION': 'str',
#             'criterion': 'gini',
#             'max_depth_UNION': 'none',
#             'max_features_UNION': 'calculated',
#             'max_leaf_nodes_UNION': 'None',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 'absolute',
#             'min_samples_split_UNION': 2,
#             'min_weight_fraction_leaf': 0.25846910367638876,
#             'presort': False,
#             'splitter': 'random',
#             'CHILD_class_weight_str': 'balanced',
#             'CHILD_max_depth_none': 'None',
#             'CHILD_max_features_calculated': 'sqrt',
#             'CHILD_min_samples_leaf_absolute': 1,
#         }
#
#         smac_config_classification_bernoulli_naive_bayes_SKlearn = {
#             'alpha': 1,
#             'binarize_UNION': 'float',
#             'fit_prior': True,
#             'CHILD_binarize_float': 0,
#         }
#
#         smac_config_classification_extra_trees_SKlearn = {
#             'bootstrap': True,
#             'class_weight_UNION': 'str',
#             'criterion': 'entropy',
#             'max_depth_UNION': 'none',
#             'max_features_UNION': 'specified_int',
#             'max_leaf_nodes_UNION': 'None',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 1,
#             'min_samples_split_UNION': 'absolute',
#             'min_weight_fraction_leaf': 0.45463907029717504,
#             'n_estimators': 10,
#             'oob_score': False,
#             'warm_start': True,
#             'CHILD_class_weight_str': 'balanced',
#             'CHILD_max_depth_none': 'None',
#             'CHILD_max_features_specified_int': 0,
#             'CHILD_min_samples_split_absolute': 2,
#         }
#
#         smac_config_classification_gradient_boosting_SKlearn = {
#             'criterion': 'mse',
#             'learning_rate': 0.1,
#             'loss': 'deviance',
#             'max_depth': 3,
#             'max_features_UNION': 'percent',
#             'max_leaf_nodes_UNION': 'int',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 'percent',
#             'min_samples_split_UNION': 'absolute',
#             'min_weight_fraction_leaf': 0.2145466599395212,
#             'n_estimators': 100,
#             'n_iter_no_change_UNION': 'int',
#             'tol': 0.0001,
#             'validation_fraction': 0.5627474111536589,
#             'warm_start': False,
#             'CHILD_max_features_percent': 0.1885731712596559,
#             'CHILD_max_leaf_nodes_int': 10,
#             'CHILD_min_samples_leaf_percent': 0.4932766751854083,
#             'CHILD_min_samples_split_absolute': 2,
#             'CHILD_n_iter_no_change_int': 5,
#             'CHILD_presort_auto': 'auto',
#         }
#
#         smac_config_classification_light_gbm_DataFrameCommon = {
#             'colsample_bytree': 0.6111280046265687,
#             'learning_rate': 0.06702102958530319,
#             'max_delta_step_UNION': 0,
#             'max_depth_UNION': 'limit',
#             'min_child_samples': 20,
#             'min_child_weight': 1,
#             'min_split_gain': 0,
#             'n_estimators': 9326,
#             'n_more_estimators': 123,
#             'num_leaves_base': 1.7694716151744858,
#             'reg_alpha': 0,
#             'reg_lambda': 1,
#             'subsample': 0.2824507793510604,
#             'subsample_for_bin': 200000,
#             'subsample_freq': 0,
#             'CHILD_max_depth_limit': 5,
#         }
#
#         smac_config_classification_linear_discriminant_analysis_SKlearn = {
#             'n_components_UNION': 'none',
#             'shrinkage_UNION': 'float',
#             'solver': 'lsqr',
#             'store_covariance': False,
#             'tol': 0.0001,
#             'CHILD_n_components_none': 'None',
#             'CHILD_shrinkage_float': 0.5531730116009316,
#         }
#
#         smac_config_classification_linear_svc_SKlearn = {
#             'C': 1,
#             'class_weight_UNION': 'None',
#             'dual': True,
#             'fit_intercept': True,
#             'intercept_scaling': '1',
#             'loss': 'hinge',
#             'max_iter': 1000,
#             'multi_class': 'ovr',
#             'penalty': 'l2',
#             'tol': 0.0001,
#         }
#
#         smac_config_classification_logistic_regression_SKlearn = {
#             'C': '1.0',
#             'class_weight_UNION': 'none',
#             'dual': True,
#             'fit_intercept': True,
#             'intercept_scaling': '1',
#             'max_iter': 100,
#             'multi_class': 'ovr',
#             'penalty': 'l2',
#             'solver': 'sag',
#             'tol': 0.0001,
#             'warm_start': True,
#             'CHILD_class_weight_none': 'None',
#         }
#
#         smac_config_classification_lupi_rf_LupiRFClassifier = {
#             'bootstrap': True,
#             'class_weight_UNION': 'str',
#             'criterion': 'entropy',
#             'max_depth_UNION': 'none',
#             'max_features_UNION': 'percent',
#             'max_leaf_nodes_UNION': 'None',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 'percent',
#             'min_samples_split_UNION': 2,
#             'min_weight_fraction_leaf': 0.3276758495900079,
#             'n_estimators': 5000,
#             'oob_score': True,
#             'warm_start': True,
#             'CHILD_class_weight_str': 'balanced_subsample',
#             'CHILD_max_depth_none': 'None',
#             'CHILD_max_features_percent': 0.8888216772250526,
#             'CHILD_min_samples_leaf_percent': 0.34287302034360695,
#         }
#
#         smac_config_classification_lupi_rfsel_LupiRFSelClassifier = {
#             'bootstrap': True,
#             'class_weight_UNION': 'balanced',
#             'criterion': 'gini',
#             'max_depth_UNION': 10,
#             'max_features_UNION': 'specified_int',
#             'max_leaf_nodes_UNION': 'none',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 'absolute',
#             'min_samples_split_UNION': 'percent',
#             'min_weight_fraction_leaf': 0.3510251901858964,
#             'n_cv_UNION': 'int',
#             'n_estimators': 5000,
#             'oob_score': False,
#             'regressor_type': 'none',
#             'warm_start': False,
#             'CHILD_max_features_specified_int': 0,
#             'CHILD_max_leaf_nodes_none': 'None',
#             'CHILD_min_samples_leaf_absolute': 1,
#             'CHILD_min_samples_split_percent': 0.6852568950564858,
#             'CHILD_n_cv_int': 3,
#         }
#
#         smac_config_classification_lupi_svm_LupiSvmClassifier = {
#             'C': 1,
#             'C_gridsearch': 'TUPLE_STR_float_-1.0,6.5,3.4',
#             'class_weight': 'balanced',
#             'coef0': '0',
#             'cv_score': 'f1',
#             'degree': 3,
#             'gamma_gridsearch': 'TUPLE_STR_float_-1.0,6.5,3.4',
#             'gamma_UNION': 'float',
#             'kernel': 'rbf',
#             'probability': 'False',
#             'shrinking': 'True',
#             'tol': 0.001,
#             'CHILD_gamma_float': 0.1,
#         }
#
#         smac_config_classification_mlp_SKlearn = {
#             'activation': 'identity',
#             'alpha': 0.0001,
#             'batch_size_UNION': 'int',
#             'beta_1': 0.7323522463579131,
#             'beta_2': 0.47246580793071224,
#             'CHILD_hidden_layer_sizes_0': 100,
#             'early_stopping': True,
#             'epsilon': 1e-08,
#             'hidden_layer_sizes_LIST': 1,
#             'learning_rate': 'constant',
#             'learning_rate_init': 0.001,
#             'max_iter': 200,
#             'momentum': 0.09944621425731592,
#             'n_iter_no_change': 10,
#             'nesterovs_momentum': True,
#             'power_t': 0.5,
#             'shuffle': False,
#             'solver': 'adam',
#             'tol': 0.0001,
#             'validation_fraction': 0.1,
#             'warm_start': True,
#             'CHILD_batch_size_int': 16,
#         }
#
#         smac_config_classification_nearest_centroid_SKlearn = {
#             'metric': 'manhattan',
#             'shrink_threshold_UNION': 'none',
#             'CHILD_shrink_threshold_none': 'None',
#         }
#
#         smac_config_classification_passive_aggressive_SKlearn = {
#             'C': 1,
#             'average_UNION': False,
#             'class_weight_UNION': 'none',
#             'early_stopping': True,
#             'fit_intercept': True,
#             'loss': 'hinge',
#             'max_iter_UNION': 'int',
#             'n_iter_no_change': 5,
#             'shuffle': False,
#             'tol_UNION': 'float',
#             'validation_fraction': 0.6319773609831988,
#             'warm_start': True,
#             'CHILD_class_weight_none': 'None',
#             'CHILD_max_iter_int': 1000,
#             'CHILD_tol_float': 0.001,
#         }
#
#         smac_config_classification_quadratic_discriminant_analysis_SKlearn = {
#             'reg_param': 0.14277098321157333,
#             'store_covariance_UNION': 'None',
#             'tol': 0.0001,
#         }
#
#         smac_config_classification_random_forest_DataFrameCommon = {
#             'bootstrap': 'bootstrap',
#             'criterion': 'entropy',
#             'max_depth_UNION': 'limit',
#             'max_features_UNION': 'fixed',
#             'max_leaf_nodes_UNION': 'unlimited',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 'ratio',
#             'min_samples_split_UNION': 'fixed',
#             'min_weight_fraction_leaf': 0.3635588186047856,
#             'n_estimators': 3210,
#             'n_more_estimators': 9255,
#             'CHILD_max_depth_limit': 10,
#             'CHILD_max_features_fixed': 1,
#             'CHILD_max_leaf_nodes_unlimited': 'None',
#             'CHILD_min_samples_leaf_ratio': 0.3382107655451178,
#             'CHILD_min_samples_split_fixed': 2,
#         }
#
#         smac_config_classification_random_forest_SKlearn = {
#             'bootstrap': False,
#             'class_weight_UNION': 'None',
#             'criterion': 'entropy',
#             'max_depth_UNION': 'None',
#             'max_features_UNION': 'specified_int',
#             'max_leaf_nodes_UNION': 'None',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 1,
#             'min_samples_split_UNION': 2,
#             'min_weight_fraction_leaf': 0.3444093147827589,
#             'n_estimators': 10,
#             'oob_score': True,
#             'warm_start': False,
#             'CHILD_max_features_specified_int': 0,
#         }
#
#         smac_config_classification_sgd_SKlearn = {
#             'alpha': 0.0001,
#             'average_UNION': 'int',
#             'class_weight_UNION': 'None',
#             'early_stopping': True,
#             'epsilon': 0.1,
#             'eta0': 0.0,
#             'fit_intercept': False,
#             'l1_ratio': 0.35561795170749555,
#             'learning_rate': 'optimal',
#             'loss': 'log',
#             'max_iter_UNION': 1000,
#             'n_iter_no_change': 5,
#             'penalty': 'l1',
#             'power_t': 0.5,
#             'shuffle': True,
#             'tol_UNION': 'float',
#             'validation_fraction': 0.042313377407882324,
#             'warm_start': True,
#             'CHILD_average_int': 2,
#             'CHILD_tol_float': 0.001,
#         }
#
#         smac_config_classification_svc_SKlearn = {
#             'C': 1,
#             'cache_size': 200,
#             'class_weight_UNION': 'None',
#             'coef0': '0',
#             'decision_function_shape': 'ovr',
#             'degree': 3,
#             'gamma_UNION': 'float',
#             'kernel': 'precomputed',
#             'max_iter': -1,
#             'probability': False,
#             'shrinking': False,
#             'tol': 0.001,
#             'CHILD_gamma_float': 0.1,
#         }
#
#         smac_config_classification_xgboost_dart_DataFrameCommon = {
#             'base_score': 0.5,
#             'colsample_bylevel': 0.4449939251953702,
#             'colsample_bytree': 0.23898573388347485,
#             'gamma': 0.0,
#             'learning_rate': 0.8266641688082723,
#             'max_delta_step_UNION': 'unlimited',
#             'max_depth_UNION': 'limit',
#             'min_child_weight': 1,
#             'n_estimators': 1304,
#             'n_more_estimators': 5307,
#             'normalize_type': 'tree',
#             'one_drop': 0,
#             'rate_drop': 0.31879405302131336,
#             'reg_alpha': 0,
#             'reg_lambda': 1,
#             'sample_type': 'uniform',
#             'scale_pos_weight': 1,
#             'skip_drop': 0.5688198218534442,
#             'subsample': 0.20440109201311074,
#             'CHILD_max_delta_step_unlimited': 0,
#             'CHILD_max_depth_limit': 3,
#         }
#
#         smac_config_classification_xgboost_gbtree_DataFrameCommon = {
#             'base_score': 0.5,
#             'colsample_bylevel': 0.6192973012812882,
#             'colsample_bytree': 0.006453873646577278,
#             'gamma': 0.0,
#             'importance_type': 'gain',
#             'learning_rate': 0.5662544800656831,
#             'max_delta_step_UNION': 'limit',
#             'max_depth_UNION': 3,
#             'min_child_weight': 1,
#             'n_estimators': 7266,
#             'n_more_estimators': 6297,
#             'reg_alpha': 0,
#             'reg_lambda': 1,
#             'scale_pos_weight': 1,
#             'subsample': 0.5029691929768613,
#             'CHILD_max_delta_step_limit': 1,
#         }
#
#         smac_config_clustering_ekss_Umich = {
#             'dim_subspaces': 43,
#             'n_base': 598,
#             'thresh_UNION': 5,
#         }
#
#         smac_config_clustering_ssc_admm_Umich = {
#             'alpha_UNION': -1,
#         }
#
#         smac_config_clustering_ssc_cvx_Umich = {
#             'alpha_UNION': 'bounded',
#             'CHILD_alpha_bounded': 150,
#         }
#
#         smac_config_data_cleaning_imputer_SKlearn = {
#             'fill_value_UNION': 'none',
#             'strategy': 'most_frequent',
#             'CHILD_fill_value_none': 'None',
#         }
#
#         smac_config_data_cleaning_missing_indicator_SKlearn = {
#             'error_on_new': False,
#             'features': 'missing-only',
#             'sparse_UNION': 'auto',
#             'CHILD_sparse_auto': 'auto',
#         }
#
#         smac_config_data_compression_go_dec_Umich = {
#             'c_UNION': 'boundedint',
#             'epsilon': 0.016026087037044796,
#             'power': 10,
#             'r_UNION': 'boundedfloat',
#             'CHILD_c_boundedint': 10,
#             'CHILD_r_boundedfloat': 0.05217552760188583,
#         }
#
#         smac_config_data_compression_pcp_ialm_Umich = {
#             'epsilon': 0.10805824734234455,
#             'lamb_UNION': 'boundedfloat',
#             'mu_UNION': 'enum',
#             'rho': 1.5,
#             'CHILD_lamb_boundedfloat': 0.4550396918999975,
#             'CHILD_mu_enum': -1,
#         }
#
#         smac_config_data_preprocessing_dataset_sample_Common = {
#             'sample_size_UNION': 'absolute',
#             'CHILD_sample_size_absolute': 1,
#         }
#
#         smac_config_data_preprocessing_feature_agglomeration_SKlearn = {
#             'affinity': 'manhattan',
#             'compute_full_tree_UNION': 'bool',
#             'connectivity_UNION': 'ndarray',
#             'linkage': 'complete',
#             'n_clusters': 2,
#             'CHILD_compute_full_tree_bool': False,
#             'CHILD_connectivity_ndarray': 'NDARRAY_STR_',
#         }
#
#         smac_config_data_preprocessing_nystroem_SKlearn = {
#             'coef0_UNION': 'none',
#             'degree_UNION': 'none',
#             'gamma_UNION': 'None',
#             'kernel': 'linear',
#             'n_components': 100,
#             'CHILD_coef0_none': 'None',
#             'CHILD_degree_none': 'None',
#         }
#
#         smac_config_data_preprocessing_random_trees_embedding_SKlearn = {
#             'max_depth_UNION': 5,
#             'max_leaf_nodes_UNION': 'int',
#             'min_impurity_decrease': 0,
#             'min_impurity_split_UNION': 'none',
#             'min_weight_fraction_leaf': 0.14118348858122243,
#             'n_estimators': 10,
#             'sparse_output': False,
#             'warm_start': False,
#             'CHILD_max_leaf_nodes_int': 10,
#             'CHILD_min_impurity_split_none': 'None',
#         }
#
#         smac_config_data_transformation_fast_ica_SKlearn = {
#             'algorithm': 'deflation',
#             'fun_CHOICE': 'exp',
#             'max_iter': 200,
#             'n_components_UNION': 'int',
#             'tol': 0.0001,
#             'w_init_UNION': 'None',
#             'whiten': True,
#             'CHILD_n_components_int': 0,
#         }
#
#         smac_config_feature_construction_corex_continuous_DSBOX = {
#             'n_hidden_UNION': 0.5,
#         }
#
#         smac_config_feature_construction_deep_feature_synthesis_Featuretools = {
#             'aggregation_avg_time_between': 'False',
#             'aggregation_count': 'False',
#             'aggregation_max': 'True',
#             'aggregation_mean': 'True',
#             'aggregation_median': 'False',
#             'aggregation_min': 'True',
#             'aggregation_mode': 'False',
#             'aggregation_num_true': 'False',
#             'aggregation_num_unique': 'True',
#             'aggregation_percent_true': 'True',
#             'aggregation_skew': 'True',
#             'aggregation_std': 'True',
#             'aggregation_sum': 'True',
#             'encode': 'True',
#             'find_equivalent_categories': 'True',
#             'include_unknown': 'True',
#             'max_depth': 1,
#             'min_categorical_nunique_UNION': 0.1,
#             'normalize_single_table': 'True',
#             'remove_low_information': 'True',
#             'sample_learning_data': 'None',
#             'top_n': 248,
#             'transform_hour': 'False',
#             'transform_is_weekend': 'False',
#             'transform_week': 'False',
#             'transform_weekday': 'True',
#             'transform_year': 'True',
#         }
#
#         smac_config_feature_extraction_kernel_pca_SKlearn = {
#             'alpha': '1',
#             'coef0': '1',
#             'degree': 3,
#             'eigen_solver': 'arpack',
#             'fit_inverse_transform': True,
#             'gamma_UNION': 'none',
#             'kernel': 'sigmoid',
#             'max_iter_UNION': 'None',
#             'n_components_UNION': 'None',
#             'remove_zero_eig': True,
#             'tol': 0,
#             'CHILD_gamma_none': 'None',
#         }
#
#         smac_config_feature_extraction_pca_SKlearn = {
#             'iterated_power_UNION': 'int',
#             'n_components_UNION': 'float',
#             'svd_solver': 'full',
#             'tol': 0.0,
#             'whiten': False,
#             'CHILD_iterated_power_int': 0,
#             'CHILD_n_components_float': 0.8072439448672528,
#         }
#
#         smac_config_feature_selection_generic_univariate_select_SKlearn = {
#             'mode': 'percentile',
#             'param_UNION': 'float',
#             'score_func': 'f_regression',
#             'CHILD_param_float': 1e-05,
#         }
#
#         smac_config_regression_extra_trees_SKlearn = {
#             'bootstrap': False,
#             'criterion': 'mae',
#             'max_depth_UNION': 'int',
#             'max_features_UNION': 'specified_int',
#             'max_leaf_nodes_UNION': 'none',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 1,
#             'min_samples_split_UNION': 2,
#             'min_weight_fraction_leaf': 0.10144368490331218,
#             'n_estimators': 10,
#             'oob_score': True,
#             'warm_start': True,
#             'CHILD_max_depth_int': 5,
#             'CHILD_max_features_specified_int': 0,
#             'CHILD_max_leaf_nodes_none': 'None',
#         }
#
#         smac_config_regression_gaussian_process_SKlearn = {
#             'alpha_UNION': 'float',
#             'copy_X_train': False,
#             'n_restarts_optimizer': 0,
#             'normalize_y': True,
#             'optimizer': 'fmin_l_bfgs_b',
#             'CHILD_alpha_float': 1e-10,
#         }
#
#         smac_config_regression_gradient_boosting_SKlearn = {
#             'alpha': 0.9,
#             'criterion': 'friedman_mse',
#             'learning_rate': 0.1,
#             'loss': 'quantile',
#             'max_depth': 3,
#             'max_features_UNION': 'percent',
#             'max_leaf_nodes_UNION': 'None',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 'percent',
#             'min_samples_split_UNION': 2,
#             'min_weight_fraction_leaf': 0.19694688996254356,
#             'n_estimators': 100,
#             'n_iter_no_change_UNION': 'none',
#             'presort_UNION': 'bool',
#             'subsample': 1,
#             'tol': 0.0001,
#             'validation_fraction': 0.6948661895790231,
#             'warm_start': True,
#             'CHILD_max_features_percent': 0.5222040411907665,
#             'CHILD_min_samples_leaf_percent': 0.47744831789648295,
#             'CHILD_n_iter_no_change_none': 'None',
#             'CHILD_presort_bool': True,
#         }
#
#         smac_config_regression_lars_SKlearn = {
#             'eps': 2.220446049250313e-16,
#             'fit_intercept': True,
#             'fit_path': False,
#             'n_nonzero_coefs': 500,
#             'normalize': True,
#             'positive': True,
#             'precompute_UNION': 'auto',
#             'CHILD_precompute_auto': 'auto',
#         }
#
#         smac_config_regression_lasso_SKlearn = {
#             'alpha': 1,
#             'fit_intercept': False,
#             'max_iter': 1000,
#             'normalize': False,
#             'positive': False,
#             'precompute_UNION': False,
#             'selection': 'random',
#             'tol': 0.0001,
#             'warm_start': True,
#         }
#
#         smac_config_regression_lasso_cv_SKlearn = {
#             'alphas_UNION': 'none',
#             'cv_UNION': 'int',
#             'eps': 0.001,
#             'fit_intercept': True,
#             'max_iter': 1000,
#             'n_alphas': 100,
#             'normalize': True,
#             'positive': False,
#             'precompute_UNION': 'auto',
#             'selection': 'cyclic',
#             'tol': 0.0001,
#             'CHILD_alphas_none': 'None',
#             'CHILD_cv_int': 5,
#             'CHILD_precompute_auto': 'auto',
#         }
#
#         smac_config_regression_mlp_SKlearn = {
#             'activation': 'logistic',
#             'alpha': 0.0001,
#             'batch_size_UNION': 'auto',
#             'beta_1': 0.16519972702957786,
#             'beta_2': 0.8386534723159227,
#             'CHILD_hidden_layer_sizes_0': 100,
#             'early_stopping': True,
#             'epsilon': 1e-08,
#             'hidden_layer_sizes_LIST': 1,
#             'learning_rate': 'constant',
#             'learning_rate_init': 0.001,
#             'max_iter': 200,
#             'momentum': 0.6036793610170241,
#             'n_iter_no_change': 10,
#             'nesterovs_momentum': False,
#             'power_t': 0.5,
#             'shuffle': False,
#             'solver': 'sgd',
#             'tol': 0.0001,
#             'validation_fraction': 0.1,
#             'warm_start': False,
#             'CHILD_batch_size_auto': 'auto',
#         }
#
#         smac_config_regression_passive_aggressive_SKlearn = {
#             'C': '1',
#             'average_UNION': False,
#             'early_stopping': False,
#             'epsilon': 0.1,
#             'fit_intercept': True,
#             'loss': 'squared_epsilon_insensitive',
#             'max_iter': 1000,
#             'n_iter_no_change': 5,
#             'shuffle': False,
#             'tol_UNION': 'none',
#             'validation_fraction': 0.6961770520336076,
#             'warm_start': False,
#             'CHILD_tol_none': 'None',
#         }
#
#         smac_config_regression_random_forest_SKlearn = {
#             'bootstrap': False,
#             'criterion': 'mse',
#             'max_depth_UNION': 'none',
#             'max_features_UNION': 'specified_int',
#             'max_leaf_nodes_UNION': 'none',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 'absolute',
#             'min_samples_split_UNION': 'percent',
#             'min_weight_fraction_leaf': 0.47805805082502384,
#             'n_estimators': 10,
#             'oob_score': True,
#             'warm_start': False,
#             'CHILD_max_depth_none': 'None',
#             'CHILD_max_features_specified_int': 0,
#             'CHILD_max_leaf_nodes_none': 'None',
#             'CHILD_min_samples_leaf_absolute': 1,
#             'CHILD_min_samples_split_percent': 0.28672273908041157,
#         }
#
#         smac_config_regression_ridge_SKlearn = {
#             'alpha': 1,
#             'fit_intercept': True,
#             'max_iter_UNION': 'None',
#             'normalize': True,
#             'solver': 'svd',
#             'tol': 0.001,
#         }
#
#         smac_config_regression_sgd_SKlearn = {
#             'alpha': 0.0001,
#             'average_UNION': False,
#             'early_stopping': False,
#             'epsilon': 0.1,
#             'eta0': 0.01,
#             'fit_intercept': False,
#             'l1_ratio': 0.5396841089159834,
#             'learning_rate': 'invscaling',
#             'loss': 'squared_loss',
#             'max_iter_UNION': 'int',
#             'n_iter_no_change': 5,
#             'penalty_UNION': 'str',
#             'power_t': 0.25,
#             'shuffle': False,
#             'tol_UNION': 'none',
#             'validation_fraction': 0.6490249482183607,
#             'warm_start': True,
#             'CHILD_max_iter_int': 1000,
#             'CHILD_penalty_str': 'l2',
#             'CHILD_tol_none': 'None',
#         }
#
#         smac_config_regression_xgboost_gbtree_DataFrameCommon = {
#             'base_score': 0.5,
#             'colsample_bylevel': 0.3391912080963938,
#             'colsample_bytree': 0.7044958882298847,
#             'gamma': 0.0,
#             'importance_type': 'cover',
#             'learning_rate': 0.08045413224869317,
#             'max_delta_step_UNION': 'unlimited',
#             'max_depth_UNION': 'unlimited',
#             'min_child_weight': 1,
#             'n_estimators': 9799,
#             'n_more_estimators': 6993,
#             'reg_alpha': 0,
#             'reg_lambda': 1,
#             'scale_pos_weight': 1,
#             'subsample': 0.02926125439522215,
#             'CHILD_max_delta_step_unlimited': 0,
#             'CHILD_max_depth_unlimited': 0,
#         }
#
#         smac_config_layer_average_pooling_1d_KerasWrap = {
#             'data_format': 'channels_last',
#             'padding': 'valid',
#             'pool_size': 18,
#             'previous_layer_UNION': 'previous_layer',
#             'strides': 11,
#             'CHILD_previous_layer_previous_layer_PRIMITIVE': 'd3m.primitives.layer.batch_normalization.KerasWrap',
#         }
#
#         smac_config_layer_average_pooling_2d_KerasWrap = {
#             'data_format': 'channels_last',
#             'padding': 'causal',
#             'pool_size': 13,
#             'previous_layer_UNION': 'None',
#             'strides': 13,
#         }
#
#         smac_config_layer_average_pooling_3d_KerasWrap = {
#             'data_format': 'channels_first',
#             'padding': 'causal',
#             'pool_size': 5,
#             'previous_layer_UNION': 'none',
#             'strides': 13,
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_layer_batch_normalization_KerasWrap = {
#             'beta_constraint_CHOICE': 'UnitNorm',
#             'beta_initializer_CHOICE': 'he_normal',
#             'beta_regularizer_CHOICE': 'l1_l2',
#             'center': True,
#             'gamma_initializer_CHOICE': 'lecun_normal',
#             'gamma_regularizer_CHOICE': 'l1',
#             'momentum': 0.6879188834932191,
#             'previous_layer_UNION': 'previous_layer',
#             'scale': True,
#             'CHILD_beta_constraint_UnitNorm_axis': '0',
#             'CHILD_beta_initializer_he_normal_seed': 'None',
#             'CHILD_beta_regularizer_l1_l2_l1': 0.01,
#             'CHILD_beta_regularizer_l1_l2_l2': 0.01,
#             'CHILD_gamma_initializer_lecun_normal_seed': 'None',
#             'CHILD_gamma_regularizer_l1_l': 0.01,
#             'CHILD_moving_mean_initializer_RandomUniform_maxval': 0.05,
#             'CHILD_moving_mean_initializer_RandomUniform_minval': -0.05,
#             'CHILD_moving_mean_initializer_RandomUniform_seed': 'None',
#             'CHILD_previous_layer_previous_layer_PRIMITIVE': 'd3m.primitives.layer.concat.KerasWrap',
#         }
#
#         smac_config_layer_convolution_1d_KerasWrap = {
#             'activation': 'tanh',
#             'activity_regularizer_CHOICE': 'none',
#             'bias_constraint_CHOICE': 'NonNeg',
#             'bias_initializer_CHOICE': 'VarianceScaling',
#             'bias_regularizer_CHOICE': 'l1_l2',
#             'data_format': 'channels_last',
#             'dilation_rate': 9,
#             'filters': 232160438467,
#             'kernel_constraint_CHOICE': 'none',
#             'kernel_initializer_CHOICE': 'he_uniform',
#             'kernel_regularizer_CHOICE': 'l1_l2',
#             'kernel_size': 6,
#             'padding': 'causal',
#             'previous_layer_UNION': 'none',
#             'strides': 4,
#             'use_bias': True,
#             'CHILD_bias_initializer_VarianceScaling_distribution': 'uniform',
#             'CHILD_bias_initializer_VarianceScaling_mode': 'fan_in',
#             'CHILD_bias_initializer_VarianceScaling_scale': '1.0',
#             'CHILD_bias_initializer_VarianceScaling_seed': 'None',
#             'CHILD_bias_regularizer_l1_l2_l1': 0.01,
#             'CHILD_bias_regularizer_l1_l2_l2': 0.01,
#             'CHILD_kernel_initializer_he_uniform_seed': 'None',
#             'CHILD_kernel_regularizer_l1_l2_l1': 0.01,
#             'CHILD_kernel_regularizer_l1_l2_l2': 0.01,
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_layer_convolution_2d_KerasWrap = {
#             'activation': 'elu',
#             'activity_regularizer_CHOICE': 'l2',
#             'bias_constraint_CHOICE': 'MinMaxNorm',
#             'bias_initializer_CHOICE': 'he_normal',
#             'bias_regularizer_CHOICE': 'l2',
#             'data_format': 'channels_first',
#             'dilation_rate': 5,
#             'filters': 477234864921,
#             'kernel_constraint_CHOICE': 'MinMaxNorm',
#             'kernel_initializer_CHOICE': 'TruncatedNormal',
#             'kernel_regularizer_CHOICE': 'l2',
#             'kernel_size': 10,
#             'padding': 'same',
#             'previous_layer_UNION': 'none',
#             'strides': 19,
#             'use_bias': True,
#             'CHILD_activity_regularizer_l2_l': 0.01,
#             'CHILD_bias_constraint_MinMaxNorm_axis': '0',
#             'CHILD_bias_constraint_MinMaxNorm_max_value': '1.0',
#             'CHILD_bias_constraint_MinMaxNorm_min_value': '0.0',
#             'CHILD_bias_constraint_MinMaxNorm_rate': '1.0',
#             'CHILD_bias_initializer_he_normal_seed': 'None',
#             'CHILD_bias_regularizer_l2_l': 0.01,
#             'CHILD_kernel_constraint_MinMaxNorm_axis': '0',
#             'CHILD_kernel_constraint_MinMaxNorm_max_value': '1.0',
#             'CHILD_kernel_constraint_MinMaxNorm_min_value': '0.0',
#             'CHILD_kernel_constraint_MinMaxNorm_rate': '1.0',
#             'CHILD_kernel_initializer_TruncatedNormal_mean': '0.0',
#             'CHILD_kernel_initializer_TruncatedNormal_seed': 'None',
#             'CHILD_kernel_initializer_TruncatedNormal_steddev': 0.05,
#             'CHILD_kernel_regularizer_l2_l': 0.01,
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_layer_convolution_3d_KerasWrap = {
#             'activation': 'tanh',
#             'activity_regularizer_CHOICE': 'l1_l2',
#             'bias_constraint_CHOICE': 'NonNeg',
#             'bias_initializer_CHOICE': 'Constant',
#             'bias_regularizer_CHOICE': 'l1',
#             'data_format': 'channels_first',
#             'dilation_rate': 4,
#             'filters': 457598654848,
#             'kernel_constraint_CHOICE': 'UnitNorm',
#             'kernel_initializer_CHOICE': 'Identity',
#             'kernel_regularizer_CHOICE': 'none',
#             'kernel_size': 11,
#             'padding': 'causal',
#             'previous_layer_UNION': 'None',
#             'strides': 9,
#             'use_bias': True,
#             'CHILD_activity_regularizer_l1_l2_l1': 0.01,
#             'CHILD_activity_regularizer_l1_l2_l2': 0.01,
#             'CHILD_bias_initializer_Constant_value': '0.0',
#             'CHILD_bias_regularizer_l1_l': 0.01,
#             'CHILD_kernel_constraint_UnitNorm_axis': '0',
#             'CHILD_kernel_initializer_Identity_gain': '1.0',
#         }
#
#         smac_config_layer_dense_KerasWrap = {
#             'activation': 'elu',
#             'activity_regularizer_CHOICE': 'l1',
#             'bias_constraint_CHOICE': 'MinMaxNorm',
#             'bias_initializer_CHOICE': 'Ones',
#             'bias_regularizer_CHOICE': 'l1',
#             'kernel_constraint_CHOICE': 'NonNeg',
#             'kernel_initializer_CHOICE': 'RandomNormal',
#             'kernel_regularizer_CHOICE': 'none',
#             'previous_layer_UNION': 'none',
#             'CHILD_activity_regularizer_l1_l': 0.01,
#             'CHILD_bias_constraint_MinMaxNorm_axis': '0',
#             'CHILD_bias_constraint_MinMaxNorm_max_value': '1.0',
#             'CHILD_bias_constraint_MinMaxNorm_min_value': '0.0',
#             'CHILD_bias_constraint_MinMaxNorm_rate': '1.0',
#             'CHILD_bias_regularizer_l1_l': 0.01,
#             'CHILD_kernel_initializer_RandomNormal_mean': '0.0',
#             'CHILD_kernel_initializer_RandomNormal_seed': 'None',
#             'CHILD_kernel_initializer_RandomNormal_stddev': 0.05,
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_layer_dropout_KerasWrap = {
#             'previous_layer_UNION': 'none',
#             'rate': 0.7701534545122561,
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_layer_flatten_KerasWrap = {
#             'previous_layer_UNION': 'none',
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_layer_global_average_pooling_1d_KerasWrap = {
#             'data_format': 'channels_last',
#             'previous_layer_UNION': 'previous_layer',
#             'CHILD_previous_layer_previous_layer_PRIMITIVE': 'd3m.primitives.layer.null.KerasWrap',
#         }
#
#         smac_config_layer_global_average_pooling_2d_KerasWrap = {
#             'data_format': 'channels_last',
#             'previous_layer_UNION': 'None',
#         }
#
#         smac_config_layer_global_average_pooling_3d_KerasWrap = {
#             'data_format': 'channels_last',
#             'previous_layer_UNION': 'none',
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_layer_max_pooling_1d_KerasWrap = {
#             'data_format': 'channels_last',
#             'padding': 'same',
#             'pool_size': 2,
#             'previous_layer_UNION': 'None',
#             'strides': 2,
#         }
#
#         smac_config_layer_max_pooling_2d_KerasWrap = {
#             'data_format': 'channels_last',
#             'padding': 'same',
#             'pool_size': 15,
#             'previous_layer_UNION': 'none',
#             'strides': 8,
#             'CHILD_previous_layer_none': 'None',
#         }
#
#         smac_config_layer_max_pooling_3d_KerasWrap = {
#             'data_format': 'channels_last',
#             'padding': 'valid',
#             'pool_size': 2,
#             'previous_layer_UNION': 'previous_layer',
#             'strides': 10,
#             'CHILD_previous_layer_previous_layer_PRIMITIVE': 'd3m.primitives.layer.convolution_2d.KerasWrap',
#         }
#
#         smac_config_learner_model_KerasWrap = {
#             'batch_size': 66,
#             'loss_PRIMITIVE': 'd3m.primitives.loss_function.categorical_accuracy.KerasWrap',
#             'metric_PRIMITIVE': 'd3m.primitives.loss_function.squared_hinge.KerasWrap',
#             'model_type': 'classification',
#             'optimizer_CHOICE': 'SGD',
#             'previous_layer_UNION': 'None',
#             'CHILD_optimizer_SGD_decay': '0.0',
#             'CHILD_optimizer_SGD_lr': 0.01,
#             'CHILD_optimizer_SGD_momentum': '0.0',
#             'CHILD_optimizer_SGD_nesterov': 'False',
#         }
#
#         smac_config_layer_subtract_KerasWrap = {
#             'previous_layer_left_UNION': 'previous_layer_left',
#             'previous_layer_right_UNION': 'previous_layer_right',
#             'CHILD_previous_layer_left_previous_layer_left_PRIMITIVE': 'd3m.primitives.layer.average_pooling_2d.KerasWrap',
#             'CHILD_previous_layer_right_previous_layer_right': 'PRIMITIVEBASEMETA_STR_d3m.primitives.layer.null.KerasWrap',
#         }
#
#         smac_config_regression_decision_tree_SKlearn = {
#             'criterion': 'mse',
#             'max_depth_UNION': 'None',
#             'max_features_UNION': 'none',
#             'max_leaf_nodes_UNION': 'none',
#             'min_impurity_decrease': 0.0,
#             'min_samples_leaf_UNION': 1,
#             'min_samples_split_UNION': 2,
#             'min_weight_fraction_leaf': 0.24687276732657137,
#             'presort': False,
#             'splitter': 'best',
#             'CHILD_max_features_none': 'None',
#             'CHILD_max_leaf_nodes_none': 'None',
#         }
#
#         smac_config_data_preprocessing_count_vectorizer_SKlearn = {
#             'analyzer': 'word',
#             'binary': True,
#             'CHILD_ngram_range_0': 1,
#             'CHILD_ngram_range_1': 1,
#             'CHILD_ngram_range_ASCENDING': 'True',
#             'lowercase': False,
#             'max_df_UNION': 1.0,
#             'max_features_UNION': 'absolute',
#             'min_df_UNION': 1,
#             'ngram_range_SORTEDLIST': 2,
#             'stop_words_UNION': 'none',
#             'strip_accents_UNION': 'accents',
#             'token_pattern': '(?u)\b\w\w+\b',
#             'CHILD_max_features_absolute': 1,
#             'CHILD_stop_words_none': 'None',
#             'CHILD_strip_accents_accents': 'ascii',
#         }
#
#         smac_config_data_preprocessing_tfidf_vectorizer_SKlearn = {
#             'analyzer': 'char',
#             'binary': True,
#             'CHILD_ngram_range_0': 1,
#             'CHILD_ngram_range_1': 1,
#             'CHILD_ngram_range_ASCENDING': 'True',
#             'lowercase': True,
#             'max_df_UNION': 'absolute',
#             'max_features_UNION': 'absolute',
#             'min_df_UNION': 'absolute',
#             'ngram_range_SORTEDLIST': 2,
#             'norm_UNION': 'none',
#             'smooth_idf': True,
#             'stop_words_UNION': 'string',
#             'strip_accents_UNION': 'None',
#             'sublinear_tf': True,
#             'token_pattern': '(?u)\b\w\w+\b',
#             'use_idf': False,
#             'CHILD_max_df_absolute': 1,
#             'CHILD_max_features_absolute': 1,
#             'CHILD_min_df_absolute': 1,
#             'CHILD_norm_none': 'None',
#             'CHILD_stop_words_string': 'english',
#         }
#
#         smac_config_data_transformation_one_hot_encoder_SKlearn = {
#             'categories': 'auto',
#             'handle_unknown': 'error',
#             'n_values_UNION': 'int',
#             'sparse': False,
#             'CHILD_n_values_int': 10,
#         }
#
#         fail_num = 0
#         with patch.object(PipelineTuner, "__init__", lambda x, y, z, w: None):
#             tuner = PipelineTuner(1, 2, 3)
#             for name, smac_config in {
#                 "d3m.primitives.classification.bernoulli_naive_bayes.SKlearn": smac_config_classification_bernoulli_naive_bayes_SKlearn,
#                 "d3m.primitives.classification.extra_trees.SKlearn": smac_config_classification_extra_trees_SKlearn,
#                 "d3m.primitives.data_preprocessing.random_trees_embedding.SKlearn": smac_config_data_preprocessing_random_trees_embedding_SKlearn,
#                 "d3m.primitives.data_transformation.fast_ica.SKlearn": smac_config_data_transformation_fast_ica_SKlearn,
#                 'd3m.primitives.classification.bagging.SKlearn': smac_config_classification_bagging_SKlearn,
#                 'd3m.primitives.classification.decision_tree.SKlearn': smac_config_classification_decision_tree_SKlearn,
#                 'd3m.primitives.classification.gradient_boosting.SKlearn': smac_config_classification_gradient_boosting_SKlearn,
#                 'd3m.primitives.classification.light_gbm.DataFrameCommon': smac_config_classification_light_gbm_DataFrameCommon,
#                 'd3m.primitives.classification.linear_discriminant_analysis.SKlearn': smac_config_classification_linear_discriminant_analysis_SKlearn,
#                 'd3m.primitives.classification.linear_svc.SKlearn': smac_config_classification_linear_svc_SKlearn,
#                 'd3m.primitives.classification.logistic_regression.SKlearn': smac_config_classification_logistic_regression_SKlearn,
#                 'd3m.primitives.classification.lupi_rf.LupiRFClassifier': smac_config_classification_lupi_rf_LupiRFClassifier,
#                 'd3m.primitives.classification.lupi_rfsel.LupiRFSelClassifier': smac_config_classification_lupi_rfsel_LupiRFSelClassifier,
#                 'd3m.primitives.classification.lupi_svm.LupiSvmClassifier': smac_config_classification_lupi_svm_LupiSvmClassifier,
#                 'd3m.primitives.classification.mlp.SKlearn': smac_config_classification_mlp_SKlearn,
#                 'd3m.primitives.classification.nearest_centroid.SKlearn': smac_config_classification_nearest_centroid_SKlearn,
#                 'd3m.primitives.classification.passive_aggressive.SKlearn': smac_config_classification_passive_aggressive_SKlearn,
#                 'd3m.primitives.classification.quadratic_discriminant_analysis.SKlearn': smac_config_classification_quadratic_discriminant_analysis_SKlearn,
#                 'd3m.primitives.classification.random_forest.DataFrameCommon': smac_config_classification_random_forest_DataFrameCommon,
#                 'd3m.primitives.classification.random_forest.SKlearn': smac_config_classification_random_forest_SKlearn,
#                 'd3m.primitives.classification.sgd.SKlearn': smac_config_classification_sgd_SKlearn,
#                 'd3m.primitives.classification.svc.SKlearn': smac_config_classification_svc_SKlearn,
#                 'd3m.primitives.classification.xgboost_dart.DataFrameCommon': smac_config_classification_xgboost_dart_DataFrameCommon,
#                 'd3m.primitives.classification.xgboost_gbtree.DataFrameCommon': smac_config_classification_xgboost_gbtree_DataFrameCommon,
#                 'd3m.primitives.clustering.ekss.Umich': smac_config_clustering_ekss_Umich,
#                 'd3m.primitives.clustering.ssc_admm.Umich': smac_config_clustering_ssc_admm_Umich,
#                 'd3m.primitives.clustering.ssc_cvx.Umich': smac_config_clustering_ssc_cvx_Umich,
#                 'd3m.primitives.data_cleaning.imputer.SKlearn': smac_config_data_cleaning_imputer_SKlearn,
#                 'd3m.primitives.data_cleaning.missing_indicator.SKlearn': smac_config_data_cleaning_missing_indicator_SKlearn,
#                 'd3m.primitives.data_compression.go_dec.Umich': smac_config_data_compression_go_dec_Umich,
#                 'd3m.primitives.data_compression.pcp_ialm.Umich': smac_config_data_compression_pcp_ialm_Umich,
#                 'd3m.primitives.data_preprocessing.dataset_sample.Common': smac_config_data_preprocessing_dataset_sample_Common,
#                 'd3m.primitives.data_preprocessing.feature_agglomeration.SKlearn': smac_config_data_preprocessing_feature_agglomeration_SKlearn,
#                 'd3m.primitives.data_preprocessing.nystroem.SKlearn': smac_config_data_preprocessing_nystroem_SKlearn,
#                 'd3m.primitives.feature_construction.corex_continuous.DSBOX': smac_config_feature_construction_corex_continuous_DSBOX,
#                 'd3m.primitives.feature_construction.deep_feature_synthesis.Featuretools': smac_config_feature_construction_deep_feature_synthesis_Featuretools,
#                 'd3m.primitives.feature_extraction.kernel_pca.SKlearn': smac_config_feature_extraction_kernel_pca_SKlearn,
#                 'd3m.primitives.feature_extraction.pca.SKlearn': smac_config_feature_extraction_pca_SKlearn,
#                 'd3m.primitives.feature_selection.generic_univariate_select.SKlearn': smac_config_feature_selection_generic_univariate_select_SKlearn,
#                 'd3m.primitives.regression.extra_trees.SKlearn': smac_config_regression_extra_trees_SKlearn,
#                 'd3m.primitives.regression.gaussian_process.SKlearn': smac_config_regression_gaussian_process_SKlearn,
#                 'd3m.primitives.regression.gradient_boosting.SKlearn': smac_config_regression_gradient_boosting_SKlearn,
#                 'd3m.primitives.regression.lars.SKlearn': smac_config_regression_lars_SKlearn,
#                 'd3m.primitives.regression.lasso.SKlearn': smac_config_regression_lasso_SKlearn,
#                 'd3m.primitives.regression.lasso_cv.SKlearn': smac_config_regression_lasso_cv_SKlearn,
#                 'd3m.primitives.regression.mlp.SKlearn': smac_config_regression_mlp_SKlearn,
#                 'd3m.primitives.regression.passive_aggressive.SKlearn': smac_config_regression_passive_aggressive_SKlearn,
#                 'd3m.primitives.regression.random_forest.SKlearn': smac_config_regression_random_forest_SKlearn,
#                 'd3m.primitives.regression.ridge.SKlearn': smac_config_regression_ridge_SKlearn,
#                 'd3m.primitives.regression.sgd.SKlearn': smac_config_regression_sgd_SKlearn,
#                 'd3m.primitives.regression.xgboost_gbtree.DataFrameCommon': smac_config_regression_xgboost_gbtree_DataFrameCommon,
#                 'd3m.primitives.layer.average_pooling_1d.KerasWrap': smac_config_layer_average_pooling_1d_KerasWrap,
#                 'd3m.primitives.layer.average_pooling_2d.KerasWrap': smac_config_layer_average_pooling_2d_KerasWrap,
#                 'd3m.primitives.layer.average_pooling_3d.KerasWrap': smac_config_layer_average_pooling_3d_KerasWrap,
#                 'd3m.primitives.layer.batch_normalization.KerasWrap': smac_config_layer_batch_normalization_KerasWrap,
#                 'd3m.primitives.layer.convolution_1d.KerasWrap': smac_config_layer_convolution_1d_KerasWrap,
#                 'd3m.primitives.layer.convolution_2d.KerasWrap': smac_config_layer_convolution_2d_KerasWrap,
#                 'd3m.primitives.layer.convolution_3d.KerasWrap': smac_config_layer_convolution_3d_KerasWrap,
#                 'd3m.primitives.layer.dense.KerasWrap': smac_config_layer_dense_KerasWrap,
#                 'd3m.primitives.layer.dropout.KerasWrap': smac_config_layer_dropout_KerasWrap,
#                 'd3m.primitives.layer.flatten.KerasWrap': smac_config_layer_flatten_KerasWrap,
#                 'd3m.primitives.layer.global_average_pooling_1d.KerasWrap': smac_config_layer_global_average_pooling_1d_KerasWrap,
#                 'd3m.primitives.layer.global_average_pooling_2d.KerasWrap': smac_config_layer_global_average_pooling_2d_KerasWrap,
#                 'd3m.primitives.layer.global_average_pooling_3d.KerasWrap': smac_config_layer_global_average_pooling_3d_KerasWrap,
#                 'd3m.primitives.layer.max_pooling_1d.KerasWrap': smac_config_layer_max_pooling_1d_KerasWrap,
#                 'd3m.primitives.layer.max_pooling_2d.KerasWrap': smac_config_layer_max_pooling_2d_KerasWrap,
#                 'd3m.primitives.layer.max_pooling_3d.KerasWrap': smac_config_layer_max_pooling_3d_KerasWrap,
#                 'd3m.primitives.learner.model.KerasWrap': smac_config_learner_model_KerasWrap,
#                 'd3m.primitives.layer.subtract.KerasWrap': smac_config_layer_subtract_KerasWrap,
#                 'd3m.primitives.regression.decision_tree.SKlearn': smac_config_regression_decision_tree_SKlearn,
#                 'd3m.primitives.data_preprocessing.count_vectorizer.SKlearn': smac_config_data_preprocessing_count_vectorizer_SKlearn,
#                 'd3m.primitives.data_preprocessing.tfidf_vectorizer.SKlearn': smac_config_data_preprocessing_tfidf_vectorizer_SKlearn,
#                 'd3m.primitives.data_transformation.one_hot_encoder.SKlearn': smac_config_data_transformation_one_hot_encoder_SKlearn,
#             }.items():
#                 primitive = index.get_primitive(name)
#                 step = PrimitiveStep(primitive=primitive, resolver=custom_resolver.BlackListResolver())
#                 try:
#                     # search_space = tuner._get_primitive_search_space(primitive)
#                     tuner._set_primitive_hyperparameters(step, smac_config)
#                 except Exception as e:
#                     print(e)
#                     print('{} doest not set its primitive search space'.format(name))
#                     fail_num += 1
#         self.assertEqual(fail_num, 0)
#
#     def test__get_set_primitve_search_space_all(self):
#         all_primitives = index.get_loaded_primitives()
#         print('Test total primitives: {}'.format(len(all_primitives)))
#         fail_num = 0
#         success_num = 0
#         none_num = 0
#
#         with patch.object(PipelineTuner, "__init__", lambda x, y, z, w: None):
#             tuner = PipelineTuner(1, 2, 3)
#             for primitive in all_primitives:
#                 name = str(primitive)
#                 step = PrimitiveStep(primitive=primitive, resolver=custom_resolver.BlackListResolver())
#                 try:
#                     try:
#                         search_space = tuner._get_primitive_search_space(primitive)
#                     except Exception as e:
#                         print(e)
#                         print('{} cannot get its search space'.format(name))
#                         fail_num += 1
#                         continue
#
#                     if not search_space is None:
#                         smac_config = search_space.sample_configuration()
#                         smac_config_dict = dict((k, smac_config[k]) for k in smac_config.keys())
#                         tuner._set_primitive_hyperparameters(step, smac_config_dict)
#                         success_num += 1
#                     else:
#                         none_num += 1
#                 except Exception as e:
#                     print(e)
#                     print('{} cannot set its search space'.format(name))
#                     fail_num += 1
#
#         print('{} success, {} are not tunerable, {} failure'.format(success_num, none_num, fail_num))
#
#         self.assertEqual(fail_num, 0)
#
#     def test_update_pipelines(self):
#         # pipeline = self.get_simple_pipeline('d3m.primitives.classification.search_hybrid.Find_projections')
#         # pipeline_path = parsers.save_pipeline_to_json_file(pipeline, save_path)
#
#         # Get pipeline from QuickPipelineGenerator
#         problem_doc_uri = TestPipelineTuner.problem_path
#         inputs_metadata = TestPipelineTuner.inputs_metadata
#         time_left = 1000
#
#         quick_generator = QuickPipelineGenerator(problem_doc_uri, inputs_metadata, save_path)
#         pipelines = quick_generator.get_pipelines(num_pipelines=1, time_left=time_left)
#         pipeline_path = list(pipelines.values())[0]['path']
#
#         # Tune the pipeline hyper-parameters
#         tuner = PipelineTuner(problem_doc_uri, inputs_metadata, save_path, pipeline_path=pipeline_path)
#         num_pipelines = 3
#         for i in range(3):
#             pipelines = tuner.get_pipelines(num_pipelines=num_pipelines, time_left=time_left)
#             for j, pipeline_id in enumerate(list(pipelines.keys())):
#                 pipelines[pipeline_id].update({
#                     'status': 'COMPLETED',
#                     'internal_score': 0.7459306352271471,
#                     'rank': 0.25407823577285293,
#                     'scores': {
#                         'all': '',
#                         'condensed': {
#                             'ACCURACY': {'mean': 0.9231664726426076 + i * 0.001 + j * 0.01, 'std': 0},
#                             'F1_MACRO': {'mean': 0.5686947978116864 + i * 0.001 + j * 0.01, 'std': 0}
#                         }
#                     },
#                 })
#             tuner.update_pipelines(pipelines, time_left)
#
#     def test_evaluate_pipeline(self):
#         # Run _search_score_worker to get pipeline performance
#         # However, the evaluation is time-consuming
#         problem_path= TestPipelineTuner.problem_path
#         problem_doc_uri = TestPipelineTuner.problem_path
#         inputs_metadata = TestPipelineTuner.inputs_metadata
#         time_left = 1000
#
#         # Create a pipeline
#         quick_generator = QuickPipelineGenerator(problem_doc_uri, inputs_metadata, save_path)
#         pipelines = quick_generator.get_pipelines(num_pipelines=1, time_left=time_left)
#         pipeline_path = list(pipelines.values())[0]['path']
#
#         # tuner = PipelineTuner(problem_doc_uri, inputs_metadata, save_path, pipeline_path=pipeline_path)
#         tuner = RandomPipelineTuner(problem_doc_uri, inputs_metadata, save_path)
#         tuner.tuning_started = True
#         tuner.init(pipeline_path)
#         pipelines = tuner.get_pipelines(time_left=time_left)
#         pipeline_id = list(pipelines.keys())[0]
#
#         search_score_worker_args = {
#             'searches': {'e0e34088-8da2-464b-823e-a32b45466247': list()},
#             'problems': {
#                 'e0e34088-8da2-464b-823e-a32b45466247': problem_path},
#             'search_id': 'e0e34088-8da2-464b-823e-a32b45466247',
#             'job_type': 'SEARCH',
#             'pipelines': {
#                 pipeline_id: {
#                     'path': pipelines[pipeline_id]['path'],
#                 }
#             },
#             'pipeline_id': pipeline_id,
#             'solutions': {},
#             'inputs': inputs_list,
#             'performance_metrics': [
#                 {'metric': problem_module.PerformanceMetric.ACCURACY},
#                 {'metric': problem_module.PerformanceMetric.F1_MACRO}
#             ],
#             'configuration': {
#                 "method": "HOLDOUT",
#                 "train_score_ratio": "0.2",
#                 "shuffle": "true",
#                 "stratified": "true",
#                 "randomSeed": "42",
#             },
#             'plasma_client': plasma_client,
#             'data_preparation_id': None,
#         }
#         _search_score_worker(**search_score_worker_args)
#         PRINT(search_score_worker_args)
#         tuner.update_pipelines(search_score_worker_args['pipelines'], time_left=time_left)
#
#     def get_simple_pipeline(self, primitive_name):
#         primitives = {
#             'ConstructPredictions': 'd3m.primitives.data_transformation.construct_predictions.DataFrameCommon',
#             'DatasetToDataFrame': 'd3m.primitives.data_transformation.dataset_to_dataframe.Common',
#             'ColumnParser': 'd3m.primitives.data_transformation.column_parser.DataFrameCommon',
#             'ExtractColumnsBySemanticTypes': 'd3m.primitives.data_transformation.extract_columns_by_semantic_types.DataFrameCommon',
#             'Denormalize': 'd3m.primitives.data_transformation.denormalize.Common',
#             'Imputer': 'd3m.primitives.data_cleaning.imputer.SKlearn',
#             'SDFS': 'd3m.primitives.feature_construction.deep_feature_synthesis.SingleTableFeaturization',
#             'MDFS': 'd3m.primitives.feature_construction.deep_feature_synthesis.MultiTableFeaturization',
#             'DMap': 'd3m.primitives.operator.dataset_map.DataFrameCommon',
#             'XGBoostClassifier': 'd3m.primitives.classification.xgboost_gbtree.DataFrameCommon',
#             'XGBoostRegressor': 'd3m.primitives.regression.xgboost_gbtree.DataFrameCommon',
#             'LGBM': 'd3m.primitives.classification.light_gbm.DataFrameCommon',
#             'scaler': 'd3m.primitives.data_preprocessing.robust_scaler.SKlearn',
#             'fselect': 'd3m.primitives.feature_selection.select_fwe.SKlearn'
#         }
#         # estimator = 'XGBoostClassifier'
#
#         main_resource = None
#
#         for k in primitives.keys():
#             primitives[k] = index.get_primitive(primitives[k])
#         primitives[primitive_name] = index.get_primitive(primitive_name)
#
#         resolver = custom_resolver.BlackListResolver()
#         current_step = 0
#
#         # Creating pipeline
#         pipeline_description = Pipeline(context=Context.TESTING)
#         pipeline_description.add_input(name='inputs')
#
#         # step 0
#         dataset_parser = PrimitiveStep(primitive=primitives['DMap'], resolver=resolver)
#         dataset_parser.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
#         dataset_parser.add_hyperparameter(name='resources', argument_type=ArgumentType.VALUE, data='all')
#         dataset_parser.add_hyperparameter(name='primitive', argument_type=ArgumentType.VALUE,
#                                           data=primitives['ColumnParser'])
#         dataset_parser.add_hyperparameter(name='fit_primitive', argument_type=ArgumentType.VALUE, data='no')
#         dataset_parser.add_output('produce')
#
#         pipeline_description.add_step(dataset_parser)
#
#         # step 1
#         # denormalize_step = PrimitiveStep(primitive_description=primitives['Denormalize'].metadata.query(),
#         #                                  resolver=resolver)
#         # denormalize_step.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
#         #                               data_reference='steps.0.produce')
#         # denormalize_step.add_hyperparameter(name='starting_resource', argument_type=ArgumentType.VALUE,
#         #                                     data=main_resource)
#         # denormalize_step.add_output('produce')
#         # pipeline_description.add_step(denormalize_step)
#         # current_step += 1
#
#         # step 2
#         step_dtd = PrimitiveStep(primitive_description=primitives['DatasetToDataFrame'].metadata.query(),
#                                  resolver=resolver)
#         step_dtd.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
#                               data_reference='steps.{i}.produce'.format(i=current_step))
#         step_dtd.add_output('produce')
#         pipeline_description.add_step(step_dtd)
#         current_step += 1
#         dataframe_step = 'steps.{i}.produce'.format(i=current_step)
#
#         # step 3: get attributes
#         extract_attributes = PrimitiveStep(
#             primitive_description=primitives['ExtractColumnsBySemanticTypes'].metadata.query(),
#             resolver=resolver)
#         extract_attributes.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
#                                         data_reference=dataframe_step)
#         extract_attributes.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
#                                               data=['https://metadata.datadrivendiscovery.org/types/Attribute'])
#         extract_attributes.add_output('produce')
#         pipeline_description.add_step(extract_attributes)
#         current_step += 1
#         attributes_step = 'steps.{i}.produce'.format(i=current_step)
#
#         # step 4: get targets
#         extract_targets = PrimitiveStep(
#             primitive_description=primitives['ExtractColumnsBySemanticTypes'].metadata.query(),
#             resolver=resolver)
#         extract_targets.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER,
#                                      data_reference=dataframe_step)
#         extract_targets.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
#                                            data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
#         extract_targets.add_output('produce')
#         pipeline_description.add_step(extract_targets)
#         current_step += 1
#         targets_step = 'steps.{i}.produce'.format(i=current_step)
#
#         # step 5
#         xgb = PrimitiveStep(primitive_description=primitives[primitive_name].metadata.query(), resolver=resolver)
#         xgb.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes_step)
#         xgb.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets_step)
#         # xgb.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
#         xgb.add_output('produce')
#         pipeline_description.add_step(xgb)
#         current_step += 1
#         predictions_step = 'steps.{i}.produce'.format(i=current_step)
#         if primitive_name == 'd3m.primitives.classification.search_hybrid.Find_projections':
#             primitive = xgb.primitive
#             primitive_config = primitive.metadata.query()['primitive_code']['class_type_arguments'][
#                 'Hyperparams'].configuration
#             primitive_config['blackbox'].matching_primitives = \
#                 TestPipelineTuner.search_hybrid_Find_projections_matching_primitives
#
#
#         # step 6: Add step prediction into pipeline
#         construct = PrimitiveStep(primitive_description=primitives['ConstructPredictions'].metadata.query(),
#                                   resolver=resolver)
#         construct.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=predictions_step)
#         construct.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference=dataframe_step)
#         construct.add_output('produce')
#         pipeline_description.add_step(construct)
#         current_step += 1
#
#         # Get the last step for the output
#         output = 'steps.{i}.produce'.format(i=current_step)
#
#         # Adding output step to the pipeline
#         pipeline_description.add_output(name='Predictions from the input dataset', data_reference=output)
#
#         return pipeline_description
#
#     def get_KerasWrap_matching_primitives(self):
#         all_primitives = index.get_loaded_primitives()
#         for name in [
#             "d3m.primitives.classification.search_hybrid.Find_projections",
#             "d3m.primitives.learner.model.KerasWrap",
#             "d3m.primitives.regression.search_hybrid_numeric.Find_projections",
#             "d3m.primitives.semisupervised_classification.iterative_labeling.AutonBox",
#         ]:
#             primitive = index.get_primitive(name)
#             primitive_config = primitive.metadata.query()['primitive_code']['class_type_arguments'][
#                 'Hyperparams'].configuration
#             for hyper_param_name in [
#                 'blackbox',
#                 'loss',
#                 'metric'
#             ]:
#                 if hyper_param_name in primitive_config:
#                     hyper_param = primitive_config[hyper_param_name]
#                     if hyper_param.matching_primitives is None:
#                         hyper_param.populate_primitives(all_primitives)
#                         PRINT(name, hyper_param_name)
#                         PRINT('\n'.join([str(p) for p in hyper_param.matching_primitives]))
#
#         for name in [
#             "d3m.primitives.layer.average_pooling_1d.KerasWrap",
#             "d3m.primitives.layer.average_pooling_2d.KerasWrap",
#             "d3m.primitives.layer.average_pooling_3d.KerasWrap",
#             "d3m.primitives.layer.batch_normalization.KerasWrap",
#             "d3m.primitives.layer.convolution_1d.KerasWrap",
#             "d3m.primitives.layer.convolution_2d.KerasWrap",
#             "d3m.primitives.layer.convolution_3d.KerasWrap",
#             "d3m.primitives.layer.dense.KerasWrap",
#             "d3m.primitives.layer.dropout.KerasWrap",
#             "d3m.primitives.layer.flatten.KerasWrap",
#             "d3m.primitives.layer.global_average_pooling_1d.KerasWrap",
#             "d3m.primitives.layer.global_average_pooling_2d.KerasWrap",
#             "d3m.primitives.layer.global_average_pooling_3d.KerasWrap",
#             "d3m.primitives.layer.max_pooling_1d.KerasWrap",
#             "d3m.primitives.layer.max_pooling_2d.KerasWrap",
#             "d3m.primitives.layer.max_pooling_3d.KerasWrap",
#             "d3m.primitives.learner.model.KerasWrap",
#             "d3m.primitives.layer.subtract.KerasWrap",
#         ]:
#             primitive = index.get_primitive(name)
#             primitive_config = primitive.metadata.query()['primitive_code']['class_type_arguments'][
#                 'Hyperparams'].configuration
#             for hyper_param_name in [
#                 'previous_layer',
#                 'previous_layer_left',
#                 'previous_layer_right',
#             ]:
#                 if hyper_param_name in primitive_config:
#                     hyper_param = primitive_config[hyper_param_name].configuration[hyper_param_name]
#                     if hasattr(hyper_param, 'matching_primitives'):
#                         if hyper_param.matching_primitives is None:
#                             hyper_param.populate_primitives(all_primitives)
#                             PRINT(name, hyper_param_name)
#                             PRINT('\n'.join([str(p) for p in hyper_param.matching_primitives]))
#                     else:
#                         PRINT(hyper_param_name)
#                         PRINT(hyper_param.get_default())
#
# if __name__ == '__main__':
#     unittest.main()
