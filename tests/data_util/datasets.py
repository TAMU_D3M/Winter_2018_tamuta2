from pathlib import Path
from d3m import container
import os


def _load_dataset(dataset_name):
    dataset_doc_path = Path(os.path.dirname(__file__), '..', 'data/datasets/{}'.format(dataset_name),
                            'datasetDoc.json').as_uri()

    dataset = container.Dataset.load(dataset_doc_path)
    target_columns = dataset.metadata.get_columns_with_semantic_type(
        'https://metadata.datadrivendiscovery.org/types/SuggestedTarget', at=['learningData'])
    to_remove = ['https://metadata.datadrivendiscovery.org/types/Attribute']
    for col in target_columns:
        semantic_types = [st for st in
                          dataset.metadata.query_column(at=['learningData'], column_index=col)['semantic_types']
                          if st not in to_remove]
        semantic_types.append('https://metadata.datadrivendiscovery.org/types/TrueTarget')

        dataset.metadata = dataset.metadata.update_column(at=['learningData'],
                                                          column_index=col, metadata={
                'semantic_types': semantic_types})
    return dataset


image_dataset_1 = None
iris_dataset_1 = None
timeseries_dataset_1 = None
timeseries_dataset_2 = None
timeseries_dataset_3 = None
audio_dataset_1 = None
text_dataset_1 = None


def get(dataset_name):
    try:
        if not globals()[dataset_name]:
            return _load_dataset(dataset_name)
        else:
            return globals()[dataset_name]
    except KeyError as e:
        raise KeyError('Dataset {}, not found'.format(dataset_name)) from e
