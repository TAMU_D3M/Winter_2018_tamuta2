import logging
import os
import unittest
import warnings
from unittest.mock import patch

from d3m.metadata import base as metadata_base, problem
from d3m.metadata import pipeline as pipeline_module
from d3m.runtime import Runtime

from modules.pipeline_generator.models.random_pipeline_tuner import RandomPipelineTuner
from modules.utils.file_utils import save_problem, clean_directory
from tests.data_util import datasets
from modules.pipeline_generator.models.base_pipeline_tuner import logger as tuner_logger

# warnings.filterwarnings('ignore')
verbose = True

class TestRandomPipelineTuner(unittest.TestCase):
    iris_problem_path = os.path.join(os.path.dirname(__file__), 'data/problems/iris_problem_1/problemDoc.json')
    temp_dir = os.path.join(os.path.dirname(__file__), 'resources/temp/test_random_pipeline_tuner')

    @classmethod
    def setUpClass(cls):
        if not os.path.exists(cls.temp_dir):
            os.makedirs(cls.temp_dir)

        verbose_format = '%(asctime)s %(levelname)-8s %(processName)-15s [%(filename)s:%(lineno)d] %(message)s'
        concise_format = '%(asctime)s %(levelname)-8s %(message)s'

        log_format = verbose_format if verbose else concise_format
        logging.basicConfig(format=log_format,
                            handlers=[logging.StreamHandler(),
                                      logging.FileHandler('{}/d3m.log'.format(cls.temp_dir), 'w', 'utf-8')
                                      ],
                            datefmt='%m/%d %H:%M:%S')
        root_logger = logging.getLogger()
        root_logger.setLevel(logging.INFO)

        cls.iris_dataset = datasets.get('iris_dataset_1')
        iris_problem = problem.Problem.load('file://{}'.format(cls.iris_problem_path))
        cls.saved_iris_problem = save_problem(iris_problem, 'test_search_id', cls.temp_dir)

    @classmethod
    def tearDownClass(cls):
        if os.path.exists(cls.temp_dir):
            clean_directory(cls.temp_dir)

    def setUp(self):
        self.tuner = RandomPipelineTuner(
            problem_doc_uri=self.saved_iris_problem,
            inputs_metadata=[self.iris_dataset.metadata], save_path=self.temp_dir,
            main_resource='learningData')
        self.tuner.tuning_started = True

    def tearDown(self):
        pass

    def create_patch(self, name):
        patcher = patch(name)
        thing = patcher.start()
        self.addCleanup(patcher.stop)
        return thing

    def _fit_pipeline(self, pipeline_path):
        pipeline = pipeline_module.get_pipeline(pipeline_path)
        runtime = Runtime(pipeline=pipeline, context=metadata_base.Context.TESTING)
        fit_result = runtime.fit([self.iris_dataset])
        return fit_result

    def _pop_pipeline_path(self, pipelines):
        return list(pipelines.values())[0]['path']

    def _pop_pipeline_id(self, pipelines):
        return list(pipelines.keys())[0]

    def _tune_pipelines(self, pipeline_path, num_points=10):
        self.tuner.init(pipeline_path)
        pipelines = self.tuner.get_pipelines(num_pipelines=num_points)
        self.assertGreater(len(pipelines), 0)
        for pipeline_info in list(pipelines.values()):
            pipeline_path = pipeline_info['path']
            self.assertTrue(os.path.isfile(pipeline_path))

            fit_result = self._fit_pipeline(pipeline_path)
            self.assertIsNone(fit_result.error)

    # def test_tune_pipelines_from_qpg(self):
    #     from modules.pipeline_generator.models.quick_generator import QuickPipelineGenerator
    #     qpg = QuickPipelineGenerator(
    #         problem_doc_uri=self.saved_iris_problem,
    #         inputs_metadata=[self.iris_dataset.metadata],
    #         save_path=self.temp_dir,
    #         main_resource='learningData'
    #     )
    #     qpg.resolver._primitives_loaded = True
    #     pipeline_path = self._pop_pipeline_path(
    #         qpg.get_pipelines(num_pipelines=1)
    #     )
    #     self._tune_pipelines(pipeline_path)

    def test_update_pipelines(self):
        pipeline_path = os.path.join(os.path.dirname(__file__), 'data/pipelines/random-forest-classifier.yml')
        self.tuner.init(pipeline_path)
        for i in range(3):
            pipelines = self.tuner.get_pipelines(num_pipelines=1)
            pipeline_id = self._pop_pipeline_id(pipelines)
            # pipeline_path = self._pop_pipeline_path(pipelines)
            # fit_result = self._fit_pipeline(pipeline_path)
            # self.assertIsNone(fit_result.error)
            if i == 1:
                status = 'ERRORED'
                pipelines[pipeline_id].update({
                    'status': status,
                })
            else:
                status = 'COMPLETED'
                pipelines[pipeline_id].update({
                    'status': status,
                    'internal_score': 0.7459306352271471,
                    'rank': 0.25407823577285293,
                    'scores': {
                        'all': '',
                        'condensed': {
                            'ACCURACY': {'mean': 0.9231664726426076, 'std': 0},
                            'F1_MACRO': {'mean': 0.5686947978116864, 'std': 0}
                        }
                    },
                })
            self.tuner.update_pipelines(pipelines, time_left=1000)

    def test_tune_pipelines_from_rpg(self):
        from sklearn_wrap.SKLogisticRegression import SKLogisticRegression
        from modules.pipeline_generator.models.random_generator import RandomPipelineGenerator

        mock_get_primitives_candidates = self.create_patch(
            'modules.pipeline_generator.models.random_generator.pipeline_utils.get_primitive_candidates')
        mock_get_primitives_candidates.return_value = [
            (SKLogisticRegression, 'CLASSIFICATION')]

        rpg = RandomPipelineGenerator(
            problem_doc_uri=self.saved_iris_problem,
            inputs_metadata=[self.iris_dataset.metadata],
            save_path=self.temp_dir,
            main_resource='learningData'
        )
        pipeline_path = self._pop_pipeline_path(
            rpg.get_pipelines(num_pipelines=1)
        )
        self._tune_pipelines(pipeline_path)

    # def test_tune_pipelines_special_case(self):
    #     for file_name in [
            # '92c2a927-d553-4bb7-89f8-2f07c877d2c2.json',
            # '54a0a2be-9cef-4ab0-8ec2-29e9b9515739.json',
            # '47afcdc3-7093-4714-bb76-b20f1dc6cc18.json',
            # '2988d7ac-f12d-4724-849e-569b9689ad07.json',
            # '6ec59e11-c433-444c-8235-fc71439427ce.json',
            # 'SKSGDClassifier_b2829b18-3692-4d28-9a3e-31efe01b4442.json',
            # 'bee7f06c-7024-47c1-b616-933f55c706a3.json',
        # ]:
        #     pipeline_path = os.path.join(os.path.dirname(__file__), '../../data/pipelines/{}'.format(file_name))
        #     # self._fit_pipeline(pipeline_path)
        #     self._tune_pipelines(pipeline_path, 10)

if __name__ == '__main__':
    unittest.main()
