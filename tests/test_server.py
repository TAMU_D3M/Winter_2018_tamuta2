import os
import pandas
import pathlib
import pickle
import shutil
import unittest
from unittest.mock import patch

import grpc_testing
from ta3ta2_api import core_pb2, utils as api_utils, value_pb2
from d3m.metadata import problem as problem_module
from d3m import runtime as runtime_module
from d3m import container as container_module

from modules.server.refactor_server import Core
from modules.utils import constants, file_utils, schemas_utils

VERSION ="2020.6.2"
ALLOWED_VALUE_TYPES = ['DATASET_URI', 'CSV_URI', 'RAW']


class TestServer(unittest.TestCase):
    def setUp(self):
        # self.create_patch('modules.server.refactor_server.ExecutionManger')
        mock_init = self.create_patch('modules.manager.execution_manager.ExecutionManger.__init__')
        self.create_patch('modules.manager.execution_manager.ExecutionManger.start_workers')
        mock_init.return_value = None
        self.core = Core(process_type='spawn')
        self.service_descriptor = core_pb2.DESCRIPTOR.services_by_name['Core']
        servicers = {
            self.service_descriptor: self.core
        }
        self.test_server = grpc_testing.server_from_dictionary(
            servicers, grpc_testing.strict_real_time())
        self.test_dir = constants.Path.TEMP_STORAGE_ROOT
        self.core.manager.solutions = {}
        self.core.manager.pipelines = {}
        self.core.manager.fitted_solutions = {}
        self.dataset = 'database_dataset_4'
        self.test_data = os.path.join('tests', 'data')

    def tearDown(self):
        for dir_name in (
            self.test_dir + 'solutions',
            self.test_dir + 'fitted_solutions',
            constants.Path.TEMP_STORAGE_ROOT,
        ):
            if os.path.exists(dir_name):
                shutil.rmtree(dir_name)

    def create_patch(self, name):
        patcher = patch(name)
        thing = patcher.start()
        self.addCleanup(patcher.stop)
        return thing

    def test_Hello(self):
        request = core_pb2.HelloRequest()
        response = self._Hello(request)
        self.assertEqual(response.user_agent, 'TAMU-UW.6.3_pre')
        self.assertEqual(response.version, VERSION)
        self.assertEqual(list(response.allowed_value_types), ['RAW', 'DATASET_URI', 'CSV_URI'])

    def test_SaveSolution(self):
        solution_id = 'solution_id'
        pipeline_id = 'pipeline_id'
        pipeline_uri = 'pipeline_uri'
        problem_uri = 'problem_uri'
        solution_uri = '{}solutions/{}.json'.format(self.test_dir, solution_id)
        self.core.manager.solutions = {
            solution_id: {
                'pipeline': pipeline_id,
                'problem': problem_uri,
            }
        }
        self.core.manager.pipelines = {
            pipeline_id: {
                'path': pipeline_uri
            }
        }
        # Existing
        request = core_pb2.SaveSolutionRequest(solution_id=solution_id)
        response = self._SaveSolution(request)
        self.assertEqual(response.solution_uri, solution_uri)
        self.assertTrue(os.path.exists(solution_uri))
        loaded_solution = file_utils.load_json(solution_uri)
        self.assertEqual(loaded_solution['solution_id'], solution_id)
        self.assertEqual(loaded_solution['pipeline_id'], pipeline_id)
        self.assertEqual(loaded_solution['problem_path'], problem_uri)
        self.assertEqual(loaded_solution['pipeline_path'], pipeline_uri)
        # Non existing
        request = core_pb2.SaveSolutionRequest(solution_id='not existing')
        response = self._SaveSolution(request)
        self.assertEqual(response.solution_uri, '')

    def test_LoadSolution(self):
        solution_id = 'solution_id_load'
        pipeline_id = 'pipeline_id'
        pipeline_uri = 'pipeline_uri'
        problem_uri = 'problem_uri'
        solution = {
            'solution_id': solution_id,
            'pipeline_id': pipeline_id,
            'problem_path': problem_uri,
            'pipeline_path': pipeline_uri
        }
        solution_uri = file_utils.save_solution(solution)

        # Existing
        request = core_pb2.LoadSolutionRequest(solution_uri=solution_uri)
        response = self._LoadSolution(request)
        self.assertEqual(response.solution_id, solution_id)
        self.assertEqual(self.core.manager.solutions[solution_id]['pipeline'], pipeline_id)
        self.assertEqual(self.core.manager.solutions[solution_id]['problem'], problem_uri)
        self.assertEqual(self.core.manager.pipelines[pipeline_id]['path'], pipeline_uri)
        # Non existing
        request = core_pb2.LoadSolutionRequest(solution_uri='not existing')
        response = self._LoadSolution(request)
        self.assertEqual(response.solution_id, '')

    def test_SaveFittedSolution(self):
        fitted_solution_id = 'fitted_solution_id'
        pipeline_uri = 'pipeline_uri'
        runtime = self.__get_runtime()
        runtime_picke_uri = '{}fitted_solutions/{}_runtime.pickle'.format(self.test_dir, fitted_solution_id)
        fitted_solution_uri = '{}fitted_solutions/{}.json'.format(self.test_dir, fitted_solution_id)
        self.core.manager.fitted_solutions[fitted_solution_id] = {
            'runtime': runtime,
            'pipeline_path': pipeline_uri,
        }
        # Existing
        request = core_pb2.SaveFittedSolutionRequest(fitted_solution_id=fitted_solution_id)
        response = self._SaveFittedSolution(request)
        self.assertEqual(response.fitted_solution_uri, fitted_solution_uri)
        self.assertTrue(os.path.exists(fitted_solution_uri))
        self.assertTrue(os.path.exists(runtime_picke_uri))
        loaded_fitted_solution = file_utils.load_json(fitted_solution_uri)
        self.assertEqual(loaded_fitted_solution['fitted_solution_id'], fitted_solution_id)
        self.assertEqual(loaded_fitted_solution['runtime_path'], runtime_picke_uri)
        self.assertEqual(loaded_fitted_solution['pipeline_path'], pipeline_uri)
        # # Non existing
        request = core_pb2.SaveFittedSolutionRequest(fitted_solution_id='not existing')
        response = self._SaveFittedSolution(request)
        self.assertEqual(response.fitted_solution_uri, '')

    def test_LoadFittedSolution(self):
        fitted_solution_id = 'fitted_solution_id_load'
        runtime = self.__get_runtime()
        runtime.signature = 'test'
        pipeline_uri = 'pipeline_uri'
        fitted_solution = {
            'runtime': runtime,
            'pipeline_path': pipeline_uri
        }
        fitted_solution_uri = file_utils.save_fitted_solution(fitted_solution_id, fitted_solution)

        # Existing
        request = core_pb2.LoadFittedSolutionRequest(fitted_solution_uri=fitted_solution_uri)
        response = self._LoadFittedSolution(request)
        self.assertEqual(response.fitted_solution_id, fitted_solution_id)
        self.assertEqual(self.core.manager.fitted_solutions[fitted_solution_id]['runtime'].__dict__, runtime.__dict__)
        self.assertEqual(self.core.manager.fitted_solutions[fitted_solution_id]['pipeline_path'], pipeline_uri)
        # Non existing
        request = core_pb2.LoadFittedSolutionRequest(fitted_solution_uri='not existing')
        response = self._LoadFittedSolution(request)
        self.assertEqual(response.fitted_solution_id, '')

    def test_ScorePredictions(self):
        test_resources = 'tests/server_data'
        # predictions
        predictions_path = os.path.join(test_resources, 'database4_predictions.csv')
        predictions = value_pb2.Value(csv_uri=self.__get_uri(predictions_path))
        # score_input
        score_input_path = os.path.join(test_resources, 'database4_score_input', 'datasetDoc.json')
        score_input = value_pb2.Value(dataset_uri=self.__get_uri(score_input_path))
        # problem
        problem_path = os.path.join(
            self.test_data, 'problems', self.dataset.replace('dataset', 'problem'), 'problemDoc.json')
        problem = problem_module.Problem.load(self.__get_uri(problem_path))
        problem_description = api_utils.encode_problem_description(problem)
        # metrics
        metrics = []
        task_des = schemas_utils.get_task_des(problem['problem']['task_keywords'])
        perf = problem['problem'].get('performance_metrics', [])
        performance_metrics = schemas_utils.get_metrics_from_task(task_des, perf)
        for performance_metric in performance_metrics:
            metrics.append(api_utils.encode_performance_metric(performance_metric))

        scores_path = os.path.join(test_resources, 'database4_scores.pkl')
        with open(scores_path, 'rb') as f:
            scores = pickle.load(f)
        scores = runtime_module.combine_folds([scores])
        scores = self.core.encode_scores(scores)

        request = core_pb2.ScorePredictionsRequest(predictions=predictions, score_input=score_input,
                                                   problem=problem_description, metric=metrics)
        response = self._ScorePredictions(request)
        self.assertEqual(response.scores[0].value, scores[0].value)
        # self.assertEqual(response.scores[1].value, scores[1].value)

    def test_SplitData(self):
        # input
        input_path = os.path.join(self.test_data, 'datasets', self.dataset, 'datasetDoc.json')
        input_ = value_pb2.Value(dataset_uri=self.__get_uri(input_path))
        # scoring_configuration
        scoring_configuration = core_pb2.ScoringConfiguration(
            method='K_FOLD', folds=3, train_test_ratio=0.8, shuffle=True, random_seed=42, stratified=True
        )

        problem_path = os.path.join(self.test_data, 'problems', self.dataset.replace('dataset', 'problem'), 'problemDoc.json')
        problem = problem_module.Problem.load(self.__get_uri(problem_path))
        problem_description = api_utils.encode_problem_description(problem)

        request = core_pb2.SplitDataRequest(
            input=input_, scoring_configuration=scoring_configuration,
            allowed_value_types=ALLOWED_VALUE_TYPES,
            problem=problem_description
        )
        ans = [
            self.__get_uri(os.path.join(constants.Path.TEMP_STORAGE_ROOT,  'train_output_0', 'datasetDoc.json')),
            self.__get_uri(os.path.join(constants.Path.TEMP_STORAGE_ROOT, 'test_output_0', 'datasetDoc.json')),
            self.__get_uri(os.path.join(constants.Path.TEMP_STORAGE_ROOT, 'score_output_0', 'datasetDoc.json'))
        ]
        response = self._SplitData(request)
        for i, output in enumerate((
            response.train_output,
            response.test_output,
            response.score_output)
        ):
            decoded_output = api_utils.decode_value(output)
            self.assertEqual(decoded_output['value'], ans[i])
            container_module.dataset.get_dataset(decoded_output['value'])

    def test_DataAvailableRequest(self):
        # dummy test
        pass

    def _Hello(self, request):
        return self.__unary_unary('Hello', request)

    def _SaveSolution(self, request):
        return self.__unary_unary('SaveSolution', request)

    def _LoadSolution(self, request):
        return self.__unary_unary('LoadSolution', request)

    def _SaveFittedSolution(self, request):
        return self.__unary_unary('SaveFittedSolution', request)

    def _LoadFittedSolution(self, request):
        return self.__unary_unary('LoadFittedSolution', request)

    def _ScorePredictions(self, request):
        return self.__unary_unary('ScorePredictions', request)

    def _SplitData(self, request):
        return self.__unary_stream('SplitData', request)

    def _DataAvailableRequest(self, request):
        return self.__unary_unary('DataAvailableRequest', request)

    def __unary_unary(self, method_name, request, timeout=1):
        method = self.test_server.invoke_unary_unary(
            method_descriptor=(self.service_descriptor.methods_by_name[method_name]),
            invocation_metadata={},
            request=request,
            timeout=timeout
        )
        response, metadata, code, details = method.termination()
        return response

    def __unary_stream(self, method_name, request, timeout=None):
        method = self.test_server.invoke_unary_stream(
            method_descriptor=(self.service_descriptor.methods_by_name[method_name]),
            invocation_metadata={},
            request=request,
            timeout=timeout
        )
        metadata, code, details = method.termination()
        response = method.take_response()
        return response

    def __get_runtime(self):
        mock_init = self.create_patch('d3m.runtime.Runtime.__init__')
        mock_init.return_value = None
        runtime = runtime_module.Runtime()
        return runtime

    def __get_uri(self, path):
        return pathlib.Path(os.path.abspath(path)).as_uri()


if __name__ == '__main__':
    # unittest.TextTestRunner(verbosity=2).run(unittest.TestLoader().loadTestsFromTestCase(TestServer))
    suite = unittest.TestSuite()
    for test_case in (
        'test_Hello',
        'test_SaveSolution',
        'test_LoadSolution',
        'test_SaveFittedSolution',
        'test_LoadFittedSolution',
        'test_ScorePredictions',
        'test_SplitData',
    ):
        suite.addTest(TestServer(test_case))
    unittest.TextTestRunner(verbosity=2).run(suite)