# import queue
# import queue
# import unittest

# from d3m import utils
# from d3m.metadata import problem as problem_module

# from modules.manager import workers as workers_modules
# from modules.utils.parsers import clean_directory

# SAVE_PATH = 'tests/resources/temp/'

# # to be removed
# import logging

# logger = logging.getLogger(__name__)


# class TestWorkers(unittest.TestCase):

#     def get_job(self, job_type):
#         fit_job = {
#             'job_type': 'FIT',
#             'request_id': '8b746568-713b-4bf1-acb9-e9c97385fe59',
#             'solution_id': '1e9435b9-bc0e-4b7b-aeaa-563bf2bcd7ed',
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'expose_outputs': ['steps.5.produce'],
#             # TODO add support for multiple ways to store this.
#             'expose_value_types': ['CSV_URI'],
#             'users': 'test_user, this should be a dict',
#             'save_path': SAVE_PATH,
#         }

#         produce_job = {
#             'job_type': 'PRODUCE',
#             'request_id': 'e2865ed3-1cd4-4bbf-9626-e74ccd2f6418',
#             'fitted_solution_id': None,
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'expose_outputs': ['steps.5.produce'],
#             # TODO add support for multiple ways to store this.
#             'expose_value_types': ['CSV_URI'],
#             'users': 'test_user, this should be a dict',
#             'save_path': SAVE_PATH,
#         }

#         score_job = {
#             'request_id': '35186f63-bacb-4920-afc1-b322644df54c',
#             'job_type': 'SCORE',
#             'solution_id': '1e9435b9-bc0e-4b7b-aeaa-563bf2bcd7ed',
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'performance_metrics': [
#                 {'metric': problem_module.PerformanceMetric.ACCURACY},
#                 {'metric': problem_module.PerformanceMetric.F1_MACRO}
#             ],
#             'users': 'test_user, this should be a dict',
#             'configuration': {
#                 "method": "HOLDOUT",
#                 "train_score_ratio": "0.2",
#                 "shuffle": "true",
#                 "stratified": "true",
#                 "randomSeed": "42",
#             },
#             'request_status': 'request_status',
#             'score_solutions': 'score_solutions',
#         }

#         search_job = {
#             'search_id': 'e0e34088-8da2-464b-823e-a32b45466247',
#             'job_type': 'SEARCH',
#             'pipeline_id': '6f9e6fdf-4432-412a-a823-6df9dc0d6649',
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'performance_metrics': [
#                 {'metric': problem_module.PerformanceMetric.ACCURACY},
#                 {'metric': problem_module.PerformanceMetric.F1_MACRO}
#             ],
#             'configuration': {
#                 "method": "HOLDOUT",
#                 "train_score_ratio": "0.2",
#                 "shuffle": "true",
#                 "stratified": "true",
#                 "randomSeed": "42",
#             },
#         }

#         stop_job = {'job_type': 'STOP'}

#         if job_type == 'FIT':
#             return fit_job
#         elif job_type == 'PRODUCE':
#             return produce_job
#         elif job_type == 'SCORE':
#             return score_job
#         elif job_type == 'SEARCH':
#             return search_job
#         elif job_type == 'STOP':
#             return stop_job
#         else:
#             raise AttributeError('Attribute not supported.')

#     def get_initial_setup(self):
#         init_setup = {
#             'process_name': 'test_process',
#             'jobs': queue.Queue(),
#             'solutions': {
#                 '1e9435b9-bc0e-4b7b-aeaa-563bf2bcd7ed': {
#                     'problem': 'datasets/38/38_sick_problem/38_sk_problem_TRAIN.json',
#                     'pipeline': '6f9e6fdf-4432-412a-a823-6df9dc0d6649',
#                 }
#             },
#             'pipelines': {
#                 '6f9e6fdf-4432-412a-a823-6df9dc0d6649': {
#                     'path': 'modules/utils/pipelines/random-forest-classification.yml',
#                 }
#             },
#             'score_solutions': {},
#             'fitted_solutions': {},
#             'request_status': {},
#             'request_values': {},
#             'signal_queue': queue.Queue(),
#             'searches': {'e0e34088-8da2-464b-823e-a32b45466247': list()},
#             'problems': {'e0e34088-8da2-464b-823e-a32b45466247': 'datasets/38/38_sick_problem/38_sk_problem_TRAIN.json'}
#         }
#         return init_setup

#     def test_fit_worker(self):
#         print('\n ---------- Test Fit Worker ----------')
#         fit_job = self.get_job(job_type='FIT')
#         request_id = fit_job['request_id']

#         setup = self.get_initial_setup()
#         setup['jobs'].put(fit_job)

#         # Adding a job to stop infinite loop
#         stop_job = self.get_job(job_type='STOP')
#         setup['jobs'].put(stop_job)

#         # Silence debug and warning stuff.
#         with utils.silence():
#             workers_modules.worker(**setup)

#         self.assertEqual(setup['request_status'][request_id]['status'], 'COMPLETED')

#         # Cleaning temp outputs
#         clean_directory(SAVE_PATH)

#     def test_produce_worker(self):
#         print('\n ---------- Test Produce Worker ----------')
#         # First, we fit a pipeline
#         fit_job = self.get_job(job_type='FIT')
#         request_id_fit = fit_job['request_id']

#         setup = self.get_initial_setup()
#         setup['jobs'].put(fit_job)

#         # Adding a job to stop infinite loop
#         stop_job = self.get_job(job_type='STOP')
#         setup['jobs'].put(stop_job)

#         # Silence debug and warning stuff.
#         with utils.silence():
#             workers_modules.worker(**setup)

#         # We use that pipeline for producing.
#         produce_job = self.get_job(job_type='PRODUCE')
#         request_id_produce = produce_job['request_id']
#         produce_job['fitted_solution_id'] = setup['request_status'][request_id_fit]['fitted_solution_id']
#         setup['jobs'].put(produce_job)

#         # Adding a job to stop infinite loop
#         stop_job = self.get_job(job_type='STOP')
#         setup['jobs'].put(stop_job)

#         with utils.silence():
#             workers_modules.worker(**setup)

#         # We make sure that the job finish and is completed
#         self.assertEqual(setup['request_status'][request_id_produce]['status'], 'COMPLETED')

#         # Cleaning temp outputs
#         clean_directory(SAVE_PATH)

#     def test_score_worker(self):
#         print('\n ---------- Test Score Worker ----------')
#         score_job = self.get_job(job_type='SCORE')
#         request_id = score_job['request_id']

#         setup = self.get_initial_setup()
#         setup['jobs'].put(score_job)

#         # Adding a job to stop infinite loop
#         stop_job = self.get_job(job_type='STOP')
#         setup['jobs'].put(stop_job)

#         # Silence debug and warning stuff.
#         with utils.silence():
#             workers_modules.worker(**setup)

#         self.assertEqual(setup['request_status'][request_id]['status'], 'COMPLETED')

#     def test_search_worker(self):
#         print('\n ---------- Test Search Job Worker ----------')
#         search_job = self.get_job(job_type='SEARCH')

#         setup = self.get_initial_setup()
#         setup['jobs'].put(search_job)

#         # Adding a job to stop infinite loop
#         stop_job = self.get_job(job_type='STOP')
#         setup['jobs'].put(stop_job)

#         # Silence debug and warning stuff.
#         with utils.silence():
#             workers_modules.worker(**setup)

#         # We make sure that the job finish and is completed
#         self.assertEqual(setup['pipelines'][search_job['pipeline_id']]['status'], 'COMPLETED')

#         # Cleaning temp outputs if there is some
#         clean_directory(SAVE_PATH)


# if __name__ == '__main__':
#     unittest.main()
