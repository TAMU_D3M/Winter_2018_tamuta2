# # to be removed
# import logging
# import time
# import unittest
# import warnings

# from d3m import runtime as runtime_module
# from d3m import utils
# from d3m.metadata import problem as problem_module

# from modules.manager.execution_manager import ExecutionManger
# from modules.utils.parsers import clean_directory

# logger = logging.getLogger(__name__)

# SAVE_PATH = 'tests/resources/temp/'


# class TestExecutionManager(unittest.TestCase):

#     def test_add_fit_job(self):
#         # Ignoring all warning
#         warnings.filterwarnings("ignore")
#         root_logger = logging.getLogger()
#         root_logger.setLevel(logging.INFO)
#         print('\n ---------- Test Add Fit Job ----------')

#         # Init an state that contains a valid solution.
#         manager = ExecutionManger()
#         manager.solutions['1e9435b9-bc0e-4b7b-aeaa-563bf2bcd7ed'] = {
#             'problem': 'datasets/38/38_sick_problem/problemDoc.json',
#             'pipeline': '6f9e6fdf-4432-412a-a823-6df9dc0d6649',
#         }
#         manager.pipelines['6f9e6fdf-4432-412a-a823-6df9dc0d6649'] = {
#             'path': 'modules/utils/pipelines/random_forest_pipeline.json',
#         }

#         # pprint(problem_module.parse_problem_description('datasets/38/38_sick_problem/problemDoc.json'))
#         fit_job = {
#             'solution_id': '1e9435b9-bc0e-4b7b-aeaa-563bf2bcd7ed',
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'expose_outputs': ['steps.5.produce'],
#             'expose_value_types': ['CSV_URI'],
#             'users': 'test_user, this should be a dict',
#             'save_path': SAVE_PATH,
#         }

#         # Adding a job to the queue.
#         request_id = manager.add_fit_job(**fit_job)

#         # Starting workers.
#         manager.start_workers()

#         # Wait until the job is complete, we are going to wait at most 1 min.
#         time_left = 360
#         while True:
#             if manager.request_status[request_id]['status'] == 'COMPLETED':
#                 break
#             if time_left == 0:
#                 # Stoping workers
#                 manager.stop_workers()
#                 # Raise Error
#                 raise AssertionError('Fitting is taking more than 2 min')
#             time_left -= 1
#             time.sleep(1)
#         # Stoping workers
#         manager.stop_workers()

#         # We make sure that the job finish and is completed.
#         self.assertEqual(manager.request_status[request_id]['status'], 'COMPLETED')
#         # And the job queue is empty.
#         self.assertTrue(manager.jobs.empty())

#         # Cleaning temp outputs
#         clean_directory(SAVE_PATH)

#     def test_add_produce_job(self):
#         # Ignoring all warning
#         warnings.filterwarnings("ignore")
#         root_logger = logging.getLogger()
#         root_logger.setLevel(logging.INFO)
#         print('\n ---------- Test Add Produce Job ----------')

#         # Init an state that contains a valid solution.
#         manager = ExecutionManger()
#         manager.solutions['1e9435b9-bc0e-4b7b-aeaa-563bf2bcd7ed'] = {
#             'problem': 'datasets/38/38_sick_problem/38_sk_problem_TRAIN.json',
#             'pipeline': '6f9e6fdf-4432-412a-a823-6df9dc0d6649',
#         }
#         manager.pipelines['6f9e6fdf-4432-412a-a823-6df9dc0d6649'] = {
#             'path': 'modules/utils/pipelines/random_forest_pipeline.json',
#         }

#         fit_job = {
#             'solution_id': '1e9435b9-bc0e-4b7b-aeaa-563bf2bcd7ed',
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'expose_outputs': ['steps.5.produce'],
#             'expose_value_types': ['CSV_URI'],
#             'users': 'test_user, this should be a dict',
#             'save_path': SAVE_PATH,
#         }

#         # Adding a job to the queue.
#         request_id = manager.add_fit_job(**fit_job)

#         # Starting workers.
#         manager.start_workers()

#         # Wait until the job is complete, we are going to wait at most 1 min.
#         time_left = 360
#         while True:
#             if manager.request_status[request_id]['status'] == 'COMPLETED':
#                 break
#             if time_left == 0:
#                 # Stoping workers
#                 manager.stop_workers()
#                 # Raise Error
#                 raise AssertionError('Fitting is taking more than 1 min')
#             time_left -= 1
#             time.sleep(1)

#         # We make sure that the job finish and is completed.
#         self.assertEqual(manager.request_status[request_id]['status'], 'COMPLETED')

#         produce_job = {
#             'fitted_solution_id': manager.request_status[request_id]['fitted_solution_id'],
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'expose_outputs': ['steps.5.produce'],
#             'expose_value_types': ['CSV_URI'],
#             'users': 'test_user, this should be a dict',
#             'save_path': SAVE_PATH,
#         }

#         # Adding a job to the queue.
#         request_id_produce = manager.add_produce_job(**produce_job)

#         # Wait until the job is complete, we are going to wait at most 1 min.
#         time_left = 360
#         while True:
#             if manager.request_status[request_id_produce]['status'] == 'COMPLETED':
#                 break
#             if time_left == 0:
#                 # Stoping workers
#                 manager.stop_workers()
#                 # Raise Error
#                 raise AssertionError('Fitting is taking more than 1 min (Manager Produce Test Produce)')
#             time_left -= 1
#             time.sleep(1)

#         # Stoping workers
#         manager.stop_workers()

#         # We make sure that the job finish and is completed
#         self.assertEqual(manager.request_status[request_id_produce]['status'], 'COMPLETED')
#         # And the job queue is empty.
#         self.assertTrue(manager.jobs.empty())

#         # Cleaning temp outputs
#         clean_directory(SAVE_PATH)

#     def test_add_score_job(self):
#         # Ignoring all warning
#         warnings.filterwarnings("ignore")
#         root_logger = logging.getLogger()
#         root_logger.setLevel(logging.INFO)
#         print('\n ---------- Test Add Score Job ----------')

#         # Init an state that contains a valid solution.
#         manager = ExecutionManger()
#         manager.solutions['1e9435b9-bc0e-4b7b-aeaa-563bf2bcd7ed'] = {
#             'problem': 'datasets/38/38_sick_problem/38_sk_problem_TRAIN.json',
#             'pipeline': '6f9e6fdf-4432-412a-a823-6df9dc0d6649',
#         }
#         manager.pipelines['6f9e6fdf-4432-412a-a823-6df9dc0d6649'] = {
#             'path': 'modules/utils/pipelines/random_forest_pipeline.json',
#         }

#         score_job = {
#             'solution_id': '1e9435b9-bc0e-4b7b-aeaa-563bf2bcd7ed',
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'users': 'test_user, this should be a dict',
#             'performance_metrics': [
#                 {'metric': problem_module.PerformanceMetric.ACCURACY},
#                 {'metric': problem_module.PerformanceMetric.F1_MACRO}
#             ],
#             'configuration': {
#                 "method": "HOLDOUT",
#                 "train_score_ratio": "0.2",
#                 "shuffle": "true",
#                 "stratified": "true",
#                 "randomSeed": "42",
#             },
#             'save_path': SAVE_PATH,
#         }

#         # Adding a job to the queue.
#         request_id = manager.add_score_job(**score_job)

#         # Starting workers.
#         manager.start_workers()

#         # Wait until the job is complete, we are going to wait at most 1 min.
#         time_left = 360
#         while True:
#             if manager.request_status[request_id]['status'] == 'COMPLETED':
#                 break
#             if time_left == 0:
#                 # Stoping workers
#                 manager.stop_workers()
#                 # Raise Error
#                 raise AssertionError('Fitting is taking more than 2 min')
#             time_left -= 1
#             time.sleep(1)
#         # Stoping workers
#         manager.stop_workers()

#         # We make sure that the job finish and is completed.
#         self.assertEqual(manager.request_status[request_id]['status'], 'COMPLETED')
#         # And the job queue is empty.
#         self.assertTrue(manager.jobs.empty())

#         # Cleaning temp outputs
#         clean_directory(SAVE_PATH)

#     def test_add_search_job(self):
#         # Ignoring all warning
#         warnings.filterwarnings("ignore")
#         root_logger = logging.getLogger()
#         root_logger.setLevel(logging.INFO)
#         print('\n ---------- Test Add Search Job ----------')

#         # Init an state that contains a valid solution.
#         manager = ExecutionManger()
#         manager.searches['e0e34088-8da2-464b-823e-a32b45466247'] = manager.manager.list()
#         manager.problems[
#             'e0e34088-8da2-464b-823e-a32b45466247'] = 'datasets/38/38_sick_problem/38_sk_problem_TRAIN.json'

#         manager.pipelines['6f9e6fdf-4432-412a-a823-6df9dc0d6649'] = {
#             'path': 'modules/utils/pipelines/random_forest_pipeline.json',
#         }

#         search_job = {
#             'search_id': 'e0e34088-8da2-464b-823e-a32b45466247',
#             'pipeline_id': '6f9e6fdf-4432-412a-a823-6df9dc0d6649',
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'performance_metrics': [
#                 {'metric': problem_module.PerformanceMetric.ACCURACY},
#                 {'metric': problem_module.PerformanceMetric.F1_MACRO}
#             ],
#             'configuration': {
#                 "method": "HOLDOUT",
#                 "train_score_ratio": "0.2",
#                 "shuffle": "true",
#                 "stratified": "true",
#                 "randomSeed": "42",
#             },
#         }

#         # Adding a job to the queue.
#         manager.add_search_job(**search_job)

#         # Starting workers.
#         manager.start_workers()

#         # Wait until the job is complete, we are going to wait at most 1 min.
#         time_left = 360
#         while True:
#             if manager.pipelines[search_job['pipeline_id']]['status'] == 'COMPLETED':
#                 break
#             if time_left == 0:
#                 # Stoping workers
#                 manager.stop_workers()
#                 # Raise Error
#                 raise AssertionError('search_pipeline_scoring taking more than 2 min')
#             time_left -= 1
#             time.sleep(1)

#         # Stoping workers
#         manager.stop_workers()

#         # We make sure that the job finish and is completed.
#         self.assertEqual(manager.pipelines[search_job['pipeline_id']]['status'], 'COMPLETED')
#         # And the job queue is empty.
#         self.assertTrue(manager.jobs.empty())

#         # Cleaning temp outputs
#         clean_directory(SAVE_PATH)

#     def test_add_search_no_template(self):
#         # Ignoring all warning
#         warnings.filterwarnings("ignore")
#         print('\n ---------- Test Add Search No Template ----------')
#         root_logger = logging.getLogger()
#         root_logger.setLevel(logging.INFO)
#         # Init an state that contains a valid solution.
#         manager = ExecutionManger()

#         params = {
#             'time_bound': 200,
#             'priority': 10,
#             'allowed_value_types': ['DATASET_URI', 'CSV_URI'],
#             'problem': problem_module.parse_problem_description('datasets/38/38_sick_problem/problemDoc.json'),
#             'pipeline_template': None,
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'save_path': SAVE_PATH,
#             'search_strategy': 'TEST',
#         }

#         search_id = manager.add_search(**params)

#         # Starting workers
#         manager.start_workers()

#         # Wait until the job is complete, we are going to wait at most 1 min.
#         time_left = 360
#         while True:
#             if manager.searches_status[search_id]['status'] != 'RUNNING' and manager.searches_status[search_id][
#                 'status'] != 'PENDING':
#                 break
#             if time_left == 0:
#                 # Stoping workers
#                 manager.stop_workers()
#                 # Raise Error
#                 raise AssertionError('search is taking more than 4 min')
#             time_left -= 1
#             time.sleep(1)

#         manager.stop_workers()

#         # We make sure that the job finish and is completed.
#         self.assertEqual(manager.searches_status[search_id]['status'], 'COMPLETED')
#         # And the job queue is empty.
#         self.assertTrue(manager.search_jobs.empty())

#         # Cleaning temp outputs
#         clean_directory(SAVE_PATH)

#     def test_add_search_full_template(self):
#         # Ignoring all warning
#         warnings.filterwarnings("ignore")
#         print('\n ---------- Test Add Search Full Template ----------')
#         root_logger = logging.getLogger()
#         root_logger.setLevel(logging.INFO)

#         # Init an state that contains a valid solution.
#         manager = ExecutionManger()

#         with utils.silence():
#             pipeline_template = runtime_module.get_pipeline(
#                 pipeline_path='modules/utils/pipelines/random_forest_pipeline.json')

#         params = {
#             'time_bound': 200,
#             'priority': 10,
#             'allowed_value_types': ['DATASET_URI', 'CSV_URI'],
#             'problem': problem_module.parse_problem_description('datasets/38/38_sick_problem/problemDoc.json'),
#             'pipeline_template': pipeline_template,
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'save_path': SAVE_PATH,
#             'search_strategy': 'TEST',
#         }

#         search_id = manager.add_search(**params)

#         # Starting workers
#         manager.start_workers()

#         # Wait until the job is complete, we are going to wait at most 1 min.
#         time_left = 360
#         while True:
#             if manager.searches_status[search_id]['status'] != 'RUNNING' and \
#                     manager.searches_status[search_id]['status'] != 'PENDING':
#                 break
#             if time_left == 0:
#                 # Stopping workers
#                 manager.stop_workers()
#                 # Raise Error
#                 raise AssertionError('search is taking more than 4 min')
#             time_left -= 1
#             time.sleep(1)

#         manager.stop_workers()

#         # We make sure that the job finish and is completed.
#         self.assertEqual(manager.searches_status[search_id]['status'], 'COMPLETED')
#         # And the job queue is empty.
#         self.assertTrue(manager.search_jobs.empty())

#         # Cleaning temp outputs
#         clean_directory(SAVE_PATH)

#     def test_add_search_template(self):
#         # Ignoring all warning
#         warnings.filterwarnings("ignore")
#         print('\n ---------- Test Add Search Template ----------')
#         root_logger = logging.getLogger()
#         root_logger.setLevel(logging.INFO)

#         # Init an state that contains a valid solution.
#         manager = ExecutionManger()

#         with utils.silence():
#             pipeline_template = runtime_module.get_pipeline(
#                 pipeline_path='modules/utils/pipelines/test_placeholder_pipeline.json')

#         params = {
#             'time_bound': 200,
#             'priority': 10,
#             'allowed_value_types': ['DATASET_URI', 'CSV_URI'],
#             'problem': problem_module.parse_problem_description('datasets/38/38_sick_problem/problemDoc.json'),
#             'pipeline_template': pipeline_template,
#             'inputs': ['datasets/38/38_sick_dataset/datasetDoc.json'],
#             'save_path': SAVE_PATH,
#             'search_strategy': 'TEST',
#         }

#         search_id = manager.add_search(**params)

#         # Starting workers
#         manager.start_workers()

#         # Wait until the job is complete, we are going to wait at most 1 min.
#         time_left = 360
#         while True:
#             if manager.searches_status[search_id]['status'] != 'RUNNING' and \
#                     manager.searches_status[search_id]['status'] != 'PENDING':
#                 break
#             if time_left == 0:
#                 # Stoping workers
#                 manager.stop_workers()
#                 # Raise Error
#                 raise AssertionError('search is taking more than 4 min')
#             time_left -= 1
#             time.sleep(1)

#         manager.stop_workers()

#         # We make sure that the job finish and is completed.
#         self.assertEqual(manager.searches_status[search_id]['status'], 'COMPLETED')
#         # And the job queue is empty.
#         self.assertTrue(manager.search_jobs.empty())

#         # Cleaning temp outputs
#         # clean_directory(SAVE_PATH)
