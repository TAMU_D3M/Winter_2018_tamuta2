FROM registry.gitlab.com/tamu_d3m/winter_2018_tamuta2/primitives:latest

COPY modules /user_dev/modules
COPY dev_requirements.txt /user_dev
COPY Devd3mStart.sh /user_dev

RUN chmod a+x /user_dev/Devd3mStart.sh

ENV D3MRUN ta2ta3

EXPOSE 45042

ENTRYPOINT ["/user_dev/Devd3mStart.sh"]
